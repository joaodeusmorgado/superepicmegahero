#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTranslator>
#include <QQuickView>
#include <QQmlEngine>
#include <QQmlContext>
#include <QQmlApplicationEngine>
//#include <QObject>
#include "mytime.h"
#include "mytimer.h"
#include "mytimer2.h"
#include "placement.h"
#include "qmltranslator.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setOrganizationName("Joao de Deus");
    app.setOrganizationDomain("soft-ingenium.planetaclix.pt");
    app.setApplicationName("SuperEpicMegaHero");
    //Add icon to MacOS
    //app.setWindowIcon(QIcon(":/images/unicornhorse1024Alpha.png"));
    app.setWindowIcon(QIcon(":/images/1024-mac.png"));

    QmlTranslator translator;
    MyTime time;
    qmlRegisterType<MyTime>("com.Time", 1, 0, "Time");
    qmlRegisterType<MyTimer>("com.Timer", 1, 0, "MyTimer");
    qmlRegisterType<MyTimer>("com.Timer", 1, 0, "MyTimer2");
    qmlRegisterType<QmlTranslator>("com.QMLTranslator", 1, 0, "QmlTranslator");
    qmlRegisterType<Placement>("Placement", 1, 0, "Placement");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("_Time", &time);
    engine.rootContext()->setContextProperty("qmlTranslator", &translator);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    qDebug()<<"connection: translation"<<QObject::connect(&translator, SIGNAL(languageChanged()),
                       &engine,  SLOT(retranslate()));

    translator.setLanguage(QLocale::system().name());


    return app.exec();
}
