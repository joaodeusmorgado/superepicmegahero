#include "mytimer.h"
#include <QDebug>

MyTimer::MyTimer(QObject *parent) : QObject(parent),
    m_running(false),
    m_repeat(false)
{
    m_timer.setTimerType(Qt::PreciseTimer);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(timerUpdate())); //whien QTimer timeout, fires MyTimer triggered() signal
    m_timer.setSingleShot(true);
}


void MyTimer::timerUpdate()
{
    emit triggered();
}


void MyTimer::setInterval(const int& interval_)
{
    if (m_interval == interval_)
        return;
    m_interval = interval_;
    m_timer.setInterval(m_interval);
    emit intervalChanged(interval_);
}

void MyTimer::setRunning(const bool& running_)
{
    if (m_running == running_)
        return;
    m_running = running_;
    if (running_)
        m_timer.start();
    else
        m_timer.stop();
    emit runningChanged(running_);
}

void MyTimer::setRepeat(const bool &repeat_)
{
    if (m_repeat == repeat_)
        return;
    m_repeat = repeat_;
    m_timer.setSingleShot(!m_repeat);
    emit repeatChanged(repeat_);
}

void MyTimer::start()
{
    m_timer.start();
}

void MyTimer::stop()
{
    m_timer.stop();
}
