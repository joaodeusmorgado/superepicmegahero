<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_PT">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="48"/>
        <source>Super Epic Mega Hero - a hero in search of adventures.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="60"/>
        <source>If you like this game please buy me a coffe 😊</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="71"/>
        <source>Developer: João Morgado
Graphics: André Morgado &amp; João Morgado
Version 0.1
Email: joaodeusmorgado@yahoo.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="85"/>
        <source>This program was developed using Qt5 opensource version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="97"/>
        <source>Source code: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="114"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="125"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArrowSign</name>
    <message>
        <location filename="../qml/pages/components/ArrowSign.qml" line="54"/>
        <source>Treasure Mountain</source>
        <translation>Montanha do Tesouro</translation>
    </message>
</context>
<context>
    <name>GameMenu</name>
    <message>
        <location filename="../qml/pages/GameMenu.qml" line="166"/>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../qml/pages/GameMenu.qml" line="173"/>
        <source>Settings</source>
        <translation>Definições</translation>
    </message>
    <message>
        <location filename="../qml/pages/GameMenu.qml" line="181"/>
        <source>About</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <location filename="../qml/pages/GameMenu.qml" line="216"/>
        <source>Exit game ?</source>
        <translation>Sair do jogo?</translation>
    </message>
    <message>
        <location filename="../qml/pages/GameMenu.qml" line="218"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../qml/pages/GameMenu.qml" line="219"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
</context>
<context>
    <name>InitialPage</name>
    <message>
        <location filename="../qml/pages/InitialPage.qml" line="48"/>
        <source>Super Epic Mega Hero</source>
        <translation>Super Epico Mega Herói</translation>
    </message>
    <message>
        <location filename="../qml/pages/InitialPage.qml" line="79"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
</context>
<context>
    <name>Level01</name>
    <message>
        <location filename="../qml/pages/level01/Level01.qml" line="24"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level01/Level01.qml" line="32"/>
        <source>You lost.</source>
        <translation>Perdeste.</translation>
    </message>
</context>
<context>
    <name>Level01Content</name>
    <message>
        <location filename="../qml/pages/level01/Level01Content.qml" line="204"/>
        <source>Energy shield.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level01/Level01Content.qml" line="223"/>
        <source>Double shots!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level01/Level01Content.qml" line="247"/>
        <source>You win</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level01/Level01Content.qml" line="247"/>
        <source>coins.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level01/Level01Content.qml" line="260"/>
        <source>Delicious milky way cow milk:)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level01Intro</name>
    <message>
        <location filename="../qml/pages/level01/Level01Intro.qml" line="39"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../qml/pages/level01/Level01Intro.qml" line="52"/>
        <source>Our hero, captain Andrew, travels through space, in his spaceship, looking for adventures.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level01/Level01Intro.qml" line="56"/>
        <source>Control the spaceship by rotating your mobile to up, down, left or right. Click anywhere on the screen fire.
Your phone will be calibrated, put it in a confortable position and click &gt;&gt; to start.
If you have a keyboard, use the arrows key to move, and space key to fire.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level01/Level01Intro.qml" line="54"/>
        <source>There are enemies everywhere, they can attack at any time...</source>
        <translation>Há inimigos à espreita, podem atacar a qualquer altura...</translation>
    </message>
</context>
<context>
    <name>Level02</name>
    <message>
        <location filename="../qml/pages/level02/Level02.qml" line="41"/>
        <source>Victory</source>
        <translation>Vitória</translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02.qml" line="210"/>
        <source>Dig</source>
        <translation>Fura</translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02.qml" line="283"/>
        <source>You found the prisioner!!!</source>
        <translation>Encontraste o prisioneiro!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02.qml" line="340"/>
        <source>Hi, thanks for saving me.</source>
        <translation>Olá, obrigado por me salvares.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02.qml" line="344"/>
        <source>As a reward I offer you a fuel can, with 50 liters.</source>
        <translation type="unfinished">Como recompensa ofereço-te uma lata de gasolina com 50 litros.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02.qml" line="356"/>
        <source>That will open a book with all the object you find in you journey. Some objects will be automatically used when needed, others you need to click them. Have a nice journey!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02.qml" line="348"/>
        <source>Thanks for the fuel. How do I use it?</source>
        <translation>Obrigado pela gasolina. Como é que a uso ?</translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02.qml" line="352"/>
        <source>You can check your objects clicking on the Book button at top left.</source>
        <translation>Podes ver os objectos clickando no botão Livro em cima à esquerda.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02.qml" line="360"/>
        <source>Thank you and goodbye.</source>
        <translation>Obrigado e adeus.</translation>
    </message>
</context>
<context>
    <name>Level02Intro</name>
    <message>
        <location filename="../qml/pages/level02/Level02Intro.qml" line="45"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02Intro.qml" line="59"/>
        <source>Congratulations on finishing the previous level. You&apos;re a real hero 😊
Second level is really easy and you will learn how to use your map.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02Intro.qml" line="62"/>
        <source>Your mission, Captain Andrew, is to find a prisoner in the tunnels of an asteroid.
You will need to drill the asteriod and release the prisoner.
Click anywhere on the left or right of the screen to move and on the button to drill.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level02/Level02Intro.qml" line="66"/>
        <source>Relax, enjoy and let&apos;s do it!!!</source>
        <translation>Relaxa, diverte-te e vamos a isto!!!</translation>
    </message>
</context>
<context>
    <name>Level03</name>
    <message>
        <location filename="../qml/pages/level03/Level03.qml" line="19"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
</context>
<context>
    <name>Level03Content</name>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="80"/>
        <source>You lost.</source>
        <translation>Perdeste.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="126"/>
        <source>You found a delicious fruit!!!</source>
        <translation>Encontraste um fruto delicioso!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="154"/>
        <location filename="../qml/pages/level03/Level03Content.qml" line="216"/>
        <source>Level failled!!!</source>
        <translation>Nível falhou!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="395"/>
        <source>I will increase your life power to 5!!!</source>
        <translation>Vou aumentar a tua vida para 5 unidades!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="399"/>
        <source>I like to drink a lot of water. Delicious!!!</source>
        <translation>Eu gosto de beber muita água. É deliciosa.!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="422"/>
        <source>I will increase your life power to 10!!!</source>
        <translation>Vou aumentar a tua vida para 10 unidades!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="426"/>
        <source>I really like sunny days, they&apos;re so good to my petals!</source>
        <translation>Gosto de dias soalheiros, são bons para as minhas pétalas!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="953"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="954"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="955"/>
        <source>A - No, let me keep fighting.
B - Yes, please help me fight.</source>
        <translation>A - Não eu vou continuar a luta.
B - Sim, por favor, ajuda-me.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="994"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="995"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1137"/>
        <source>Hello. I&apos;m really thirsty. Do you have anything to drink ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1153"/>
        <source>Too bad, I&apos;m really thirsty. Oh well, bye!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1194"/>
        <source>You found a sword!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1235"/>
        <source>We&apos;re supposed to have a sword fight now, but lucky for you the the great coder still hasn&apos;t finished the sword fight algorithm for this game.
Go in peace now, we&apos;ll meet in future updates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1240"/>
        <source>Great coder ? Game ? I don&apos;t understand what you say.
What a crazy robot, I&apos;m out of here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1262"/>
        <source>Quick, we don&apos;t have much time!Double click on my head and I will take charge of the fight. I will fight without the bow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1281"/>
        <source>Oh my God, you fought like a mighty warrior!!!
Who are you ???</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1284"/>
        <source>From now on, I will take the lead if my head is double clicked.
You, Captain Andrew, will take the lead if your head is double clicked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1141"/>
        <source>Hi. I have milk from the milky way cow. Please have a drink.</source>
        <translation>Olá. Tenho leite da vaquinha da via láctea. Podes beber.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1145"/>
        <source>Hum, delicious. Thank you, please let me offer you this bow and arrows.
Look for it in your book, then to shoot an arrow tap anywhere in the screen.</source>
        <translation>Hum, delicioso. Obrigado, como recompensa, ofereço.te este arco e flechas.
Procura o arco no teu livro, para disparar clcka em qualquer sítio no ecrã.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1149"/>
        <source>Sorry, I have nothing to give you.</source>
        <translation>Desculpa, não tenho nada para te oferecer.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1179"/>
        <source>Hi, have a safe journey and be happy!!!</source>
        <translation>Olá, desejo-te uma boa jornada e sê feliz!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1187"/>
        <source>You found the starKey!!!</source>
        <translation>Encontraste a chave estrela!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1205"/>
        <source>Hi, I&apos;m Louise Pandora. I was kidnapped, can you help me?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1208"/>
        <source>Hi, I&apos;m captain Andrew! I don&apos;t know how to open the cage.</source>
        <translation>Olá, sou o capitão André! Não sei como abrir a jaula.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1211"/>
        <source>This cage only opens with a magic star key.</source>
        <comment>girlHead.png</comment>
        <translation>Esta jaula só abre com uma chave mágica de estrela.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1215"/>
        <source>I have a star key, I won it fighting a dragon. Let me try.</source>
        <translation>Tenho uma chave de estrela que ganhei numa luta contra um dragão. Posso tentar usá-la.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1219"/>
        <source>I don&apos;t have a star key.</source>
        <translation>Não tenho nenhuma chave de estrela.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1222"/>
        <source>Please find one and set me free.</source>
        <translation>Por favor encontra uma chave e liberta-me.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1225"/>
        <source>Ok, I will look for it.</source>
        <translation>Muito bem, vou à procura da chave.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1249"/>
        <source>The arrows don&apos;t do nothing to him, and you&apos;re about to die.
Let me help you fight!!!</source>
        <translation>As flechas não estão a derrotá-lo, e tu estás quase morrer.
Deixa-me ajudar-te na luta!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Content.qml" line="1283"/>
        <source>I am Louise Pandora ... and I have unleashed my power.</source>
        <translation>Eu sou Luísa Pandora .. . e libertei o meu poder.</translation>
    </message>
</context>
<context>
    <name>Level03Intro</name>
    <message>
        <location filename="../qml/pages/level03/Level03Intro.qml" line="41"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Intro.qml" line="53"/>
        <source>Captain Andrew arrives to a new planet, it&apos;s time to explore it. Poke around, there&apos;s lots of hidden stuff to be found that can help you when you&apos;re stuck.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level03/Level03Intro.qml" line="56"/>
        <source>Press anywhere, a move command will appear.
Slide left or right to move, slide up &amp; left or right to jump.
If you&apos;re using a keyboard, you can use the arrows keys to move left, right and jump.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level04</name>
    <message>
        <location filename="../qml/pages/level04/Level04.qml" line="115"/>
        <source>Hits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level04/Level04.qml" line="126"/>
        <source>Fails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level04/Level04.qml" line="137"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level04/Level04.qml" line="213"/>
        <source>Victory!</source>
        <translation type="unfinished">Vitória!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level04/Level04.qml" line="219"/>
        <source>Too many failled hits.
You lost.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level04Intro</name>
    <message>
        <location filename="../qml/pages/level04/Level04Intro.qml" line="39"/>
        <source>Skip</source>
        <translation type="unfinished">Saltar</translation>
    </message>
    <message>
        <location filename="../qml/pages/level04/Level04Intro.qml" line="51"/>
        <source>First part of the planet was explored sucessfully. Now there&apos;s strange creatures popping out of the ground, like gophers. Your mission is to smack those creatures. Go!!!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level05Content</name>
    <message>
        <location filename="../qml/pages/level05/Level05Content.qml" line="184"/>
        <source>Hi, I&apos;m hungry, do you have any fruit for me?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Content.qml" line="187"/>
        <source>I have a fruit I caught early, it fall from a tree! Take it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Content.qml" line="190"/>
        <source>Delicious, thank you.
Did you know people live sad because they can&apos;t comunicate properly?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Content.qml" line="193"/>
        <source>We had great phones called Meeg, made in our Konia factory in the midle of the ocean. Untill a mad man set a factory on fire, like a burning plataform and now phones are falling into the ocean because of the explosions and fire.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Content.qml" line="198"/>
        <source>Take my boat and please help us. Try to save the Meeg phones moving the boat to catch them.  Hurry and sail away!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Content.qml" line="202"/>
        <source>Ok, I will sail on!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Content.qml" line="205"/>
        <source>Sorry I have nothing!
Can I sail on your boat?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Content.qml" line="206"/>
        <source>No you can not.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level05Konia</name>
    <message>
        <location filename="../qml/pages/level05/Level05Konia.qml" line="232"/>
        <source>Catched</source>
        <translation>Apanhados</translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Konia.qml" line="241"/>
        <source>Missed</source>
        <translation>Falhados</translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Konia.qml" line="252"/>
        <source>Time left</source>
        <translation>Tempo restante</translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Konia.qml" line="348"/>
        <source>Vitory!!!
You caught more than 70% of the Meeg phones!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Level05Konia.qml" line="354"/>
        <source>You loose!
You need to catch more than 70% of phones!</source>
        <translation>Perdeste!
Precisas de apanhar mais de 70%dos telemóveis!</translation>
    </message>
</context>
<context>
    <name>Level05KoniaIntro</name>
    <message>
        <location filename="../qml/pages/level05/Level05KoniaIntro.qml" line="62"/>
        <source>Our hero sails throught the ocean.
Is mission: arrive at the burning Konia factory. Move the boat swiping your finger, try to catch the falling Meeg phones.
Get ready, go!!!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level06Content</name>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="24"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="34"/>
        <source>Level failed!</source>
        <translation>Nível falhado!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="472"/>
        <location filename="../qml/pages/level06/Level06Content.qml" line="480"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="473"/>
        <source>You&apos;ll need a fuel can to use the truck.
You need to find one!!!</source>
        <translation>Precisas de uma lata de gasolina para usar o veículo.
Tens de encontrar uma!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="481"/>
        <location filename="../qml/pages/level06/Level06Content.qml" line="491"/>
        <source>You need</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="481"/>
        <location filename="../qml/pages/level06/Level06Content.qml" line="491"/>
        <source>gallons to travel, and you have</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="482"/>
        <source>Try to earn some fuel before travelling.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="489"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="490"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../qml/pages/level06/Level06Content.qml" line="492"/>
        <source>Continue ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level07</name>
    <message>
        <location filename="../qml/pages/level07/Level07.qml" line="27"/>
        <source>You lost.</source>
        <translation type="unfinished">Perdeste.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level07/Level07.qml" line="34"/>
        <source>Victory!</source>
        <translation type="unfinished">Vitória!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level07/Level07.qml" line="179"/>
        <source>Time left</source>
        <translation type="unfinished">Tempo restante</translation>
    </message>
</context>
<context>
    <name>Level07Intro</name>
    <message>
        <location filename="../qml/pages/level07/Level07Intro.qml" line="38"/>
        <source>Skip</source>
        <translation type="unfinished">Saltar</translation>
    </message>
    <message>
        <location filename="../qml/pages/level07/Level07Intro.qml" line="50"/>
        <source>Captain and Pandora successfully arrived at the end of the desert. They rest for a while before continuing and captain tells Pandora a story when he was travelling in his spaceship and had to cross a tremendous asteroid field.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level08</name>
    <message>
        <location filename="../qml/pages/level08/Level08.qml" line="31"/>
        <source>Total balloons:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level08/Level08.qml" line="32"/>
        <source>Balloons shot:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level08/Level08.qml" line="33"/>
        <source>Shot percentagem:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level08/Level08.qml" line="61"/>
        <source>You shot more than 80%
Victory!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level08/Level08.qml" line="74"/>
        <source>You shot less than 80%
Try again 😟</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level08/Level08.qml" line="220"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level08Intro</name>
    <message>
        <location filename="../qml/pages/level08/Level08Intro.qml" line="35"/>
        <source>Skip</source>
        <translation type="unfinished">Saltar</translation>
    </message>
    <message>
        <location filename="../qml/pages/level08/Level08Intro.qml" line="47"/>
        <source>Way back when our hero was in the military academy, he had to practice ballon shooting with arrows. He had to shoot at least 80% of the ballons to pass the training. Let&apos;s do it.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level09</name>
    <message>
        <location filename="../qml/pages/level09/Level09.qml" line="20"/>
        <source>Victory!</source>
        <translation type="unfinished">Vitória!</translation>
    </message>
</context>
<context>
    <name>Level09Content</name>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="88"/>
        <source>You drowned.</source>
        <translation type="unfinished">Afogaste-te.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="270"/>
        <source>Unicorn
Home</source>
        <translation type="unfinished">Lar do Unicórnio</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="313"/>
        <source>Unicorn
forest</source>
        <translation type="unfinished">Floresta do Unicórnio</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="445"/>
        <source>Treasure
Mountain</source>
        <translation type="unfinished">Montanha do Tesouro</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="827"/>
        <source>You lost!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1418"/>
        <source>brrrh..., brrrh..., clip clop, clip clop, brrrh...</source>
        <translation type="unfinished">brrrh..., brrrh..., clip clop, clip clop, brrrh...</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1419"/>
        <source>Hi horsy. I don&apos;t understand what you say!</source>
        <translation type="unfinished">Olá cavalinho. Não percebo o que estás a dizer!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1420"/>
        <source>What a cute horse. I think he&apos;s hungry!!!</source>
        <translation type="unfinished">Que cavalhinho fofo. Acho que ele tem fome!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1432"/>
        <source>How!!! I can talk! Thoose carrots must be magic! Thank you.</source>
        <translation type="unfinished">Uau!!! Eu consigo falar!Essas cenouras devem ser mágicas! Obrigado.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1433"/>
        <source>Can you help us? We&apos;re going to explore the florest.</source>
        <translation type="unfinished">Podes ajudar-nos? Nós vamos explorar a floresta.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1434"/>
        <source>Sure, I will give you a ride.</source>
        <translation type="unfinished">Claro, eu dou-vos boleia.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1435"/>
        <source>Just double click my head to jump in, and double click again to jump off.
Also did you know I can jump very high?</source>
        <translation type="unfinished">Faz um duplo cliack na minha cabeça para montares, faz outro duplo click para apeares.
Sabias que eu consigo saltar muito alto?</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1444"/>
        <source>I can&apos;t go further, there&apos;s a river ahead. My wife will take you.</source>
        <translation type="unfinished">Não posso ir mais longe, há um rio à frente. A minha companheira leva-vos.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1446"/>
        <source>She can swim ?</source>
        <translation type="unfinished">Ela sabe nadar?</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1447"/>
        <source>No, she can fly. Double click my head to jump off and go talk to her.
Have a safe journey.</source>
        <translation type="unfinished">Não ela sabe voar. Faz um dupl o click na minha cabeça para apeares e vai falar com ela.
Desejo-te uma boa jornada.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1461"/>
        <source>Hi there, I see you meet my husband unicorn Einstein. I&apos;m Mary Mare.</source>
        <translation type="unfinished">Olá, vejo que já conheceram o meu marido unicórnio Einstein. Eu sao a égua Maria.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1462"/>
        <source>Hello I&apos;m captain Andrew.</source>
        <translation type="unfinished">Olá, sou o capitão André.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1463"/>
        <source>Hi, I&apos;m Louise...</source>
        <translation type="unfinished">Olá, sou a Luísa...</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1464"/>
        <source>I know who you are princess Pandora. Welcome!</source>
        <translation type="unfinished">Eu sei quem tu és, princesa Pandora. Bem-vinda!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1465"/>
        <source>Princess Pandora ???
You didn&apos;t tell me you are a princess!</source>
        <translation type="unfinished">Princesa Pandora ???
Não me tinhas dito que eras uma princesa!</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1466"/>
        <source>You didn&apos;t ask 😊</source>
        <translation type="unfinished">Tu não perguntaste 😊</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1468"/>
        <source>Double click my head to jump aboard, I will take to see the forest. Press and slide anywhere on the screen to walk, then while walking, use another finger to press anywhere to fly.</source>
        <translation type="unfinished">Faz um duplo click na minha cebeça para montares, eu levo-vos a ver a floresta. Pressiona e desliza o dedo no ecrã para eu andar, depois usa outro dedo para clickar em qualquer sítio no ecrã para eu voar.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1474"/>
        <source>If you have a keyboard, press the f key to fly and arrows keys to move.</source>
        <translation type="unfinished">Se tiveres um teclado, usa a tecla F para voar e as teclas das setas da esquerda ou direita para andar.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1487"/>
        <source>You found the blue diamonds treasure.</source>
        <translation type="unfinished">Encontraste o tesouro dos diamantes azuis.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1501"/>
        <source>Hi there, I&apos;m a scientist.
I have invented a future photo machine.</source>
        <translation type="unfinished">Olá, eu sou um cientista.
Inventei uma máquina de mostar fotos do futuro.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1504"/>
        <source>You can peak in the len and see a random photo of you in the future. I saw myself winning the nobel prize 😊
Go ahead and have a look.</source>
        <translation type="unfinished">Podes espreitar na lente e verás uma foto aleatória do teu futuro. Eu vi-me a ganhar o prémio Nobel 😊
Experimenta ver a tua foto.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1509"/>
        <source>Let me try.</source>
        <translation type="unfinished">Vou experimentar.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1514"/>
        <source>Ohhh. I saw some weird picture of a very strange place 😯!!!
A thought came to my mind: &quot;3D world is coming!&quot;
Pandora, now you take a look.</source>
        <translation type="unfinished">Uau. Eu vi uma foto de um mundo muito estranho 😯 !!!
Um pensamento veio-me à mente: &quot;O mundo 3D está a chegar!&quot;
Pandora, experimenta ver a tua foto.</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1520"/>
        <source>What 😠 ??? I&apos;m seeing a picture of... never mind, it&apos;s all blurry.
How does this machine work, is this real ?</source>
        <translation type="unfinished">O quê??? Estou a ver uma foto dos... Ah deixa lá, está tudo desfocado.
Como é que esta máquina funciona, estas fotos são reais?</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1524"/>
        <source>Well, I discovered a new type of atomic particles called TimeQuarks wich represent time particles and can be codified with complex math equations based on four dimensional matrices and quaternions. As you know quaternions are represent like a+b*i+c*j+d*k where a, b, c, and d are real numbers, and i, j, and k are the fundamental quaternion units. They are very usefull to represent the 3-sphere or glomes. What my photo time machine really does is to use a sensor that captures photons to compose a normal regular photo, and use another special type of sensor that can capture TimeQuarks that coexist all around us along with the photons. The machine then uses is very powerfull GPUs to process the image using vertex and fragment shaders, while the compute shaders process the TimeQuarks particles using complex algorithms based on the quaternions and matrices, wich are then applyied on the original photo, given us the photo of what will happen in the future. Is not quite accurate because algorithms use numeric variables that can accumulate numeric errors, but it works pretty good. I took a picture of a banana and got a picture of a banana pie, and my mother really used that banana to bake a pie, and it turned out to be a very delicious pie!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1547"/>
        <source>Opps, I just farted 😯</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1548"/>
        <source>What 😊 ??? Ahahahah!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1564"/>
        <source>Hi there. I see that you have the book of hapyness. You have finish all your tasks and you can read in the next level. Congratulations 😊!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Content.qml" line="1569"/>
        <source>Hi there. You haven&apos;t finish all your tasks and can not pass the level yet. Keep exploring, brave hero 😟!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level09Intro</name>
    <message>
        <location filename="../qml/pages/level09/Level09Intro.qml" line="44"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../qml/pages/level09/Level09Intro.qml" line="56"/>
        <source>There are forests with strange magic creatures in this planet.
Keep exploring, brave hero.</source>
        <translation type="unfinished">Há florestas com criaturas estranhas mágicas neste planeta.
Continua a explorar, herói corajoso.</translation>
    </message>
</context>
<context>
    <name>Level0fake</name>
    <message>
        <location filename="../qml/pages/Level0fake.qml" line="17"/>
        <source>Este nivel não existe :)

Jogar muitas horas é cansativo, é melhor fazer uma pausa.

Sugestão: podes ir brincar ao ar livre com os amigos, andar de bicicleta,

ou brincar com papagaio, comer um gelado, ou ler um bom livro.

Diverte-te e sê feliz!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level10</name>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="62"/>
        <source>Happy readings!
Now back to exploring 😊</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="79"/>
        <source>The Book of Happiness!
Hints to be happy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="84"/>
        <source>Don&apos;t waste to much time playing in your pc, mobile, game console. Make regular breaks, go out, play with your friends, fly a kite in the park.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="89"/>
        <source>Watch the nature and enjoy 😊</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="93"/>
        <source>Try a new flavour of ice cream, that you never tasted before.
If you don&apos;t like it, just smile and be happy, there&apos;s more flavours to try 😊</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="96"/>
        <source>If you&apos;re stressed go for a swim in the ocean, if you have the chance. Carefull not to drown 😊</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="98"/>
        <source>There&apos;s hidden secrets in nature. Be curious!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="100"/>
        <source>If you&apos;re a student, be a warrior to fight the laziness and do your work!
No one can fight your battles for you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="102"/>
        <source>Eat healthy food... at least once in a while 😊. Yummy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="104"/>
        <source>You can choose: be grumpy or smile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="106"/>
        <source>You can learn a lot by reading paper books.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level10/Level10.qml" line="108"/>
        <source>Enought advice, now go live your life 😊.
The end.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level11</name>
    <message>
        <location filename="../qml/pages/level11/Level11.qml" line="19"/>
        <source>Victory!</source>
        <translation type="unfinished">Vitória!</translation>
    </message>
</context>
<context>
    <name>Level11Content</name>
    <message>
        <location filename="../qml/pages/level11/Level11Content.qml" line="154"/>
        <source>Pandora! Please don&apos;t die!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level11/Level11Content.qml" line="155"/>
        <source>To be continued...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level11/Level11Content.qml" line="156"/>
        <source>Pandora is badly wonded 😭
Will she die...?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level11/Level11Content.qml" line="157"/>
        <source>Or will the hero find a forest doctor who will help her healing and then she will continue the adventures with captain Andrew and eventually find a misterious 3D world with more adventures ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level11/Level11Content.qml" line="161"/>
        <source>Stay tunned for the following game updates were all will be revealed.
Meanwhile, be happy 😊</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/level11/Level11Content.qml" line="163"/>
        <source>If you have enjoyed this game, please consider donating, buy the author a coffee. It will be greatly appreciated and will keep us motivated to continue developing more Super Epic Mega adventures.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LevelContentTemplate</name>
    <message>
        <location filename="../qml/pages/components/LevelContentTemplate.qml" line="96"/>
        <source>Level failled!!!</source>
        <translation type="unfinished">Nível falhou!!!</translation>
    </message>
</context>
<context>
    <name>LevelSettings</name>
    <message>
        <location filename="../qml/pages/levelSettings/LevelSettings.qml" line="42"/>
        <location filename="../qml/pages/levelSettings/LevelSettings.qml" line="48"/>
        <source>Reset Game</source>
        <translation>Reset ao jogo</translation>
    </message>
    <message>
        <location filename="../qml/pages/levelSettings/LevelSettings.qml" line="107"/>
        <source>Reset game?
All progress will be lost and the game will exit!!!
Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/levelSettings/LevelSettings.qml" line="108"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../qml/pages/levelSettings/LevelSettings.qml" line="109"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
</context>
<context>
    <name>LicenseFile</name>
    <message>
        <location filename="../qml/pages/LicenseFile.qml" line="43"/>
        <source>GNU LESSER GENERAL PUBLIC LICENSE
Version 3, 29 June 2007

Copyright © 2007 Free Software Foundation, Inc. &lt;https://fsf.org/&gt;

Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.

This version of the GNU Lesser General Public License incorporates the terms and conditions of version 3 of the GNU General Public License, supplemented by the additional permissions listed below.

0. Additional Definitions.
As used herein, “this License” refers to version 3 of the GNU Lesser General Public License, and the “GNU GPL” refers to version 3 of the GNU General Public License.

“The Library” refers to a covered work governed by this License, other than an Application or a Combined Work as defined below.

An “Application” is any work that makes use of an interface provided by the Library, but which is not otherwise based on the Library. Defining a subclass of a class defined by the Library is deemed a mode of using an interface provided by the Library.

A “Combined Work” is a work produced by combining or linking an Application with the Library. The particular version of the Library with which the Combined Work was made is also called the “Linked Version”.

The “Minimal Corresponding Source” for a Combined Work means the Corresponding Source for the Combined Work, excluding any source code for portions of the Combined Work that, considered in isolation, are based on the Application, and not on the Linked Version.

The “Corresponding Application Code” for a Combined Work means the object code and/or source code for the Application, including any data and utility programs needed for reproducing the Combined Work from the Application, but excluding the System Libraries of the Combined Work.

1. Exception to Section 3 of the GNU GPL.
You may convey a covered work under sections 3 and 4 of this License without being bound by section 3 of the GNU GPL.

2. Conveying Modified Versions.
If you modify a copy of the Library, and, in your modifications, a facility refers to a function or data to be supplied by an Application that uses the facility (other than as an argument passed when the facility is invoked), then you may convey a copy of the modified version:

a) under this License, provided that you make a good faith effort to ensure that, in the event an Application does not supply the function or data, the facility still operates, and performs whatever part of its purpose remains meaningful, or
b) under the GNU GPL, with none of the additional permissions of this License applicable to that copy.
3. Object Code Incorporating Material from Library Header Files.
The object code form of an Application may incorporate material from a header file that is part of the Library. You may convey such object code under terms of your choice, provided that, if the incorporated material is not limited to numerical parameters, data structure layouts and accessors, or small macros, inline functions and templates (ten or fewer lines in length), you do both of the following:

a) Give prominent notice with each copy of the object code that the Library is used in it and that the Library and its use are covered by this License.
b) Accompany the object code with a copy of the GNU GPL and this license document.
4. Combined Works.
You may convey a Combined Work under terms of your choice that, taken together, effectively do not restrict modification of the portions of the Library contained in the Combined Work and reverse engineering for debugging such modifications, if you also do each of the following:

a) Give prominent notice with each copy of the Combined Work that the Library is used in it and that the Library and its use are covered by this License.
b) Accompany the Combined Work with a copy of the GNU GPL and this license document.
c) For a Combined Work that displays copyright notices during execution, include the copyright notice for the Library among these notices, as well as a reference directing the user to the copies of the GNU GPL and this license document.
d) Do one of the following:
0) Convey the Minimal Corresponding Source under the terms of this License, and the Corresponding Application Code in a form suitable for, and under terms that permit, the user to recombine or relink the Application with a modified version of the Linked Version to produce a modified Combined Work, in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.
1) Use a suitable shared library mechanism for linking with the Library. A suitable mechanism is one that (a) uses at run time a copy of the Library already present on the user&apos;s computer system, and (b) will operate properly with a modified version of the Library that is interface-compatible with the Linked Version.
e) Provide Installation Information, but only if you would otherwise be required to provide such information under section 6 of the GNU GPL, and only to the extent that such information is necessary to install and execute a modified version of the Combined Work produced by recombining or relinking the Application with a modified version of the Linked Version. (If you use option 4d0, the Installation Information must accompany the Minimal Corresponding Source and Corresponding Application Code. If you use option 4d1, you must provide the Installation Information in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.)
5. Combined Libraries.
You may place library facilities that are a work based on the Library side by side in a single library together with other library facilities that are not Applications and are not covered by this License, and convey such a combined library under terms of your choice, if you do both of the following:

a) Accompany the combined library with a copy of the same work based on the Library, uncombined with any other library facilities, conveyed under the terms of this License.
b) Give prominent notice with the combined library that part of it is a work based on the Library, and explaining where to find the accompanying uncombined form of the same work.
6. Revised Versions of the GNU Lesser General Public License.
The Free Software Foundation may publish revised and/or new versions of the GNU Lesser General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.

Each version is given a distinguishing version number. If the Library as you received it specifies that a certain numbered version of the GNU Lesser General Public License “or any later version” applies to it, you have the option of following the terms and conditions either of that published version or of any later version published by the Free Software Foundation. If the Library as you received it does not specify a version number of the GNU Lesser General Public License, you may choose any version of the GNU Lesser General Public License ever published by the Free Software Foundation.

If the Library as you received it specifies that a proxy can decide whether future versions of the GNU Lesser General Public License shall apply, that proxy&apos;s public statement of acceptance of any version is permanent authorization for you to choose that version for the Library.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LicenseFile.qml" line="116"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>MessageBoard</name>
    <message>
        <location filename="../qml/pages/MessageBoard.qml" line="59"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/pages/MessageBoard.qml" line="71"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>MessageBoard2</name>
    <message>
        <location filename="../qml/pages/MessageBoard2.qml" line="103"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/pages/MessageBoard2.qml" line="115"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>PanelEndOfLevel</name>
    <message>
        <location filename="../qml/pages/PanelEndOfLevel.qml" line="29"/>
        <source>Victory</source>
        <translation>Vitória</translation>
    </message>
</context>
<context>
    <name>PanelEndOfLevel2</name>
    <message>
        <location filename="../qml/pages/PanelEndOfLevel2.qml" line="55"/>
        <source>Victory</source>
        <translation>Vitória</translation>
    </message>
</context>
<context>
    <name>Phone</name>
    <message>
        <location filename="../qml/pages/level05/Phone.qml" line="38"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../qml/pages/level05/Phone.qml" line="38"/>
        <source>W</source>
        <translation>W</translation>
    </message>
</context>
<context>
    <name>ScoreBoard</name>
    <message>
        <location filename="../qml/pages/ScoreBoard.qml" line="10"/>
        <source>Score</source>
        <translation>Pontuação</translation>
    </message>
</context>
<context>
    <name>Shop</name>
    <message>
        <location filename="../qml/pages/Shop.qml" line="131"/>
        <source>I see that you have a blue diamond and now I can sell you the Screet book of hapiness. Be warn that you can not read it yet.
It&apos;s locked and it will be revealed to you in due time!
Buy?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Shop.qml" line="183"/>
        <source>WaterPowder. Instructions: Just add water!!!
Price: 5$. Buy ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Shop.qml" line="224"/>
        <source>Carrots.
Price: 5$. Buy ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Shop.qml" line="305"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../qml/pages/Shop.qml" line="306"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../qml/pages/Shop.qml" line="374"/>
        <source>Welcome to my shop, strangers. Not much to sell right now, I&apos;m remodeling.
Feel free to look around, double tap on any item to buy. To leave the shop, just walk left.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Shop.qml" line="382"/>
        <source>That&apos;s my bike, it&apos;s not for sale. Leave it!!!</source>
        <translation>Essa é a minha bicicleta, não está à venda. Não mexas nela!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/Shop.qml" line="389"/>
        <source>Don&apos;t touch my money!!!</source>
        <translation>Não toques no meu dinheiro!!!</translation>
    </message>
    <message>
        <location filename="../qml/pages/Shop.qml" line="396"/>
        <source>I have hidden in there the Secret Book of Happiness. I will only sell it if you bring me a blue diamond.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Shop.qml" line="410"/>
        <source>Not enought money!!!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
