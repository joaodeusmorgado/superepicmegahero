#include "mytimer2.h"

/*MyTimer2::MyTimer2()
{

}
*/

/*
To void the inacuracy of Timer we will use intervalAux=1, and check in each timerUpdate() with elapsed if it's time to emit trigerred()
*/

#include <QDebug>

MyTimer2::MyTimer2(QObject *parent) : QObject(parent),
    intervalAux(0),
    m_running(false),
    m_repeat(false)
{
    //m_timer.setTimerType(Qt::PreciseTimer);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(timerUpdate())); //when QTimer timeout, fires MyTimer triggered() signal
    m_timer.setSingleShot(true);
}


void MyTimer2::timerUpdate()
{
    if (m_elapsedTimer.elapsed() > m_interval)
    {
        emit triggered();
        m_elapsedTimer.restart();
    }

}


void MyTimer2::setInterval(const int& interval_)
{
    if (m_interval == interval_)
        return;
    m_interval = interval_;

    m_timer.setInterval(intervalAux);//dont use m_interval
    emit intervalChanged(interval_);
}

void MyTimer2::setRunning(const bool& running_)
{
    if (m_running == running_)
        return;
    m_running = running_;
    if (running_)
        m_timer.start();
    else
        m_timer.stop();
    emit runningChanged(running_);
}

void MyTimer2::setRepeat(const bool &repeat_)
{
    if (m_repeat == repeat_)
        return;
    m_repeat = repeat_;
    m_timer.setSingleShot(!m_repeat);
    emit repeatChanged(repeat_);
}

void MyTimer2::start()
{
    m_timer.start();
    m_elapsedTimer.start();
}

void MyTimer2::stop()
{
    m_timer.stop();
}
