TEMPLATE = app

QT += qml quick core sensors quick3d
# CONFIG += c++11

TARGET = SuperEpicMegaHero

#TRANSLATIONS +=  translations/SuperEpicMegaHero_pt.ts
#used for updating the .ts file when using lupdate
TRANSLATIONS += qml/pages/translations/SuperEpicMegaHero_pt.ts

# add more languages here

lupdate_only{
SOURCES = *.qml \
    components/*.qml \
    libraries/*.qml \
    level01/*.qml \
    level02/*.qml \
    level03/*.qml \
    level04/*.qml \
    level05/*.qml \
    level06/*.qml \
    level07/*.qml \
    level08/*.qml \
    level09/*.qml \
    level10/*.qml \
    level11/*.qml \
    levelSettings/*.qml
}

SOURCES += main.cpp \
    mytime.cpp \
    mytimer.cpp \
    placement.cpp \
    qmltranslator.cpp \
    mytimer2.cpp

HEADERS += \
    mytime.h \
    mytimer.h \
    placement.h \
    qmltranslator.h \
    mytimer2.h

RESOURCES += \
    fonts.qrc \
    qml/pages/qml.qrc \
    qml/pages/images.qrc \
    qml/pages/skybox.qrc \
    qml/pages/qmlandroidndesktop.qrc \
    #qml/pages/sounds.qrc \
    qml/pages/translations.qrc \
    shaders.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

OTHER_FILES += \
    README

win32:RC_FILE = appicon.rc

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml \
    qml/pages/main.qml

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

contains(ANDROID_TARGET_ARCH, arm64-v8a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

ios {
    #ios_icon.files = $$files($$PWD/ios/AppIcon*.png)
    #QMAKE_BUNDLE_DATA += ios_icon

    VERSION = 2.0.0
    QMAKE_TARGET_BUNDLE_PREFIX = Apps4FunNWork
    #QMAKE_SHORT_VERSION = 3

    #QMAKE_ASSET_CATALOGS += path/to/resources.xcassets
    QMAKE_ASSET_CATALOGS += $$PWD/ios/Assets.xcassets

    QMAKE_INFO_PLIST = ios/Info.plist

    #QMAKE_ASSET_CATALOGS_APP_ICON = $$PWD/ios/AppIcon1024.png

    #QMAKE_ASSET_CATALOGS = $$PWD/ios/Images.xcassets
    #QMAKE_ASSET_CATALOGS_APP_ICON = "AppIcon"

#    QMAKE_ASSET_CATALOGS += ios/Assets.xcassets
}

#icon for macos
ICON = qml/pages/images/unicornhorse1024.icns
