//#version 330
//#extension GL_NV_shadow_samplers_cube : enable

varying highp vec3 TexCoord;
uniform samplerCube cubeTexture;

void main()
{
    gl_FragColor = textureCube(cubeTexture, TexCoord);// * vec4(f_Color, 1.0f);
}


/*
uniform sampler2D qt_Texture0;
varying highp vec4 qt_TexCoord0;

void main(void)
{
    gl_FragColor = texture2D(qt_Texture0, qt_TexCoord0.st);
}
*/
