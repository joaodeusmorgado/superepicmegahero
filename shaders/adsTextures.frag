//varying mediump vec3 Color;
varying mediump vec4 Color;
varying mediump vec3 viewNormal;
varying mediump vec3 Position;
//uniform vec3 cameraPosition;

varying mediump vec2 TexCoord;
uniform sampler2D outputTexture;


//light
struct LightInfo{
    vec3 position;  //light positon in eye coordinates
    vec3 LightIntensity;
    vec3 La; //ambiente light intensity
    vec3 Ld; //diffuse light intensity
    vec3 Ls; //specular light intensity
};

struct MaterialInfo{
    vec3 Ka; //ambiente reflectivity
    vec3 Kd; //diffuse reflectivity
    vec3 Ks; //specular reflectivity
    float Shininess;
};

uniform LightInfo Light;
uniform MaterialInfo Material;

//varying vec3 fragColor;

vec3 ads()
{
    vec3 n = normalize(viewNormal);
    vec3 s = normalize(Light.position - Position);
    vec3 v = normalize(-Position);
    vec3 r = reflect(-s, n);

    return Light.LightIntensity *
            ( Material.Ka +
              Material.Kd * max( dot(s, n), 0.0 ) +
              Material.Ks * pow( max( dot(r, v), 0.0 ), Material.Shininess ) );
}


vec3 adsHalfWayVectorold()
{
    vec3 n = normalize(viewNormal);
    vec3 s = normalize(Light.position - Position);
    vec3 v = normalize(-Position);
    vec3 h = normalize(v+s);

    return Light.LightIntensity * Material.Ks;

    return Light.LightIntensity *
            ( Material.Ka +
              Material.Kd * max( dot(s, viewNormal), 0.0 ) +
              Material.Ks * pow( max( dot(h, n), 0.0 ), Material.Shininess ) );
}

//-----------------------------------

vec3 ambient()
{
    return Material.Ka * Light.La;
}

vec3 diffuse()
{
    vec3 s = normalize(Light.position - Position);
    return Material.Kd * Light.Ld * max( dot(s, viewNormal), 0.0 ) ;
}

vec3 specular()
{
    vec3 n = normalize(viewNormal);
    vec3 v = normalize(-Position);
    vec3 s = normalize(Light.position - Position);
    vec3 h = normalize(v+s);
    return Material.Ks * Light.Ls * pow( max( dot(h, n), 0.0 ), Material.Shininess );
}

vec3 adsHalfWayVector()
{
    vec3 n = normalize(viewNormal);
    vec3 s = normalize(Light.position - Position);
    vec3 v = normalize(-Position);
    vec3 h = normalize(v+s);

    vec3 ambient = Material.Ka * Light.La;
    vec3 diffuse = Material.Kd * Light.Ld * max( dot(s, viewNormal), 0.0 ) ;
    vec3 specular = Material.Ks * Light.Ls * pow( max( dot(h, n), 0.0 ), Material.Shininess );

    return ambient + diffuse + specular;
}

void main(void)
{
    // gl_FragColor = vec4(Color,1);
    gl_FragColor = texture2D(outputTexture, TexCoord) + Color + vec4(adsHalfWayVector(), 1.0);
    //gl_FragColor = vec4(adsHalfWayVector(), 1.0); //light ADS
}
