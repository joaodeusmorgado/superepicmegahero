attribute vec3 position;
//attribute vec3 texCoord;
uniform mat4 mvp;

varying vec3 TexCoord;

void main()
{
    gl_Position = mvp * vec4(position, 1.0f);
    TexCoord = position;
}

/*
attribute highp vec4 qt_Vertex;
attribute highp vec4 qt_MultiTexCoord0;
uniform highp mat4 qt_ModelViewProjectionMatrix;
varying highp vec4 qt_TexCoord0;

void main(void)
{
    gl_Position = qt_ModelViewProjectionMatrix * qt_Vertex;
    qt_TexCoord0 = qt_MultiTexCoord0;
}
*/
