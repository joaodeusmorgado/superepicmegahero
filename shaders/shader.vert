uniform mediump mat4 modelView;
uniform mediump mat4 proj;
//uniform mediump mat4 mvp;

attribute vec4 vertexPosition;
attribute vec4 vertexColor;
//attribute vec3 vertexNormal;

varying mediump vec4 Color;
//varying mediump vec3 Normal;

void main(void)
{
    Color = vertexColor;
    gl_Position = proj * modelView * vertexPosition;
    //gl_Position = mvp * vertexPosition;
}
