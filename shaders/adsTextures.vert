uniform mediump mat4 modelView;
//uniform mediump mat4 view;
uniform mediump mat4 proj;
uniform mediump mat3 normalMatrix;
//uniform mediump mat4 mvp;

attribute vec3 vertexPosition;
attribute vec3 vertexNormal;
attribute vec4 vertexColor;
attribute vec2 texCoord;

//varying mediump vec4 Color;
varying mediump vec3 Position;
varying mediump vec3 viewNormal;
varying mediump vec2 TexCoord;
varying mediump vec4 Color;

void main(void)
{
   // LightIntensity = Ld * Kd * max( dot() ) ;

    //viewNormal = vec3(modelView * vec4(vertexNormal,0.0));
    viewNormal = normalize( normalMatrix * vertexNormal);

    Position = vec3(modelView * vec4(vertexPosition,1.0));
    Color = vertexColor;

    TexCoord = texCoord;

    gl_Position = proj * modelView * vec4(vertexPosition,1.0);
    //gl_Position = mvp * vertexPosition;
}
