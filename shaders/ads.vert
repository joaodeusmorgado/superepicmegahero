uniform mediump mat4 modelView;
//uniform mediump mat4 view;
uniform mediump mat4 proj;
//uniform mediump mat4 mvp;

attribute vec4 vertexPosition;
attribute vec3 vertexNormal;
//attribute vec4 vertexColor;

//varying mediump vec4 Color;
varying mediump vec3 Position;
varying mediump vec3 viewNormal;
varying mediump vec3 cameraPosition;

void main(void)
{
   // LightIntensity = Ld * Kd * max( dot() ) ;

    viewNormal = vec3(modelView * vec4(vertexNormal,0.0));
    Position = vec3(modelView * vertexPosition);
    //Position = vec3(modelView * vec4(cameraPosition, 1.0));
    //Color = vertexColor;
    gl_Position = proj * modelView * vertexPosition;
    //gl_Position = mvp * vertexPosition;

}
