#include "placement.h"

Placement::Placement(QObject *parent) : QObject(parent),
    delaySize(0),//15
    delayIndex(0),
    bDelayFollowingCamera(false)
{
    m_rotationDelay.resize(delaySize);
}

void Placement::resetPosition()
{
    setPosition(0,0,0);
}

void Placement::resetRotation()
{
    QQuaternion q = QQuaternion::fromAxisAndAngle(0, 0, 1, 15);
    setRotation(q);
}

void Placement::pitch(float angle)
{
    setRotation(rotation() * QQuaternion::fromAxisAndAngle(0, 1, 0, angle));
    // m_rotation *= QQuaternion::fromAxisAndAngle(0, 1, 0, angle);
    // //m_rotation.normalize();
    //emit rotationChanged(m_rotation);
    //  //m_rotation.pitch(angle);
}

void Placement::yaw(float angle)
{
    setRotation(rotation() * QQuaternion::fromAxisAndAngle(0, 0, 1, angle));

    //m_rotation *= QQuaternion::fromAxisAndAngle(0, 0, 1, angle);
    //emit rotationChanged(m_rotation);
}

void Placement::roll(float angle)
{
    setRotation(rotation() * QQuaternion::fromAxisAndAngle(1, 0, 0,angle));

    //m_rotation *= QQuaternion::fromAxisAndAngle(1, 0, 0,angle);
    //m_rotation.normalize();
    //emit rotationChanged(m_rotation);
    //m_rotation.roll(angle);
  //  m_rotation.normalize();
}

void Placement::moveFront(float d)
{
    QVector3D xAxis;
    QVector3D yAxis;
    QVector3D zAxis;

    m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    setPosition( position() + xAxis*d);

    //m_position += xAxis * d;


    //emit positionChanged(m_position);
    //emit moveFrontDelta(d);

    /*m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    m_position += xAxis * d;
    emit positionChanged(m_position);
    emit moveFrontDelta(d);*/
    emit moveFrontDelta(d);
}

void Placement::moveBack(float d)
{
    QVector3D xAxis;
    QVector3D yAxis;
    QVector3D zAxis;

    m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    setPosition( position() - xAxis*d);
    emit moveFrontDelta(-d);

    /*m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    m_position -= xAxis * d;
    emit positionChanged(m_position);
    emit moveFrontDelta(-d);*/

}

void Placement::moveLeft(float d)
{
    QVector3D xAxis;
    QVector3D yAxis;
    QVector3D zAxis;

    m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    m_position += yAxis * d;
    emit positionChanged(m_position);
}

void Placement::moveRight(float d)
{
    QVector3D xAxis;
    QVector3D yAxis;
    QVector3D zAxis;

    m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    m_position -= yAxis * d;
    emit positionChanged(m_position);
}

void Placement::moveUp(float d)
{
    QVector3D xAxis;
    QVector3D yAxis;
    QVector3D zAxis;

    m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    m_position += zAxis * d;
    emit positionChanged(m_position);
}

void Placement::moveDown(float d)
{
    QVector3D xAxis;
    QVector3D yAxis;
    QVector3D zAxis;

    m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    m_position -= zAxis * d;
    emit positionChanged(m_position);
}

QVector3D Placement::up()
{
    QVector3D xAxis;
    QVector3D yAxis;
    QVector3D zAxis;

    m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    return zAxis;
}

QVector3D Placement::front()
{
    QVector3D xAxis;
    QVector3D yAxis;
    QVector3D zAxis;

    m_rotation.getAxes(&xAxis, &yAxis, &zAxis);
    return xAxis;
}

void Placement::getAxes(QVector3D *xAxis, QVector3D *yAxis, QVector3D *zAxis) const
{
    Q_ASSERT(xAxis && yAxis && zAxis);

    m_rotation.getAxes(xAxis, yAxis, zAxis);
}

void Placement::setRotation(const QQuaternion &q)
{
    if (bDelayFollowingCamera)
    {
        qDebug()<<"setRotation() delayIndex: "<<delayIndex;
        qDebug()<<"setRotation() delaySize: "<<delaySize;
        m_rotationDelay[delayIndex] = q;
        delayIndex++;
        if (delayIndex == delaySize)
            delayIndex = 0;
    }

    if (m_rotation == q)
        return;
    m_rotation = q;
    emit rotationChanged(q);
}

void Placement::setPosition(const QVector3D &pos)
{
    if (bDelayFollowingCamera)
    {
        qDebug()<<"setPosition() delayIndex: "<<delayIndex;
        qDebug()<<"setPosition() delaySize: "<<delaySize;
        m_rotationDelay[delayIndex] = m_rotation;
        delayIndex++;
        if (delayIndex == delaySize)
            delayIndex = 0;
    }

    if (m_position == pos)
        return;
    m_position = pos;
    emit positionChanged(pos);
}

void Placement::setPosition(const float &x, const float &y, const float &z)
{
    setPosition(QVector3D(x, y, z));
}

QVector3D Placement::position()
{
    return m_position;
}

QQuaternion Placement::rotation()
{
    return m_rotation;
}

QQuaternion Placement::delayRotation()
{
    if (bDelayFollowingCamera) {
        if (delayIndex >= delaySize-1)
        {
            return m_rotationDelay.at(0);
        }
        else
        {
            return m_rotationDelay.at(delayIndex+1);
        }
        //return m_rotationDelay.at(delayIndex);
    }
    else
        return m_rotation;
}

QMatrix4x4 Placement::matrixModel()
{
    QMatrix4x4 model;
    model.rotate(m_rotation);
    model.translate(m_position);
    return model;
}
