#ifndef MYTIME_H
#define MYTIME_H

#include <QObject>
#include <QElapsedTimer>

class MyTime : public QObject
{
    Q_OBJECT
public:
    explicit MyTime(QObject *parent = 0);

signals:

public slots:
    void start(){m_time.start();}
    int restart(){return m_time.restart();}
    int elapsed(){return m_time.elapsed();}

private:
    QElapsedTimer m_time;
};

#endif // MYTIME_H
