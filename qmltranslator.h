#ifndef QMLTRANSLATOR_H
#define QMLTRANSLATOR_H

#include <QObject>
#include <QGuiApplication>
#include <QTranslator>
#include <QDir>
#include <QDebug>

class QmlTranslator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString language READ language WRITE setLanguage NOTIFY languageChanged)
public:
    explicit QmlTranslator(QObject *parent = nullptr);

    QString language(){return m_language;}

signals:
    void languageChanged();

public slots:

    void setLanguage(const QString &language)
    {
        if(language == m_language)
            return;

        m_language = language;

        if (!appTranslator.isEmpty())
            qApp->removeTranslator(&appTranslator);

        qDebug()<<"loading translation: "<<appTranslator.load(
                      (":/translations/SuperEpicMegaHero_")+language);

        qApp->installTranslator(&appTranslator);
        emit languageChanged();
    }

private:
    QTranslator appTranslator;
    QString m_language;
    //QString qmPath;
};

#endif // QMLTRANSLATOR_H
