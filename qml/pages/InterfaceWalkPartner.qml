import QtQuick 2.0

Item {
    id: root

    property var leader: null//({})
    property var follower: null//({})
    property real followingDistance: (leader !== null) ? leader.width*1.5 : 0

    /*command.walk.connect( heroObj.walk )
        command.startWalking.connect( heroObj.startWalking )
        command.stopWalking.connect( heroObj.stopWalking )
        command.direction.connect( heroObj.direction )
        command.jump.connect( heroObj.jump )*/

    signal destroyItem()

    onDestroyItem: {
        disconnect()
        root.destroy()
    }

    function connect() {
        if ( (leader !== null) && (leader !== undefined) && (follower !== null) && (follower !== undefined)) {
            leader.startWalking.connect( follower.startWalking )
            leader.stopWalking.connect( follower.stopWalking )
            leader.direction.connect( follower.direction )
            leader.jump.connect( follower.jump )
            leader.walk.connect( root.walkAtDistance )
        }
    }

    function disconnect() {
        //console.log("leader: "+leader)
        //console.log("follower: "+follower)
        //if ( (leader !== null) && (leader !== undefined) && (follower !== null) && (follower !== undefined)) {
        if ( leader && follower) {
            leader.startWalking.disconnect( follower.startWalking )
            leader.stopWalking.disconnect( follower.stopWalking )
            leader.direction.disconnect( follower.direction )
            leader.jump.disconnect( follower.jump )
            leader.walk.disconnect( follower.walk )
            follower.isWalking = false
        }
    }

    signal walkAtDistance()
    onWalkAtDistance: {
       // console.log("follow the leader")
        //var dist = leader.width*1.5//16*mm
        //if ( Math.abs(leader.x -follower.x) > followingDistance )
          // follower.walk()
        if (leader.dir === 1) {
            if ( (leader.x-followingDistance) > follower.x)
                follower.walk()
        }

        if (leader.dir === -1) {
            if ( (leader.x+followingDistance) < follower.x)
                follower.walk()
        }

    }
}
