import QtQuick 2.0
import QtSensors 5.0
import QtQuick.Window 2.2

Item {

    property alias active: rotSensor.active

    property real valueX: rotSensor.reading ? rotSensor.reading.x.toFixed(2) : 0
    property real valueY: rotSensor.reading ? rotSensor.reading.y.toFixed(2) : 0
    property real valueYnormalized: valueY

    //property real moveHorizontal: Screen.primaryOrientation === Qt.LandscapeOrientation ? valueX : valueY
    //property real moveVertical: Screen.primaryOrientation === Qt.LandscapeOrientation ? valueY : valueX

    property real calibrateX
    property real calibrateY

    property real velocityX
    property real velocityY

    property real velocityXFactor: 0.8
    property real velocityYFactor: velocityXFactor


    signal move(real cx, real cy)
    signal calibrate()

    onCalibrate: {
        console.log("calibrating...................")
        calibrateX = rotSensor.reading.x.toFixed(2)
        calibrateY = rotSensor.reading.y.toFixed(2)

        if (calibrateY < 0)
            calibrateY = 360+calibrateY

        console.log("calibrating Rotation sensor, x: "+calibrateX+ " : "+ calibrateY)
    }


    /*property string txt: calibrateX.toString()
    +"\n"+calibrateY.toString()
    +"\n"+valueX.toString()
    +"\n"+valueYnormalized.toString()
    +"\n"+velocityX.toString()
    +"\n"+velocityY.toString()

    Text {
        id: a1
        text: txt
        color: "red"
        font.bold: true
    }*/


    Timer {
        id: timerCalibrate
        interval: 100//fps_ms
        repeat : false
        running : false
        onTriggered: {
            calibrate()
        }
    }

    RotationSensor {
        id: rotSensor
        dataRate: 100

        onActiveChanged: {
            if(rotSensor.active)
                timerCalibrate.start()
        }

        onReadingChanged: {
            //console.log("dataRate rotation command: "+dataRate)
            valueYnormalized = valueY
            if (valueYnormalized < 0)
                valueYnormalized = 360 + valueYnormalized

            velocityX = (calibrateX - valueX) * velocityXFactor
            velocityY = -(calibrateY - valueYnormalized) * velocityYFactor

            move(velocityX, velocityY)
        }
    }
}
