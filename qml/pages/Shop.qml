import QtQuick 2.3
import "components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    property bool leaveShop: hero.x < 0
    //property string itemSelected: ""
    //property real itemPrice: 0

    signal exitShop()
    signal menu()

    onLeaveShopChanged: {
        if (leaveShop)
            exitShop()
    }

    onVisibleChanged: {
        if (visible) {
            levelStartSetup()
        }
        else {
            levelEndCleanup()
        }
    }

    MouseArea {
        anchors.fill: parent
        onDoubleClicked: exitShop()
    }

    CommandMultiTouchPad3 {
       id: commandTouchPad
       anchors.left: parent.left
       anchors.bottom: parent.bottom
       anchors.margins: defaultMargins
    }

    Rectangle {
        id: walls
        color: "LightGoldenRodYellow"
        width: parent.width
        height: parent.height*0.65
        anchors.top: parent.top
    }

    Rectangle {
        id: floor
        color: "Sienna"
        width: parent.width
        height: parent.height*0.35
        anchors.bottom: parent.bottom
    }

    Image {
        id: windowLeft
        width: 35*mm
        height: width * 624/796
        mipmap: true
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/shopWindow.png"
        anchors.bottom: walls.bottom
        anchors.bottomMargin: height/2
        anchors.left: parent.left
        anchors.leftMargin: defaultMargins*2
    }

    Image {
        id: windowRight
        width: 35*mm
        height: width * 624/796
        y: windowLeft.y
        mipmap: true
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/shopWindow.png"
        anchors.right: parent.right
        anchors.rightMargin: defaultMargins*2
    }

    Image {
        id: imgTable
        width: 60*mm
        height: 30*mm
        anchors.bottom: floor.top
        anchors.bottomMargin: -height/2
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.2
        source: "qrc:/images/table.png"
        mipmap: true
        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                //console.log("exiting shop........")
                exitShop()
            }
        }
    }

    Image {
        id: bookShelf
        width: height * 332/485
        height: 65*mm
        anchors.bottom: floor.top
        anchors.bottomMargin: -imgTable.height/2
        anchors.left: imgCarrots.right
        anchors.leftMargin: imgCarrots.width
        mipmap: true
        source: "qrc:/images/bookShelf.png"

        /*MouseArea {
            id: upperMouseArea
            width: parent.width
            height: parent.height/2
            anchors.top: parent.top

        }*/

        MouseArea {
            id: lowerMouseArea
            width: parent.width
            height: parent.height
            anchors.bottom: parent.bottom
            onDoubleClicked: {

                if (heroMap.treasureBlueDiamondsUnlocked) {
                    buyItemBoard.itemForSale = "screetBookOfHapiness"
                    buyItemBoard.itemPrice = 0
                    buyItemBoard.text = qsTr("I see that you have a blue diamond and now I can sell you "
                                             +"the Screet book of hapiness. Be warn that you can not read it yet.\n"
                                             + "It's locked and it will be revealed to you in due time!"
                               +"\nBuy?")
                    buyItemBoard.visible = true
                }
                else {
                    console.log("no diamonds")
                    createDialogBookOfHapiness()
                    talkBoard.show()
                }

            }
        }

    }

    Image {
        id: exitSign
        source: "qrc:/images/exitShop.png"
        width: 25*mm
        height: width * 235/320
        mipmap: true
        anchors.bottom: salesMan.top
        anchors.margins: defaultMargins * 3
        anchors.horizontalCenter: salesMan.horizontalCenter
    }

    Image {
        id: salesMan
        source: "qrc:/images/shopsalesman.png"
        width: 25*mm
        height: width * 481/451
        mipmap: true
        anchors.bottom: imgTable.top
        anchors.horizontalCenter: imgTable.horizontalCenter
    }

    Image {
        id: imgWaterPowder
        source: "qrc:/images/waterPowder.png"
        width: 8*mm
        height: width * 472/334
        mipmap: true
        anchors.bottom: imgTable.top
        anchors.right: imgTable.right
        anchors.rightMargin: defaultMargins
        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                buyItemBoard.itemForSale = "waterPowder"
                buyItemBoard.itemPrice = 5
                buyItemBoard.text = qsTr("WaterPowder."
                           +"\nInstructions: Just add water!!!\nPrice: 5$. Buy ?")
                buyItemBoard.visible = true
            }
        }
    }

    Image {
        id: imgRegister
        source: "qrc:/images/cashDesk.png"
        width: 10*mm
        height: width * 257/229
        mipmap: true
        anchors.bottom: imgTable.top
        anchors.left: imgTable.left
        anchors.leftMargin: defaultMargins
        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                createDialogRegister()
                talkBoard.show()
            }
        }
    }


    Image {
        id: imgCarrots
        source: "qrc:/images/carrots.png"
        width: 20*mm
        height: width * 612/444
        mipmap: true
        anchors.bottom: imgTable.bottom
        anchors.left: imgTable.right
        anchors.leftMargin: defaultMargins
        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                buyItemBoard.itemForSale = "carrots"
                buyItemBoard.itemPrice = 5
                buyItemBoard.text = qsTr("Carrots."
                           +"\nPrice: 5$. Buy ?")
                buyItemBoard.visible = true
            }
        }
    }

    Image {
        id: imgBike
        source: "qrc:/images/bike.png"
        width: 40*mm
        height: 30*mm
        mipmap: true
        anchors.bottom: floor.bottom
        anchors.bottomMargin: floor.height/2
        anchors.right: parent.right
        anchors.rightMargin: defaultMargins*2
        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                createDialogBike()
                talkBoard.show()
            }
        }
    }

    Person {
        id: hero
        width: 18*mm
        height: width * 320/156

        function reset(){
            x = 0//mainRoot.width * 4
            y = walls.height-hero.height/2
            hero.dir = 1
            hero.worldLimitLeft = -hero.width
            hero.worldLimitRight = parent.width-hero.width
            hero.worldLimitSky = 0
            hero.worldLimitGround = hero.y
        }
        onXChanged: x >= (salesMan.x+salesMan.width/2) ? salesMan.mirror = true : salesMan.mirror = false
    }


    Person {
        id: girl
        imgName: "girl"
        width: height * 145/320
        height: hero.height*0.95

        function reset() {
            x = 0//mainRoot.width * 4
            y = walls.height-hero.height/2
            girl.dir = 1
            girl.worldLimitLeft = -girl.width
            girl.worldLimitRight = parent.width-girl.width
            girl.worldLimitSky = 0
            girl.worldLimitGround = girl.y
        }
    }

    InterfaceWalkPartner {
        id: interfaceWalkGirlHero
        leader: hero
        follower: girl
        //call function connect() when the hero saves the girl from the cage
        // that will make the girl walk, following the hero
    }


    MessageBoard2 {
        id: buyItemBoard
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.margins: defaultMargins
        //text: qsTr("")
        pixelSize: 10*mm
        horizontalAlignment: Text.AlignHCenter //Text.AlignJustify
        verticalAlignment: Text.AlignVCenter
        visible: false
        showCancelBtn: true
        bntOkText: qsTr("Yes")
        bntCancelText: qsTr("No")

        property string itemForSale: ""
        property real itemPrice: 0

        onOk: {
            buyItemBoard.visible = false
            buyItem()
        }
        onCancel: buyItemBoard.visible = false
    }

    ConversationBoard2 {
        id: talkBoard
        width: mainRoot.width * 0.6
        height: mainRoot.height * 0.6
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        opacity: 0.75
        //imageLeft: "heroHead.png"
        imageRight: "shopsalesman.png"
        imageBottomWidth: 20*mm
        imageBottomHeight: 20*mm
        visible: false
    }

    TouchPad2 {
        id: touchPad
        centerX: commandTouchPad.touchPad.centerX
        centerY: commandTouchPad.touchPad.centerY
        visible: commandTouchPad.touchPad.visible
    }


    //----------------------------

    function levelStartSetup(){
        //createItems()
        createConnections()
        hero.reset()
        girl.reset()
        interfaceWalkGirlHero.connect()
        welcomeDialogue()
        talkBoard.show()
    }

    function levelEndCleanup(){
        destroyConnections()
    }


    function createConnections() {
        lib.connectCommandWalk(commandTouchPad, hero)
        lib.connectCommandWalk(commandKeyb, hero)
    }

    function destroyConnections() {
        lib.disconnectCommandWalk(commandTouchPad, hero)
        lib.disconnectCommandWalk(commandKeyb, hero)
    }


    //-------------------------------------------

    function welcomeDialogue() {
        talkBoard.clear()
        talkBoard.imageLeft = ""
        talkBoard.imageRight = "shopsalesman.png"
        talkBoard.addMessage4(qsTr("Welcome to my shop, strangers. Not much to sell right now, I'm remodeling.\n"
                                   + "Feel free to look around, double tap on any item to buy. To leave the shop, just walk left."), "shopsalesman.png", 2, false)
    }

    function createDialogBike() {
        talkBoard.clear()
        talkBoard.imageLeft = ""
        talkBoard.imageRight = "shopsalesman.png"
        talkBoard.addMessage4(qsTr("That's my bike, it's not for sale. Leave it!!!"), "shopsalesman.png", 2, false)
    }

    function createDialogRegister() {
        talkBoard.clear()
        talkBoard.imageLeft = ""
        talkBoard.imageRight = "shopsalesman.png"
        talkBoard.addMessage4(qsTr("Don't touch my money!!!"), "shopsalesman.png", 2, false)
    }

    function createDialogBookOfHapiness() {
        talkBoard.clear()
        talkBoard.imageLeft = ""
        talkBoard.imageRight = "shopsalesman.png"
        talkBoard.addMessage4(qsTr("I have hidden in there the Secret Book of Happiness. I will only sell it if you bring me a blue diamond."), "shopsalesman.png", 2, false)
    }


    function buyItem() {
        //check if we have enought money
        //buyItemBoard.showOkBtn = true
        //buyItemBoard.bntOkText = qsTr("Yes")
        //buyItemBoard.bntCancelText = qsTr("No")

        if (buyItemBoard.itemPrice > heroMap.money) {
            //buyItemBoard.showOkBtn = false
            //buyItemBoard.bntCancelText = qsTr("Ok")

            buyItemBoard.text = qsTr("Not enought money!!!")
            buyItemBoard.visible = true
            return;
        }

        heroMap.buyItem(buyItemBoard.itemPrice)
        //heroMap.money -= buyItemBoard.itemPrice

        //check item bought and unlock it
        if (buyItemBoard.itemForSale === "carrots") {
            heroMap.carrotsUnlocked = true
        }

        if (buyItemBoard.itemForSale === "screetBookOfHapiness") {
            if (heroMap.treasureBlueDiamondsUnlocked)
                heroMap.bookOfHapinessUnlocked = true
        }

        if (buyItemBoard.itemForSale === "waterPowder") {
            heroMap.waterPowderUnlocked = true
        }

    }//buyItem()

}
