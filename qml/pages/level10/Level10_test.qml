import QtQuick 2.0
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "lightblue"

    signal menu()


    property bool point1IsJoystick1: false
    property bool point1IsJoystick2: false

    property bool point2IsJoystick1: false
    property bool point2IsJoystick2: false


    MultiPointTouchArea {
      anchors.fill: parent
      touchPoints: [
          TouchPoint {
              id: point1

              onPressedChanged: {
                  if (pressed) {
                      //console.log("startX: "+startX)
                      if (joyStick1Pressed(startX, startY)) {
                          point1IsJoystick1 = true
                      }

                      if (joyStick2Pressed(startX, startY)) {
                          point1IsJoystick2 = true
                      }
                  }
                  else {
                      point1IsJoystick2 = false
                      point1IsJoystick1 = false
                  }

              }

              onXChanged: {
                  console.log("startX: "+startX)
                  if (point1IsJoystick1) {
                      joyStick1BtnMove(x, y)
                  }

                  if (point1IsJoystick2) {
                      joyStick2BtnMove(x, y)
                  }
              }

              onYChanged: {
                  console.log("startX: "+startX)
                  if (point1IsJoystick1) {
                      joyStick1BtnMove(x, y)
                  }

                  if (point1IsJoystick2) {
                      joyStick2BtnMove(x, y)
                  }
              }



          },
          TouchPoint {
              id: point2

              onPressedChanged: {
                  if (pressed) {
                      //console.log("startX: "+startX)
                      if (joyStick1Pressed(startX, startY)) {
                          point2IsJoystick1 = true
                      }

                      if (joyStick2Pressed(startX, startY)) {
                          point2IsJoystick2 = true
                      }
                  }
                  else {
                      point2IsJoystick2 = false
                      point2IsJoystick1 = false
                  }

              }

              onXChanged: {
                  console.log("startX: "+startX)
                  if (point2IsJoystick1) {
                      joyStick1BtnMove(x, y)
                  }

                  if (point2IsJoystick2) {
                      joyStick2BtnMove(x, y)
                  }
              }

              onYChanged: {
                  console.log("startX: "+startX)
                  if (point2IsJoystick1) {
                      joyStick1BtnMove(x, y)
                  }

                  if (point2IsJoystick2) {
                      joyStick2BtnMove(x, y)
                  }
              }

          }

      ]


      Rectangle {
        id: joystick1
        width: 50
        height: 150
        color: "gray"
        radius: 25
        x: 150
        y: 20
      }

      Rectangle {
        id: joystick1Btn
        width: 50
        height: 50
        radius: 25
        color: "blue"
        x: joystick1.x
        y: joystick1.y
      }


      Rectangle {
        id: joystick2
        width: 150
        height: 50
        x: 300
        y: 200
        color: "gray"
        radius: 25
      }

      Rectangle {
        id: joystick2Btn
        width: 50
        height: 50
        radius: 25
        color: "blue"
        x: joystick2.x
        y: joystick2.y
      }

    }//MultiPointTouchArea



    function joyStick1Pressed(xx, yy) {
        if (    xx >= joystick1Btn.x && xx <= joystick1Btn.x+joystick1Btn.width
                && yy >= joystick1Btn.y && yy <= joystick1Btn.y+joystick1Btn.height) {
            console.log("click inside joystick1")
            return true;
        }
        else {
            console.log("click outside joystick1")
            return false;
        }
    }

    function joyStick1BtnMove(xx, yy) {
        if (yy > joystick1.y+joystick1.height-joystick1Btn.height)
            joystick1Btn.y = joystick1.y+joystick1.height-joystick1Btn.height
         else if(yy < joystick1.y)
            joystick1Btn.y = joystick1.y
        else
            joystick1Btn.y = yy
    }



    function joyStick2Pressed(xx, yy) {
          if (    xx >= joystick2Btn.x && xx <= joystick2Btn.x+joystick2Btn.width
                && yy >= joystick2Btn.y && yy <= joystick2Btn.y+joystick2Btn.height) {
            //console.log("click inside joystick2")
            return true;
        }
        else {
            //console.log("click outside joystick2")
            return false;
        }
    }

    function joyStick2BtnMove(xx, yy) {
        if (xx < joystick2.x)
            joystick2Btn.x = joystick2.x
         else if(xx > joystick2.x+joystick2.width-joystick2Btn.width)
            joystick2Btn.x = joystick2.x+joystick2.width-joystick2Btn.width
        else
            joystick2Btn.x = xx
    }


    MenuPopUpVertical {
        id: menuVertical
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }

}
