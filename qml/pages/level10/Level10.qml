import QtQuick 2.0
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "blue" //"lightblue"

    signal menu()
    signal nextLevel()

    onVisibleChanged: {
        if (visible) {            
            createConversation()
            levels.levelUnlocked = levels.levelUnlocked === 10 ? 11 : levels.levelUnlocked
            msg.show()
        }
    }

    ConversationBoard2 {
        id: msg
        color: "purple" //"darkblue"
        //width: parent.width
        //height: parent.height - menuVertical.height - 2*defaultMargins
        //x: parent.width * 0.5 - msg.width/2
        anchors.top: menuVertical.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        visible: false
        imageLeft: ""
        imageRight: ""
        onVisibleChanged: {
            if (!visible)
                //console.log("book is now visible")
                panelEOL.visible = true
            else
                panelEOL.visible = false
        }
    }

    MenuPopUpVertical {
        id: menuVertical
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: {
            levels.levelUnlocked = levels.levelUnlocked === 10 ? 11 : levels.levelUnlocked
            menu()
        }
        onBook: heroMap.visible = true
    }

    PanelEndOfLevel2 {
        id: panelEOL
        color: "purple"//"blue"
        visible: false
        anchors.centerIn: parent
        pixelSize: 8*mm
        text: qsTr("Happy readings!\nNow back to exploring 😊")
        onMenu: parent.menu()
        onReplay: {
            msg.show()
        }
        onNext: parent.nextLevel()
    }

    function createConversation() {
        msg.clear()

        //msg.imageBottomWidth = 80*mm
        //msg.imageBottomHeight = 80*mm

        msg.imageBottomWidth = msg.width*0.7
        msg.imageBottomHeight = msg.height*0.7

        msg.addMessage5(qsTr("The Book of Happiness!\nHints to be happy."), "", 0, false, "bookOfHappiness.png")


        //msg.imageBottomWidth = 40*mm
        //msg.imageBottomHeight = 40*mm
        msg.addMessage5(qsTr("Don't waste to much time playing in your pc, mobile, game console. Make regular breaks,"
                             +" go out, play with your friends, fly a kite in the park."), "", 0, false, "kite.png")

        //msg.imageBottomWidth = 80*mm
        //msg.imageBottomHeight = 80*mm
        msg.addMessage5(qsTr("Watch the nature and enjoy 😊"), "", 0, false, "nature.png")

        //msg.imageBottomWidth = 40*mm
        //msg.imageBottomHeight = 40*mm
        msg.addMessage5(qsTr("Try a new flavour of ice cream, that you never tasted before.\n"
                             +"If you don't like it, just smile and be happy, there's more flavours to try 😊"), "", 0, false, "icecream.png")

        msg.addMessage5(qsTr("If you're stressed go for a swim in the ocean, if you have the chance. Carefull not to drown 😊"), "", 0, false, "ocean.png")

        msg.addMessage5(qsTr("There's hidden secrets in nature. Be curious!"), "", 0, false, "flower.png")

        msg.addMessage5(qsTr("If you're a student, be a warrior to fight the laziness and do your work!\nNo one can fight your battles for you."), "", 0, false, "BoyHelmet.png")

        msg.addMessage5(qsTr("Eat healthy food... at least once in a while 😊. Yummy."), "", 0, false, "horseEatingCarrot.png")

        msg.addMessage5(qsTr("You can choose: be grumpy or smile."), "", 0, false, "robonoidSmile.png")

        msg.addMessage5(qsTr("You can learn a lot by reading paper books."), "", 0, false, "bookShelf.png")

        msg.addMessage5(qsTr("Enought advice, now go live your life 😊.\nThe end."), "", 0, false, "family.png")

    }

}
