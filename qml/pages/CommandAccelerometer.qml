import QtQuick 2.0
import QtSensors 5.0
//import QtQuick.Window 2.2


Item {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    signal move(real cx, real cy)
    signal direction(int direct)// -1->left ; 1->right
    signal calibrate()

    readonly property double radians_to_degrees: 180 / Math.PI
    property real calibrationX: 0
    property real calibrationY: 0
    property bool calibrateNow: true
    property alias active: accel.active

    onActiveChanged: {
        if (active)
            calibrate()
    }

    onCalibrate: {
        if (!accel.active)
            return;
        calibrateNow = true;
    }

    /*Text {
        id: xx
    }
    Text {
        id: yy
        anchors.top: xx.bottom
    }
    Text {
        id: zz
        anchors.top: yy.bottom
    }*/

    Accelerometer {
        id: accel
        dataRate: 100
        active: false

        onReadingChanged: {
            console.log("dataRate accelerometer command: "+dataRate)
            var velocity = 0.6;
            var newY = (dummySpaceShip.y - calcRoll(accel.reading.x, accel.reading.y, accel.reading.z) * velocity) + calibrationY
            var newX = (dummySpaceShip.x - calcPitch(accel.reading.x, accel.reading.y, accel.reading.z) * velocity) + calibrationX

            if (calibrateNow) {
                calibrationY = calcRoll(accel.reading.x, accel.reading.y, accel.reading.z) * velocity
                calibrationX = calcPitch(accel.reading.x, accel.reading.y, accel.reading.z) * velocity
                calibrateNow = false
            }


            if (isNaN(newX) || isNaN(newY))
                return;

            /*if (newX < 0)
                newX = 0

            if (newX > root.width - dummySpaceShip.width)
                newX = root.width - dummySpaceShip.width

            if (newY < dummySpaceShip.height)
                newY = dummySpaceShip.height

            if (newY > root.height - dummySpaceShip.height)
                newY = root.height - dummySpaceShip.height*/

            dummySpaceShip.x = newX
            dummySpaceShip.y = newY



            var moveX = 0;
            var moveY = 0;
            velocity = 6
            if (accel.reading.x > 0) moveX= velocity;
            if (accel.reading.x < 0) moveX= -velocity;
            if (accel.reading.y > 0) moveY= velocity;
            if (accel.reading.y < 0) moveY= -velocity;

            //xx.text = moveX//accel.reading.x
            //yy.text = moveY//accel.reading.y
            //zz.text = accel.reading.z


            move(moveY, -moveX)
            //move(dummySpaceShip.x, dummySpaceShip.y)
        }
    }

    function calcPitch(x,y,z) {
        return -Math.atan2(y, Math.sqrt(x*x + z*z) ) * radians_to_degrees;
    }
    function calcRoll(x,y,z) {
        return -Math.atan2(x, Math.sqrt(y*y + z*z) ) * radians_to_degrees;
    }

    Item {
        id: dummySpaceShip // todo: refactor later, for now it's easy
        //to make this dummy spaceship move and send their position to the real spaceship
        //using the move signal
        width: 20*mm
        height: width
        x: parent.width/2
        y: parent.height-dummySpaceShip.height
    }

}
