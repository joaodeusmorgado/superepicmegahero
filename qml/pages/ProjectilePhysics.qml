import QtQuick 2.0

// Abstraction physics for the bullets collision detection
// How to use this:

// - It should be created a Physics item for each bullet

// Create connections to this item:
// - signal checkCollision() should be called once per main game timer loop
// - connect destroyBullet() to the real bullet
// - connect enemyHit() to some other enemy signal (where enemy life should, for example,
//  decrement, destroyed,...)


//Note: right now this only checks collision with the list of enemies
//Todo: add a target so that this can be used to target only one object
//i.e the hero, that way this can be a bullet shot from the enemy trying to hurt the hero
// use some kind of flag to mark with as a enemy fired bullet trying to hit the hero
// or a hero fired bullet trying to hit a listOfEnemies

Item {
    id: root

    property var listOfEnemies: ({})
    property var delta //if enemy distance from bullet is smaller than delta, we have a collision

    property real velocity: 2*mm
    property real angle: -Math.PI/2
    property real _cosAngle: Math.cos(angle) //cache
    property real _sinAngle: Math.sin(angle) //cache

    //bullet will be destroyed if outside the gamming area
    property real xMinBulletDestroy: 0
    property real xMaxBulletDestroy: mainRoot.width
    property real yMinBulletDestroy: 0
    property real yMaxBulletDestroy: mainRoot.height

    property point topLeft: Qt.point(x,y)
    property point bottomRight: Qt.point(x+width,y+height)

    property alias running: timer.running

    Component.onCompleted: {
        listOfEnemies = activeEnemiesList.activeEnemiesList
    }


    signal checkCollisionWithEnemies()//check collision between bullet and enemy
    signal destroyBullet() //call this if a collision has ocurred or if the bullet is out of reach
    //signal enemyHit() // bullet has hit the enemy

    onDestroyBullet: {
        root.destroy()
    }

    signal moved()

    onMoved: checkCollisionWithEnemies()

    onCheckCollisionWithEnemies: {
        //console.log("collision check")

        //if there's a collision with the enemy destroy bullet
        //todo: check arguments
        _checkCollisionWithActiveEnemies()

        // if the bullet is outside the gamming area, destroy it
        if ( root &&  _checkBulletOutsideGammingArea() ) {
            destroyBullet()
            //console.log("bullet outside gaming area, so destroy physics")
            //root.destroy()
            return;
        }

    }

    //moves the shot and emit the signal moved()
    Timer {
        id: timer
        interval: fps_ms; running: true; repeat: true
        onTriggered: {
            move() //mode arrow
            checkCollisionWithEnemies()
            //moved() // emit moved signal
        }
    }


    function _checkCollisionWithActiveEnemies(){
        //todo: check delta value
        for (var i = 0; i<listOfEnemies.count; i++)
        {
            delta = listOfEnemies.get(i).obj.width/2
            //console.log("listOfEnemies.count"+listOfEnemies.count)
            //console.log("red enemy y: "+ listOfEnemies.get(0).obj.y)
            //console.log("arrow y: "+ bullet.y)

            //check collision between bullet and enemies list,
            //if there's a collision, destroy bullet and send a imHit() signal to the enemy

            if (lib.checkCollison(listOfEnemies.get(i).obj.topLeft, listOfEnemies.get(i).obj.bottomRight, root.topLeft, root.bottomRight))
            /*if ( lib.distance(listOfEnemies.get(i).obj.x + listOfEnemies.get(i).obj.width/2,
                                         listOfEnemies.get(i).obj.y + listOfEnemies.get(i).obj.height/2,
                                         root.x + root.width/2, root.y + root.width/2, delta) )*/
            {
                //console.log("Enemy hit")
                //enemyHit()
                listOfEnemies.get(i).obj.imHit()
                destroyBullet()
                //console.log("collision detected, so destroy physics")
                //root.destroy()
                return;

            }
        }
    }

    function _checkBulletOutsideGammingArea() {
        if (root.x < xMinBulletDestroy || root.x > xMaxBulletDestroy
                || root.y < yMinBulletDestroy || root.y > yMaxBulletDestroy)
            return true;
        else
            return false;
    }

    function move() {
        x += velocity * _cosAngle //Math.cos(angle)
        y += velocity * _sinAngle //Math.sin(angle)
        //x += velocity * Math.cos(angle)
        //y += velocity * Math.sin(angle)
        /*console.log("bullet move--------------")
        console.log("angle: "+angle)
        console.log("_cosAngle: "+_cosAngle)
        console.log("_sinAngle: "+_sinAngle)
        console.log("x: "+x)
        console.log("y: "+y)*/
    }

    function init(xPos,yPos) {
        root.x = xPos
        root.y = yPos
    }

    function setAngle(xx, yy) {
        angle = Math.atan2(yy, xx)
        //console.log("xx: "+xx+" yy: " +yy +" angleRad: "+angle + " angleDed: "+ angle * 57.2957795131)
    }

    onAngleChanged: {
        _cosAngle = Math.cos(angle)
        _sinAngle = Math.sin(angle)
    }

}
