//import QtQuick 2.0
import QtQuick 2.3
//import com.Timer 1.0

/* Person rectangle coordinates
  (x,y)      (x+width,y)
     -----------
    |           |
    |           |
    |           |
     -----------
  (x,y1)      (x+width,y1)

  y1 = y+height
*/

MyPath2 {
    id: root
    width: 18*mm
    height: 35*mm

    property string name //useful for debbugging
    property bool isWalking:false
    property string imgName: "hero"
    //property string imgWalk: "01" //to refactor later, right now "01" means
    //that an animation is expecting 3 files with names "xxx"+"01/02/03.png"
    // and "0" means no animation

    property alias imageSource: img.source
    property int imgWalkCount: 3 // number of image walk animation
    property int imgWalkIterator: 1
    property int imgType: 1 //type of Image:
    // 0 is no animation
    // 1 is walking animation
    // -1 is fireShot animation
    // 2 is faint animation


    //0 means that there is no animation and image file is "imgName.png"
    // imgWalk > 0, means that there is a animation expecting N files with names "xxx"+"1/2/.../N.png"
    // imaWalk === -1 means a temporary fireShot position

    property string imgFireShot: ""
    property string imgFaints: ""

    //when putting a person on a ground is more easy to define the person y1 coordinate
    property real y1 //y1 = y + height
    y: y1-height //we avoid having to explicit define y with this biding

    property real x1: x+width // convenience variable to describe the x position at the right

    property real midX: x+width/2
    property real midY: y+height/2

    property real velocityWalk: 1.5*mm
    property int dir: 1//direction left = -1 ; right = 1

    property real maxLife: 3
    property real life: maxLife //life value is between 0 and maxLife

    property point topLeft: Qt.point(x,y)
    property point bottomRight: Qt.point(x+width,y+height)

    function lifeInit() {
        life = maxLife
    }

    onMaxLifeChanged: {
        lifeInit()
    }

    property bool jumpAnimationRotation: false //wip
    property real jumpRotationDelta: jumpAnimationRotation ? -10 * dir : 0
    property real jumpLenght: root.height*0.8
    property real jumpTimeDuration: 800
    property real fallTimeDuration: 800
    property real jumpStartY
    property real jumpMaxHeight
    property real jumpEndY
    property real fallStartY
    property real fallEndY

    property bool canFireMagicShots: false //make true for girl, who fires white shots

    property bool canFly: false//use this property when making connections
    //if (canFly) lib.connectCommandFly(commandTouchPad, somePerson2Obj);
    //if (canFly) lib.connectCommandFly(commandKeyb, somePerson2Obj);

    property bool isFLying: false

    property bool isJump: false
    property real t
    property real dt: 0.1//jumpXdist *0.05
    property real flyVelocityY: mm

    property alias mouseAreaEnabled: mouseAreaPerson.enabled

    //used for checkPath when jumping
    property real y1Previous

    Component.onCompleted: {
        //xPrevious = x
        //y1Previous = y1
    }

    signal doubleClick()
    signal startWalking()
    signal stopWalking()
    signal walk()
    signal direction(int d)
    signal jump()
    signal fall()
    signal startFlying()
    signal stopFlying()
    signal fly()
    signal fireShot() // used to temporary change the image to simulate a fire shot

    //property real hitDamage: 1
    signal faint()
    signal imHit(real hitDamage)
    signal imDestroyed()

    onImHit: {
        life -= hitDamage
        //console.log("I'm hit")
        if (life === 0)
            imDestroyed()
    }

    onStartWalking: isWalking = true
    onStopWalking: isWalking = false
    onDirection: dir = d

    onStartFlying: {
        console.log("start flying...........................")
        isFLying = true
        if (fallAnimation.running)
            fallAnimation.stop()
    }
    onStopFlying: {
        console.log("stop flying...........................")
        isFLying = false
        fall()
    }

    onFly: {
        y1 -= flyVelocityY
    }

    onFireShot: {
        //imgWalkIterator = imgWalk
        imgType = -1
        fireShotTimer.start()
    }

    Image {
        id: img
        width: parent.width
        height: parent.height
        fillMode: Image.PreserveAspectFit
        //source: imgWalk === "0" ? "qrc:/images/"+imgName+".png" : "qrc:/images/"+imgName+imgWalk+".png"
        source: imgType === 0 ? "qrc:/images/"+imgName+".png" : //normal walk
                                imgType === 1 ? "qrc:/images/"+imgName+imgWalkIterator.toString()+".png" :
                                imgType === -1 ? "qrc:/images/"+imgFireShot+".png" : //girl shooting white fire
                                imgType === 2 ? "qrc:/images/"+imgFaints+".png" // girl faints
                                              : "qrc:/images/"+imgName+".png" // default
        //if we add more imgTypes we need to continue to expand this

        mirror: dir === 1 ? false : true
        mipmap: true
    }

    //--jump----------------------------------------
    ParallelAnimation {
        id: jumpAnimation
        alwaysRunToEnd: false
        onStarted: {
            y1Previous = y1
            isJump = true
        }
        onStopped: {
            isJump = false
        }

        // rise and fall
        SequentialAnimation {
            //start raising
            NumberAnimation {
                target: root; property: "y1";
                //from: jumpStartY; to: jumpEndY;
                from: jumpStartY; to: jumpMaxHeight;
                duration: jumpTimeDuration/2
                easing {  type: Easing.OutQuad }
            }

            //start falling
            NumberAnimation {
                target: root; property: "y1";
                from: jumpMaxHeight; to: jumpEndY;
                duration: jumpTimeDuration/2
                easing { type: Easing.InQuad }
            }
        }//SequentialAnimation

        //inclination while jumping
        SequentialAnimation {
            NumberAnimation {
                target: root; property: "rotation";
                from: 0; to: jumpRotationDelta;
                duration: jumpTimeDuration*0.25
                easing {  type: Easing.InQuad }
            }

            NumberAnimation {
                target: root; property: "rotation";
                from: jumpRotationDelta; to: 0;
                duration: jumpTimeDuration*0.25
                easing {  type: Easing.Linear }
            }

            NumberAnimation {
                target: root; property: "rotation";
                from: 0; to: -jumpRotationDelta;
                duration: jumpTimeDuration*0.25
                easing {  type: Easing.Linear }
            }

            NumberAnimation {
                target: root; property: "rotation";
                from: -jumpRotationDelta; to: 0;
                duration: jumpTimeDuration*0.25
                easing {  type: Easing.OutQuad }
            }
        }
    }//ParallelAnimation - jump
    //--jump----------------------------------------



    //--fall----------------------------------------
    NumberAnimation {
        id: fallAnimation
        alwaysRunToEnd: false
        target: root; property: "y1";
        from: fallStartY; to: fallEndY;
        duration: fallTimeDuration //* (fallEndY-fallStartY)
        easing { type: Easing.InQuad }
        onStarted: {
            console.log("start falling...............................................")
            console.log("fallStartY: "+fallStartY)
            console.log("fallEndY: "+fallEndY)
            console.log("duration: "+duration)
            console.log("fallenAnimation: "+fallTimeDuration)
        }
    }
    //--fall----------------------------------------

    onJump: {
        if (isJump)
            return;
        //console.log(name + " jump............................................................")
        jumpStartY = root.y1
        jumpMaxHeight = root.y1 - jumpLenght
        jumpEndY = worldLimitGround
        jumpAnimation.start()
        isJump = true
    }

    onFall: {
        //console.log(name+" fall...................................")
        if (isJump)
            return;
        //console.log("falling...........................")
        fallStartY = y1
        fallEndY = worldLimitGround
        fallAnimation.start()
    }

    //onYChanged: {
        //console.log("onYChanged - name: " + name + " -y: "+ y + " - y1: "+y1+" : y1previous: "+y1Previous)
    //}

    onY1Changed: {
        //console.log("onY1Changed - name: " + name + " - y1: "+y1+" : y1previous: "+y1Previous)
        if ( !canMoveInPathY(y1-y1Previous) ) {
            //console.log(name + " onY1Changed cant move in Path Y......")
            jumpAnimation.stop()
            fallAnimation.stop()
            y1 = y1Previous
            //y1 = y1Previous = obstacleTopLimit //avoid the person to be slight above the obstacle
            return;
        }
        //else {
          //  console.log("jump can jump in Path Y......")
        //}
        y1Previous = y1
    }

    onWalk: {
        //console.log("walking")
        moveInPathX(velocityWalk * dir)

        if (isFLying)
            return;

        if (y1 === worldLimitGround)
            return;

        //if (y1 === obstacleTopLimit)
          //  return;

        if (isJump) {
            //console.log(name + " dont fall while is walking becausa is jumping")
            return;
        }

        if ( canMoveInPathY(height*0.01) ) {
            //console.log(name + " fall while walking ----------------------------------------------------------- ")
            fall()
        }
    }


    Timer {
        id: animateHeroWalkingTimer
        interval: 90
        running: isWalking || isFLying
        repeat: true
        onTriggered: {

            if (imgType <= 0)
                return;

            if (imgWalkIterator === imgWalkCount)
                imgWalkIterator = 1
            else
                imgWalkIterator++;
        }
        onRunningChanged: {
            if (!running)
                imgWalkIterator = 1 //"01"
        }
    }

    Timer {
        id: fireShotTimer
        interval: 500
        running: false
        repeat: false
        onTriggered: {
            imgType = -1
            imgWalkIterator = 1
        }

        onRunningChanged: {
            if (!running){
                imgType = 1
                imgWalkIterator = 1
            }
        }
    }

    Timer {
        id: faintsTimer
        interval: 500
        running: false
        repeat: true
        onTriggered: {
            imgType = 2
            imgWalkIterator = 1
        }
        onRunningChanged: {
            imgWalkIterator = 0
        }
    }

    MouseArea {
        id: mouseAreaPerson
        width: parent.width
        height: parent.height/2
        anchors.top: parent.top
        //anchors.fill: parent
        onDoubleClicked: {
            console.log("double click on person2")
            parent.doubleClick()
        }
        enabled: mouseAreaEnabled
        //propagateComposedEvents: true
    }

    //property alias lowerMouseArea: lowerMouseArea
    property bool lowerMouseAreaEnabled: false
    signal lowerMouseAreaDoubleClick()
    MouseArea {
        id: lowerMouseArea
        width: parent.width
        height: parent.height*0.5
        anchors.bottom: parent.bottom
        enabled: lowerMouseAreaEnabled
        onDoubleClicked: lowerMouseAreaDoubleClick()
    }
}
