import QtQuick 2.3
import "level03"

Item {
    id: root

    //property real destroyableRows: 5
    //property real rows: 20
    property real cols: 1
    property real sizeOfHero: 35*mm

    //rockswall
    property int numOfRocksSizeOfHero: Math.ceil(sizeOfHero/rocksHeight)
    property int numOfRocksDestroyable: numOfRocksSizeOfHero * cols
    property int numOfVerticalRocksWall: numOfRocksLeftRight - numOfRocksSizeOfHero -1
    property int numOfRocksWall: numOfVerticalRocksWall * cols//numOfRocksLeftRight * cols//(rows-1) * cols

    //property real numOfRocksWall: (rows-destroyableRows) * cols //todo: check this value
    //property real numOfDestroyableRocksWall: (destroyableRows) * cols //todo: check this value

    property real rocksWidth: rocksHeight //rockWall.width / cols
    property real rocksHeight: 10*mm//mainRoot.height/rows

    //ground rocks
    property int numOfRocksGround: Math.ceil(root.width / rocksWidth)
    property int numOfRocksLeftRight: Math.ceil(root.height / rocksHeight)

    property alias rockWallPositionX: rockWall.x
    property alias wallRocksModel: wallRocksModel


    /*Component.onCompleted: {
        console.log("numOfRocksSizeOfHero: " + numOfRocksSizeOfHero)
        console.log("numOfRocksDestroyable: " + numOfRocksDestroyable)
        console.log("numOfVerticalRocksWall: " + numOfVerticalRocksWall)
        console.log("numOfRocksWall: " + numOfRocksWall)
    }*/

    ListModel {
        id: wallRocksModel //wall of rocks on the top center
    }

    ListModel {
        id: wallDestroyableRocksModel //wall of destroyable rocks on the top center
    }

    ListModel {
        id: groundRocksModel //rocks on the floor
    }

    ListModel {
        id: leftRocksModel //rocks at the left of the level
    }

    ListModel {
        id: rightRocksModel //rocks at the right of the level
    }

    //ground rocks
    ListView {
        id: rockGround
        width: root.width
        height: rocksHeight
        anchors.bottom: parent.bottom
        orientation: ListView.Horizontal
        //layoutDirection: Qt.RightToLeft
        model: groundRocksModel
        delegate: rockDelegate
    }

    //rocks at the left
    ListView {
        id: rocksLeft
        width: rocksWidth
        height: root.height
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        model: leftRocksModel
        delegate: rockDelegate
    }

    //rocks at the right
    ListView {
        id: rocksRight
        width: rocksWidth
        height: root.height
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        model: rightRocksModel
        delegate: rockDelegate
    }


    //top wall of rocks in the "top" of the level, undestroyable rocks
    GridView {
        id: rockWall
        width: rocksWidth * cols //mainRoot.width*0.2 // ->1/5
        height: rocksHeight * numOfVerticalRocksWall// to change   //(rows-destroyableRows) //mainRoot.height - rocksHeight
        x: mainRoot.width*0.6 // -> 3/5
        anchors.top: parent.top
        interactive: false
        //verticalLayoutDirection: GridView.BottomToTop
        cellWidth: rocksWidth
        cellHeight: rocksHeight
        model: wallRocksModel
        delegate: rockDelegate
    }

    Component {
        id: rockDelegate
        Item {
            width: rocksWidth; height: rocksHeight
            Image {
                id: img
                anchors.fill: parent
                anchors.margins: -defaultMargins
                source: "qrc:/images/rocks0"+ obj +".png"
                mipmap: true
            }
        }//delegate
    }

    function createDestroyableRocks() {
        //wall of destroyable rocks
        var i;
        var j;
        var num;
        var rockDestroyable;

        var startX = mainRoot.width*4 + mainRoot.width*0.6-defaultMargins
        var startY = mainRoot.height + rockWall.height

        for (i=0;i<cols;i++) {
            for (j=0;j<numOfRocksSizeOfHero;j++) {
                rockDestroyable = lib.createItem("Rock.qml", parent)
                num = lib.getRandom(1, 5)
                rockDestroyable.rockNumber = num.toString();
                rockDestroyable.type = "rock"

                rockDestroyable.addToEnemiesList.connect( activeEnemiesList.addEnemy )
                rockDestroyable.removeFromEnemiesList.connect( activeEnemiesList.removeEnemy )
                rockDestroyable.active = true//active objects are added to the enemiesList
                rockDestroyable.width = 10*mm+defaultMargins*2
                rockDestroyable.x = startX + i*rocksWidth
                rockDestroyable.y = startY + j*rocksHeight
            }
        }
        console.log("going to show rocks add to enemies list...............................................................")
        activeEnemiesList.show()
    } //createDestroyableRocks

    /*function removeAllDestroyableRocks() {
        activeEnemiesList.clearEnemies()
    }*/

    function addRocks() {
        var num;
        var i;
        //wall of rocks
        for (i=0;i < numOfRocksWall;i++) {
        //for (i=0;i < 0;i++) {
            num = lib.getRandom(1, 5)
            wallRocksModel.append({"obj": num})
        }




        //ground rocks
        for (i=0;i < numOfRocksGround;i++) {
            num = lib.getRandom(1, 5)
            groundRocksModel.append({"obj": num})
        }

        //vertical rocks at the left
        console.log("numOfRocksLeftRight: "+numOfRocksLeftRight-1)
        for (i=0;i < numOfRocksLeftRight;i++) {
            num = lib.getRandom(1, 5)
            leftRocksModel.append({"obj": num})
        }

        //vertical rocks at the right
        for (i=0;i < numOfRocksLeftRight;i++) {
            num = lib.getRandom(1, 5)
            rightRocksModel.append({"obj": num})
        }
    } //addRocks

    function removeRocks() {
        wallRocksModel.clear()
        groundRocksModel.clear()
        leftRocksModel.clear()
        rightRocksModel.clear()
    }
}
