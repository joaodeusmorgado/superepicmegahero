import QtQuick 2.3
//import com.Timer 1.0

Item {
    id: enemyRed
    width: 40*mm
    height: width

    y: yPos - yMovement + yMovement * Math.cos(Math.PI*t/2+Math.PI/2*teta)
    property real yPos
    property real teta: 0
    property real yMovement: mainRoot.height*0.3

    property bool inMotion: false
    property real t: 0
    property real dt: 0.04
    //property var enemyTarget
    property bool alive: true
    property int life: 2
    property real hitDamage: 1

    signal scoreUp(real score) //when the enemy is destroyed, you get to score
    signal imHit()
    signal remove()
    signal destroyItem()

    onInMotionChanged: {

        if (inMotion)
            activeEnemiesList.append({"obj": enemyRed})
        //else
          //  enemyRed.destroy()
    }



    onImHit: {
        life -= hitDamage;
        if (life === 0) {
            scoreUp(500)

            //console.log("enemiesModel.count before destroy:"+activeEnemiesList.count)
            destroyItem()
            console.log("enemy killed, size of list is "+activeEnemiesList.count)
            return true; //destroyed
        }
        else {
            return false; //still alive
        }
    }

    onDestroyItem: {
        //console.log("EnemyRedBall destroy")
        alive = false
        inMotion = false
        remove()
        enemyRed.destroy()
    }


    Image {
        id: explosionImg
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/EnemyRedBall.png"
        visible: true
        mipmap: true
    }

    Timer {
        id: timerMovement
        interval: fps_ms
        running: inMotion
        repeat: true
        onTriggered: {
            t += dt;
        }
    }

}
