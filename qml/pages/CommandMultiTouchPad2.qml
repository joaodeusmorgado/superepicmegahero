import QtQuick 2.0
//import com.Timer 1.0

Item {
    id: root
    width: mainRoot.width
    height: mainRoot.height


    //property real value: slider

    property real velocity: 16
    property real sliderValued: 1
    property bool isFlying: false

    property real touchPadCenterX: touchPad.x + touchPad.width/2
    property real touchPadCenterY: touchPad.y + touchPad.height/2

    signal press()
    signal release()
    signal move(real cx, real cy)// not used anymore, it was used for moving the spaceship with the touchpad
    signal shotFired()
    signal shotFiredXY(real cx, real cy)
    signal direction(int direct)// -1->left ; 1->right
    signal jump()
    signal startFlying()
    signal stopFlying()
    signal fly()
    signal startWalking()
    signal stopWalking()
    signal walk()

    onStartFlying: isFlying = true
    onStopFlying: isFlying = false

    property real radiusLower: touchPad.width*0.25//only above this value the touch press will make action
    property alias touchPad: touchPad
    TouchPad2 {
        id: touchPad
        //anchors.centerIn: parent
        //anchors.left: parent.left
        //anchors.bottom: parent.bottom
        anchors.margins: defaultMargins * 4
        visible: point1.pressed
    }

    MultiPointTouchArea {
        anchors.fill: parent
        //mouseEnabled: false
        touchPoints: [
            TouchPoint {
                id: point1
                onPressedChanged: {
                    if (pressed) {
                        touchPad.centerX = x
                        touchPad.centerY = y
                        touchPressed(x, y)
                    }
                    else
                        touchReleased()
                }
                //onXChanged: console.log("point x1 changed:"+x)
                //onYChanged: console.log("point y1 changed:"+y)
            },
            TouchPoint { id: point2
                onPressedChanged: {
                    if (pressed)
                        touchPressed(x, y)
                    else
                        touchReleased()
                }
            }
        ]
    }


    Timer {
        id: timerMultiTouchPad
        interval: fps_ms
        running: point1.pressed
        repeat: true
        onTriggered: {
            _move()

            if (isFlying)
                fly()
        }
    }

    function touchPressed(xx, yy) {
        if (isTouchInsideTouchPad(xx, yy)) {
            //console.log("start walking..................................")
            //startWalking()
        }
        else {
            shotFiredXY(xx, yy)
            startFlying()
        }
    }

    function touchReleased() {
        stopWalking()
        stopFlying()
        /*if (isTouchInsideTouchPad(xx, yy)) {
            //console.log("stop walking..................................")
            stopWalking()
        }
        else {
            stopFlying()
        }*/
    }

    function isTouchInsideTouchPad(xx, yy) {
        if ( xx >= touchPad.x && xx <= touchPad.x+touchPad.width &&
                yy >= touchPad.y && yy <= touchPad.y+touchPad.height )
            return true;
        else
            return false;
    }

    function _move() {

        var xx = (point1.x-touchPadCenterX)
        var yy = (-(point1.y-touchPadCenterY))

        console.log("xx: "+xx)
        console.log("yy: "+yy)

        var radius = Math.sqrt(xx*xx+yy*yy)
        if (radius < radiusLower) {
            stopWalking() //todo: improve this so it will only be fired once after a startWalking()
            return;
        }

        startWalking() //todo: improve this so it will only be fired once after a stopWalking()

        var angle = Math.atan2(yy, xx) * 180 / Math.PI;
        //var angleABS = Math.abs(angle);
        //console.log("angle: "+angle)

        var angle1 = 30
        var angle2 = 180-angle1

        if (angle > angle1 && angle < angle2) {
            jump()
        }

        // move, right now it's not being used, it was for spaceship, but we replace it with keyboard and sensors command
        move(xx * velocity / touchPad.width, yy * velocity / touchPad.height)
        walk() // walk the hero

        if (xx >= 0)
            direction(1)
        else
            direction(-1)
    }

}
