import QtQuick 2.3
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "brown"

    property real itemWidth: mygrid.width / 3 - defaultMargins
    property real itemHeight: mygrid.height / 3 - defaultMargins

    property real hitPoints: 0
    property real failledHits: 0
    property real hitScore: 1
    property int showNumber: -1
    property bool wasHit: false
    property int levelDuration: 30//seconds

    signal menu()
    signal nextLevel()
    signal levelFailled()
    signal levelWin()


    ListModel {
        id: listModel
        ListElement {
            pic: "gopher01"
        }
        ListElement {
            pic: "gopher02"
        }
        ListElement {
            pic: "gopher03"
        }
        ListElement {
            pic: "gopher04"
        }
        ListElement {
            pic: "gopher05"
        }
        ListElement {
            pic: "gopher06"
        }
        ListElement {
            pic: "gopher07"
        }
        ListElement {
            pic: "gopher08"
        }
        ListElement {
            pic: "gopher09"
        }
    }

    GridView {
        id: mygrid
        width: root.width
        height: root.height - scoreBoard.height - defaultMargins * 2
        anchors.bottom: parent.bottom
        interactive: false

        cellWidth: itemWidth
        cellHeight: itemHeight

        model: listModel
        delegate: Item {
            width: itemWidth; height: itemHeight
            Image {
                id: gopherHole
                width: itemWidth; height: itemHeight
                fillMode: Image.Stretch
                source: "qrc:/images/gopherHole.png"
                mipmap: true
            }

            Image {
                id: img
                width: itemWidth; height: itemHeight
                visible: (showNumber === index /*&& wasHit === false*/)
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/"+ pic +".png"
                mipmap: true
            }
            Image {
                id: imgHited
                width: itemWidth; height: itemHeight
                visible: (showNumber === index && wasHit === true)
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/Explosion.png"
                mipmap: true
            }

            MouseArea{
                anchors.fill: parent
                onPressed: {
                    //console.log("I'm clicking......")
                    if (showNumber === index) {
                        wasHit = true
                        hitPoints += hitScore
                    }
                }
            }//MouseArea
        }//delegate
    }

    ScoreBoard {
        id: scoreBoard
        width: 48*mm
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: defaultMargins
        color: "yellow"
        label: qsTr("Hits")
        score: hitPoints
    }

    ScoreBoard {
        id: failledHitsBoard
        width: 48*mm
        anchors.top: parent.top
        anchors.right: scoreBoard.left
        anchors.margins: defaultMargins
        color: scoreBoard.color
        label: qsTr("Fails")
        score: failledHits
    }

    ScoreBoard {
        id:timeRemaining
        width: 48*mm
        anchors.top: parent.top
        anchors.right: failledHitsBoard.left
        anchors.margins: defaultMargins
        color: scoreBoard.color
        label: qsTr("Time")
        score: timerCount
    }

    MenuPopUpVertical {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: root.menu()
        onBook: heroMap.visible = true
    }


    PanelEndOfLevel2 {
        id: panelEOL
        visible: false
        anchors.centerIn: parent
        showNextBtn: levels.levelUnlocked > 4
        onMenu: root.menu()
        onReplay: initLevel()
        onNext: root.nextLevel()
    }


    property int timerCount: 0
    onVisibleChanged: {
        if (visible) {
            initLevel()
        }
        else
            timerCount = 0
    }


    Timer {
        interval: 800;
        running: timerCount > 0; repeat: true
        onTriggered: {
            timerCount--;

            if (wasHit === false)
                failledHits++ ;
            else
                wasHit = false

            showNumber = lib.getRandom(0, 8)
            console.log("showNumber"+showNumber)
            console.log("timerCount: "+timerCount)

            if (timerCount <= 0) {
                endOfLevel()
            }
        }
    }

    function initLevel() {
        timerCount = levelDuration
        panelEOL.visible = false
        hitPoints = 0
        failledHits = 0
        showNumber = -1
        wasHit = false
    }

    function endOfLevel(){
        wasHit = false
        showNumber = -2
        //todo:
        if ( hitPoints * 0.7 > failledHits)
            levelWin()
        else
            levelFailled()
    }

    onLevelWin: {
        levels.levelUnlocked = levels.levelUnlocked === 4 ? 5 : levels.levelUnlocked
        panelEOL.text = qsTr("Victory!")
        //panelEOL.showNextBtn = true
        panelEOL.visible = true
    }

    onLevelFailled: {
        panelEOL.text = qsTr("Too many failled hits.\nYou lost.")
        //panelEOL.showNextBtn = false
        panelEOL.visible = true
    }

}
