import QtQuick 2.0
import "../components"
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    signal close()

    ConversationBoard2 {
        id: talkBoard
        anchors.top: parent.top
        anchors.bottom: btnClose.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        width: parent.width - btnClose.width*2 - 4*defaultMargins

        imageLeft: "asteroidSnorlex.png"
        imageRight: "robonoid.png"
        imageBottom: "asteroidSnorlex.png"

        imageBottomWidth: 50*mm
        imageBottomHeight: 50*mm

        visible: false

        onEndDialog: close()//btnClose.text = qsTr("Ok")
    }

    MyButton {
        id: btnClose
        //width: btnSize * 1.25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onBtnClick: close()
        text: qsTr("Skip")
    }

    onVisibleChanged: {
        if (visible) {
            addTalk()
            talkBoard.visible = true
        }
    }

    function addTalk() {
        talkBoard.clear()
        talkBoard.addMessage5(qsTr("First part of the planet was explored sucessfully. "
                                   + "Now there's strange creatures popping out of the ground, like gophers. "
                                    + "Your mission is to smack those creatures. Go!!!")
                                   , "", -1, false, "gopher01.png")
    }

    /*signal buttonOk()


    Flickable {
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: parent.height //- btnOkCancel.height
        contentWidth: parent.width
        contentHeight: helpColumn.height + defaultMargins * 2

        Column {
            id: helpColumn
            spacing: defaultMargins
            anchors.top: parent.top
            anchors.topMargin: defaultMargins
            anchors.left: parent.left
            anchors.leftMargin: defaultMargins
            anchors.right: parent.right
            anchors.rightMargin: defaultMargins

            ImageFrame {
                id: imag1
                width: 20 * mm//root.width * 0.2
                source: "qrc:/images/gopher01.png"
            }

            TextTemplate {
                id: aboutText
                width: parent.width - 2 * defaultMargins
                //anchors.top: imag1.bottom
                anchors.left: parent.left
                anchors.margins: defaultMargins
                bold: false
                font: "Purisa"
                //pixelSize: 18//8*mm

                //property string messagePT: "Chegando a um planeta estranho, o nosso herói descobre estranhas criaturas que aparecem vindas do chão, tal como toupeiras.\n"
                //+"A sua missão é esmagar estas criaturas e ganhar moedas."

                label: qsTr("First part of the planet was explored sucessfully."
                            + "\nNow there's strange creatures popping out of the ground, like gophers.\n"
                            + "Your mission is to smack those creatures. Go!!!")
            }
        }
    }

    signal close()
    MouseArea {
        anchors.fill: parent
        onClicked: close()
    }*/
}
