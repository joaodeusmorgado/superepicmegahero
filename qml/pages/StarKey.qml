import QtQuick 2.3

Item {
    id: starkey
    width: 25*mm
    height: width*215/345

    property alias mirror: imgStarKey.mirror

    Image {
        id: imgStarKey
        width: parent.width
        height: parent.height
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/keyStar.png"
        mipmap: true
    }
}
