import QtQuick 2.3
import "components"

Rectangle {
    id: map
    width: mainRoot.width
    height: mainRoot.height
    radius: 5*mm

    border.width: 2*mm
    border.color: "grey"
    color: "Gold"

    property int currentIndex: -1
    property string currentItemID: ""

    property real opaqueDisable: 0.6

    property string activeOption: ""
    property int fuel: 0

    property int money: 0
    property bool moneyUnlocked: releaseMode ? false : true //money > 0
    //property string moneyStr: money.toString()

    property int blueDiamonds: 0
    property string blueDiamondsStr: blueDiamonds.toString()

    property bool milk: releaseMode ? false : true
    property bool fruit: releaseMode ? false : true

    property bool swordUnlocked: releaseMode ? false : true
    property bool swordSelected: currentItemID === "sword"

    property bool bowAndArrowUnlocked: releaseMode ? false : true
    property bool bowAndArrowSelected: currentItemID === "bowAndArrows"

    property bool starKeyUnlocked: releaseMode ? false : true
    property bool starKeySelected: currentItemID === "starKey"

    property bool carrotsUnlocked: releaseMode ? false : true
    property bool carrotsSelected: currentItemID === "carrots"

    property bool waterPowderUnlocked: releaseMode ? false : true
    property bool waterPowderSelected: currentItemID === "waterPowder"

    property bool treasureBlueDiamondsUnlocked: releaseMode ? false : true
    property bool treasureBlueDiamondsSelected: currentItemID === "treasureBlueDiamonds"

    property bool bookOfHapinessUnlocked: releaseMode ? false : true
    property bool bookOfHapinessSelected: currentItemID === "bookOfHapiness"


    property bool sword: releaseMode ? false : true
    property bool fuelCan: releaseMode ? false : true


    signal addFuel(real fuel_)
    signal addBlueDiamonds(real blueDiamonds_)

    signal showSword()
    signal hideSword()

    signal showBowAndArrow()
    signal hideBowAndArrow()

    signal showStarKey()
    signal hideStarKey()

    signal showCarrots()
    signal hideCarrots()

    signal showBlueDiamonds()
    signal hideBlueDiamonds()


    onSwordSelectedChanged: swordSelected ? showSword() : hideSword()
    onBowAndArrowSelectedChanged: bowAndArrowSelected ? showBowAndArrow() : hideBowAndArrow()
    onStarKeySelectedChanged: starKeySelected ? showStarKey() : hideStarKey()
    onCarrotsSelectedChanged: carrotsSelected ? showCarrots() : hideCarrots()
    onTreasureBlueDiamondsSelectedChanged: treasureBlueDiamondsSelected ? showBlueDiamonds() : hideBlueDiamonds()

    onMilkChanged: {
        //console.log("you catched milk")
        var index = getModelID("milk")
        //index = 1
        //console.log("onMilkChanged: "+index)
        if (index === -1) return;
        if (milk)
            mapModel.set(index, {"imageName": "milk.png", "label": "milk", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onMoneyUnlockedChanged: {
        var index = getModelID("coins")
        if (index === -1) return;
        if (moneyUnlocked)
            mapModel.set(index, {"imageName": "coin.png", "label": "fuelCan", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onFuelCanChanged: {
        var index = getModelID("fuelCan")
        if (index === -1) return;
        if (fuelCan)
            mapModel.set(index, {"imageName": "fuelCan.png", "label": "fuelCan", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onFruitChanged: {
        var index = getModelID("fruit")
        if (index === -1) return;
        if (fruit)
            mapModel.set(index, {"imageName": "fruit.png", "label": "fruit", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onSwordUnlockedChanged: {
        var index = getModelID("sword")
        if (index === -1) return;
        if (swordUnlocked)
            mapModel.set(index, {"imageName": "sword.png", "label": "sword", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onBowAndArrowUnlockedChanged: {
        var index = getModelID("bowAndArrows")
        if (index === -1) return;
        if (bowAndArrowUnlocked)
            mapModel.set(index, {"imageName": "bowArrow.png", "label": "bow", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onStarKeyUnlockedChanged: {
        var index = getModelID("starKey")
        if (index === -1) return;
        if (starKeyUnlocked)
            mapModel.set(index, {"imageName": "keyStar.png", "label": "StarKey", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onCarrotsUnlockedChanged: {
        var index = getModelID("carrots")
        if (index === -1) return;
        if (carrotsUnlocked)
            mapModel.set(index, {"imageName": "carrot.png", "label": "Carrots", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onWaterPowderUnlockedChanged: {
        var index = getModelID("waterPowder")
        if (index === -1) return;
        if (waterPowderUnlocked)
            mapModel.set(index, {"imageName": "waterPowder.png", "label": "WaterP", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onTreasureBlueDiamondsUnlockedChanged: {
        var index = getModelID("treasureBlueDiamonds")
        if (index === -1) return;
        if (treasureBlueDiamondsUnlocked)
            mapModel.set(index, {"imageName": "diamondArk.png", "label": "Diamonds", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onBookOfHapinessUnlockedChanged: {
        var index = getModelID("bookOfHapiness")
        if (index === -1) return;
        if (bookOfHapinessUnlocked)
            mapModel.set(index, {"imageName": "bookOfHappiness.png", "label": "Book", "opaque": 1})
        else
            mapModel.set(index, {"imageName": "questionMark.png", "label": "", "opaque": opaqueDisable})
    }

    onAddFuel: {
        if ( (fuel + fuel_) < 0)
            return false;
        fuel += fuel_

        if (fuel > 50)//limit fuel to 50
            fuel = 50

        var index = getModelID("fuelCan")
        if (index === -1) return;
        mapModel.set(index, {"label": fuel.toString()})
        return true;
    }

    onAddBlueDiamonds: {
        if ( (blueDiamonds + blueDiamonds_) < 0)
            return false;
        blueDiamonds += blueDiamonds_
        var index = getModelID("treasureBlueDiamonds")
        if (index === -1) return;
        mapModel.set(index, {"label": blueDiamonds.toString()})
        return true;
    }

    Component.onCompleted: {
        var index
        if (fuelCan){
            index = getModelID("fuelCan")
            if (index !== -1)
                mapModel.set(index, {"label": fuel.toString()})
        }

        if (moneyUnlocked){
            index = getModelID("coins")
            if (index !== -1)
                mapModel.set(index, {"label": money.toString()})
        }

    }

    GridView {
        id: grid
         anchors.fill: parent
         anchors.margins: defaultMargins * 2
         cellWidth: 38*mm
         cellHeight: 32*mm

         model: mapModel
         delegate: m_delegate

        // highlight: Rectangle { color: "lightsteelblue"; radius: 4*mm }
     }
    MyButton{
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: defaultMargins*2
        onBtnClick: map.visible = false
        text: "Ok"
    }

    Component {
        id: m_delegate

        Item {
            width: grid.cellWidth
            height: grid.cellHeight

            Rectangle {
                id: rect
                width: parent.width * 0.8
                height: parent.height * 0.8
                radius: width*0.25
                anchors.centerIn: parent
                opacity: opaque

                color: index === currentIndex ? "Chocolate" : "lightsteelblue"
                border.width: 2*mm
                border.color: index === currentIndex ? "darkgrey" : "grey"

                Item {
                    anchors.fill: parent
                    anchors.margins: defaultMargins

                    Item {
                        id: imagePlaceHolder
                        anchors.top: parent.top
                        width: parent.width
                        height: parent.height * 0.6

                        Image {
                            anchors.fill: parent
                            anchors.horizontalCenter: parent.horizontalCenter
                            fillMode: Image.PreserveAspectFit
                            source: "qrc:/images/"+ imageName
                            mipmap: true
                        }
                    }

                    Item {
                        id: textPlaceHolder
                        anchors.bottom: parent.bottom
                        width: parent.width
                        height: parent.height * 0.4

                        Text {
                            id: tex
                            anchors.horizontalCenter: parent.horizontalCenter
                            horizontalAlignment: Text.AlignHCenter
                            font.bold: index === currentIndex ? true : false
                            text: label
                        }
                    }
                }//Item

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        currentIndex === index ? currentIndex = -1 : currentIndex = index
                        if (currentItemID === itemID) //is is already selected
                            currentItemID = "" //unselect it
                        else
                            currentItemID = itemID //select it
                    }
                }
            }//Rectangle
        }//Item delegate
    }


    ListModel {
        id: mapModel

        ListElement { //index 0
            imageName: "questionMark.png"
            label: ""
            itemID: "coins"
            opaque: 0.6
            mipmap: true
        }
        ListElement { //index 1
            imageName: "questionMark.png"
            label: ""
            itemID: "milk"
            opaque: 0.6
            mipmap: true
        }
        ListElement { //index 2
            imageName: "questionMark.png"
            label: ""
            itemID: "fuelCan"
            opaque: 0.6
        }
        ListElement { //index 3
            imageName: "questionMark.png"
            label: ""
            itemID: "fruit"
            opaque: 0.6
        }
        ListElement { //index 4
            imageName: "questionMark.png"
            label: ""
            itemID: "bowAndArrows"
            opaque: 0.6
        }
        ListElement { //index 5
            imageName: "questionMark.png"
            label: ""
            itemID: "starKey"
            opaque: 0.6
        }
        ListElement { //index 6
            imageName: "questionMark.png"
            label: ""
            itemID: "sword"
            opaque: 0.6
        }
        ListElement { //index 7
            imageName: "questionMark.png"
            label: ""
            itemID: "carrots"
            opaque: 0.6
        }
        ListElement { //index 8
            imageName: "questionMark.png"
            label: ""
            itemID: "waterPowder"
            opaque: 0.6
        }
        ListElement { //index 9
            imageName: "questionMark.png"
            label: ""
            itemID: "treasureBlueDiamonds"
            opaque: 0.6
        }
        ListElement { //index 10
            imageName: "questionMark.png"
            label: ""
            itemID: "bookOfHapiness"
            opaque: 0.6
        }
    }

    function getModelID(str) {
        for (var i=0;i<mapModel.count;i++) {
            //console.log("getModelID "+i+" : "+mapModel.get(i).itemID + " : "+ str)
            if (mapModel.get(i).itemID === str)
                return i;
        }
        return -1;
    }

    function setMilk(){
        milk = true
    }

    function setFuelCan(){
        fuelCan = true
    }

    function checkTotalMoney(){
        return money;
    }

    function buyItem(money_){
        console.log("buyItem")
        if (money_ <= money) {
            money -= money_            
            var index = getModelID("coins")
            if (index === -1) return;
            mapModel.set(index, {"label": money.toString()})
            return true;
        }
        else {
            //not enought money to pay, return false
            return false;
        }
    }

    //signal addMoney(real money_)
    //onAddMoney: {
    function addMoney(money_) {
        moneyUnlocked = true
        if ( (money + money_) < 0)
            return false;
        money += money_

        var index = getModelID("coins")
        if (index === -1) return;
        mapModel.set(index, {"label": money.toString()})
        return true;
    }

    function checkTotalFuel(){
        return fuel;
    }

    function removeFuel(fuel_){
        if (fuel_ <= fuel) {
            fuel -= fuel_

            var index = getModelID("fuelCan")
            if (index === -1) return;
            mapModel.set(index, {"label": fuel.toString()})

            return true;
        }
        else {
            //not enought fuel
            return false;
        }
    }

}
