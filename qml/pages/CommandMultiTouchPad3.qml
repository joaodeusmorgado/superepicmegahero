import QtQuick 2.0
//import com.Timer 1.0

Item {
    id: root
    width: mainRoot.width
    height: mainRoot.height


    //property real value: slider

    property real velocity: 16
    property real sliderValued: 1
    property bool isFlying: false

    property real touchPadCenterX: touchPad.x + touchPad.width/2
    property real touchPadCenterY: touchPad.y + touchPad.height/2

    property bool fireOnReleaseP1: false
    property bool showCommand: false
    property bool isSwordDefense: false
    property real pressedX1
    property real pressedY1

    //onShowCommandChanged: console.log("showCommand: "+showCommand)

    signal press()
    signal release()
    signal move(real cx, real cy)// not used anymore, it was used for moving the spaceship with the touchpad
    signal shotFired()
    signal shotFiredXY(real cx, real cy) // used for arrow shots, sword attack, truck shots, ...
    signal direction(int direct)// -1->left ; 1->right
    signal jump()
    signal swordDefenseStart()
    signal swordDefenseStop()
    signal swordAttack()
    signal startFlying()
    signal stopFlying()
    signal fly()
    signal startWalking()
    signal stopWalking()
    signal walk()

    onStartFlying: isFlying = true
    onStopFlying: isFlying = false

    onSwordDefenseStart: isSwordDefense = true
    onSwordDefenseStop: isSwordDefense = false

    onShotFiredXY: swordAttack()

    property real radiusLower: touchPad.width*0.25//only above this value the touch press will make action
    property alias touchPad: touchPad
    TouchPad2 {
        id: touchPad
        anchors.margins: defaultMargins * 4
        visible: showCommand
    }

    MultiPointTouchArea {
        anchors.fill: parent
        //mouseEnabled: false
        touchPoints: [
            TouchPoint {
                id: point1
                onPressedChanged: {
                    if (pressed) { //pressed
                        //touch1Pressed(x, y)
                        touchPad.centerX = x
                        touchPad.centerY = y
                        showCommand = false
                        fireOnReleaseP1 = true
                        console.log("pressed XY: " +x +" : " + y)
                    }
                    else { //released
                        stopWalking()
                        showCommand = false

                        if (lib.distance(touchPad.centerX, touchPad.centerY, x, y, radiusLower))
                            shotFiredXY(x, y)
                        //if (fireOnReleaseP1) // doenst work on mobiles with big resolution ???
                            //shotFiredXY(x, y)
                        //touch1Released()
                        if (isSwordDefense) {
                            swordDefenseStop()
                        }
                        console.log("released XY: " +x +" : " + y)
                    }
                }
                onXChanged: {
                    showCommand = true
                    fireOnReleaseP1 = false
                    //console.log("showCommand: "+showCommand)
                    //console.log("point x1 changed:"+x)
                }
                onYChanged: {
                    showCommand = true
                    fireOnReleaseP1 = false
                    //console.log("showCommand: "+showCommand)
                }
            },
            TouchPoint { id: point2
                onPressedChanged: {
                    if (pressed) { //pressed
                        startFlying()
                    }
                    else { //released
                        shotFiredXY(x, y)
                        stopFlying()
                    }
                }
            }
        ]
    }


    Timer {
        id: timerMultiTouchPad
        interval: fps_ms
        running: showCommand// point1.pressed
        repeat: true
        onTriggered: {
            _move()

            if (isFlying)
                fly()
        }
    }

    /*function touch1Pressed(xx, yy) {
        touchPad.centerX = xx
        touchPad.centerY = yy
        fireOnReleaseP1 = true
        showCommand = false
        //console.log("showCommand: "+showCommand)
    }

    function touch1Released() {
        stopWalking()
        showCommand = false
        if (fireOnReleaseP1)
            shotFiredXY(x, y)
    }*/

    function isTouchInsideTouchPad(xx, yy) {
        if ( xx >= touchPad.x && xx <= touchPad.x+touchPad.width &&
                yy >= touchPad.y && yy <= touchPad.y+touchPad.height )
            return true;
        else
            return false;
    }

    function _move() {

        var xx = (point1.x-touchPadCenterX)
        var yy = (-(point1.y-touchPadCenterY))

        //console.log("xx: "+xx)
        //console.log("yy: "+yy)

        var radius = Math.sqrt(xx*xx+yy*yy)
        if (radius < radiusLower) {
            stopWalking() //todo: improve this so it will only be fired once after a startWalking()
            if (isSwordDefense) {
                swordDefenseStop()
            }
            return;
        }



        var angle = Math.atan2(yy, xx) * 180 / Math.PI;
        //var angleABS = Math.abs(angle);
        //console.log("angle: "+angle)


        var angle1SwordDefense = -30
        var angle2SwordDefense = -150  //-180+30

        if (angle > angle2SwordDefense && angle < angle1SwordDefense) {
            if (!isSwordDefense) {
                swordDefenseStart()
            }
            return;
        }



        startWalking() //todo: improve this so it will only be fired once after a stopWalking()

        var angle1 = 30
        var angle2 = 180-angle1

        if (angle > angle1 && angle < angle2) {
            jump()
        }

        // move, right now it's not being used, it was for spaceship, but we replace it with keyboard and sensors command
        move(xx * velocity / touchPad.width, yy * velocity / touchPad.height)
        walk() // walk the hero

        if (xx >= 0)
            direction(1)
        else
            direction(-1)
    }

}
