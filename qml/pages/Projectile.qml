import QtQuick 2.3

ProjectileTemplate {
    id: root

    width: 7*mm
    height: width

    property alias color: projectileRect.color
    property alias border: projectileRect.border

    property string imageName: ""

    Rectangle {
        id: projectileRect
        anchors.fill: parent
        radius: width/2
        color: "grey"
        border.width: 0.5*mm
        visible: imageName === ""
    }

    rotation: angle

    Image {
        id: projectileImg
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        rotation: angle * 57.2957795131
        source: imageName !== "" ? "qrc:/images/"+imageName : ""
        visible: imageName !== ""
        mipmap: true
    }
}
