<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_PT">
<context>
    <name>About</name>
    <message>
        <location filename="../About.qml" line="49"/>
        <source>Super Epic Mega Hero - a hero in search of adventures.</source>
        <translation>Super Épico Mega Herói - Em busca de aventuras.</translation>
    </message>
    <message>
        <location filename="../About.qml" line="62"/>
        <source>If you like this game please buy me a coffe 😊</source>
        <translation>Se gostas deste jogo, podes pagar-me um café 😊</translation>
    </message>
    <message>
        <location filename="../About.qml" line="74"/>
        <source>Developer: João Morgado
Graphics: André Morgado &amp; João Morgado
Version: 1.0.2
Email: joaodeusmorgado@yahoo.com</source>
        <translation>Programador: João Morgado
Gráficos: André Morgado &amp; João Morgado
Versão: 1.0.2
Email: joaodeusmorgado@yahoo.com</translation>
    </message>
    <message>
        <location filename="../About.qml" line="89"/>
        <source>This program was developed using Qt5 opensource version</source>
        <translation>Este programa foi desenvolvido em Qt5 versão opensource</translation>
    </message>
    <message>
        <location filename="../About.qml" line="102"/>
        <source>Source code: </source>
        <translation>Código fonte: </translation>
    </message>
    <message>
        <location filename="../About.qml" line="119"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../About.qml" line="129"/>
        <source>License</source>
        <translation>Licença</translation>
    </message>
</context>
<context>
    <name>ArrowSign</name>
    <message>
        <location filename="../components/ArrowSign.qml" line="54"/>
        <source>Treasure Mountain</source>
        <translation>Montanha do Tesouro</translation>
    </message>
</context>
<context>
    <name>GameMenu</name>
    <message>
        <location filename="../GameMenu.qml" line="175"/>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../GameMenu.qml" line="182"/>
        <source>Settings</source>
        <translation>Definições</translation>
    </message>
    <message>
        <location filename="../GameMenu.qml" line="190"/>
        <source>About</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <location filename="../GameMenu.qml" line="225"/>
        <source>Exit game ?</source>
        <translation>Sair do jogo?</translation>
    </message>
    <message>
        <location filename="../GameMenu.qml" line="227"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../GameMenu.qml" line="228"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
</context>
<context>
    <name>InitialPage</name>
    <message>
        <location filename="../InitialPage.qml" line="48"/>
        <source>Super Epic Mega Hero</source>
        <translation>Super Épico Mega Herói</translation>
    </message>
    <message>
        <location filename="../InitialPage.qml" line="79"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
</context>
<context>
    <name>Level01</name>
    <message>
        <location filename="../level01/Level01.qml" line="24"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
    <message>
        <location filename="../level01/Level01.qml" line="32"/>
        <source>You lost.</source>
        <translation>Perdeste.</translation>
    </message>
</context>
<context>
    <name>Level01Content</name>
    <message>
        <location filename="../level01/Level01Content.qml" line="204"/>
        <source>Energy shield.</source>
        <translation>Escudo de energia</translation>
    </message>
    <message>
        <location filename="../level01/Level01Content.qml" line="223"/>
        <source>Double shots!!!</source>
        <translation>Tiros duplos!!!</translation>
    </message>
    <message>
        <location filename="../level01/Level01Content.qml" line="247"/>
        <source>You win</source>
        <translation>Ganhaste</translation>
    </message>
    <message>
        <location filename="../level01/Level01Content.qml" line="247"/>
        <source>coins.</source>
        <translation>moedas.</translation>
    </message>
    <message>
        <location filename="../level01/Level01Content.qml" line="260"/>
        <source>Delicious milky way cow milk:)</source>
        <translation>Delicioso leite da vaquinha da via láctea :)</translation>
    </message>
</context>
<context>
    <name>Level01Intro</name>
    <message>
        <location filename="../level01/Level01Intro.qml" line="39"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../level01/Level01Intro.qml" line="52"/>
        <source>Our hero, captain Andrew, travels through space, in his spaceship, looking for adventures.</source>
        <translation>O nosso herói, capitão André, viaja pelo espaço, na sua nave, à procura de aventuras.</translation>
    </message>
    <message>
        <location filename="../level01/Level01Intro.qml" line="56"/>
        <source>Control the spaceship by rotating your mobile to up, down, left or right. Click anywhere on the screen fire.
Your phone will be calibrated, put it in a confortable position and click &gt;&gt; to start.
If you have a keyboard, use the arrows key to move, and space key to fire.</source>
        <translation>Controla a nave girando o telemóvel para frente, para trás, esquerda ou direita. Clica em qualquer sitio no écrã para disparar.
Os sensores do telemóvel vão ser calibrados, coloca o telemóvel numa posição confortável e clica no botão &gt;&gt; para começar.
Se estiveres a jogar com um teclado, usa as teclas das setas para mover a nave e a tecla espaço para disparar.
</translation>
    </message>
    <message>
        <location filename="../level01/Level01Intro.qml" line="54"/>
        <source>There are enemies everywhere, they can attack at any time...</source>
        <translation>Há inimigos por toda a parte, podem atacar a qualquer momento ...</translation>
    </message>
</context>
<context>
    <name>Level02</name>
    <message>
        <location filename="../level02/Level02.qml" line="41"/>
        <source>Victory</source>
        <translation>Vitória</translation>
    </message>
    <message>
        <location filename="../level02/Level02.qml" line="210"/>
        <source>Dig</source>
        <translation>Fura</translation>
    </message>
    <message>
        <location filename="../level02/Level02.qml" line="283"/>
        <source>You found the prisioner!!!</source>
        <translation>Encontraste o prisioneiro!!!</translation>
    </message>
    <message>
        <location filename="../level02/Level02.qml" line="338"/>
        <source>Hi, thanks for saving me.</source>
        <translation>Olá, obrigado por me salvares.</translation>
    </message>
    <message>
        <location filename="../level02/Level02.qml" line="341"/>
        <source>As a reward I offer you a fuel can, with 50 liters.</source>
        <translation>Como recompensa ofereço-te uma lata de gasolina com 50 litros.</translation>
    </message>
    <message>
        <location filename="../level02/Level02.qml" line="349"/>
        <source>That will open a book with all the object you find in you journey. Some objects will be automatically used when needed, others you need to click them. Have a nice journey!</source>
        <translation>Esse botão abre o livro com todos os objectos que encontraste na tua jornada. Alguns objectos são usados automaticamente quando precisares, outros precisas de clicar neles. Desejo-te uma boa jornada!</translation>
    </message>
    <message>
        <location filename="../level02/Level02.qml" line="344"/>
        <source>Thanks for the fuel. How do I use it?</source>
        <translation>Obrigado pela gasolina. Como é que a uso ?</translation>
    </message>
    <message>
        <location filename="../level02/Level02.qml" line="346"/>
        <source>You can check your objects clicking on the Book button at top left.</source>
        <translation>Podes ver os objectos clicando no botão Livro em cima à esquerda.</translation>
    </message>
    <message>
        <location filename="../level02/Level02.qml" line="353"/>
        <source>Thank you and goodbye.</source>
        <translation>Obrigado e adeus.</translation>
    </message>
</context>
<context>
    <name>Level02Intro</name>
    <message>
        <location filename="../level02/Level02Intro.qml" line="45"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../level02/Level02Intro.qml" line="59"/>
        <source>Congratulations on finishing the previous level. You&apos;re a real hero 😊
Second level is really easy and you will learn how to use your map.</source>
        <translation>Parabéns por teres terminado o nível anterior. És um verdadeiro herói 😊
O segundo nível é muito fácil e irás aprender a usar o teu mapa.</translation>
    </message>
    <message>
        <location filename="../level02/Level02Intro.qml" line="62"/>
        <source>Your mission, Captain Andrew, is to find a prisoner in the tunnels of an asteroid.
You will need to drill the asteriod and release the prisoner.
Click anywhere on the left or right of the screen to move and on the button to drill.</source>
        <translation>A tua missão, capitão André, é encontrar um prisioneiro nos túneis de um asteróide.
Vais ter que furar o asteróide para libertar o prisioneiro.
Clica em qualquer sítio à esquerda ou direita no ecrã para moveres e clica no botão para furares.</translation>
    </message>
    <message>
        <location filename="../level02/Level02Intro.qml" line="66"/>
        <source>Relax, enjoy and let&apos;s do it!!!</source>
        <translation>Relaxa, diverte-te e vamos a isto!!!</translation>
    </message>
</context>
<context>
    <name>Level03</name>
    <message>
        <location filename="../level03/Level03.qml" line="19"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
</context>
<context>
    <name>Level03Content</name>
    <message>
        <location filename="../level03/Level03Content.qml" line="80"/>
        <source>You lost.</source>
        <translation>Perdeste.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="138"/>
        <source>You found a delicious fruit!!!</source>
        <translation>Encontraste um fruto delicioso!!!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="166"/>
        <location filename="../level03/Level03Content.qml" line="228"/>
        <source>Level failled!!!</source>
        <translation>Perdeste!!!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="407"/>
        <source>I will increase your life power to 5!!!</source>
        <translation>Vou aumentar a tua vida para 5 unidades!!!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="411"/>
        <source>I like to drink a lot of water. Delicious!!!</source>
        <translation>Eu gosto de beber muita água. É deliciosa.!!!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="434"/>
        <source>I will increase your life power to 10!!!</source>
        <translation>Vou aumentar a tua vida para 10 unidades!!!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="438"/>
        <source>I really like sunny days, they&apos;re so good to my petals!</source>
        <translation>Gosto de dias soalheiros, são bons para as minhas pétalas!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="965"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="966"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="967"/>
        <source>A - No, let me keep fighting.
B - Yes, please help me fight.</source>
        <translation>A - Não, eu vou continuar a luta.
B - Sim, por favor, ajuda-me.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1006"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1007"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1149"/>
        <source>Hello. I&apos;m really thirsty. Do you have anything to drink ?</source>
        <translation>Olá. Tenho muita sede. Tens alguma coisa para beber?</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1165"/>
        <source>Too bad, I&apos;m really thirsty. Oh well, bye!</source>
        <translation>Que pena, eu tenho muita sede. Bem, adeus!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1206"/>
        <source>You found a sword!!!</source>
        <translation>Encontraste uma espada!!!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1247"/>
        <source>We&apos;re supposed to have a sword fight now, but lucky for you the the great coder still hasn&apos;t finished the sword fight algorithm for this game.
Go in peace now, we&apos;ll meet in future updates.</source>
        <translation>Agora devíamos ter uma luta de espadas, mas felizmente para ti que o Grande Programador ainda não acabou o algoritmo de luta para este jogo. 
Vai em paz, encontramo-nos numa futura atualização.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1252"/>
        <source>Great coder ? Game ? I don&apos;t understand what you say.
What a crazy robot, I&apos;m out of here.</source>
        <translation>Grande Programador ? Jogo ? Não estou a perceber nada.
Que robô maluco, vou mas é embora daqui.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1274"/>
        <source>Quick, we don&apos;t have much time!Double click on my head and I will take charge of the fight. I will fight without the bow.</source>
        <translation>Depressa, não temos muito tempo! Faz um duplo click na minha cabeça, e eu vou tomar conta da luta. Eu vou lutar sem o arco.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1293"/>
        <source>Oh my God, you fought like a mighty warrior!!!
Who are you ???</source>
        <translation>Ó meu Deus, tu lutaste como uma guerreira poderosa!!!
Quem és tu?</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1296"/>
        <source>From now on, I will take the lead if my head is double clicked.
You, Captain Andrew, will take the lead if your head is double clicked.</source>
        <translation>A partir de agora eu tomo a liderança se a minha cabeça levar um duplo click.
Tu, capitão André, ficas com a liderança se a tua cabeça levar um duplo click.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1153"/>
        <source>Hi. I have milk from the milky way cow. Please have a drink.</source>
        <translation>Olá. Tenho leite da vaquinha da via láctea. Podes beber.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1157"/>
        <source>Hum, delicious. Thank you, please let me offer you this bow and arrows.
Look for it in your book, then to shoot an arrow tap anywhere in the screen.</source>
        <translation>Hum, delicioso. Obrigado, como recompensa, ofereço-te este arco e flechas.
Procura o arco no teu livro, para disparar clica em qualquer sítio no ecrã.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1161"/>
        <source>Sorry, I have nothing to give you.</source>
        <translation>Desculpa, não tenho nada para te oferecer.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1191"/>
        <source>Hi, have a safe journey and be happy!!!</source>
        <translation>Olá, desejo-te uma boa jornada e sê feliz!!!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1199"/>
        <source>You found the starKey!!!</source>
        <translation>Encontraste a chave estrela!!!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1217"/>
        <source>Hi, I&apos;m Louise Pandora. I was kidnapped, can you help me?</source>
        <translation>Olá, chamo-me Luísa Pandora. Fui raptada, podes ajudar-me?</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1220"/>
        <source>Hi, I&apos;m captain Andrew! I don&apos;t know how to open the cage.</source>
        <translation>Olá, sou o capitão André! Não sei como abrir a jaula.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1223"/>
        <source>This cage only opens with a magic star key.</source>
        <comment>girlHead.png</comment>
        <translation>Esta jaula só abre com uma chave mágica de estrela.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1227"/>
        <source>I have a star key, I won it fighting a dragon. Let me try.</source>
        <translation>Tenho uma chave de estrela que ganhei numa luta contra um dragão. Posso tentar usá-la.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1231"/>
        <source>I don&apos;t have a star key.</source>
        <translation>Não tenho nenhuma chave de estrela.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1234"/>
        <source>Please find one and set me free.</source>
        <translation>Por favor encontra uma e liberta-me.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1237"/>
        <source>Ok, I will look for it.</source>
        <translation>Muito bem, vou à procura da chave.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1261"/>
        <source>The arrows don&apos;t do nothing to him, and you&apos;re about to die.
Let me help you fight!!!</source>
        <translation>As flechas não estão a derrotá-lo, e tu estás quase morrer.
Deixa-me ajudar-te na luta!!!</translation>
    </message>
    <message>
        <location filename="../level03/Level03Content.qml" line="1295"/>
        <source>I am Louise Pandora ... and I have unleashed my power.</source>
        <translation>Eu sou Luísa Pandora ... e libertei o meu poder.</translation>
    </message>
</context>
<context>
    <name>Level03Intro</name>
    <message>
        <location filename="../level03/Level03Intro.qml" line="41"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../level03/Level03Intro.qml" line="53"/>
        <source>Captain Andrew arrives to a new planet, it&apos;s time to explore it. Poke around, there&apos;s lots of hidden stuff to be found that can help you when you&apos;re stuck.</source>
        <translation>O capitão André chega a um planeta novo, é altura de explorá-lo. Procura coisas para clicar, há muitos segredos escondidos para descobrir que podem ajudar-te quando estiveres em apuros.</translation>
    </message>
    <message>
        <location filename="../level03/Level03Intro.qml" line="56"/>
        <source>Press anywhere, a move command will appear.
Slide left or right to move, slide up &amp; left or right to jump.
If you&apos;re using a keyboard, you can use the arrows keys to move left, right and jump.</source>
        <translation>Clica no ecrã e aparece um comando.
Desliza o dedo para a esquerda ou direita para andar, desliza para cima e esquerda ou direita para saltar.
Se estiveres a usar um teclado, usa as teclas das setas para andar e saltar.</translation>
    </message>
</context>
<context>
    <name>Level04</name>
    <message>
        <location filename="../level04/Level04.qml" line="115"/>
        <source>Hits</source>
        <translation>Acertaste</translation>
    </message>
    <message>
        <location filename="../level04/Level04.qml" line="126"/>
        <source>Fails</source>
        <translation>Falhaste</translation>
    </message>
    <message>
        <location filename="../level04/Level04.qml" line="137"/>
        <source>Time</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="../level04/Level04.qml" line="213"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
    <message>
        <location filename="../level04/Level04.qml" line="219"/>
        <source>Too many failled hits.
You lost.</source>
        <translation>Muitas pancadas falhadas.
Perdeste.</translation>
    </message>
</context>
<context>
    <name>Level04Intro</name>
    <message>
        <location filename="../level04/Level04Intro.qml" line="39"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../level04/Level04Intro.qml" line="51"/>
        <source>First part of the planet was explored sucessfully. Now there&apos;s strange creatures popping out of the ground, like gophers. Your mission is to smack those creatures. Go!!!</source>
        <translation>Primeira parte do planeta explorada com sucesso. Agora há estranhas criatutas que aparecem no chão, como toupeiras. A tua missão é acertá-las com uma pancada.</translation>
    </message>
</context>
<context>
    <name>Level05Content</name>
    <message>
        <location filename="../level05/Level05Content.qml" line="184"/>
        <source>Hi, I&apos;m hungry, do you have any fruit for me?</source>
        <translation>Olá, tenho fome, tens algum fruto para mim?</translation>
    </message>
    <message>
        <location filename="../level05/Level05Content.qml" line="187"/>
        <source>I have a fruit I caught early, it fall from a tree! Take it!</source>
        <translation>Tenho um fruto que apanhei numa árvore. Ofereço-te!</translation>
    </message>
    <message>
        <location filename="../level05/Level05Content.qml" line="190"/>
        <source>Delicious, thank you.
Did you know people live sad because they can&apos;t comunicate properly?</source>
        <translation>Delicioso, obrigado. Sabias que aqui as pessoas vivem tristes pois não conseguem comunicar com qualidade?</translation>
    </message>
    <message>
        <location filename="../level05/Level05Content.qml" line="193"/>
        <source>We had great phones called Meeg, made in our Konia factory in the midle of the ocean. Untill a mad man set a factory on fire, like a burning plataform and now phones are falling into the ocean because of the explosions and fire.</source>
        <translation>Tínhamos grandes telemóveis Meeg, feitos na nossa fabrica Konia no meio do oceano. Mas veio um homem que pegou fogo à fábrica, ficou como uma plataforma em chamas e agora os telefones estão a cair no oceano por causa do fogo e das explosões.</translation>
    </message>
    <message>
        <location filename="../level05/Level05Content.qml" line="198"/>
        <source>Take my boat and please help us. Try to save the Meeg phones moving the boat to catch them.  Hurry and sail away!!!</source>
        <translation>Leva o meu barco e ajuda-nos. Tenta salvar os telemóveis Meeg movendo o barco para apanhá-los. Depressa, vai velejar!!!</translation>
    </message>
    <message>
        <location filename="../level05/Level05Content.qml" line="202"/>
        <source>Ok, I will sail on!</source>
        <translation>Ok, eu vou velejar!</translation>
    </message>
    <message>
        <location filename="../level05/Level05Content.qml" line="205"/>
        <source>Sorry I have nothing!
Can I sail on your boat?</source>
        <translation>Desculpa, não tenho nada!
Posso velejar no teu barco?</translation>
    </message>
    <message>
        <location filename="../level05/Level05Content.qml" line="206"/>
        <source>No you can not.</source>
        <translation>Não, não podes.</translation>
    </message>
</context>
<context>
    <name>Level05Konia</name>
    <message>
        <location filename="../level05/Level05Konia.qml" line="232"/>
        <source>Catched</source>
        <translation>Apanhados</translation>
    </message>
    <message>
        <location filename="../level05/Level05Konia.qml" line="241"/>
        <source>Missed</source>
        <translation>Perdidos</translation>
    </message>
    <message>
        <location filename="../level05/Level05Konia.qml" line="250"/>
        <source>Time left</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="../level05/Level05Konia.qml" line="346"/>
        <source>Vitory!!!
You caught more than 70% of the Meeg phones!!!</source>
        <translation>Vitória!!!
Apanhaste mais de 70% dos telemóveis Meeg!!!</translation>
    </message>
    <message>
        <location filename="../level05/Level05Konia.qml" line="352"/>
        <source>You loose!
You need to catch more than 70% of phones!</source>
        <translation>Perdeste!
Precisas de apanhar mais de 70%dos telemóveis!</translation>
    </message>
</context>
<context>
    <name>Level05KoniaIntro</name>
    <message>
        <location filename="../level05/Level05KoniaIntro.qml" line="62"/>
        <source>Our hero sails throught the ocean.
Is mission: arrive at the burning Konia factory. Move the boat swiping your finger, try to catch the falling Meeg phones.
Get ready, go!!!</source>
        <translation>O nosso herói navega no oceano.
A sua missão: chegar à plataform em chamas Konia. Move o barco com o dedo, tenta salvar os telefones Meeg. Vai !!!</translation>
    </message>
</context>
<context>
    <name>Level06Content</name>
    <message>
        <location filename="../level06/Level06Content.qml" line="24"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
    <message>
        <location filename="../level06/Level06Content.qml" line="34"/>
        <source>Level failed!</source>
        <translation>Perdeste!</translation>
    </message>
    <message>
        <location filename="../level06/Level06Content.qml" line="472"/>
        <location filename="../level06/Level06Content.qml" line="480"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../level06/Level06Content.qml" line="473"/>
        <source>You&apos;ll need a fuel can to use the truck.
You need to find one!!!</source>
        <translation>Precisas de uma lata de gasolina para usar o veículo.
Tens de encontrar uma!!!</translation>
    </message>
    <message>
        <location filename="../level06/Level06Content.qml" line="481"/>
        <location filename="../level06/Level06Content.qml" line="491"/>
        <source>You need</source>
        <translation>Precisas de</translation>
    </message>
    <message>
        <location filename="../level06/Level06Content.qml" line="481"/>
        <location filename="../level06/Level06Content.qml" line="491"/>
        <source>gallons to travel, and you have</source>
        <translation>litros para viajar, e tens</translation>
    </message>
    <message>
        <location filename="../level06/Level06Content.qml" line="482"/>
        <source>Try to earn some fuel before travelling.</source>
        <translation>Tenta ganhar mais gasolina antes de viajar.</translation>
    </message>
    <message>
        <location filename="../level06/Level06Content.qml" line="492"/>
        <source>Continue ?</source>
        <translation>Continuar ?</translation>
    </message>
    <message>
        <location filename="../level06/Level06Content.qml" line="489"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../level06/Level06Content.qml" line="490"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
</context>
<context>
    <name>Level07</name>
    <message>
        <location filename="../level07/Level07.qml" line="27"/>
        <source>You lost.</source>
        <translation>Perdeste.</translation>
    </message>
    <message>
        <location filename="../level07/Level07.qml" line="34"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
    <message>
        <location filename="../level07/Level07.qml" line="179"/>
        <source>Time left</source>
        <translation>Tempo</translation>
    </message>
</context>
<context>
    <name>Level07Intro</name>
    <message>
        <location filename="../level07/Level07Intro.qml" line="38"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../level07/Level07Intro.qml" line="50"/>
        <source>Captain and Pandora successfully arrived at the end of the desert. They rest for a while before continuing and captain tells Pandora a story when he was travelling in his spaceship and had to cross a tremendous asteroid field.</source>
        <translation>O capitão e Pandora chegaram com sucesso ao fim do deserto. Eles descansam um pouco antes de continuarem e o capitão conta a Pandora uma história sobre quando ele viajava na sua nave e teve de atravessar um tremendo campo de asteróides.</translation>
    </message>
</context>
<context>
    <name>Level08</name>
    <message>
        <location filename="../level08/Level08.qml" line="31"/>
        <source>Total balloons:</source>
        <translation>Total de balões:</translation>
    </message>
    <message>
        <location filename="../level08/Level08.qml" line="32"/>
        <source>Balloons shot:</source>
        <translation>Balões rebentados:</translation>
    </message>
    <message>
        <location filename="../level08/Level08.qml" line="33"/>
        <source>Shot percentagem:</source>
        <translation>Precentagem de arrebentamentos:</translation>
    </message>
    <message>
        <location filename="../level08/Level08.qml" line="61"/>
        <source>You shot more than 80%
Victory!!!</source>
        <translation>Arrebentaste mais de 80%
Vitória!!!</translation>
    </message>
    <message>
        <location filename="../level08/Level08.qml" line="74"/>
        <source>You shot less than 80%
Try again 😟</source>
        <translation>Arrebentaste menos de 80%
Tenta outra vez 😟</translation>
    </message>
    <message>
        <location filename="../level08/Level08.qml" line="220"/>
        <source>Time</source>
        <translation>Duração</translation>
    </message>
</context>
<context>
    <name>Level08Intro</name>
    <message>
        <location filename="../level08/Level08Intro.qml" line="35"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../level08/Level08Intro.qml" line="47"/>
        <source>Way back when our hero was in the military academy, he had to practice ballon shooting with arrows. He had to shoot at least 80% of the ballons to pass the training. Let&apos;s do it.</source>
        <translation>Quando o nosso herói estudava na academia militar, um dos treinos era rebentar balões com flechas. Ele tinha que rebentar mais de 80% dos balões para terminar o treino com sucesso. Vamos a isto!</translation>
    </message>
</context>
<context>
    <name>Level09</name>
    <message>
        <location filename="../level09/Level09.qml" line="20"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
</context>
<context>
    <name>Level09Content</name>
    <message>
        <location filename="../level09/Level09Content.qml" line="88"/>
        <source>You drowned.</source>
        <translation>Afogaste-te.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="270"/>
        <source>Unicorn
Home</source>
        <translation>Lar do Unicórnio</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="313"/>
        <source>Unicorn
forest</source>
        <translation>Floresta do
Unicórnio</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="445"/>
        <source>Treasure
Mountain</source>
        <translation>Montanha
 do Tesouro</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="827"/>
        <source>You lost!!!</source>
        <translation>Perdeste!!!</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1418"/>
        <source>brrrh..., brrrh..., clip clop, clip clop, brrrh...</source>
        <translation>brrrh..., brrrh..., clip clop, clip clop, brrrh...</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1419"/>
        <source>Hi horsy. I don&apos;t understand what you say!</source>
        <translation>Olá cavalinho. Não percebo o que estás a dizer!</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1420"/>
        <source>What a cute horse. I think he&apos;s hungry!!!</source>
        <translation>Que cavalhinho fofo. Acho que ele tem fome!!!</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1432"/>
        <source>How!!! I can talk! Thoose carrots must be magic! Thank you.</source>
        <translation>Uau!!! Eu consigo falar! Essas cenouras devem ser mágicas! Obrigado.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1433"/>
        <source>Can you help us? We&apos;re going to explore the florest.</source>
        <translation>Podes ajudar-nos? Nós vamos explorar a floresta.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1434"/>
        <source>Sure, I will give you a ride.</source>
        <translation>Claro, eu dou-vos boleia.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1435"/>
        <source>Just double click my head to jump in, and double click again to jump off.
Also did you know I can jump very high?</source>
        <translation>Faz um duplo click na minha cabeça para montares, faz outro duplo click para apeares.
Sabias que eu consigo saltar muito alto?</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1437"/>
        <source>But what ever you do, please don&apos;t ever double click my feet.</source>
        <translation>Mas por favor, nunca faças um duplo click nos meus pés.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1446"/>
        <source>I can&apos;t go further, there&apos;s a river ahead. My wife will take you.</source>
        <translation>Não posso ir mais longe, há um rio à frente. A minha esposa leva-vos.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1448"/>
        <source>She can swim ?</source>
        <translation>Ela sabe nadar?</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1449"/>
        <source>No, she can fly. Double click my head to jump off and go talk to her.
Have a safe journey.</source>
        <translation>Não, ela sabe voar. Faz um duplo click na minha cabeça para apeares e vai falar com ela.
Desejo-te uma boa jornada.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1463"/>
        <source>Hi there, I see you meet my husband unicorn Einstein. I&apos;m Mary Mare.</source>
        <translation>Olá, vejo que já conheceram o meu marido unicórnio Einstein. Eu sao a égua Maria.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1464"/>
        <source>Hello I&apos;m captain Andrew.</source>
        <translation>Olá, sou o capitão André.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1465"/>
        <source>Hi, I&apos;m Louise...</source>
        <translation>Olá, sou a Luísa...</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1466"/>
        <source>I know who you are princess Pandora. Welcome!</source>
        <translation>Eu sei quem tu és, princesa Pandora. Bem-vinda!</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1467"/>
        <source>Princess Pandora ???
You didn&apos;t tell me you are a princess!</source>
        <translation>Princesa Pandora ???
Não me tinhas dito que eras uma princesa!</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1468"/>
        <source>You didn&apos;t ask 😊</source>
        <translation>Tu não perguntaste 😊</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1470"/>
        <source>Double click my head to jump aboard, I will take to see the forest. Press and slide anywhere on the screen to walk, then while walking, use another finger to press anywhere to fly.</source>
        <translation>Faz um duplo click na minha cabeça para montares, eu levo-vos a ver a floresta. Pressiona e desliza o dedo no ecrã para eu andar, depois usa outro dedo para clicar em qualquer sítio no ecrã para eu voar.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1476"/>
        <source>If you have a keyboard, press the f key to fly and arrows keys to move.</source>
        <translation>Se tiveres um teclado, usa a tecla F para voar e as teclas das setas da esquerda ou direita para andar.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1489"/>
        <source>You found the blue diamonds treasure.</source>
        <translation>Encontraste o tesouro dos diamantes azuis.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1503"/>
        <source>Hi there, I&apos;m a scientist.
I have invented a future photo machine.</source>
        <translation>Olá, eu sou um cientista.
Inventei uma máquina de mostar fotos do futuro.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1506"/>
        <source>You can peak in the len and see a random photo of you in the future. I saw myself winning the nobel prize 😊
Go ahead and have a look.</source>
        <translation>Podes espreitar na lente e verás uma foto aleatória do teu futuro. Eu vi-me a ganhar o prémio Nobel 😊
Experimenta ver a tua foto.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1511"/>
        <source>Let me try.</source>
        <translation>Vou experimentar.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1516"/>
        <source>Ohhh. I saw some weird picture of a very strange place 😯!!!
A thought came to my mind: &quot;3D world is coming!&quot;
Pandora, now you take a look.</source>
        <translation>Uau. Eu vi uma foto de um mundo muito estranho 😯 !!!
Um pensamento veio-me à mente: &quot;O mundo 3D está a chegar!&quot;
Pandora, experimenta ver a tua foto.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1522"/>
        <source>What 😠 ??? I&apos;m seeing a picture of... never mind, it&apos;s all blurry.
How does this machine work, is this real ?</source>
        <translation>O quê 😠??? Estou a ver uma foto dos... Ah deixa lá, está tudo desfocado.
Como é que esta máquina funciona, estas fotos são reais?</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1526"/>
        <source>Well, I discovered a new type of atomic particles called TimeQuarks wich represent time particles and can be codified with complex math equations based on four dimensional matrices and quaternions. As you know quaternions are represent like a+b*i+c*j+d*k where a, b, c, and d are real numbers, and i, j, and k are the fundamental quaternion units. They are very usefull to represent the 3-sphere or glomes. What my photo time machine really does is to use a sensor that captures photons to compose a normal regular photo, and use another special type of sensor that can capture TimeQuarks that coexist all around us along with the photons. The machine then uses is very powerfull GPUs to process the image using vertex and fragment shaders, while the compute shaders process the TimeQuarks particles using complex algorithms based on the quaternions and matrices, wich are then applyied on the original photo, given us the photo of what will happen in the future. Is not quite accurate because algorithms use numeric variables that can accumulate numeric errors, but it works pretty good. I took a picture of a banana and got a picture of a banana pie, and my mother really used that banana to bake a pie, and it turned out to be a very delicious pie!</source>
        <translation>Bem, eu descobri um novo tipo de partículas atómicas chamadas TimeQuarks que representam partículas temporais e podem ser codificadas com equações matemáticas complexas baseadas em matrizes de 4 dimensões e quatérnios. Como sabes, os quatérnios são representados por a+b*i+c*j+d*k onde a, b, c, e d são números reais e  i, j, e k são as unidades fundamentais dos quatérnios.Eles são muito úteis para representar 3-esferas ou glomes. O que a minha máquina faz é utilizar um sensor que captura fotões, para compor uma foto normal, e usa outro tipo de sensor especial que captura TimeQuarks que coexistem à nossa volta, juntamente com os fotões. Depois, a máquina usa GPU&apos;s de última geração para processar a imagem, utilizando shaders vertex e fragment, enquanto os shaders compute processam as partículas TimeQuarks, utilizando algoritmos avançados, baseados nas matrizes e nos quatérnios, que depois são aplicados na foto original, o que resulta numa foto daquilo que vai acontecer no futuro. Não é muito preciso, por causa dos algoritmos que utilizam variáveis númericas que acumulam erros, mas funciona bastante bem. Eu tirei um foto a uma banana, e vi uma tarte de banana, e na realidade, mais tarde, a minha mão utilizou a banana para cozinhar uma tarte, eu provei a tarte e era uma autêntica delícia.</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1549"/>
        <source>Opps, I just farted 😯</source>
        <translation>Opps, dei uma bufa 😯</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1550"/>
        <source>What 😊 ??? Ahahahah!</source>
        <translation>O quê 😊 ??? Ahahahah!</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1566"/>
        <source>Hi there. I see that you have the book of hapyness. You have finish all your tasks and you can read in the next level. Congratulations 😊!</source>
        <translation>Olá. Vejo que tens o livro da Felicidade contigo. Terminaste todas as tuas tarefas e podes lê-lo no próximo nível. Parabéns 😊!</translation>
    </message>
    <message>
        <location filename="../level09/Level09Content.qml" line="1571"/>
        <source>Hi there. You haven&apos;t finish all your tasks and can not pass the level yet. Keep exploring, brave hero 😟!</source>
        <translation>Olá. Ainda não terminaste todas as tuas tarefas e ainda não podes passar de nível. Continua a explorar, herói corajoso 😟!</translation>
    </message>
</context>
<context>
    <name>Level09Intro</name>
    <message>
        <location filename="../level09/Level09Intro.qml" line="44"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
    <message>
        <location filename="../level09/Level09Intro.qml" line="56"/>
        <source>There are forests with strange magic creatures in this planet.
Keep exploring, brave hero.</source>
        <translation>Há florestas com criaturas estranhas mágicas neste planeta.
Continua a explorar, herói corajoso.</translation>
    </message>
</context>
<context>
    <name>Level0fake</name>
    <message>
        <location filename="../Level0fake.qml" line="17"/>
        <source>Este nivel não existe :)

Jogar muitas horas é cansativo, é melhor fazer uma pausa.

Sugestão: podes ir brincar ao ar livre com os amigos, andar de bicicleta,

ou brincar com papagaio, comer um gelado, ou ler um bom livro.

Diverte-te e sê feliz!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Level10</name>
    <message>
        <location filename="../level10/Level10.qml" line="62"/>
        <source>Happy readings!
Now back to exploring 😊</source>
        <translation>Boas leituras!
Agora volta à exploração.😊</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="79"/>
        <source>The Book of Happiness!
Hints to be happy.</source>
        <translation>O livro da felicidade!
Dicas para ser feliz.</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="84"/>
        <source>Don&apos;t waste to much time playing in your pc, mobile, game console. Make regular breaks, go out, play with your friends, fly a kite in the park.</source>
        <translation>Não percas muito tempo jogando no pc, telemóvel ou consola. Faz pausas regulares e vai passear com amigos, vai ao parque brincar com papagaios de papel.</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="89"/>
        <source>Watch the nature and enjoy 😊</source>
        <translation>Contempla a naturaza e disfruta 😊</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="93"/>
        <source>Try a new flavour of ice cream, that you never tasted before.
If you don&apos;t like it, just smile and be happy, there&apos;s more flavours to try 😊</source>
        <translation>Experimenta um novo sabor de gelado, que nunca tenhas comido. Se não gostares, não faz mal, há mais sabores para experimentar que poderás gostar 😊</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="96"/>
        <source>If you&apos;re stressed go for a swim in the ocean, if you have the chance. Carefull not to drown 😊</source>
        <translation>Se estiveres com stress vai à praia nadar no oceano, se tiveres essa oportunidade. Cuidado para não te afogares 😊</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="98"/>
        <source>There&apos;s hidden secrets in nature. Be curious!</source>
        <translation>Há segredos escondidos na natureza. Sê curioso!</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="100"/>
        <source>If you&apos;re a student, be a warrior to fight the laziness and do your work!
No one can fight your battles for you.</source>
        <translation>Se és estudante, luta como um guerreiro contra a preguiça e estuda! Ninguém pode lutar por ti as tuas batalhas.</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="102"/>
        <source>Eat healthy food... at least once in a while 😊. Yummy.</source>
        <translation>Come comida saudável...pelo menos de vez em quando 😊. Delícia.</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="104"/>
        <source>You can choose: be grumpy or smile.</source>
        <translation>Podes escolher a tua vida: ser um refilão ou ser bem disposto e sorrir.</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="106"/>
        <source>You can learn a lot by reading paper books.</source>
        <translation>Podes aprender muito lendo livros de papel.</translation>
    </message>
    <message>
        <location filename="../level10/Level10.qml" line="108"/>
        <source>Enought advice, now go live your life 😊.
The end.</source>
        <translation>Já chega de conselhos, vai viver a tua vida e sê feliz 😊.
Fim!</translation>
    </message>
</context>
<context>
    <name>Level11</name>
    <message>
        <location filename="../level11/Level11.qml" line="19"/>
        <source>Victory!</source>
        <translation>Vitória!</translation>
    </message>
</context>
<context>
    <name>Level11Content</name>
    <message>
        <location filename="../level11/Level11Content.qml" line="156"/>
        <source>Pandora! Please don&apos;t die!!!</source>
        <translation>Pandora! Não morras!!!</translation>
    </message>
    <message>
        <location filename="../level11/Level11Content.qml" line="157"/>
        <source>To be continued...</source>
        <translation>Continua em futuras atualizações...</translation>
    </message>
    <message>
        <location filename="../level11/Level11Content.qml" line="158"/>
        <source>Pandora is badly wonded 😭
Will she die...?</source>
        <translation>Pandora está gravemente ferida 😭.
Será que vai morrer...?</translation>
    </message>
    <message>
        <location filename="../level11/Level11Content.qml" line="159"/>
        <source>Or will the hero find a forest doctor who will help her healing and then she will continue the adventures with captain Andrew and eventually find a misterious 3D world with more adventures ?</source>
        <translation>Ou será que o nosso herói encontrará um médico da floresta que vai ajudar a curá-la e ela continuará as suas aventures com o capitão André e eventualmente encontrarão um mundo misterioso 3D com mais aventuras?</translation>
    </message>
    <message>
        <location filename="../level11/Level11Content.qml" line="163"/>
        <source>Stay tunned for the following game updates were all will be revealed.
Meanwhile, be happy 😊</source>
        <translation>Nas próximas atualizações tudo será revelado.
Entetanto sê feliz 😊</translation>
    </message>
    <message>
        <location filename="../level11/Level11Content.qml" line="165"/>
        <source>If you have enjoyed this game, please consider donating, buy the author a coffee. It will be greatly appreciated and will keep us motivated to continue developing more Super Epic Mega adventures.</source>
        <translation>Se gostaste deste jogo, podes fazer uma doação, pagando um café ao autor. Será muito apreciado e motivante para continuar a desenvolver mais super épicas mega aventuras.</translation>
    </message>
</context>
<context>
    <name>LevelContentTemplate</name>
    <message>
        <location filename="../components/LevelContentTemplate.qml" line="96"/>
        <source>Level failled!!!</source>
        <translation>Perdeste!!!</translation>
    </message>
</context>
<context>
    <name>LevelSettings</name>
    <message>
        <location filename="../levelSettings/LevelSettings.qml" line="42"/>
        <location filename="../levelSettings/LevelSettings.qml" line="48"/>
        <source>Reset Game</source>
        <translation>Reset ao jogo</translation>
    </message>
    <message>
        <location filename="../levelSettings/LevelSettings.qml" line="107"/>
        <source>Reset game?
All progress will be lost and the game will exit!!!
Are you sure?</source>
        <translation>Reset ao jogo ?
Todo o progresso será perdido e o jogo será terminado!!!
Continuar?</translation>
    </message>
    <message>
        <location filename="../levelSettings/LevelSettings.qml" line="108"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../levelSettings/LevelSettings.qml" line="109"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
</context>
<context>
    <name>LicenseFile</name>
    <message>
        <location filename="../LicenseFile.qml" line="43"/>
        <source>GNU LESSER GENERAL PUBLIC LICENSE
Version 3, 29 June 2007

Copyright © 2007 Free Software Foundation, Inc. &lt;https://fsf.org/&gt;

Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.

This version of the GNU Lesser General Public License incorporates the terms and conditions of version 3 of the GNU General Public License, supplemented by the additional permissions listed below.

0. Additional Definitions.
As used herein, “this License” refers to version 3 of the GNU Lesser General Public License, and the “GNU GPL” refers to version 3 of the GNU General Public License.

“The Library” refers to a covered work governed by this License, other than an Application or a Combined Work as defined below.

An “Application” is any work that makes use of an interface provided by the Library, but which is not otherwise based on the Library. Defining a subclass of a class defined by the Library is deemed a mode of using an interface provided by the Library.

A “Combined Work” is a work produced by combining or linking an Application with the Library. The particular version of the Library with which the Combined Work was made is also called the “Linked Version”.

The “Minimal Corresponding Source” for a Combined Work means the Corresponding Source for the Combined Work, excluding any source code for portions of the Combined Work that, considered in isolation, are based on the Application, and not on the Linked Version.

The “Corresponding Application Code” for a Combined Work means the object code and/or source code for the Application, including any data and utility programs needed for reproducing the Combined Work from the Application, but excluding the System Libraries of the Combined Work.

1. Exception to Section 3 of the GNU GPL.
You may convey a covered work under sections 3 and 4 of this License without being bound by section 3 of the GNU GPL.

2. Conveying Modified Versions.
If you modify a copy of the Library, and, in your modifications, a facility refers to a function or data to be supplied by an Application that uses the facility (other than as an argument passed when the facility is invoked), then you may convey a copy of the modified version:

a) under this License, provided that you make a good faith effort to ensure that, in the event an Application does not supply the function or data, the facility still operates, and performs whatever part of its purpose remains meaningful, or
b) under the GNU GPL, with none of the additional permissions of this License applicable to that copy.
3. Object Code Incorporating Material from Library Header Files.
The object code form of an Application may incorporate material from a header file that is part of the Library. You may convey such object code under terms of your choice, provided that, if the incorporated material is not limited to numerical parameters, data structure layouts and accessors, or small macros, inline functions and templates (ten or fewer lines in length), you do both of the following:

a) Give prominent notice with each copy of the object code that the Library is used in it and that the Library and its use are covered by this License.
b) Accompany the object code with a copy of the GNU GPL and this license document.
4. Combined Works.
You may convey a Combined Work under terms of your choice that, taken together, effectively do not restrict modification of the portions of the Library contained in the Combined Work and reverse engineering for debugging such modifications, if you also do each of the following:

a) Give prominent notice with each copy of the Combined Work that the Library is used in it and that the Library and its use are covered by this License.
b) Accompany the Combined Work with a copy of the GNU GPL and this license document.
c) For a Combined Work that displays copyright notices during execution, include the copyright notice for the Library among these notices, as well as a reference directing the user to the copies of the GNU GPL and this license document.
d) Do one of the following:
0) Convey the Minimal Corresponding Source under the terms of this License, and the Corresponding Application Code in a form suitable for, and under terms that permit, the user to recombine or relink the Application with a modified version of the Linked Version to produce a modified Combined Work, in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.
1) Use a suitable shared library mechanism for linking with the Library. A suitable mechanism is one that (a) uses at run time a copy of the Library already present on the user&apos;s computer system, and (b) will operate properly with a modified version of the Library that is interface-compatible with the Linked Version.
e) Provide Installation Information, but only if you would otherwise be required to provide such information under section 6 of the GNU GPL, and only to the extent that such information is necessary to install and execute a modified version of the Combined Work produced by recombining or relinking the Application with a modified version of the Linked Version. (If you use option 4d0, the Installation Information must accompany the Minimal Corresponding Source and Corresponding Application Code. If you use option 4d1, you must provide the Installation Information in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.)
5. Combined Libraries.
You may place library facilities that are a work based on the Library side by side in a single library together with other library facilities that are not Applications and are not covered by this License, and convey such a combined library under terms of your choice, if you do both of the following:

a) Accompany the combined library with a copy of the same work based on the Library, uncombined with any other library facilities, conveyed under the terms of this License.
b) Give prominent notice with the combined library that part of it is a work based on the Library, and explaining where to find the accompanying uncombined form of the same work.
6. Revised Versions of the GNU Lesser General Public License.
The Free Software Foundation may publish revised and/or new versions of the GNU Lesser General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.

Each version is given a distinguishing version number. If the Library as you received it specifies that a certain numbered version of the GNU Lesser General Public License “or any later version” applies to it, you have the option of following the terms and conditions either of that published version or of any later version published by the Free Software Foundation. If the Library as you received it does not specify a version number of the GNU Lesser General Public License, you may choose any version of the GNU Lesser General Public License ever published by the Free Software Foundation.

If the Library as you received it specifies that a proxy can decide whether future versions of the GNU Lesser General Public License shall apply, that proxy&apos;s public statement of acceptance of any version is permanent authorization for you to choose that version for the Library.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LicenseFile.qml" line="116"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>MessageBoard</name>
    <message>
        <location filename="../MessageBoard.qml" line="59"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../MessageBoard.qml" line="71"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>MessageBoard2</name>
    <message>
        <location filename="../MessageBoard2.qml" line="103"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../MessageBoard2.qml" line="115"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>PanelEndOfLevel2</name>
    <message>
        <location filename="../PanelEndOfLevel2.qml" line="55"/>
        <source>Victory</source>
        <translation>Vitória</translation>
    </message>
</context>
<context>
    <name>Phone</name>
    <message>
        <location filename="../level05/Phone.qml" line="38"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../level05/Phone.qml" line="38"/>
        <source>W</source>
        <translation>W</translation>
    </message>
</context>
<context>
    <name>ScoreBoard</name>
    <message>
        <location filename="../ScoreBoard.qml" line="10"/>
        <source>Score</source>
        <translation>Pontuação</translation>
    </message>
</context>
<context>
    <name>Shop</name>
    <message>
        <location filename="../Shop.qml" line="131"/>
        <source>I see that you have a blue diamond and now I can sell you the Screet book of hapiness. Be warn that you can not read it yet.
It&apos;s locked and it will be revealed to you in due time!
Buy?</source>
        <translation>Vejo que já tens um diamante azul e agora posso vender-te O livro secreto da felicidade. Ficas avisado que ainda não podes lê-lo.
Está selado e será revelado na altura certa!
Comprar?</translation>
    </message>
    <message>
        <location filename="../Shop.qml" line="183"/>
        <source>WaterPowder.
Instructions: Just add water!!!
Price: 5$. Buy ?</source>
        <translation>Água em pó.
Instruções: Basta juntar água!!!
Preço: 5$. Comprar ?</translation>
    </message>
    <message>
        <location filename="../Shop.qml" line="223"/>
        <source>Carrots.
Price: 5$. Buy ?</source>
        <translation>Cenouras.
Preço: 5$. Comprar?</translation>
    </message>
    <message>
        <location filename="../Shop.qml" line="304"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../Shop.qml" line="305"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../Shop.qml" line="373"/>
        <source>Welcome to my shop, strangers. Not much to sell right now, I&apos;m remodeling.
Feel free to look around, double tap on any item to buy. To leave the shop, just walk left.</source>
        <translation>Bem vindo à minha loja, estrangeiros. Não há muito para vender agora, estou em remodelações.
Podem olhar à vontade, façam um duplo click em qualquer item para comprar. Para sair da loja, caminhem para a esquerda.</translation>
    </message>
    <message>
        <location filename="../Shop.qml" line="381"/>
        <source>That&apos;s my bike, it&apos;s not for sale. Leave it!!!</source>
        <translation>Essa é a minha bicicleta, não está à venda. Não mexas nela!!!</translation>
    </message>
    <message>
        <location filename="../Shop.qml" line="388"/>
        <source>Don&apos;t touch my money!!!</source>
        <translation>Não toques no meu dinheiro!!!</translation>
    </message>
    <message>
        <location filename="../Shop.qml" line="395"/>
        <source>I have hidden in there the Secret Book of Happiness. I will only sell it if you bring me a blue diamond.</source>
        <translation>Eu escondi aí o livro secreto da Felicidade. Só o vendo se me trouxerem um diamante azul.</translation>
    </message>
    <message>
        <location filename="../Shop.qml" line="409"/>
        <source>Not enought money!!!</source>
        <translation>Dinheiro insuficiente!!!</translation>
    </message>
</context>
</TS>
