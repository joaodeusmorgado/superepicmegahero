import QtQuick 2.0
//import com.Timer 1.0

ProjectilePhysics {
    id: root
    width: 2*mm
    height: width

    property alias color: bullet.color
    property alias border: bullet.border

    Rectangle {
        id: bullet
        anchors.fill: parent
        radius: width/2
        color: "greenyellow"
    }
}
