import QtQuick 2.0
import "../components"
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    //color: "darkblue"

    signal close()

    ConversationBoard2 {
        id: talkBoard
        anchors.top: parent.top
        anchors.bottom: btnClose.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        width: parent.width - btnClose.width*2 - 4*defaultMargins

            /*width: mainRoot.width * 0.6
        height: mainRoot.height * 0.6//0.5
        x: mainRoot.width*0.5 - talkBoard.width/2
        y: defaultMargins*/

        imageLeft: "asteroidSnorlex.png"
        imageRight: "robonoid.png"
        imageBottom: "asteroidSnorlex.png"

        imageBottomWidth: 50*mm
        imageBottomHeight: 50*mm

        visible: false

        onEndDialog: close()//btnClose.text = qsTr("Ok")
    }

    MyButton {
        id: btnClose
        //width: btnSize * 1.25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onBtnClick: close()
        text: qsTr("Skip")
    }

    onVisibleChanged: {
        if (visible) {
            //btnClose.text = qsTr("Skip")
            addTalk()
            talkBoard.visible = true
        }
    }

    function addTalk() {
        talkBoard.clear()
        //talkBoard.addMessage4(qsTr("Congratulations on finishing the previous level. You're a real hero :)", "asteroidSnorlex.png", 2, false))
        talkBoard.addMessage5(qsTr("Congratulations on finishing the previous level. You're a real hero 😊"
                                   +"\nSecond level is really easy and you will learn how to use your map.")
                                   , "", -1, false, "asteroidSnorlex.png")
        talkBoard.addMessage5(qsTr("Your mission, Captain Andrew, is to find a prisoner in the tunnels of an asteroid."
                                   +"\nYou will need to drill the asteriod and release the prisoner."
                                   + "\nClick anywhere on the left or right of the screen to move and on the button to drill.")
                                   , "", -1, false, "asteroidSnorlex.png")
        talkBoard.addMessage5(qsTr("Relax, enjoy and let's do it!!!") , "", -1, false, "asteroidSnorlex.png")

    }


    /*Flickable {
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: parent.height //- btnOkCancel.height
        contentWidth: parent.width
        contentHeight: helpColumn.height + defaultMargins * 2

        Column {
            id: helpColumn
            spacing: defaultMargins
            anchors.top: parent.top
            anchors.topMargin: defaultMargins
            anchors.left: parent.left
            anchors.leftMargin: defaultMargins
            anchors.right: parent.right
            anchors.rightMargin: defaultMargins

            ImageFrame {
                id: imgFrame
                width: 30 * mm//root.width * 0.2
                source: "qrc:/images/asteroidSnorlex.png"
            }


            Rectangle {
                id: rect1
                width: parent.width
                height:  aboutText.height + 2 * defaultMargins
                radius: 2 * mm
                color: root.color
                border.width: mm
                border.color: "orange"

                TextTemplate {
                    id: aboutText
                    width: parent.width - 2 * defaultMargins
                    anchors.top: rect1.top
                    anchors.left: parent.left
                    anchors.margins: defaultMargins
                    bold: false
                    font: "Purisa"
                    //pixelSize: 18//8*mm

                    //property string messagePT: "A missão do capitão André é um encontrar um prisioneiro nos túneis de um asteroid."
                      //                  + "\nTerá de usar a sua broca para furar o asteroid e libertar e prisioneiro."
                         //               + "\nClica no lado direito ou esquerdo do ecrã para moveres a broca e no botão para escavar!\nVamos começar!!!"

                    label: qsTr("Congratulations on finishing the previous level. You're a real hero :)"
                                + "\nSecond level is really easy, and you will learn how to use your map."
                                + "\nYour mission, Captain Andrew, is to find a prisoner in the tunnels of an asteriod."
                                + "\nYou will need to drill the asteriod and release the prisoner."
                                + "\nClick anywhere on the left or right of the screen to move and on the button to drill."
                                + "\nRelax, enjoy and let's do it!!!")
                }
            }
        }
    }

    signal close()
    MouseArea {
        anchors.fill: parent
        onClicked: close()
    }*/

    /*ButtonBox {
        id: btnOkCancel
        anchors.bottom: parent.bottom
        showCancel: false
        onBtnOk: buttonOk()
    }*/
}
