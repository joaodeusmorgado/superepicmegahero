import QtQuick 2.3
import "../"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    //color: "lightyellow"
    color: "darkblue"

    property bool bPressed: false
    property bool moveLeft: false
    property bool isDigging: false
    property bool isExcavatorRotating: false
    property real angle: Math.random()*360 //prisoner initial angle position

    property real radius: asteroid2.width*0.5
    property real myAngle: Math.PI / 2
    property real angleDelta: Math.PI/200
    property real x0: parent.width/2
    property real y0: parent.height/2

    property real holeX0: 0
    property real holeY0: 0
    property real holeX1: 0
    property real holeY1: 0

    property real deltaFindPrisoner: excavatorDigging.height/2
    property bool prisonerFound: false

    signal menu()
    signal level02End()
    signal startLevel()
    signal nextLevel()
    signal endLevel()
    signal youWin()


    onYouWin: {
        panelEOL.text = qsTr("Victory")
        //panelEOL.showNextBtn = true
        levels.levelUnlocked = levels.levelUnlocked === 2 ? 3 : levels.levelUnlocked
        msgBoard.visible = true
    }

    onVisibleChanged: {
        visible === false ? endLevel() : startLevel()
    }

    onEndLevel: {
        panelEOL.visible = false
        diggingModel.clear()
        canvas.requestPaint()
    }

    onStartLevel: {
        myAngle = Math.PI / 2
        holeX0 = holeY0 = holeX1 = holeY1 = 0
        angle = Math.random()* Math.PI * 2
        asteroidPrisoner.visible = false
        isDigging = false
        isExcavatorRotating = false
        diggingModel.clear()
        prisonerFound = false
        conversationBoard.visible = false
        msgBoard.visible = false
    }

    Rectangle {
        id: asteroid2
        width: parent.width > parent.height ? parent.height*0.7 : parent.width*0.7
        height: width
        radius: width/2
        color: "brown"
        anchors.centerIn: parent

        Image {
            id: asteroidImg
            anchors.fill: parent
            anchors.margins: -asteroid2.width * 0.21
            source: "qrc:/images/astroid.png"
            mipmap: true
            transform: Translate {
                x: -asteroid2.width*0.012
                y: -asteroid2.width*0.014
            }
        }
    }

    Canvas {
        id: canvas
        //width: asteroid2.width
        //height: asteroid2.height
        width: parent.width
        height: parent.height
        //anchors.centerIn: parent

        property color color: "red"
        property var ctx
        onPaint: {
            ctx = getContext('2d')
            ctx.lineWidth = 6*mm
            ctx.strokeStyle = "grey"

            // glow effect
            /*var  hue = 10 * Math.random();
            ctx.strokeStyle = 'hsl(' + hue + ', 50%, 50%)';
            ctx.shadowColor = 'white';
            ctx.shadowBlur = 10;*/

            canvas.ctx.clearRect(0, 0, canvas.width, canvas.height)
            ctx.beginPath()
            drawHole(ctx)

            if (isDigging)//if a hole is being made, draw it
                updateCurrentHole(ctx)

            ctx.stroke()
        }
    }

    Item {
        id: excavator
        width: 30*mm
        height: width * 0.688
        x: x0 + radius * Math.cos(myAngle) - excavator.width/2
        y: y0 - radius * Math.sin(myAngle) - excavator.height/2
        //transform: Rotation { origin.x: x0; origin.y: y0; angle: myAngle}
        rotation: -(myAngle * 180/Math.PI -90)

        Image {
            anchors.fill: parent
            mipmap: true
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/excavator.png"
            rotation: 0
        }
    }

    Image {
        id: excavatorDigging
        visible: !excavator.visible
        width: excavator.width
        height: excavator.height
        x: excavator.x
        y: excavator.y
        rotation: excavator.rotation
        mipmap: true
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/excavatorDigging.png"
        property real midX: x+width/2
        property real midY: y+height/2
    }

    property vector2d excavatorDiggingXY: Qt.vector2d(excavatorDigging.x, excavatorDigging.y)
    onExcavatorDiggingXYChanged: {
        if (isDigging) {
            canvas.requestPaint()
            checkForPrisoner()
        }
    }

    Image {
        id: asteroidPrisoner
        width: 20*mm
        height: width
        x: x0 + radius * Math.cos(angle) * 0.7 - width/2
        y: y0 - radius * Math.sin(angle) * 0.7 - height/2
        mipmap: true
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/asteroidSnorlex.png"
        visible: false
        property real midX: x+width/2
        property real midY: y+height/2
    }

    Timer {
        id: timer
        running: bPressed
        interval: fps_ms
        repeat: true
        onTriggered: {
            moveExcavator()
        }
    }

    MouseArea {
        id: ma
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton        
        onMouseXChanged: moveLeft = mouseX  < parent.width/2 ? true : false
        onPressed: bPressed = true
        onReleased: bPressed = false
    }

    MenuPopUpVertical {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }

    MyButton {
        id: btnExcavate
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        text: qsTr("Dig")
        onBtnClick: {
            excavator.visible = !excavator.visible
            if (!excavator.visible) {
                animX.start()
                animY.start()
            }
        }
        enabled: !animX.running && !rotateExcavator.running
    }

    NumberAnimation {
        id: animX
        target: excavatorDigging
        property: "x"
        from: x0 + radius * Math.cos(myAngle) - excavator.width/2
        to: x0 + radius * Math.cos(myAngle+Math.PI) - excavator.width/2
        duration: 3000
        onStarted: {
            isDigging = true

            holeX0 = excavatorDigging.x + excavatorDigging.width/2
            holeY0 = excavatorDigging.y + excavatorDigging.height/2
        }
        onStopped: {
            isDigging = false

            holeX1 = excavatorDigging.x + excavatorDigging.width/2
            holeY1 = excavatorDigging.y + excavatorDigging.height/2
            //var data = Qt.vector4d(holeX0, holeY0, holeX1, holeY1)
            //diggingModel.append(data)
            diggingModel.append({"x0": holeX0, "y0": holeY0, "x1": holeX1, "y1": holeY1})
            canvas.requestPaint()

            rotateExcavator.start()
        }
    }

    NumberAnimation {
        id: animY
        target: excavatorDigging
        property: "y"
        from: y0 - radius * Math.sin(myAngle) - excavator.height/2
        to: y0 - radius * Math.sin(myAngle+Math.PI) - excavator.height/2
        duration: animX.duration
    }

    NumberAnimation {
        id: rotateExcavator
        target: excavatorDigging
        property: "rotation"
        from: excavator.rotation
        to: excavator.rotation + 180
        duration: animX.duration
        onStarted: isExcavatorRotating = true
        onStopped: {
            isExcavatorRotating = false
            myAngle += Math.PI
            excavator.visible = true

            if (prisonerFound) {
                youWin()
                //msgBoard.visible = true
            }
        }
    }

    MessageBoard {
        id: msgBoard
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.margins: defaultMargins
        // add pt translation qsTr("Encontraste o prisioneiro!!!")
        text: qsTr("You found the prisioner!!!")
        pixelSize: 10*mm
        horizontalAlignment: Text.AlignHCenter //Text.AlignJustify
        verticalAlignment: Text.AlignVCenter
        visible: false
        onOk: {
            msgBoard.visible = false
            conversationBoard.visible = true
        }
    }

    ConversationBoard2 {
        id: conversationBoard
        height: parent.height * 0.8
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.margins: defaultMargins
        visible: false
        verticalAlignment: Text.AlignTop

        //imageBottom: "btnsPauseBook.png"
        imageBottomWidth: imageBottomHeight * 2.2
        imageBottomHeight: 20*mm

        onEndDialog: {
            console.log("ConversationBoard2 -> onEndDialog")
            heroMap.setFuelCan()
            heroMap.addFuel(50)
            panelEOL.visible = true
        }
    }

    PanelEndOfLevel2 {
        id: panelEOL
        visible: false
        anchors.centerIn: parent
        showNextBtn: levels.levelUnlocked > 2
        onMenu: root.menu()
        onReplay: root.startLevel()
        onNext: root.nextLevel()
    }


    Component.onCompleted: {
        setupConversation()
        }

    ListModel {
        id: diggingModel
    }


    function setupConversation() {
        conversationBoard.clear()

        conversationBoard.addMessage4(qsTr("Hi, thanks for saving me."),"asteroidSnorlexHead.png", 1, false)


        conversationBoard.addMessage5(qsTr("As a reward I offer you a fuel can, with 50 liters."),"asteroidSnorlexHead.png", 1, false, "fuelCan.png")


        conversationBoard.addMessage4(qsTr("Thanks for the fuel. How do I use it?"),"heroHead.png", 2, true)

        conversationBoard.addMessage5(qsTr("You can check your objects clicking on the Book button at top left."),
                                      "asteroidSnorlexHead.png", 1, false, "book.png")

        conversationBoard.addMessage4(qsTr("That will open a book with all the object you find in you journey. "
                                           +"Some objects will be automatically used when needed, others you need to click them. "
                                           +"Have a nice journey!"),"asteroidSnorlexHead.png", 1, false)

        conversationBoard.addMessage4(qsTr("Thank you and goodbye."),"heroHead.png", 2, true)
    }

    function drawHole(ctx){
        //console.log("drawHole diggingModel.count: "+diggingModel.count)
        for (var i=0;i < diggingModel.count;i++) {
            //ctx.moveTo(diggingModel.get(i).x,diggingModel.get(i).y)
            //ctx.lineTo(diggingModel.get(i).z,diggingModel.get(i).w)
            ctx.moveTo(diggingModel.get(i).x0,diggingModel.get(i).y0)
            ctx.lineTo(diggingModel.get(i).x1,diggingModel.get(i).y1)
        }
    }

    function updateCurrentHole(ctx) {
        holeX1 = excavatorDigging.x + excavatorDigging.width/2
        holeY1 = excavatorDigging.y + excavatorDigging.height/2
        ctx.moveTo(holeX0, holeY0)
        ctx.lineTo(holeX1, holeY1)
    }

    function checkForPrisoner() {
        if (lib.distance(excavatorDigging.midX, excavatorDigging.midY,
                 asteroidPrisoner.midX, asteroidPrisoner.midY, deltaFindPrisoner) ) {
            asteroidPrisoner.visible = true
            prisonerFound = true
        }
    }

    function moveExcavator() {
        if (isDigging)
            return;

        if (isExcavatorRotating)
            return;

        if (moveLeft)
            myAngle += angleDelta
        else
            myAngle -= angleDelta
    }

}
