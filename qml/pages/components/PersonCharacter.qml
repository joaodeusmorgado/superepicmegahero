import QtQuick 2.3
import "../"

Item {
    id: root
    width: 24*mm
    height: 24*mm

    property string imgName: "hero1.png"
    property alias mirror: img.mirror
    property alias img: img

    Image {
        id: img
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/" + imgName
        mipmap: true
    }

}
