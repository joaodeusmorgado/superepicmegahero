import QtQuick 2.0
import "../"

Item {
    id: levelContentTemplate

    width: mainRoot.width
    height: mainRoot.height


    property var leaderObj: hero
    property var followerObj: girl
    property alias hero: hero
    property alias girl: girl
    property alias interfaceWalkGirlHero: interfaceWalkGirlHero
    property var swordObj: null

    property real groundLimit: sky.height+hero.height/2


    property alias sky: sky
    property alias grass: grass

    //property real contentWidth: mainRoot.width*2
    property real skyHeight: mainRoot.height * 0.7 // 70% of mainRoot screen height
    property real grassHeight: mainRoot.height - sky.height // 30% of mainRoot screen height
    property color skyColor: "lightcyan"
    property color grassColor: "lime"

    property int screenNumber: Math.floor(leaderObj.x / flickLevel.width)
    property int screenRow: Math.floor(leaderObj.y / flickLevel.height) //screen 0: upper; screenRow 1: lower

    property bool walkPartnerActive: true

    signal destroyItems()
    signal levelStartSetup()
    signal levelEndCleanup()

    onVisibleChanged: visible ? levelStartSetup() : levelEndCleanup()

    Rectangle {
        id: sky
        width: parent.width
        height: skyHeight
        color: skyColor
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#0099ff" }
            GradientStop { position: 0.33; color: "lightblue" }
            GradientStop { position: 1.0; color: "lightcyan" }
        }
    }

    /*Rectangle {
        id: sky
        width: parent.width
        height: mainRoot.height * 0.7 // 70% of the screen height
        color: "lightcyan"
        anchors.top: parent.top

        gradient: Gradient {
                  GradientStop { position: 0.0; color: "#0099ff" }
                  GradientStop { position: 0.33; color: "lightblue" }
                  GradientStop { position: 1.0; color: "lightcyan" }
              }
    }*/

    Rectangle {
        id: grass
        width: parent.width
        height: grassHeight
        color: grassColor
        y: sky.height
        //anchors.top: sky.bottom
    }

    Person3 {
        id: girl
        imgName: "girl"
        imgFireShot: "girlFire"
        imgFaints: "girlfaints2"
        canFireMagicShots: true
        worldLimitGround: groundLimit
        z: 1
        function reset() {
            dir = 1
            visible = true
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
            //worldLimitGround = groundLimit
            x = 0
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
        }
        mouseAreaEnabled: true
        onDoubleClick: {
            setLeaderFollower(girl, hero)
            mouseAreaEnabled = false
            hero.mouseAreaEnabled = true
        }
    }

    Person3 {
        id: hero
        width: height * imageSourceSize.width / imageSourceSize.height
        height: 36.9*mm
        //jumpLenght: width*3.5
        //imgFireShot: "girlFire"
        //canFireMagicShots: true
        //canFly: true

        imgName: "hero"
        z: 1
        worldLimitGround: groundLimit
        onImDestroyed: {
            //heroLifeBar.hitDestroy()
            levelFailed(qsTr("Level failled!!!"))
        }
        onImHit: heroLifeBar.hit()

        function reset(){
            hero.dir = 1
            visible = true
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
            //worldLimitGround = groundLimit
            x = 0
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
            //heroLifeBar.initialLife = 3
            //heroLifeBar.reset()
            leaderObj = hero
        }
        mouseAreaEnabled: false
        onDoubleClick: {
            setLeaderFollower(hero, girl)
            mouseAreaEnabled = false
            girl.mouseAreaEnabled = true
        }
    }


    InterfaceWalkPartner {
            id: interfaceWalkGirlHero
            leader: leaderObj
            follower: followerObj
            //call function connect() to make the girl follow the hero
    }


    onLevelStartSetup: {
        console.log("level ContentTemplate start setup")
        hero.reset()
        girl.reset()
        createConnections()
    }


    onLevelEndCleanup: {
        //console.log("level 11 end cleanup")
        destroyConnections()
        destroyItems()
        activeEnemiesList.clearEnemies()
        obstaclesList.clearObstacles()
        hero.stopWalking()
        girl.stopWalking()
    }

    function createConnections() {

        lib.connectCommandWalk(commandTouchPad, leaderObj)
        lib.connectCommandWalk(commandKeyb, leaderObj)


        if (leaderObj.canFly) {
            lib.connectCommandFly(commandTouchPad, leaderObj)
            lib.connectCommandFly(commandKeyb, leaderObj)
        }


        interfaceWalkGirlHero.leader = leaderObj
        interfaceWalkGirlHero.follower = followerObj
        if (walkPartnerActive)
            interfaceWalkGirlHero.connect()

        heroMap.showBowAndArrow.connect( createBow )
        heroMap.hideBowAndArrow.connect( destroyBow )

        heroMap.showSword.connect( createSword )
        heroMap.hideSword.connect( destroySword )

        flickLevel.contentX = Qt.binding( function(){ //moves the scene when the hero reachs the end of the screen
            return Math.floor(leaderObj.x / flickLevel.width) * flickLevel.width} )

        flickLevel.contentY = Qt.binding( function(){
            return Math.floor(leaderObj.y / flickLevel.height) * flickLevel.height} )

        //heroMap.showCarrots.connect( showCarrot )


        if (leaderObj.canFireMagicShots) {
            commandTouchPad.shotFiredXY.connect( createPersonShot )
            createPersonShot.connect( leaderObj.fireShot ) // used to temporary change the person image to simulate a fire shot
        }
            //girlShotCreateConnection()
    }

    function destroyConnections() {
        lib.disconnectCommandWalk(commandTouchPad, leaderObj)
        lib.disconnectCommandWalk(commandKeyb, leaderObj)

        if (leaderObj.canFly) {
            lib.disconnectCommandFly(commandTouchPad, leaderObj)
            lib.disconnectCommandFly(commandKeyb, leaderObj)
        }

        if (walkPartnerActive)
            interfaceWalkGirlHero.disconnect()

        heroMap.showBowAndArrow.disconnect( createBow )
        heroMap.hideBowAndArrow.disconnect( destroyBow )

        //flickLevel.contentX = 0
        //flickLevel.contentY = 0

        //heroMap.hideCarrots.connect( hideCarrot )

        if (leaderObj.canFireMagicShots)
            commandTouchPad.shotFiredXY.disconnect( createPersonShot )
            createPersonShot.disconnect( leaderObj.fireShot ) // used to temporary change the person image to simulate a fire shot
            //girlShotDestroyConnection()
    }

    function setLeaderFollower(leader, follower) {
        if (leader === leaderObj)//if there is no change return
            return;

        destroyConnections()//destroy connections with current leader, and destroy bow if exists
        if (heroMap.bowAndArrowSelected)
            destroyBow()

        if (heroMap.swordSelected)//destroy connections with current leader, and destroy sword if exists
            destroySword()

        leaderObj = leader//set new leader
        followerObj = follower//and new follower

        if (heroMap.bowAndArrowSelected)//create bow if existed
            createBow()

        if (heroMap.swordSelected)//create sword if existed before chaging leader
            createSword()

        createConnections()//restore connections with the new leader
    }



    //-- Bow & Arrows-----------------------------------------------
    function createBow(){
        lib.createBow(leaderObj, parent)
        commandTouchPad.shotFiredXY.connect( createArrow )
    }

    function destroyBow(){
        commandTouchPad.shotFiredXY.disconnect( createArrow )
        lib.destroyBow()
    }

    function createArrow(touchX, touchY) {
        //var arrow = lib.createArrow(touchX, touchY, bowObj, screenNumber, screenRow, root)
        var arrow = lib.createProjectile(touchX, touchY, lib.bowObj.midX, lib.bowObj.midY, screenNumber, screenRow, parent)
        if (arrow === null)
            return;

        arrow.imageName = "arrow.png"
        arrow.projectileType = "arrow"
        arrow.width = 20*mm
        arrow.height = 20*mm*0.273
    }
    //-- Bow & Arrows-----------------------------------------------



    //-- Person Magic Shot-----------------------------------------------
    property bool isFiringMagicShot: false
    signal createPersonShot(var touchX, var touchY)

    onCreatePersonShot: {
        if (swordObj !== null || lib.bowObj !== null)
            return;//only fire if person is not holding a sword or a bow

        isFiringMagicShot = true;
        var magicShotObj = lib.createProjectile(touchX, touchY, leaderObj.midX, leaderObj.midY-leaderObj.height*0.2, screenNumber, screenRow, root)
        if (magicShotObj === null) {
            isFiringMagicShot = false;
            return;
        }

        magicShotObj.width = 14*mm
        magicShotObj.height = magicShotObj.width
        magicShotObj.color = "white"
        magicShotObj.border.width = 1*mm
        magicShotObj.border.color = "dimgrey"
        //magicShotObj.z = 1
        isFiringMagicShot = false;
    }
    //-- Person Magic Shot-----------------------------------------------



    //-- sword--------------------------------------------
    function createSword() {
        swordObj = lib.createSword(swordObj, leaderObj, parent)

        //todo: connect add attack signals
        lib.connectSwordAttack(commandTouchPad, swordObj)
        lib.connectSwordAttack(commandKeyb, swordObj)

        swordObj.attackIsRunning.connect( updateRec)//visual sword attack debug
        destroyItems.connect( swordObj.destroy )
    }

    //for debbuging sword attack position
    function updateRec(xx, yy){
        redrec.x = xx
        redrec.y = yy
    }

    //for debbuging sword attack position
    Rectangle {
        id: redrec
        width: 2
        height: 2
        color: "red"
    }

    function destroySword() {
        //todo: add disconnections made in createSword()
        lib.disconnectSwordAttack(commandTouchPad, swordObj)
        lib.disconnectSwordAttack(commandKeyb, swordObj)
        swordObj.destroy()
        swordObj = null

    }
    //-- sword--------------------------------------------
}
