import QtQuick 2.3


Item {
    id: mountainID
    width: mainRoot.width
    height: baseHeight + centerHeight + topHeight
    //color: "orange"


    property color mountainColor: "SaddleBrown"
    property color mountainBorderColor: "black"
    property real baseHeight: mainRoot.height * 0.4
    property real centerHeight: mainRoot.height * 0.3
    property real topHeight: mainRoot.height *  0.3

    property point mountainEntrace: Qt.point(mountainID.x+mountainCenter.x, mountainID.y+mountainCenter.y+mountainCenter.height/2)
    property point mountainExit: Qt.point(mountainID.x+mountainCenter.x+mountainCenter.width, mountainID.y+mountainCenter.y+mountainCenter.height/2)

    /*property point baseTL: Qt.point(mountainID.x+mountainBase.x, mountainID.y+mountainBase.y) //Top Left
    property point baseBR: Qt.point(mountainID.x+mountainBase.x+mountainBase.width, mountainID.y+mountainBase.y+mountainBase.height) //Bottom Right

    property point centerTL: Qt.point(mountainID.x+mountainCenter.x, mountainID.y+mountainCenter.y) //Top Left
    property point centerBR: Qt.point(mountainID.x+mountainCenter.x+mountainCenter.width, mountainID.y+mountainCenter.y+mountainCenter.height) //Bottom Right

    property point topTL: Qt.point(mountainID.x+mountainTop.x, mountainID.y+mountainTop.y) //Top Left
    property point topBR: Qt.point(mountainID.x+mountainTop.x+mountainTop.width, mountainID.y+mountainTop.y+mountainTop.height) //Bottom Right

    property alias mountainBase: mountainBase
    */

    property real baseX: mountainID.x+mountainBase.x
    property real baseX1: mountainID.x+mountainBase.x+mountainBase.width
    property real baseY: mountainID.y+mountainBase.y
    property real baseY1: mountainID.y+mountainBase.y+mountainBase.height

    property real centerX: mountainID.x+mountainCenter.x
    property real centerX1: mountainID.x+mountainCenter.x+mountainCenter.width
    property real centerY: mountainID.y+mountainCenter.y
    property real centerY1: mountainID.y+mountainCenter.y+mountainCenter.height

    property real topX: mountainID.x+mountainTop.x
    property real topX1: mountainID.x+mountainTop.x+mountainTop.width
    property real topY: mountainID.y+mountainTop.y
    property real topY1: mountainID.y+mountainTop.y+mountainTop.height



    Rectangle {
        id: mountainBase
        width: mountainID.width
        height: baseHeight
        radius: defaultMargins
        color: "SaddleBrown"
        border.color: mountainBorderColor
        anchors.bottom: parent.bottom
    }

    Rectangle {
        id: mountainCenter
        width: mountainBase.width * 0.7
        height: centerHeight
        radius: defaultMargins
        color: "SaddleBrown"
        border.color: mountainBorderColor
        anchors.bottom: mountainBase.top
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Rectangle {
        id: tunnelIn
        width: 5*defaultMargins
        height: mountainCenter.height
        anchors.bottom: mountainCenter.bottom
        anchors.horizontalCenter: mountainCenter.left
        radius: height*0.5
        color: "black"
    }

    Rectangle {
        id: tunnelOut
        width: tunnelIn.width
        height: mountainCenter.height
        anchors.bottom: mountainCenter.bottom
        anchors.horizontalCenter: mountainCenter.right
        radius: tunnelIn.radius
        color: tunnelIn.color
    }

    Rectangle {
        id: mountainGateLeft
        width: 5*defaultMargins
        height: mountainCenter.height
        anchors.bottom: mountainCenter.bottom
        //anchors.horizontalCenter: mountainCenter.left
        x: mountainCenter.x + 5*defaultMargins
        radius: height*0.5
        //color: "golden"
    }

    Rectangle {
        id: mountainTop
        width: mountainCenter.width * 0.7
        height: topHeight
        radius: defaultMargins
        color: "SaddleBrown"
        border.color: mountainBorderColor
        anchors.bottom: mountainCenter.top
        anchors.horizontalCenter: parent.horizontalCenter
    }

}
