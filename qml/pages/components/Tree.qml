import QtQuick 2.3

Item {
    id: tree
    width: height * 402/732
    height: 80*mm

    //property alias fruitVisible: apple.visible
    signal fruitFalled()

    Image {
        id: treeImage
        //width: parent.width
        //height: parent.height
        //anchors.centerIn: parent
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/tree2.png"
        mipmap: true
    }

    Image {
        id: apple
        width: parent.height * 0.08
        height: width * 66/51
        x: tree.width*0.45
        y: tree.height*0.3
        source: "qrc:/images/fruit.png"
        mipmap: true
        visible: false
    }

    NumberAnimation {
        id: appleFall
        target: apple
        properties: "y"
        from: tree.height*0.3
        to: tree.height
        duration: 2500
        //loops: Animation.Infinite
        easing {type: Easing.InQuad/*; overshoot: 500*/}
        onRunningChanged: {
            if (!running)
                fruitFalled()//only fire the signal when the fruit finished falling
        }
        onStarted: {
            apple.visible = true
            apple.y = tree.height*0.3
        }
        onStopped: apple.visible = false
    }

    MouseArea{
        //anchors.fill: parent
        anchors.top: parent.top
        width: parent.width
        height: parent.height/2
        onDoubleClicked: {
            apple.y = tree.height*0.3
            //apple.visible = true
            appleFall.start()//parent.doubleClick()
        }
    }

    /*function init() {
        apple.y = tree.height*0.3
        //apple.visible = true
    }*/
}
