import QtQuick 2.0

Item {
    id: root

    width: 64*mm
    height: 8*mm

    property real value: sliderBtn.x / (root.width - sliderBtn.width)
    //(sliderPos.width /*- sliderBtn.radius*/) / (sliderBtn)

    onValueChanged: console.log("slider: "+value)

    Rectangle {
        id: sliderLine
        width: parent.width
        height: parent.height * 0.4
        anchors.verticalCenter: parent.verticalCenter
        radius: height/2
        color: "lightgrey"
    }

    Rectangle {
        id: sliderPos
        width: sliderBtn.x //+ sliderBtn.radius
        height: parent.height * 0.4
        anchors.verticalCenter: parent.verticalCenter
        radius: height/2
        color: "navy"
    }

    Rectangle {
        id: sliderBtn
        width: height
        height: parent.height
        radius: width/2
        anchors.verticalCenter: parent.verticalCenter
        color: "blue"
        //border.color: "black"

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            drag.target: sliderBtn
            drag.axis: Drag.XAxis
            drag.minimumX: 0
            drag.maximumX: root.width - sliderBtn.width
        }
    }





}
