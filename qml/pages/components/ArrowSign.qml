import QtQuick 2.0
import "../"

Item {
    id: arrowSign
    width: 22*mm
    height: width * 1.3

    property color borderColor: "#00ccff"
    property color backgroundColor: "#99ffff"
    property color mastColor: "#cc6633"

    //arrow sign
    Rectangle {
        id: arrowSideOutter
        height: outterRect.height * Math.sqrt(2)*0.5
        width: height
        color: borderColor
        anchors.verticalCenter: outterRect.verticalCenter
        anchors.right: parent.right
        rotation: 45

        Rectangle {
            anchors.fill: parent
            anchors.margins: innerRect.anchors.topMargin / Math.sqrt(2)
            color: backgroundColor
        }
    }

    //body sign
    Rectangle {
        id: outterRect
        width: parent.width - arrowSideOutter.width*0.5
        height: parent.height*0.35
        color: borderColor

        Rectangle {
            id: innerRect
            anchors.top: parent.top
            anchors.topMargin: defaultMargins*2
            anchors.bottom: parent.bottom
            anchors.bottomMargin: defaultMargins*2
            anchors.left: parent.left
            anchors.leftMargin: defaultMargins*2
            anchors.right: parent.right
            color: backgroundColor
        }

        TextTemplate {
            anchors.fill: parent
            anchors.margins: defaultMargins*2
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            label: qsTr("Treasure Mountain")
        }
    }

    //mast
    Rectangle {
        id: mast
        color: mastColor
        width: parent.width*0.1
        anchors.top: outterRect.bottom
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

}
