import QtQuick 2.3

Image {
    property string iconName: "hero1.png"
    width: 20 * mm //mainRoot.width - 2 * defaultMargins
    fillMode: Image.PreserveAspectFit
    source: "qrc:/images/"+iconName
    mipmap: true
}
