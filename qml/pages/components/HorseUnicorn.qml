import QtQuick 2.3

Item {
    id: root
    width: 22*mm
    height: 18*mm

    Image {
        id: img
        anchors.fill: parent
        source: "qrc:/images/unicornhorse.png"
        mipmap: true
    }

}
