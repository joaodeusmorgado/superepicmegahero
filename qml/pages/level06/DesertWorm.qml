import QtQuick 2.0
import "../"

EnemyTemplate {
    id: worm

    width: 25*mm
    height: width*126/265
    imageName: "desertWorm.png"
    type: "deserWorm"
    lifePower: 1

    topLeft: Qt.point(x+width*0.2,y+height*0.2)//hack to make the collison with target less sensible
    bottomRight: Qt.point(x+width*0.8,y+height*0.8)//hack to make the collison with target less sensible

    property real dx: mm

    property int screenNumber //binded to the current screenNumber of level6
    property int i //screenNumber where worm is created, it's a auxiliar variable for binding the dynamic created objects to a screen number
    //property bool test: i === screenNumber

    //alive: screenNumber === i && x > (mainRoot.width*i-width)
    //inMotion: alive
    //active: alive
    alive: true //x > (mainRoot.width*i-width)
    inMotion: false//screenNumber === i
    active: false//inMotion

    onScreenNumberChanged: {
        if (screenNumber === i) {
            inMotion = active = true
        }
    }

    onXChanged: {
        if (x < -width) {
            alive = inMotion = active = false
        }
    }

    onInMotionChanged: console.log("worm inMotion changed")
    onActiveChanged: console.log("worm desert active is: "+active)

    onMove: {
        //console.log("worm is moving")
        x -= dx
    }

}
