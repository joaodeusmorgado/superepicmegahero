import QtQuick 2.0
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "grey"

    signal menu()
    signal nextLevel()

    CommandMultiTouchPad3 {
       id: commandTouchPad
       anchors.left: parent.left
       anchors.bottom: parent.bottom
       anchors.margins: defaultMargins
    }

    Flickable {
        id: flickLevel

        width: mainRoot.width
        height: mainRoot.height
        contentWidth: levelContent.width
        contentHeight: levelContent.height
        interactive: false
        Level06Content {
            id: levelContent
            width: mainRoot.width * 11
            height: mainRoot.height
        }
    }//Flickable


    TouchPad2 {
        id: touchPad
        centerX: commandTouchPad.touchPad.centerX
        centerY: commandTouchPad.touchPad.centerY
        visible: commandTouchPad.touchPad.visible
    }

    MenuPopUpVertical {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }


    PanelEndOfLevel2 {
        id: panelEOL
        visible: false
        anchors.centerIn: parent
        showNextBtn: levels.levelUnlocked > 6
        onMenu: parent.menu()
        onReplay: {
            levelContent.endLevel()
            levelContent.startLevel()
        }
        onNext: parent.nextLevel()
    }


    LifeBar {
        id: truckLifeBar
        anchors.bottom: parent.bottom
        anchors.right: badJoeLifeBar.left
        anchors.margins: defaultMargins
        visible: levelContent.badJoeObj !== null && levelContent.badJoeObj.active === true
    }

    LifeBar {
        id: badJoeLifeBar
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: defaultMargins
        visible: levelContent.badJoeObj !== null && levelContent.badJoeObj.active === true
        barColor: "#73679b"
    }

}
