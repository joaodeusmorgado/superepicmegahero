import QtQuick 2.3
import "../"
import "../components"
import "../libraries"

Rectangle {
    id: root
    width: mainRoot.width * 8
    height: mainRoot.height * 2
    color: "grey"

    property var leaderObj: hero
    property int screenNumber: Math.floor(leaderObj.x / flickLevel.width)
    property var badJoeObj: null

    property real ground: sky.height+truck.height/2

    signal destroyItems()
    signal levelFailed()
    signal levelWin()

    onLevelWin: {
        levels.levelUnlocked = levels.levelUnlocked === 6 ? 7 : levels.levelUnlocked
        panelEOL.text = qsTr("Victory!")
        panelEOL.showNextBtn = true
        panelEOL.visible = true
        destroyItems()
    }


    onLevelFailed: {
        destroyConnections()
        destroyItems()
        panelEOL.text = qsTr("Level failed!")
        panelEOL.visible = true
    }

    onVisibleChanged: {
        if (visible)
            startLevel()
        else
            endLevel()
    }

    Rectangle {
        id: sky
        width: parent.width
        height: mainRoot.height - mainRoot.height * 0.3
        color: "lightcyan"
        anchors.top: parent.top
    }

    Rectangle {
        id: terrain
        width: parent.width
        height: mainRoot.height - sky.height
        color: "brown"
        anchors.top: sky.bottom
    }

    Image {
        id: mountain1
        width: 64*mm
        height: width * 102/206
        source: "qrc:/images/mountain1.png"
        x: mainRoot.width * 0.4
        y: sky.height - mountain1.height
        mipmap: true
    }

    Image {
        id: mountain2
        width: 48*mm
        height: width * 103/184
        x: mainRoot.width * 1.2
        y: sky.height - mountain2.height
        source: "qrc:/images/mountain2.png"
        mipmap: true
    }

    Image {
        id: mountain3
        width: 65*mm
        height: width * 40/153
        x: mainRoot.width * 2.7
        y: sky.height - mountain3.height
        source: "qrc:/images/mountain3.png"
        mipmap: true
    }

    /*Image {
        id: stone1
        width: 12*mm; height: width * 20/48
        x: mainRoot.width * 1.4
        y: sky.height + truck.height/2 - stone1.height
        source: "qrc:/images/stone.png"; mipmap: true
    }*/

    Person2 {
        id: truck
        width: 65*mm//38*mm
        height: width * 304/332
        //x: mainRoot.width-truck.width
        //y: sky.height-height/2
        jumpLenght: height*0.5
        jumpAnimationRotation: true
        imgName: "truck"
        worldLimitLeft: 0
        worldLimitRight: parent.width
        worldLimitSky: 0
        worldLimitGround: ground

        onImHit: truckLifeBar.hit()

        onImDestroyed: {
            //heroLifeBar.hitDestroy()
            levelFailed()
        }

        function reset(){
            truck.x = mainRoot.width-truck.width
            truck.y1 = ground
            truck.dir = 1
            maxLife = 5
            lifeInit()
            truckLifeBar.lifeBarInit(truck.life)
        }
    }

    Person2 {
        id: girl
        imgName: "girl"

        worldLimitLeft: 0
        worldLimitRight: parent.width
        worldLimitSky: 0
        worldLimitGround: ground

        function reset() {
            dir = 1
            x = 0
            y1 = ground
            girl.visible = true
        }
    }

    Person2 {
        id: hero
        width: 18*mm
        height: width * 320/156
        worldLimitLeft: 0
        worldLimitRight: mainRoot.width
        worldLimitSky: 0
        worldLimitGround: ground
        function reset(){
            x = 0
            y1 = ground
            hero.dir = 1
            hero.visible = true
        }
    }


    PersonCharacter {
        id: flagEndOfLevel
        width: height * 124/350
        height: 80*mm //25*mm
        x: mainRoot.width * 10.5
        y: ground - height
        imgName: "flag.png" // to change later
    }

    InterfaceWalkPartner {
        id: interfaceWalkGirlHero
        //leader: hero
        //follower: girl
        //call function connect() that will make the girl walk, following the hero
    }

    /*CheckPersonNear {
        id: checkTruckStone1Bump
        me: stone1
        target: truck
        distanceNear: truck.width/2 //(truck.width+me.width)*0.95
        onTargetChangedNear: {
            truck.jump()
        }
        isEnabled: screenNumber === 1
        debugInfo: "truck rock1 bump"
    }*/

    CheckPersonNear{
        id: checkHeroTruck
        me: truck
        target: hero
        onTargetChangedNear: {
            createDialogHeroTruck()
            truckMessage.visible = true
        }
        onTargetChangedFar: {
            truckMessage.visible = false
        }

        isEnabled: screenNumber === 0
        debugInfo: "hero truck"
    }

    CheckPersonNear {
        id: checkFlagHeroDistance
        me: flagEndOfLevel
        //distanceNear: truck.width
        target: truck
        onTargetChangedNear: {
            if (badJoeObj === null)
            levelWin()
        }
        isEnabled: screenNumber === 10
        debugInfo: "target flag end of level"
    }


    //call this function when visible change to true
    function startLevel() {
        console.log("startLevel06: "+activeEnemiesList.count)
        activeEnemiesList.clearEnemies()
        leaderObj = hero
        panelEOL.visible = false
        //createItems()

        truck.reset()
        hero.reset()
        girl.reset()
        //worm.reset()
        createConnections()

        createItems()
    }

    //call this function when visible change to false or level ends
    function endLevel() {
        console.log("endLevel06: "+activeEnemiesList.count)
        activeEnemiesList.clearEnemies()
        destroyConnections()
        destroyItems()
    }


    function createItems() {

        for (var i = 1; i < 7;i++) {
            //console.log("i: "+i)
            var wormObj = lib.createItem("level06/DesertWorm.qml", parent)
            wormObj.name = "worm"+i
            wormObj.i = i
            //console.log("wormObj.aux: "+wormObj.i)
            wormObj.x = mainRoot.width*(i+1)-wormObj.width
            wormObj.y = ground - wormObj.height            
            wormObj.screenNumber = Qt.binding( function(){return screenNumber})
            destroyItems.connect( wormObj.destroy )
            wormObj.deltaHit = truck.width
            wormObj.enemyTarget = Qt.binding( function(){return truck})
            wormObj.destroyTarget.connect( levelFailed )
            wormObj.removeFromEnemiesList.connect( activeEnemiesList.removeEnemy )
            wormObj.addToEnemiesList.connect( activeEnemiesList.addEnemy )
            wormObj.showExplosionOnDestroy = true
            wormObj.explosionDuration = 300
        }

        for (i = 4; i < 7;i++) {
            //console.log("i: "+i)
            var wormObj2 = lib.createItem("level06/DesertWorm.qml", parent)
            wormObj2.imageName = "desertWorm2.png"
            wormObj2.name = "worm2"+i
            wormObj2.i = i
            //console.log("wormObj.aux: "+wormObj.i)
            wormObj2.x = mainRoot.width*(i+1)-wormObj2.width+mainRoot.width*0.5
            wormObj2.y = ground - wormObj2.height
            wormObj2.screenNumber = Qt.binding( function(){return screenNumber})
            destroyItems.connect( wormObj2.destroy )
            wormObj2.deltaHit = truck.width
            wormObj2.enemyTarget = Qt.binding( function(){return truck})
            wormObj2.destroyTarget.connect( levelFailed )
            wormObj2.removeFromEnemiesList.connect( activeEnemiesList.removeEnemy )
            wormObj2.addToEnemiesList.connect( activeEnemiesList.addEnemy )
            wormObj2.showExplosionOnDestroy = true
            wormObj2.explosionDuration = 300
        }



        for (var j = 0;j < 7;j++) {
            //var rockObj = lib.createItem("PersonCharacter.qml", parent)
            var rockObj = lib.createItem("components/PersonCharacter.qml", parent)
            rockObj.width = 12*mm
            rockObj.height = 12*mm * 20/48
            rockObj.x = mainRoot.width*j + mainRoot.width * lib.getRandom(1,7)/10
            rockObj.y = sky.height + truck.height/2 - rockObj.height//sky.height-truck.height/2 + truck.height
            rockObj.imgName = "stone.png"
            destroyItems.connect( rockObj.destroy )


            var checkNear = lib.createItem("libraries/CheckPersonNear.qml", parent)
            checkNear.me = rockObj
            checkNear.target = truck
            checkNear.distanceNear = truck.width/2
            checkNear.i = j
            checkNear.screenNum = Qt.binding( function(){return screenNumber})
            //checkNear.isEnabled = Qt.binding( function(){ return  (checkNear.screenNum === checkNear.i) } )//doesnt work ?
            checkNear.targetChangedNear.connect( truck.jump )
            checkNear.debugInfo = "checknear debugInfo"+j
            destroyItems.connect( checkNear.destroy )
        }


        createBadJoeSteamer()
    }


    function createBadJoeSteamer() {
        //dragon---------------------------------------------
        badJoeObj = lib.createItem("Dragon2.qml", parent)
        badJoeObj.imageName = "steamerBadJoe.png"
        badJoeObj.xPos = mainRoot.width * 7.5
        badJoeObj.yPos = mainRoot.height * 0.1
        badJoeObj.fire.connect( createBadJoeShot )
        badJoeObj.inMotion = Qt.binding( function(){ return  screenNumber === 7 } )
        badJoeObj.active = false
        badJoeObj.removeFromEnemiesList.connect( activeEnemiesList.removeEnemy )
        badJoeObj.addToEnemiesList.connect( activeEnemiesList.addEnemy )
        badJoeObj.enemyTarget = truck
        badJoeObj.destroyTarget.connect(levelFailed)
        destroyItems.connect( badJoeObj.destroy )
        badJoeObj.lifePower = 5

        badJoeLifeBar.lifeBarInit(badJoeObj.lifePower)
        badJoeObj.imHitDamageMade.connect( badJoeLifeBar.projectileDamage )
        //dragon---------------------------------------------
    }


    NumberAnimation {
        id: moveBadJoeToAnotherScreen
        duration: 1000
    }

    onScreenNumberChanged: {
        //console.log("level 6, I'm on screen number: "+screenNumber)
        if (screenNumber === 7) {
            badJoeObj.inMotion = true
            badJoeObj.active = true
        }
        if (badJoeObj !== null && badJoeObj.active === true)
        moveBadJoe2AnotherScreen()
    }

    function moveBadJoe2AnotherScreen(){
        moveBadJoeToAnotherScreen.target = badJoeObj
        moveBadJoeToAnotherScreen.property = "xPos"
        moveBadJoeToAnotherScreen.to = mainRoot.width * (0.5+screenNumber)
        moveBadJoeToAnotherScreen.start()
    }

    function createTruckShot(touchX, touchY) {
        var truckShotObj = lib.createProjectile(touchX, touchY, truck.x + truck.width/2, truck.y, screenNumber, 0, parent)
        if (truckShotObj === null)
            return;
        truckShotObj.width = 8*mm
        truckShotObj.height = 8*mm
        truckShotObj.color = "brown"
        truckShotObj.border.width = 1*mm
        truckShotObj.border.color = "black"
        //truckShotObj.velocity = 4*mm
    }


    function createBadJoeShot(xx, yy) {
        //console.log("create dragon fire....................................")
        var steamerFireObj = lib.createItem("ProjectileEnemy.qml", parent)

        steamerFireObj.xMinBulletDestroy = mainRoot.width * screenNumber
        steamerFireObj.xMaxBulletDestroy = mainRoot.width * screenNumber + mainRoot.width
        steamerFireObj.yMinBulletDestroy = 0
        steamerFireObj.yMaxBulletDestroy = mainRoot.height

        steamerFireObj.bulletTarget = truck
        steamerFireObj.width = 8*mm
        steamerFireObj.height = 8*mm
        //steamerFireObj.init(xx, yy)
        steamerFireObj.setMidPosXY(xx, yy)
        //console.log("hero.x: "+heroObj.x+" hero.y: "+heroObj.y+" xx: " +xx+" yy: "+yy)
        steamerFireObj.setAngle(truck.midX-xx, truck.midY-yy)
        steamerFireObj.running = true
        steamerFireObj.hit.connect( truck.imHit )
        destroyItems.connect( steamerFireObj.destroy )
    }

    onDestroyItems: {

    }

    function createConnections() {
        //console.log("truck create connections")
        //moves the scene when the truck reachs the end of the screen
        flickLevel.contentX = Qt.binding( function(){
            return Math.floor(leaderObj.x / flickLevel.width) * flickLevel.width} )

        lib.connectCommandWalk(commandTouchPad, leaderObj)
        lib.connectCommandWalk(commandKeyb, leaderObj)

        lib.createLeaderFollowerConnection(interfaceWalkGirlHero, hero, girl)
    }

    function destroyConnections() {
        //console.log("truck remove connections")
        flickLevel.contentX = 0

        lib.disconnectCommandWalk(commandTouchPad, leaderObj)
        lib.disconnectCommandWalk(commandKeyb, leaderObj)

        deleteTruckShotFireConnection()

        lib.deleteLeaderFollowerConnection(interfaceWalkGirlHero, hero, girl)
    }


    function createTruckShotFireConnection(){
        // truck shot with touchscreen-----------------------------------
        commandTouchPad.shotFiredXY.connect( createTruckShot )
    }

    function deleteTruckShotFireConnection(){
        // truck shot with touchscreen-----------------------------------
        commandTouchPad.shotFiredXY.disconnect( createTruckShot )
    }

    MessageBoard{
        id: truckMessage
        anchors.top: parent.top
        anchors.margins: defaultMargins
        x: mainRoot.width*0.5 - truckMessage.width/2
        //text: qsTr("You'll need 10 gallons of fuel to travel.")
        pixelSize: 10*mm
        horizontalAlignment: Text.AlignHCenter //Text.AlignJustify
        verticalAlignment: Text.AlignVCenter
        visible: false
        showCancelBtn: true
        onOk: {
            truckMessage.visible = false
            //todo: put the truck running ....
            heroMap.removeFuel(travelCost)
            hero.visible = false
            girl.visible = false

            destroyConnections()
            leaderObj = truck
            createConnections()

            createTruckShotFireConnection()
            //createBadJoeSteamer()
        }
        onCancel: truckMessage.visible = false
    }


    property int travelCost: 10

    function createDialogHeroTruck() {
        //var travelCost = 10;
        if (!heroMap.fuelCan){
            truckMessage.showCancelBtn = true
            truckMessage.showOkBtn = false
            truckMessage.bntCancelText = qsTr("Ok")
            truckMessage.text = qsTr("You'll need a fuel can to use the truck.\nYou need to find one!!!");
            return;
        }

        if (heroMap.fuel < travelCost) {
            truckMessage.showCancelBtn = true
            truckMessage.showOkBtn = false
            truckMessage.bntCancelText = qsTr("Ok")
            truckMessage.text = qsTr("You need") +" "+travelCost +" "+ qsTr("gallons to travel, and you have") +" "+ heroMap.fuel + ".\n" +
                    qsTr("Try to earn some fuel before travelling.");
            return;
        }

        if (heroMap.fuel >= travelCost) {
            truckMessage.showCancelBtn = true
            truckMessage.showOkBtn = true
            truckMessage.bntCancelText = qsTr("No")
            truckMessage.bntOkText = qsTr("Yes")
            truckMessage.text = qsTr("You need") +" "+travelCost +" "+ qsTr("gallons to travel, and you have") +" "+ heroMap.fuel + ".\n" +
                    qsTr("Continue ?");
            return;
        }

        /*else {
            truckMessage.showCancelBtn = true
            truckMessage.showOkBtn = false
            truckMessage.bntCancelText = qsTr("Ok")
            truckMessage.text = qsTr("You need "+travelCost +" to travel, and you have "+heroFuel+ " in your fuelcan.\nTry to earn some money before travelling.");
        }*/
    } //createDialogHeroTruck()


}
