import QtQuick3D

Node {
    id: tree

    Model {
        id: treeTrunk
        source: "#Cylinder"
        scale: Qt.vector3d(0.4, 1.5, 0.4)
        materials: [ DefaultMaterial {diffuseColor: "brown"}]
    }
    Model {
        id: treeTop
        source: "#Sphere"
        y: 160
        scale: Qt.vector3d(2.2, 1.8, 2.2)
        materials: [ DefaultMaterial {diffuseColor: "lightgreen"}]
    }
}
