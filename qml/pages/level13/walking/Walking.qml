import QtQuick3D
import QtQuick 2.15
import QtQuick.Timeline 1.0

Node {
    id: rootNode


    Model {
        id: boy01_Body_Geo
        source: "meshes/boy01_Body_Geo.mesh"

        DefaultMaterial {
            id: boy01_Body_MAT_material
            diffuseColor: "#ffcccccc"
        }
        materials: [
            boy01_Body_MAT_material
        ]
    }

    Model {
        id: h_Geo
        source: "meshes/h_Geo.mesh"

        DefaultMaterial {
            id: boy01_Mouth_MAT1_material
            diffuseColor: "#ffcccccc"
        }
        materials: [
            boy01_Mouth_MAT1_material
        ]
    }

    Model {
        id: boy01_Eyes_Geo
        source: "meshes/boy01_Eyes_Geo.mesh"

        DefaultMaterial {
            id: boy01_Eyes_MAT1_material
            diffuseColor: "#ffcccccc"
        }
        materials: [
            boy01_Eyes_MAT1_material
        ]
    }

    Model {
        id: boy01_Brows_Geo
        source: "meshes/boy01_Brows_Geo.mesh"
        materials: [
            boy01_Eyes_MAT1_material
        ]
    }

    Timeline {
        id: timeline0
        startFrame: 0
        endFrame: 0
        currentFrame: 0
        enabled: true
        animations: [
            TimelineAnimation {
                duration: 0
                from: 0
                to: 30
                running: true
            }
        ]
    }
}
