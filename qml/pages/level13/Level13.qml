//import QtQuick 2.0
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick3D
import Placement 1.0
import "boy"
import "walking"

import "../"
import "../components"
import "../libraries"

Rectangle {
    id: teste
    color: "grey"
    width: mainRoot.width
    height: mainRoot.height

    signal menu()
    /*MouseArea{
        anchors.fill: parent
        onDoubleClicked: menu()
    }*/

    //property alias obj: brickManPlacement
    property alias obj: camPlacement//hero//snowMan
    //property alias obj: snowMan

    property real rotateDelta: 1
    property real moveDelta: 2

    signal moveX(real value)
    signal moveY(real value)
    signal moveZ(real value)
    signal rotateX(real value)
    signal rotateY(real value)
    signal rotateZ(real value)
    signal jump()

    onMoveX: placement.moveFront(value)
    onMoveY: placement.moveRight(value)
    onMoveZ: placement.moveUp(value)

    onRotateX: placement.pitch(value)
    onRotateY: placement.roll(value) //obj.rotation = qmult(obj.rotation, angleAxisToQuat(value, 0, 1, 0))
    onRotateZ: placement.yaw(value)//obj.rotation = qmult(obj.rotation, angleAxisToQuat(value, 0, 0, 1))

    onJump: {
        console.log("jump..................................")
        if (!jumpAnimation.running)
            jumpAnimation.start()
    }


    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_Up() { moveX(moveDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_Down() { moveX(-moveDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_Left() { rotateZ(rotateDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_Right() { rotateZ(-rotateDelta) } }

    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_W() { moveX(moveDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_S() { moveX(-moveDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_A() { moveY(-moveDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_D() { moveY(moveDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_R() { moveZ(moveDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_F() { moveZ(-moveDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_4() { rotateZ(rotateDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_6() { rotateZ(-rotateDelta) } }

    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_Y() { rotateX(rotateDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_H() { rotateX(-rotateDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_U() { rotateY(rotateDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_J() { rotateY(-rotateDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_I() { rotateZ(rotateDelta) } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_K() { rotateZ(-rotateDelta) } }

    Connections { target: commandKeyb; enabled: parent.visible; function onKeyIsPressed_Space() { jump() } }
    Connections { target: commandKeyb; enabled: parent.visible; function onKeyPressed(v) { keyPressed(v) }}

    Connections{
        target: placement
        function onRotationChanged() {obj.rotation = placement.rotation}
    }
    Connections{
        target: placement
        function onPositionChanged() {obj.position = placement.position}
    }

    View3D {
        id: view3D
        visible: true
        width: mainRoot.width
        height: mainRoot.height
        renderMode: View3D.Offscreen


        /*function updateCamera() {
            camPlacement.rotation = snowManPlacement.rotation
            pAux.rotation = snowManPlacement.rotation
            pAux.position = snowManPlacement.position
            pAux.moveFront(-100)
            camPlacement.position = pAux.position
        }

        Placement {
            id: pAux
        }*/

        onVisibleChanged: {
            if (visible) {
               // commandKeyb.bAutoRepeat = true
            }
            else {
               // commandKeyb.bAutoRepeat = false
            }
        }

        Placement{
            id: placement
            position: obj.position
            rotation: obj.rotation
        }


        environment: SceneEnvironment {
            //backgroundMode: SceneEnvironment.Color
            backgroundMode:  SceneEnvironment.SkyBox
            antialiasingMode: SceneEnvironment.MSAA
            antialiasingQuality: SceneEnvironment.VeryHigh
            //aoStrength: 100//aoEnabled ? 100 : 0
            //probeBrightness: 500//lightBrightness.value

            lightProbe: Texture {
                source: "qrc:/skybox/skybox.hdr"
                //source: "qrc:/skybox/small_rural_road.hdr"
                mappingMode: Texture.LightProbe
            }
           // clearColor: "skyblue"
        }




        //! [camera]
        PerspectiveCamera {
            id: cam
            position: camPlacement.position
            rotation: camPlacement.rotation
            clipFar: 4200
            clipNear: 1

            Placement{
                id: camPlacement
                position: Qt.vector3d(0, 0, 500)
                //Component.onCompleted: roll(90)
            }
        }
        //! [camera]

        //! [light]
        DirectionalLight {
            eulerRotation.x: -45
           // eulerRotation.y: 70
        }
        //! [light]

        /*Model {
            id: ball
            //source: "#Cylinder"
            source: "#Cylinder"
            scale: Qt.vector3d(1, 1, 1)
            materials: [ DefaultMaterial {
                    diffuseColor: "red"
                }
            ]
            position: Qt.vector3d(-100, -150, 0)
            onPositionChanged: console.log(position)
            //visible: false
        }*/

        Hero {
            id: hero
            position: Qt.vector3d(125, -150, 0)
            animateArms: commandTouchPad.showCommand || commandKeyb.key_Up ||
                         commandKeyb.key_Down || commandKeyb.key_Left ||
                         commandKeyb.key_Right || commandKeyb.key_Space
        }

        SnowMan {
            id: snowMan
            position: Qt.vector3d(150, 0, 50)

        }

        Brickman {
            id: brickMan
            visible: false
        }

        Boy {
            id: boyy
            //://level13/boy/meshes/boy01_Eyes_Geo.mesh
        }

        /*Walking{

        }*/

        /*Model {
            id: ground
            position: Qt.vector3d(0, 0, -50)
            source: "#Rectangle"
            scale: Qt.vector3d(10, 10, 10)
            materials: [ DefaultMaterial {
                    diffuseColor: "green"
                }
            ]
            //visible: false
        }*/

        Tree3D {
            id: tree
            position: Qt.vector3d(0,0, -300)
        }


        // camera-----------------------------
        SequentialAnimation {
            id: jumpAnimation
            alwaysRunToEnd: false
            property real jumpTimeDuration: 1000
            property real jumpHeight: 200
            property real jumpInitialPos
            property real jumpTopPos
            property real jumpPos
            property real jumpPos_previous
            property real delta
            onJumpPosChanged: {
                console.log("delta: "+delta)
                moveZ(jumpPos-jumpPos_previous)
                jumpPos_previous = jumpPos
            }
            onStarted: {
                console.log("gonna jump............................................")
                jumpPos = jumpPos_previous = jumpInitialPos = placement.position.z
                jumpTopPos = jumpInitialPos + jumpHeight
            }
            onStopped: placement.setPosition(placement.position.x, placement.position.y, jumpInitialPos)
            NumberAnimation {
                target: jumpAnimation; property: "jumpPos";
                to: jumpAnimation.jumpTopPos;
                duration: jumpAnimation.jumpTimeDuration/2
                easing {  type: Easing.OutQuad }
            }
            //start falling
            NumberAnimation {
                target: jumpAnimation; property: "jumpPos";
                to: jumpAnimation.jumpInitialPos
                duration: jumpAnimation.jumpTimeDuration/2
                easing { type: Easing.InQuad }
            }
        }// camera-----------------------------

    }//View3D

    CommandMultiTouchPad4 {
       id: commandTouchPad
       anchors.left: parent.left
       anchors.bottom: parent.bottom
       anchors.margins: defaultMargins
       onMoveUp: moveZ(-moveDelta)
       onMoveDown: moveZ(moveDelta)
       onMoveRight: rotateX(-rotateDelta)
       onMoveLeft: rotateX(rotateDelta)

       onPoint1Clicked: jump()
       onPoint2Clicked: jump()

       onPoint1IsPressed: console.log("Point1IsPressed: "+ cx + " : "+cy)
    }

    function keyPressed(key) {
        console.log(key)

        if (key === Qt.Key_F7){
            changeVelocity()
        }
    }

    function changeVelocity() {
        if (moveDelta === 2)
            moveDelta = 4
        else
            moveDelta = 2
    }

}
