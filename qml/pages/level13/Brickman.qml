import QtQuick 2.0
import QtQuick3D

Node {
    id: brickman

    position: Qt.vector3d(0, 0, 0)
    Model {
        id: head
        position: Qt.vector3d(0, 0, 125)
        source: "#Sphere"
        scale: Qt.vector3d(0.75, 0.75, 0.75)
        materials: [ DefaultMaterial {diffuseColor: "yellow"}]
    }

    Model {
        id: body
        position: Qt.vector3d(0, 0, 60)
        source: "#Cube"
        scale: Qt.vector3d(0.375, 0.75, 0.75)
        materials: [ DefaultMaterial {diffuseColor: "green"}]
    }

    Model {
        id: armRight
        position: Qt.vector3d(0, -50, 70)
        source: "#Cube"
        scale: Qt.vector3d(0.375, 0.25, 0.5)
        materials: [ DefaultMaterial {diffuseColor: "yellow"}]
    }

    Model {
        id: armLeft
        position: Qt.vector3d(0, 50, 70)
        source: "#Cube"
        scale: Qt.vector3d(0.375, 0.25, 0.5)
        materials: [ DefaultMaterial {diffuseColor: "yellow"}]     
    }


    Model {
        id: legLeft
        position: Qt.vector3d(0, 50, 70)
        source: "#Cube"
        scale: Qt.vector3d(0.25, 0.25, 0.5)
        materials: [ DefaultMaterial {diffuseColor: "yellow"}]
    }

/*
    Model {
        position: Qt.vector3d(0, -250, 0)
        source: "#Cube"
        scale: Qt.vector3d(1, 1, 1)

        materials: [ DefaultMaterial {
                diffuseColor: "seagreen"
            }
        ]
    }

    Model {
        position: Qt.vector3d(0, 120, 0)
        source: "#Sphere"
        scale: Qt.vector3d(1, 1, 1)

        materials: [ DefaultMaterial {
                diffuseColor: "yellow"
            }
        ]
    }

    Model {
        position: Qt.vector3d(0, 0, -100)
        source: "#Cube"
        scale: Qt.vector3d(1.25, 1.5, 1)

        materials: [ DefaultMaterial {
                diffuseColor: "limegreen"
            }
        ]
    }

    Model {
        position: Qt.vector3d(-85, 0, -100)
        source: "#Cube"
        scale: Qt.vector3d(0.5, 1.5, 1)

        materials: [ DefaultMaterial {
                diffuseColor: "yellow"
            }
        ]
    }

    Model {
        position: Qt.vector3d(85, 0, -100)
        source: "#Cube"
        scale: Qt.vector3d(0.5, 1.5, 1)

        materials: [ DefaultMaterial {
                diffuseColor: "yellow"
            }
        ]
    }*/

}
