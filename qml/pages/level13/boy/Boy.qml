import QtQuick3D
import QtQuick 2.15
import QtQuick.Timeline 1.0

Node {
    id: rootNode

    PointLight {
        id: light
        x: -407.625
        y: 590.386
        z: 100.545
        eulerRotation.x: 137.534
        eulerRotation.y: -72.7792
        eulerRotation.z: -10.7437
        color: "#ffffffff"
        quadraticFade: 2.22222e-07
    }

    PerspectiveCamera {
        id: camera
        x: -735.889
        y: 495.831
        z: -692.579
        eulerRotation.x: 26.4407
        eulerRotation.y: 46.6919
        eulerRotation.z: -7.46827e-06
        fieldOfView: 39.5978
        fieldOfViewOrientation: PerspectiveCamera.Horizontal
    }


    Model {
        id: boy01_Eyes_Geo
        eulerRotation.x: -180
        eulerRotation.y: -5.00896e-06
        eulerRotation.z: 180
        source: "meshes/boy01_Eyes_Geo.mesh"

        DefaultMaterial {
            id: boy01_Eyes_MAT1_material
        }
        materials: [
            boy01_Eyes_MAT1_material
        ]
    }

    Model {
        id: boy01_Body_Geo
        eulerRotation.x: -180
        eulerRotation.y: -5.00896e-06
        eulerRotation.z: 180
        source: "meshes/boy01_Body_Geo.mesh"

        DefaultMaterial {
            id: boy01_Body_MAT_material
        }
        materials: [
            boy01_Body_MAT_material
        ]
    }

    Model {
        id: boy01_Brows_Geo
        eulerRotation.x: -180
        eulerRotation.y: -5.00896e-06
        eulerRotation.z: 180
        source: "meshes/boy01_Brows_Geo.mesh"
        materials: [
            boy01_Eyes_MAT1_material
        ]
    }

    Model {
        id: h_Geo
        eulerRotation.x: -180
        eulerRotation.y: -5.00896e-06
        eulerRotation.z: 180
        source: "meshes/h_Geo.mesh"
        materials: [
            boy01_Eyes_MAT1_material
        ]
    }

    Timeline {
        id: timeline0
        startFrame: 0
        endFrame: 30
        currentFrame: 15
        enabled: true

        animations: [
            TimelineAnimation {
                duration: 5000
                from: 0
                to: 30
                running: true
            }
        ]
    }
}
