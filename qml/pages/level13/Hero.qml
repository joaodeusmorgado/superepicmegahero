import QtQuick 2.0
import QtQuick3D

Node {
    id: hero

    property bool animateArms: true
    property color bodyColor: "darkgrey"

    Model {
        id: head
        source: "#Sphere"
        z: 136//100*0.64 + 100*.52
        scale: Qt.vector3d(0.52, 0.52, 0.52)
        materials: [ DefaultMaterial {diffuseColor: "white"}]
    }    
    Model {
        id: eyeLeft
        source: "#Sphere"
        z: head.z+7
        x: 20
        y: -10
        scale: Qt.vector3d(0.1, 0.1, 0.1)
        materials: [ DefaultMaterial {diffuseColor: "lightblue"}]
        eulerRotation.y: 20
    }
    Model {
        id: eyeRight
        source: "#Sphere"
        z: eyeLeft.z
        x: eyeLeft.x
        y: -eyeLeft.y
        scale: Qt.vector3d(0.1, 0.1, 0.1)
        materials: [ DefaultMaterial {diffuseColor: "lightblue"}]
        eulerRotation.y: 20
    }



    Model {
        id: body
        source: "#Cylinder"
        z: 80
        //scale: Qt.vector3d(0.64, 0.64, 0.64)
        scale: Qt.vector3d(0.1, 0.6, 0.1)
        eulerRotation.x: 90
        materials: [ DefaultMaterial {diffuseColor: bodyColor}]
    }

    Node {
        id: nodeLegLeft
        x: 0
        z: 50
        y: 5
        Model {
            id: legLeft
            source: "#Cylinder"
            x: 0
            z: -50
            y: Math.cos(eulerRotation.x * Math.PI / 180) * 50
            scale: Qt.vector3d(0.1, 1, 0.1)
            materials: [ DefaultMaterial {diffuseColor: bodyColor}]
            eulerRotation.x: -80
        }
        SequentialAnimation on eulerRotation.y {
            loops: Animation.Infinite
            running: animateArms
            NumberAnimation {
                to: 20
                duration: 1500
                easing.type: Easing.InOutQuad
            }
            NumberAnimation {
                to: -20
                duration: 1500
                easing.type: Easing.InOutQuad
            }
        }
    }

    Node {
        id: nodeLegRight
        x: 0
        z: nodeLegLeft.z
        y: -nodeLegLeft.y
        Model {
            id: legRight
            source: "#Cylinder"
            x: legLeft.x
            z: legLeft.z
            y: -legLeft.y
            scale: legLeft.scale//Qt.vector3d(0.1, 1, 0.1)
            materials: [ DefaultMaterial {diffuseColor: bodyColor}]
            eulerRotation.x: legLeft.eulerRotation.x * -1
        }
        eulerRotation.y: nodeLegLeft.eulerRotation.y * -1
    }


    //--arms--------------------------------------------------------------

    Node {
        id: nodeArmLeft
        x: 0
        z: 115
        y: 0
        Model {
            id: armLeft
            source: "#Cylinder"
            x: 0//-4
            z: -50
            y: 2+Math.cos(eulerRotation.x * Math.PI / 180) * 50
            scale: Qt.vector3d(0.1, 0.7, 0.1)
            materials: [ DefaultMaterial {diffuseColor: bodyColor}]
            eulerRotation.x: -75
        }
        //eulerRotation.y: -15
        /*SequentialAnimation on eulerRotation.y {
            loops: Animation.Infinite
            running: animateArms
            NumberAnimation {
                to: -10
                duration: 1500
                easing.type: Easing.InOutQuad
            }
            NumberAnimation {
                to: 10
                duration: 1500
                easing.type: Easing.InOutQuad
            }
        }*/
    }

    Node {
        id: nodeArmRight
        x: 0
        z: nodeArmLeft.z
        y: -nodeArmLeft.y
        Model {
            id: armRight
            source: "#Cylinder"
            x: armLeft.x
            z: armLeft.z
            y: -armLeft.y
            scale: armLeft.scale//Qt.vector3d(0.1, 1, 0.1)
            materials: [ DefaultMaterial {diffuseColor: bodyColor}]
            eulerRotation.x: armLeft.eulerRotation.x * -1
        }
        eulerRotation.y: nodeArmLeft.eulerRotation.y
    }

    /*Model {
        id: armLeft
        source: "#Cylinder"
        x: 8
        z: body.z
        y: 15
        scale: Qt.vector3d(0.1, 0.4, 0.1)
        materials: [ DefaultMaterial {diffuseColor: "grey"}]
        eulerRotation.x: -60
        eulerRotation.y: -30

        SequentialAnimation on eulerRotation.y {
            loops: Animation.Infinite
            running: animateArms
            NumberAnimation {
                to: -120
                duration: 1500
                easing.type: Easing.InOutQuad
            }
            NumberAnimation {
                to: -30
                duration: 1500
                easing.type: Easing.InOutQuad
            }
        }
    }

    Model {
        id: armRight
        source: "#Cylinder"
        x: armLeft.x
        z: body.z
        y: -armLeft.y
        scale: Qt.vector3d(0.1, 0.4, 0.1)
        materials: [ DefaultMaterial {diffuseColor: "grey"}]
        eulerRotation.x: 60
        eulerRotation.y: -30

        SequentialAnimation on eulerRotation.y {
            loops: Animation.Infinite
            running: animateArms
            NumberAnimation {
                to: -120
                duration: 1500
                easing.type: Easing.InOutQuad
            }
            NumberAnimation {
                to: -30
                duration: 1500
                easing.type: Easing.InOutQuad
            }
        }
    }*/

}//snowMan
