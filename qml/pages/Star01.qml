import QtQuick 2.3

Item {
    id:star
    width: 6*mm
    height: width

    signal destroyItem()
    onDestroyItem: {
        //console.log("star destroy")
        star.destroy()
    }

    Image {
        id: starImage
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        property string num: Math.floor((Math.random() * 3) + 1);
        source: "qrc:/images/estrela0"+( num ).toString()+".png"
        mipmap: true
    }
}
