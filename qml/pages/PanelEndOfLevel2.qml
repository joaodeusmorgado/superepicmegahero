import QtQuick 2.0
import "components"

Rectangle {
    id: root
    width: mainRoot.width / 2
    height: mainRoot.height / 2
    color: "blue"//"purple"
    opacity: 0.75
    radius: 5*mm
    property alias text: tex.label
    property alias showNextBtn: btnNextLevel.visible
    property alias pixelSize: tex.pixelSize
    property alias horizontalAlignment: tex.horizontalAlignment
    property alias verticalAlignment: tex.verticalAlignment

    signal replay()
    signal menu()
    signal next()

    Flickable {
        id: flick
        clip: true
        anchors.top: parent.top
        anchors.topMargin: defaultMargins//imgLeft.visible || imgRight.visible ? imgSize : defaultMargins

        anchors.left: parent.left
        anchors.leftMargin: defaultMargins
        anchors.right: parent.right
        anchors.rightMargin: defaultMargins

        flickableDirection: Flickable.VerticalFlick

        height: parent.height - btnReplay.height - defaultMargins*3
        contentHeight: tex.height


        TextTemplate {
            id: tex
            anchors.top: parent.top
            anchors.topMargin: defaultMargins//imgLeft.visible || imgRight.visible ? imgSize : defaultMargins

            //anchors.bottom: btnNext.top
            //anchors.bottomMargin: defaultMargins

            anchors.left: parent.left
            anchors.leftMargin: defaultMargins
            anchors.right: parent.right
            anchors.rightMargin: defaultMargins

            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignHCenter //Text.AlignJustify
            pixelSize: 18*mm
            color: "orange"
            label: qsTr("Victory")
        }
    }//Flickable



    MyButton {
        id: btnReplay
        backGrdColor: "red"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        anchors.left: parent.left
        anchors.leftMargin: parent.width/4 - width
        image: "btnReplay.png"
        onBtnClick: replay()
    }


    MyButton {
        id: btnMenu
        backGrdColor: "red"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        anchors.horizontalCenter: parent.horizontalCenter
        image: "btnMenu.png"
        onBtnClick: menu()
    }

    MyButton {
        id: btnNextLevel
        backGrdColor: "red"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        anchors.right: parent.right
        anchors.rightMargin: parent.width/4 - width
        image: "arrowRight.png"
        onBtnClick: next()
    }


}

