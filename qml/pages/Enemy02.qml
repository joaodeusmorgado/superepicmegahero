import QtQuick 2.3
//import com.Timer 1.0

//todo: refactor to use enemyTemplate

Item {
    id: enemy02
    width: isBoss ? 80*mm : 30*mm
    height: width

    property point topLeft: Qt.point(x,y)
    property point bottomRight: Qt.point(x+width,y+height)

    property real life: isBoss ? 15 : 2
    property real hitDamage: 1
    property bool isBoss: false
    property bool timerRunning: y > spaceLimitYmin && y < spaceLimitYmax
    property variant enemyTarget //checks for collisions with the starship, if there's a collision, the starship is destroyed
    property bool alive: true
    property real explosionDuration: 1000
    property bool gamePaused: false

    signal fire(real fx, real fy)
    signal scoreUp(real score) //when the enemy is destroyed, you get to score
    signal bossDestroyed()
    signal collision()
    signal destroyItem()
    signal remove()
    signal imHit()


    onDestroyItem: {
        explosionImg.visible = true
        explosionTimer.running = true
    }

    property real count: 0
    Timer {
        id: explosionTimer
        interval: fps_ms
        running: !alive && !gamePaused
        repeat: true
        onTriggered: {
            count += fps_ms
            enemy02.opacity = 1 - count/explosionDuration
            if (count >= explosionDuration) {
                running = false
                alive = false
                remove()
                enemy02.destroy()
            }
        }
    }

    //onCollision: console.log("collision -------------------------------------------------------")

    //if the emeny is active (i.e. visible within the screen)
    //we need to add it to the active enemies list, bullets fired by the starship will
    //use this list to check for collisions
    //when the enemy is offscreen we can destroy it and remove it from the list
    onTimerRunningChanged: {
        if (timerRunning) {
            activeEnemiesList.addEnemy(enemy02)
            //console.log("added enemy to the list, size is "+activeEnemiesList.count)
        } else {
            destroyItem()
            //remove()
            //enemy02.destroy()
            //console.log("removed enemy from the list, size is "+activeEnemiesList.count)
        }
    }



    //enemy moves horizontally widthin spaceWidt limits
    property real spaceWidth: mainRoot.width

    //enemies should only fire when they are in screen bondaries
    property real spaceLimitYmin
    property real spaceLimitYmax

    signal setSpaceLimitsY(var ymin, var ymax)
    onSetSpaceLimitsY: {
        spaceLimitYmin = ymin;
        spaceLimitYmax = ymax;
    }



    Image {
        id: enemyImage
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/inimigo02.png"
        mipmap: true
    }


    Image {
        id: explosionImg
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/Explosion.png"
        visible: false
        mipmap: true
    }

    //enemies fire a shot at every 1 second
    Timer {
        id: timerShot
        interval: 1000
        running: timerRunning && !gamePaused
        repeat: true
        onTriggered: {
            fire(x+width/2, y+height*0.7)
            if (isBoss) {
                fire(x+width*0.8, y+height*0.7)
                fire(x+width*0.2, y+height*0.7)
            }
            //console.log("enemy02:"+spaceLimitYmin)
        }
    }


    property real animationDuration: 10000
    property real pos1
    property real pos2
    property real direction
    property real dist: 1*mm

    Component.onCompleted: {
        var rand = Math.random()*10
        if (rand > 5) {
            //pos1 = spaceWidth-enemy02.width
            //pos2 = 0
            direction = 1
        } else {
            //pos2 = spaceWidth-enemy02.width
            //pos1 = 0
            direction = -1
        }
    }

    onGamePausedChanged: {
        console.log("GamePaused: "+gamePaused)
        console.log("timerMovement: "+timerMovement.running)
    }

    Timer {
        id: timerMovement
        interval: fps_ms//Qt.platform.os === "linux" ? 40 : fps_ms
        running: timerRunning && !gamePaused //doenst work ???
        repeat: true
        onTriggered: {


            enemy02.x += dist * direction

            if (enemy02.x > spaceWidth-enemy02.width)
                direction = -direction
            if (enemy02.x < 0)
                direction = -direction

            if (enemyTarget === null)
                return;

            if ( lib.distance(enemyTarget.x+enemyTarget.width/2, enemyTarget.y+enemyTarget.height/2,
                          x+width/2, y+height/2, enemyTarget.height ) )
            {
                if (!enemyTarget.shieldActivated)//if spaceship shield is active, the bullet will not hit the spaceship
                    collision()

                //console.log("I was shot")
                destroyItem()
                //remove()
                //enemy02.destroy()
            }
        }
    }

    RotationAnimation on rotation {
        running: false
        duration: 4000
        loops: Animation.Infinite
        from: 0
        to: 360
    }

    //disable animations and use a Timer instead
    //Animations seem to cause velocity problems with the level01 main Timer

    /*SequentialAnimation on x {
        id: xAnimation
        running: timerRunning


        loops: Animation.Infinite
        PropertyAnimation {
            duration: animationDuration
            to: pos1
        }
        PropertyAnimation {
            duration: animationDuration
            to: pos2
        }
    }

    //animation only for boss
    SequentialAnimation on y {
        running: false//timerRunning && isBoss
        loops: Animation.Infinite
        PropertyAnimation {
            duration: animationDuration/5
            to: pos1/4
        }
        PropertyAnimation {
            duration: animationDuration/5
            to: pos2/4
        }
    }*/



    onImHit: {
        life -= hitDamage;
        if (life === 0) {
            scoreUp(isBoss ? 5000 : 1000) //if enemy is a boss sends 5000 bonus points, else sends 1000 points
            if (isBoss)
                bossDestroyed() // signal used to end the level, connect to the signal youWin
            //console.log("enemiesModel.count before destroy:"+activeEnemiesList.count)
            destroyItem()
            //console.log("enemy killed, size of list is "+activeEnemiesList.count)
            return true; //destroyed
        }
        else {
            return false; //still alive
        }
    }

}
