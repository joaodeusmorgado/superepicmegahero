import QtQuick 2.0
//import com.Timer 1.0
import QtSensors 5.0
import QtQuick.Window 2.2


Item {
    id: root

    property alias active: tilt.active

    property real valueX: tilt.reading ? tilt.reading.xRotation.toFixed(2) : 0
    property real valueY: tilt.reading ? tilt.reading.yRotation.toFixed(2) : 0

    property real moveHorizontal: Screen.primaryOrientation === Qt.LandscapeOrientation ? valueX : valueY
    property real moveVertical: Screen.primaryOrientation === Qt.LandscapeOrientation ? valueY : valueX


    signal move(real cx, real cy)
    signal direction(int direct)// -1->left ; 1->right
    //signal calibrate()

    TiltSensor {
        id: tilt
        active: false
        onReadingChanged: console.log("dataRate tilt command: "+dataRate)
        onActiveChanged: if (active) {
                             timerCalibrate.start()
                             console.log("TiltSensor dataRate: "+dataRate)
                         }
    }

    Timer {
        id: timerCalibrate
        interval: 100//fps_ms
        repeat: false
        running: false
        onTriggered: {
            tilt.calibrate()
        }
    }

    //property real accelHoriz: 1//2 - sensor reading apperar to be broken in new versions, change value to 1 ???
    //property real accelVert: moveVertical >=0 ? 2.5 : 3.5
    property real accelVert: 0.4
    //property real accelHoriz: 1

    Timer {
        id: timerMove; interval: fps_ms; repeat: true
        running: tilt.active
        onTriggered: {
            move(moveHorizontal /* * accelHoriz*/, moveVertical * accelVert)
        }
    }
    /*onValueXChanged: {
        //console.log("Tilt X changed: "+tilt.reading.xRotation.toFixed(2))
        move(moveHorizontal/accelHoriz, moveVertical/accelVert)
    }

    onValueYChanged: {
        //console.log("Tilt Y changed: "+tilt.reading.yRotation.toFixed(2))
        move(moveHorizontal/accelHoriz, moveVertical/accelVert)
    }*/

}
