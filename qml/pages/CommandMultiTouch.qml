import QtQuick 2.0
//import com.Timer 1.0

Item {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    //TODO: Outdated, to remove, use CommandMultiTouchPad

    //property real value: slider

    property real velocity: 12
    property real sliderValued: 1


    signal move(real cx, real cy)
    signal moveX(real deltaX)
    signal moveY(real deltaY)
    signal shotFired()
    signal direction(int direct)// -1->left ; 1->right

    //onMoveX: {
      //  console.log("onMoveX: " + deltaX)
    //}

    //Component.onCompleted: {
        //print("touchpad: "+ touchPadCenterX + " : " + touchPadCenterY)
    //}

    Button {
        id: btnFire
        width: 15*mm
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins * 4
    }

    property real prevX: 0
    property real prevY: 0
    property real acceleX: 1
    property real acceleY: 1
    property bool existPreviousXMove: false
    property bool existPreviousYMove: false

    MultiPointTouchArea {
        anchors.fill: parent
        touchPoints: [
            TouchPoint {
                id: point1
                onXChanged: {
                    print("x:"+x)
                    print("previousX:"+prevX)
                    var deltaX = x-prevX
                    print("deltaX:"+deltaX)

                    //move(x-startX, y-startY)
                    if (!existPreviousXMove)
                        prevX = x;
                    moveX( (x-prevX) * acceleX );
                    existPreviousXMove = true
                    prevX = x
                }

                onYChanged: {

                    if (!existPreviousYMove)
                        prevY = y;
                    moveY( (y-prevY) * acceleY );
                    existPreviousYMove = true
                    prevY = y
                }

                onPressedChanged: {

                    if (pressed) {
                        existPreviousXMove = false;
                        existPreviousYMove = false;
                        prevX = x;
                        prevY = y
                    }
                    //prevX = x;
                    //prevY = y;

                    //console.log("presses: "+pressed)
                    //console.log("x and y: "+pressed)
                    if (!pressed && !existPreviousXMove && !existPreviousYMove) {
                        shotFired();
                        /*if (x > btnFire.x && x < btnFire.x+btnFire.width &&
                                y > btnFire.y && y < btnFire.y+btnFire.height) {
                            shotFired();
                        }*/
                    }
                }

            },
            TouchPoint { id: point2
                onPressedChanged: {
                    if (!pressed) {
                        shotFired();

                        //console.log("moving and fireing.......")
                        /*if (x > btnFire.x && x < btnFire.x+btnFire.width &&
                                y > btnFire.y && y < btnFire.y+btnFire.height) {
                            shotFired();
                            console.log("shot fired from multitouch pad")
                        }*/
                   }
                }
            }
        ]
    }//MultiPointTouchArea


}
