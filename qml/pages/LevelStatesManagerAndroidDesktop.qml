import QtQuick 2.0
import QtQuick.Window 2.2

//import Qt.labs.settings 1.0 //for android OS
//import Nemo.Configuration 1.0 //for sailfish OS
import QtCore
import "level01"
import "level02"
import "level03"
import "level04"
import "level05"
import "level06"
import "level07"
import "level08"
import "level09"
import "level10"
import "levelSettings"

Item {
    id: mainRoot

    //property string rootLocale: "PT" // "PT" //todo: remove this and use proper localization

    property real calibrationFactor: Qt.platform.os === "android"  || Qt.platform.os === "ios" ?
                                         0.4 : 0.6
    //property real calibrationFactor: 0.4 //sailfish OS

    property real mm: Screen.pixelDensity * calibrationFactor
    //intex aqua fish: 294 dpi = 11.58 dots per milimeter
    //inch = 25.4mm
    //property real mm: 11.58 * calibrationFactor // for sailfish OS

    /*property real btnSize: 20 * mm
    property real defaultMargins: 2 * mm
    property int fps: 60
    property int fps_ms: 1000/fps*/

    Settings { //for android and desktop
        id: settings
        property alias calibrationFactor: mainRoot.calibrationFactor
        //uncomment for release
        property alias money: levels.money
        property alias moneyUnlocked: levels.moneyUnlocked
        property alias fuel: levels.fuel
        property alias fuelCan: levels.fuelCan
        property alias milk: levels.milk
        property alias fruit: levels.fruit
        property alias bowAndArrowUnlocked: levels.bowAndArrowUnlocked
        property alias starKeyUnlocked: levels.starKeyUnlocked
        property alias swordUnlocked: levels.swordUnlocked
        property alias carrotsUnlocked: levels.carrotsUnlocked
        property alias waterPowderUnlocked: levels.waterPowderUnlocked
        property alias levelUnlocked: levels.levelUnlocked// uncoment for release, comment for debbuging
        property alias girlIsReleased: levels.girlIsReleased
        property alias denWallDestroyed: levels.denWallDestroyed
        property alias treasureBlueDiamondsUnlocked: levels.treasureBlueDiamondsUnlocked
        property alias bookOfHapinessUnlocked: levels.bookOfHapinessUnlocked
    }

    Component.onCompleted: {
        //coment this for release mode ???
        //levels.releaseMode ? levels.levelUnlocked = settings.levelUnlocked : levels.levelUnlocked = 100
    }

    Component.onDestruction: {
        //coment this for release mode ???
        //levels.releaseMode ? settings.levelUnlocked = levels.levelUnlocked : levels.levelUnlocked = 100
    }




    /*Component.onDestruction: {
        settings.levelUnlock = fullResetGame ?  8 : 8

    }*/

    /*Component.onCompleted: {
        //calibrationFactor = (Qt.platform.os === "android") ? 0.4 : 0.8
        console.log("fps_ms: "+ fps_ms)
        console.log("Screen.desktopAvailableWidth: "+Screen.desktopAvailableWidth)
        console.log("Screen.desktopAvailableHeight: "+Screen.desktopAvailableHeight)
        console.log("Screen.width: "+Screen.width)
        console.log("Screen.height: "+Screen.height)
        console.log("calibrationFactor: "+calibrationFactor)
        console.log("Screen.pixelDensity: "+Screen.pixelDensity)
        console.log("mm: "+mm)
        console.log("show OS plataform: "+Qt.platform.os)
        console.log("denWallDestroyed: "+denWallDestroyed)
    }*/

    LevelStates {
        id: levels
        anchors.fill: parent
    }
}
