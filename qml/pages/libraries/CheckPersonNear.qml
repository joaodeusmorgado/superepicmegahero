import QtQuick 2.0
//import "../"

/*
This library is used for persons who are waiting for the hero to came near,
for example, the milkThirstyGuy in level 2, when the hero arrives near, a action
is triggered, for example a dialog with a conversation is displayed

me - should be binded to the person who is waiting, for example the milkthirsty guy
target - should be binded to, for example, the hero
*/

Item {
    id: root

    property var me: null //({})   // nothing = undefined//the person who is waiting for the hero, or whatever, to arrive
    property var target: null//({})   // nothing = undefined // the hero, or whatever
    property real distanceNear: me === null || target === null ? 0 : me.width/2 + target.width/2 // just a arbitrary value, change to fit your needs
    property bool isEnabled: screenNum === i//false

    property int screenNum: -1  //can be binded to the current screenNumber
    property int i: -2 //it's a auxiliar variable for binding the dynamic created objects to a screen number
    //when CheckPersonNear is dynamically created can be used to set isEnabled in certain screen number with:
    // isEnabled = Qt.binding( function(){ return  screenNum === i } )

    property string debugInfo: "CheckPersonNear default" //change to something more decriptive in the object

    property real _targetX: target !== null && isEnabled ? target.x : 0
    property real _targetY: target !== null && isEnabled ? target.y : 0

    property bool _isPersonNear: false

    signal targetChangedNear() //fired when the target distance to me changes to less than distanceNear
    signal targetChangedFar() //fired when the target distance to me changes to more than distanceNear

    onIsEnabledChanged: console.log(debugInfo + " isEnabled: "+isEnabled)
    onScreenNumChanged: console.log(debugInfo + " onScreenNumChanged: " + screenNum + " i: " + i + " ;isEnabled: "+isEnabled)

    on_IsPersonNearChanged: {
        if (_isPersonNear)
            targetChangedNear()
        else
            targetChangedFar()
    }


    function checkDistance() {
        //console.log("on_TargetXYChanged: "+_targetX +" : "+_targetY)
        /*if ( typeof(me) == 'undefined' || me === null ) {
            console.log("I dont exist")
            return;
        }*/
        //console.log(debugInfo)

        //if ( lib.dist(target.x, target.y, me.x, me.y, distanceNear) ) {
        if ( lib.distance(target.x+target.width/2, target.y+target.height/2, me.x+me.width/2, me.y+me.height/2, distanceNear) ) {
            //console.log("target found")
            if (_isPersonNear === false)
                _isPersonNear = true
        }
        else {
            //console.log("target not found")
            if (_isPersonNear === true) {
                _isPersonNear = false
            }
        }
    }

    on_TargetXChanged: checkDistance()
    on_TargetYChanged: checkDistance()

    //onTargetChangedNear: console.log("I'm near.................")
    //onTargetChangedFar: console.log("I'm far.................")
}
