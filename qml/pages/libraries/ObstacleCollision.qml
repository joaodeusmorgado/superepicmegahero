import QtQuick 2.0

Item {
    id: root

    property alias count: obstaclesList.count

    ListModel {
        id: obstaclesList
        //obstacles are rectangles that cannot be transposed, like walls for example
        //l is the top left coordinate
        //r is the bottom right coordinate
        //ListElement {
            //l: ""
            //r: ""
        //}
    }

    function addObstacle(topLeftX, topLeftY, bottomRightX, bottomRightY) {
        //console.log("adding obstacles to list")
        obstaclesList.append({"lX": topLeftX, "lY": topLeftY, "rX": bottomRightX, "rY": bottomRightY})
    }

/*    function addObstacle(l, r) {
        //console.log("adding obstacles to list")
        obstaclesList.append({"l": l, "r": r})
    }*/

    function clearObstacles() {
        obstaclesList.clear()
    }

    function getObstacle(i) {
        return obstaclesList.get(i)
    }

    //if ( rectanglesOverlap(pL_, pR_, obstaclesList.get(i).l, obstaclesList.get(i).r) )

}
