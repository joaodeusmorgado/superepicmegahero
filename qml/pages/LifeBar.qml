import QtQuick 2.0

Rectangle {
    id: root
    width: 50*mm
    height: 7*mm
    color: "white"
    radius: height/2

    property color barColor: "red"

    Rectangle {
        id: lifeBar
        width: parent.width
        height: parent.height
        radius: height/2
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        color: barColor
    }

    Text {
        id: label
        anchors.centerIn: parent
        text: life.toString()
    }

    property real maxLife: 10
    property real life: 10
    property real hitDamage: 1

    signal projectileDamage(var hitDamage_)

    function hit() {
        if (life > 0) {
            life -= hitDamage
            if (life < 0)
                life = 0

            if (maxLife !== 0)
                lifeBar.width = root.width * life / maxLife
            else
                lifeBar.width = 0
        }
    }

    onProjectileDamage: {
        console.log("LifeBar onProjectileDamage: "+hitDamage_)
        life -= hitDamage_
        if (life < 0)
            life = 0
        if (maxLife !== 0)
            lifeBar.width = root.width * life / maxLife
        else
            lifeBar.width = 0
    }

    function lifeBarInit(max) {
        life = maxLife = max
        lifeBar.width = root.width
    }
}
