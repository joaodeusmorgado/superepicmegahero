import QtQuick 2.0

Item {
    id: root

    property real worldLimitLeft: 0
    property real worldLimitRight: width//mainRoot.width
    property real worldLimitSky: 0
    property real worldLimitGround: height//mainRoot.height

    property real obstacleTopLimit: 0


    // Person movement should be kept inside the world limits
    property point wL: Qt.point(worldLimitLeft, worldLimitSky) //world limit top left
    property point wR: Qt.point(worldLimitRight, worldLimitGround) //world limit bottom right

    //Before moving a person we must check if it doenst cross the world limits
    //so we use the helper variables, dx, dy and pL, pR
    //dx, dy are values to make the person walk to the next position in x or y, but before the person actually moves
    //we must check if the new position is still inside the world
    //pL, pR are the person top left and bottom right new positions that need to be evaluted before moving the person
    //instead of pL, pR, we use (pLdx, pRdx) and (pLdy, pRdy) to distinguish from checking on x or y axis
    property real dx: 0
    property real dy: 0

    //Person geometry is a rectangle with coordinates pL(person top left) and pR (person bottom right)
    //property point pL: Qt.point(x+dx, y+dy)
    //property point pR: Qt.point(x+dx+width, y+dy+height)

    property point pLdx: Qt.point(x+dx, y)
    property point pRdx: Qt.point(x+dx+width, y+height)

    property point pLdy: Qt.point(x, y+dy)
    property point pRdy: Qt.point(x+width, y+dy+height)

    function moveInPathX(dx_) {
        dx = dx_ //necessary to update pLdx, pRdx
        //console.log("moveInPathX dx: "+dx)

        if (dx === 0)
            return false;

        if (checkObstaclesCollision(pLdx, pRdx)) {
            //console.log("there's a obstacle we can't continue, return false")
            return false;
        }
        //console.log("no obstacles, continue .....")

        if (pLdx.x >= wL.x && pRdx.x <= wR.x) { //check if move is inside horizontal world limits
            //console.log("check if move is inside horizontal world limits, Ok lets move")
            x += dx
            return true; //move sucessful
        }
        //console.log("cant move inside world, exit .....")
        return false;
    }

    function canMoveInPathY(dy_) {
        dy = dy_ //necessary to update pLdy, pRdy

        if (dy_ === 0)
            return false;

        var ret = false
        if (checkObstaclesCollision(pLdy, pRdy)) {
            return false;
        }

        if (pLdy.y >= wL.y && pRdy.y <= wR.y) {
            //y1 += dy
            ret = true
        }
        return ret;
    }

    //checkObstaclesCollision() return true if there's a obstacle ahead
    //returns false if there's not
    function checkObstaclesCollision(pL_, pR_) {
        if (obstaclesList.count === 0)
            return false; // no obstacles exist
        //console.log("obstaclesList.count: "+obstaclesList.count)

        for (var i=0; i < obstaclesList.count; i++) {
            if ( lib.rectanglesOverlap(pL_, //person top left coordinate
                                   pR_, //person bottom right coordinate
                                   Qt.point(obstaclesList.getObstacle(i).lX, obstaclesList.getObstacle(i).lY), //obstacle top left
                                   Qt.point(obstaclesList.getObstacle(i).rX, obstaclesList.getObstacle(i).rY) //obstacle bottom right
                    ) ) {
                obstacleTopLimit = obstaclesList.getObstacle(i).lY //sabe the top y obstacle coordinate
                return true;
            }
        }
        return false;
    }

    /*function rectanglesOverlap(pL_, pR_, oL, oR) {
        // If one rectangle is on left side of other
        //console.log("rectangles overlap---------------")
        //console.log("obstacle top left xy: "+oL.x+" : "+oL.y)
        //console.log("obstacle bottom right xy: "+oR.x+" : "+oR.y)
        if (pR_.x < oL.x || oR.x < pL_.x) {
            return false;
        }

        // If one rectangle is above other
        if (pR_.y < oL.y || oR.y < pL_.y) {
            return false;
        }
        return true;
    }*/

}
