import QtQuick 2.3
import "../"
import "../components"
import "../libraries"

LevelContentTemplate {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    grass.color: "SaddleBrown"

    //onVisibleChanged: visible ? levelStartSetup() : levelEndCleanup()
    onLevelStartSetup: {
        girl.rotation = 0
        talkBoard.visible = false
    }

    Image {
        id: imgEvilMan
        width: height * 296 / 390
        height: 40*mm
        x: mainRoot.width*0.5+imgCastle.width*0.22
        y: grass.y+grass.height*0.25-imgCastle.height+imgCastle.height*0.22
        source: "qrc:/images/evilMan.png"
        mipmap: true
    }

    Image {
        id: imgCastle
        width: height * 689/450 //0.652 689x450
        height: 115*mm
        //fillMode: Image.PreserveAspectFit
        source: "qrc:/images/castle.png"
        mipmap: true
        x: mainRoot.width*0.5
        y: grass.y+grass.height*0.25-imgCastle.height
    }

    Component.onCompleted: console.log("outside castle y: "+imgCastle.y)

    //This "hidden" item is used to fire the girl attack
    Item {
        id: item2FireGirlAttack
        width: 8*mm
        height: 40*mm
        x: mainRoot.width*0.3
        y: groundLimit-height
    }

    CheckPersonNear{
        id: checkGirlIsAttacked
        me: item2FireGirlAttack
        target: girl
        distanceNear: girl.height
        isEnabled: screenNumber === 0 && parent.visible
        onTargetChangedNear: {
            //console.log("Girl attack......................")
            evilManUpDown.start()
        }
        debugInfo: "target girl attacked"
    }    

    property real upDown: 500
    SequentialAnimation {
        id: evilManUpDown
        onStopped: {
            interfaceWalkGirlHero.disconnect()
            //lib.connectCommandWalk(commandTouchPad, hero)
            //lib.connectCommandWalk(commandKeyb, hero)
            //console.log("animaion stopped- imgCastle.y:"+imgCastle.y)
            createDialogGirlFaints()
        }

        NumberAnimation {
            id: evilManUp
            target: imgEvilMan
            properties: "y"
            from: imgCastle.y+imgCastle.height*0.22
            to: imgCastle.y*0.95
            duration: upDown
            easing {  type: Easing.OutQuad }
        }
        ScriptAction {
            script: {
                lib.disconnectCommandWalk(commandTouchPad, leaderObj)
                lib.disconnectCommandWalk(commandKeyb, leaderObj)
                girl.stopWalking()
                hero.stopWalking()
                createArrowEnemy(); //fire arrow
            }
        }
        NumberAnimation {
            id: evilManDown
            target: imgEvilMan
            properties: "y"
            from: imgCastle.y*0.95
            to: imgCastle.y+imgCastle.height*0.22
            duration: upDown
            easing { type: Easing.InQuad }
        }
    }


    ConversationBoard2 {
        id: talkBoard
        width: mainRoot.width * 0.6
        height: mainRoot.height * 0.6//0.5
        z: 1
        //opacity: 0.75
        //imageLeft: "heroHead.png"
        //imageRight: "unicornHorseHead.png"

        imageBottomWidth: 20*mm
        imageBottomHeight: 20*mm
        visible: false
        onEndDialog: showAboutDialogue()
    }

    function createArrowEnemy() {
        //var arrow = lib.createArrow(touchX, touchY, bowObj, screenNumber, screenRow, root)
        console.log("fire arrow------------------------")
        console.log("evilManUp.x: "+imgEvilMan.x)
        console.log("evilManUp.y: "+imgEvilMan.y)
        console.log("girl.x: "+girl.x)
        console.log("girl.y: "+girl.y)

        var arrowEnemy = lib.createProjectileEnemy(girl.x+girl.width*0.5, girl.y+girl.height*0.5, imgEvilMan.x, imgEvilMan.y+imgEvilMan.height*0.5, 0, 0, parent)
        if (arrowEnemy === null)
            return;

        arrowEnemy.imageName = "arrow.png"
        arrowEnemy.projectileType = "arrow"
        arrowEnemy.width = 20*mm
        arrowEnemy.height = 20*mm*0.273
        arrowEnemy.velocity = 8*mm
        arrowEnemy.bulletTarget = girl
        arrowEnemy.hit.connect( girl.faintStart )
    }


    function createDialogGirlFaints() {

        if (girl.x < hero.x) {
            hero.direction(-1)
        }

        talkBoard.x = mainRoot.width*0.5 - talkBoard.width/2
        talkBoard.y = defaultMargins

        talkBoard.clear()

        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = "girlHead.png"

        talkBoard.addMessage4(qsTr("Pandora! Please don't die!!!"), "heroHead.png", 2, true)
        talkBoard.addMessage4(qsTr("To be continued..."), "", 0, true)
        talkBoard.addMessage4(qsTr("Pandora is badly wonded 😭\nWill she die...?"), "", 0, true)
        talkBoard.addMessage4(qsTr("Or will the hero find a forest doctor who will help her healing and "
                                   +"then she will continue the adventures with captain Andrew and eventually find a misterious 3D world with more adventures ?"), "", 0, true)


        talkBoard.addMessage4(qsTr("Stay tunned for the following game updates were all will be revealed.\nMeanwhile, be happy 😊"), "", 0, true)

        talkBoard.addMessage4(qsTr("If you have enjoyed this game, please consider donating, buy the author a coffee. "
                                   +"It will be greatly appreciated and will keep us motivated to continue developing more Super Epic Mega adventures."), "", 0, true)


        talkBoard.showDelay(1500)
    }

}
