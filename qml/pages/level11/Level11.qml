import QtQuick 2.3
import "../"
import "../components"

Rectangle {
    id: rootlevel11
    width: mainRoot.width
    height: mainRoot.height
    color: "grey"

    signal menu()
    signal nextLevel()
    signal levelWin()
    signal showAboutDialogue()

    onLevelWin: {
        levels.levelUnlocked = levels.levelUnlocked === 11 ? 12 : levels.levelUnlocked
        levelContent.levelEndCleanup()
        panelEOL.text = qsTr("Victory!")
        //panelEOL.showNextBtn = true
        panelEOL.visible = true
    }

    CommandMultiTouchPad3 {
       id: commandTouchPad
       anchors.left: parent.left
       anchors.bottom: parent.bottom
       anchors.margins: defaultMargins
    }

    Flickable {
        id: flickLevel
        width: mainRoot.width
        height: mainRoot.height
        contentWidth: levelContent.width
        contentHeight: levelContent.height
        interactive: false
        Level11Content {
            id: levelContent
            width: mainRoot.width
            height: mainRoot.height
        }
    }//Flickable


    TouchPad2 {
        id: touchPad
        centerX: commandTouchPad.touchPad.centerX
        centerY: commandTouchPad.touchPad.centerY
        visible: commandTouchPad.touchPad.visible
    }

    MenuPopUpVertical {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }


    PanelEndOfLevel2 {
        id: panelEOL
        visible: false
        anchors.centerIn: parent
        onMenu: parent.menu()
        onReplay: {
            levelContent.levelEndCleanup()
            levelContent.levelStartSetup()
        }
        onNext: parent.nextLevel()
    }

}
