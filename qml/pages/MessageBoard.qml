import QtQuick 2.3
import "components"

Rectangle {
    id: root
    width: mainRoot.width / 2
    height: mainRoot.height / 2
    color: "purple"
    border.color: "orange"
    border.width: mm
    opacity: 0.75
    radius: 4*mm//width*0.1

    property alias text: txt.label
    property alias horizontalAlignment: txt.horizontalAlignment
    property alias verticalAlignment: txt.verticalAlignment
    property alias pixelSize: txt.pixelSize
    property string image: "" //"keyStar.png"
    property alias showCancelBtn: btnCancel.visible
    property alias showOkBtn: btnOk.visible
    property alias bntCancelText: btnCancel.text
    property alias bntOkText: btnOk.text

    signal ok()
    signal cancel()

    Image {
        id: img
        width: parent.width
        height: image !== "" ? parent.height/2 : 0
        anchors.top: parent.top
        fillMode: Image.PreserveAspectFit
        source: image === "" ? "" : "qrc:/images/"+image
        mipmap: true
    }

    TextTemplate {
        id: txt
        width: parent.width
        height: parent.height - img.height - btnOk.height // todo: minus margins
        anchors.top: img.bottom
        anchors.bottom: btnOk.top
        anchors.left: parent.left
        anchors.right: parent.right
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        color: "orange"
        label: "" //qsTr("You've found a Star Key!!!")
    }

    MyButton {
        id: btnCancel
        backGrdColor: "red"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.right: btnOk.left
        anchors.rightMargin: defaultMargins
        text: qsTr("Cancel")
        //image: "btnMenu.png"
        onBtnClick: cancel()
        visible: false
    }

    MyButton {
        id: btnOk
        backGrdColor: "red"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Ok")
        //image: "btnMenu.png"
        onBtnClick: ok()
    }
}

