import QtQuick 2.0

EnemyTemplate {
    id: enemyRed

    width: 40*mm

    y: yPos - yMovement + yMovement * Math.cos(Math.PI*t/2+Math.PI/2*teta)
    property real yPos
    property real yMovement: mainRoot.height*0.3
    property real teta: 0

    imageName: "EnemyRedBall.png"

}
