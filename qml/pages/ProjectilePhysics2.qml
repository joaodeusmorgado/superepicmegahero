import QtQuick 2.0

// Abstraction physics for the bullets collision detection
// How to use this:

// - It should be created a Physics item for each bullet

// Create connections to this item:
// - connect enemyHit() to some other enemy signal (where enemy life should, for example,
//  decrement, destroyed,...)


//Note: right now this only checks collision with the list of enemies
//Todo: add a target so that this can be used to target only one object
//i.e the hero, that way this can be a bullet shot from the enemy trying to hurt the hero
// use some kind of flag to mark with as a enemy fired bullet trying to hit the hero
// or a hero fired bullet trying to hit a listOfEnemies

Item {
    id: root

    //property var listOfEnemies: null//({})
    //property var delta //if enemy distance from bullet is smaller than delta, we have a collision

    property real midX: x + width*0.5
    property real midY: y + height*0.5

    property real velocity: 2*mm
    property real angle: -Math.PI/2
    property real _cosAngle: Math.cos(angle) //cache
    property real _sinAngle: Math.sin(angle) //cache

    //bullet will be destroyed if outside the gamming area
    property real xMinBulletDestroy: 0
    property real xMaxBulletDestroy: mainRoot.width
    property real yMinBulletDestroy: 0
    property real yMaxBulletDestroy: mainRoot.height

    property alias running: timer.running

    /*Component.onCompleted: {
        listOfEnemies = activeEnemiesList.activeEnemiesList
    }*/

    property real hitDamage: 1
    property string projectileType: "" //example, can be "arrow", "girlFirePower", "heroSpaceShipShot", ...
    signal hit(var damage_, var projectileType_) // send when there's a collision

    signal move()
    signal destroyBullet() //call this if a collision has ocurred or if the bullet is out of reach

    onDestroyBullet: {
        timer.stop()
        root.destroy()
    }

    //moves the shot and emit the signal moved()
    Timer {
        id: timer
        interval: fps_ms; running: true; repeat: true
        onTriggered: {
            move() //mode arrow
            //checkCollisionWithEnemies()
        }
    }

    onMove: {
        x += velocity * _cosAngle //Math.cos(angle)
        y += velocity * _sinAngle //Math.sin(angle)
        _checkBulletOutsideGammingArea()
    }

    function _checkBulletOutsideGammingArea() {
        if (root.x < xMinBulletDestroy || root.x > xMaxBulletDestroy
                || root.y < yMinBulletDestroy || root.y > yMaxBulletDestroy) {
            //console.log("ProjectilePhysics2.qml - projectile outside gamming area......")
            destroyBullet(); //destroy bullet, it's outside the gamming area
        }
    }

    function setPosXY(xPos,yPos) {
        root.x = xPos
        root.y = yPos
    }

    function setMidPosXY(midX_, midY_) {
        root.x = midX_ - width*0.2
        root.y = midY_ - height*0.2
    }

    function setAngleFromPoint(xx, yy) {
        //angle = Math.atan2(yy - yCenter, xx - xCenter)
        angle = Math.atan2(yy - midY, xx - midX)

        //truckShotObj.setAngle(xx- truckShotObj.xCenter, yy - truckShotObj.yCenter)
    }

    function setAngle(xx, yy) {
        angle = Math.atan2(yy, xx)
        //console.log("xx: "+xx+" yy: " +yy +" angleRad: "+angle + " angleDed: "+ angle * 57.2957795131)
    }

    onAngleChanged: {
        _cosAngle = Math.cos(angle)
        _sinAngle = Math.sin(angle)
    }

}
