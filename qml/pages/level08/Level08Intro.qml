import QtQuick 2.0
import "../"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    signal close()

    ConversationBoard2 {
        id: talkBoard
        anchors.top: parent.top
        anchors.bottom: btnClose.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        width: parent.width - btnClose.width*2 - 4*defaultMargins

        imageBottomWidth: 50*mm
        imageBottomHeight: 50*mm

        visible: false

        onEndDialog: close()//btnClose.text = qsTr("Ok")
    }

    MyButton {
        id: btnClose
        //width: btnSize * 1.25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onBtnClick: close()
        text: qsTr("Skip")
    }

    onVisibleChanged: {
        if (visible) {
            addTalk()
            talkBoard.visible = true
        }
    }

    function addTalk() {
        talkBoard.clear()
        talkBoard.addMessage5(qsTr("Way back when our hero was in the military academy, he had to practice ballon shooting with arrows. "
                                   +"He had to shoot at least 80% of the ballons to pass the training. Let's do it."),
                              "", -1, false, "balloon01.png")
    }

}
