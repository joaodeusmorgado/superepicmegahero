import QtQuick 2.0
import "../"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "grey"

    enabled: visible

    property real groundLimit: sky.height+hero.height/2

    //balloonColumnSize is the total number of columns that could be filled with balloons, depend on the screen size but should not be bigger than 6
    //but we dont want all the columns filled, we want just a few colums filled i.e. balloonPerColumn
    //so we can get a nice effect of balloons appearing randomly
    property int balloonColumnSize:  (Math.ceil( (mainRoot.width*0.55) / (dummyBalloon.width) ) ) > 6 ?
                                         6 : (Math.ceil( (mainRoot.width*0.55) / (dummyBalloon.width) ) )
                                         //55% of width is the size of balloons column
    property int balloonPerColumn: Math.floor(balloonColumnSize*0.6)//only 60% of colum has balloons
    property var arr: lib.createOrderedArray(balloonColumnSize)
    property var shuffleArr
    property real timeCount
    property int levelDuration: 30000 //milliseconds
    property bool levelFail: false
    property int balloonCount: 0
    property int balloonDestroyedCount: 0
    property real percentagemBalloonDestroyed: balloonDestroyedCount/balloonCount

    property string balloonsText: qsTr("Total balloons:") + " " + balloonCount + "\n"
                            + qsTr("Balloons shot:") + " " + balloonDestroyedCount + "\n"
                            + qsTr("Shot percentagem:") + " " + (percentagemBalloonDestroyed*100).toFixed(1) + "%\n"

    signal menu()
    signal nextLevel()
    signal levelWin()
    signal levelFailed()
    signal destroyItems()
    signal endOfLevel()
    signal countDestroyedBallons()

    onCountDestroyedBallons: {
        balloonDestroyedCount++;
        console.log("balloonDestroyedCount: "+balloonDestroyedCount)
    }

    onEndOfLevel: {
        console.log("signal end of level fired.........................................")

        // we need to destroy at least 80 percent of the balloons to win the level
        if ( percentagemBalloonDestroyed >= 0.8 && !levelFail)
            levelWin()
        else
            levelFailed()
    }

    onLevelWin: {
        levels.levelUnlocked = levels.levelUnlocked === 8 ? 9 : levels.levelUnlocked
        //levelContent.levelEndCleanup()
        panelEOL.text = balloonsText + qsTr("You shot more than 80%\nVictory!!!")
        //todo: change this text
        //panelEOL.showNextBtn = true
        panelEOL.visible = true
    }

    onLevelFailed: {
        dummyBalloon.inMotion = false
        dummyBalloon.active = false
        timerLevelFinished.running = false
        //panelEOL.showNextBtn = false
        //destroyItems()
        //activeEnemiesList.clearEnemies()
        panelEOL.text = panelEOL.text = balloonsText + qsTr("You shot less than 80%\nTry again 😟")
        panelEOL.visible = true
    }

    onVisibleChanged: {
        if (visible) {
            levelStartSetup()
        }
        else {
            levelEndCleanup()
        }
    }

    CommandMultiTouchPad3 {
       id: commandTouchPad
       anchors.left: parent.left
       anchors.bottom: parent.bottom
       anchors.margins: defaultMargins
    }

    Rectangle {
        id: sky
        width: parent.width
        height: mainRoot.height * 0.7 // 70% of mainRoot screen height
        color: "lightcyan"
        anchors.top: parent.top
    }

    Rectangle {
        id: grass
        width: mainRoot.width
        height: mainRoot.height - sky.height
        color: "lime"
        anchors.top: sky.bottom
    }

    Rectangle {
        id: florestGround
        width: parent.width *grass.width
        height: mainRoot.height * 2 - sky.height
        color: "SaddleBrown" // "Sienna"
        anchors.top: sky.bottom
        anchors.left: grass.right
    }

    Person2 {
        id: hero
        width: 18*mm
        height: width * 320/156
        //jumpLenght: width*3.5
        z: 1
        worldLimitGround: groundLimit
        onImDestroyed: {
            levelFail = true
            endOfLevel()
        }
        onImHit: heroLifeBar.hit()

        function reset(){
            hero.dir = 1
            visible = true
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
            //worldLimitGround = groundLimit
            x = 0
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
        }
        mouseAreaEnabled: false
    }

    Balloon {
        id: dummyBalloon
        x: 0
        y: mainRoot.height - defaultMargins - dummyBalloon.height
        visible: false
        alive: false
        inMotion: false
        function reset() {
           // y = mainRoot.height
            alive = true
            inMotion = true
        }
        onYChanged: {
            if (!root.visible)
                return;
            if ( y < (mainRoot.height - dummyBalloon.height - defaultMargins) ) {
                y = mainRoot.height
                createBalloons()
                //console.log("dummy balloon y: " +y)
            }
        }
    }

    Timer {
        id: timerLevelFinished
        interval: levelDuration
        running: false
        repeat: false
        onTriggered: {
            //console.log("balloon level finished..............................")
            dummyBalloon.inMotion = false
            dummyBalloon.active = false
        }
    }

    Timer {
        id: timeCountTimer
        running: timerLevelFinished.running
        interval: 1000
        repeat: true
        onTriggered: {
            timeCount -= timeCountTimer.interval
        }
        onRunningChanged: {
            if(running) {
                timeCount = levelDuration
            }
        }
    }

    TouchPad2 {
        id: touchPad
        centerX: commandTouchPad.touchPad.centerX
        centerY: commandTouchPad.touchPad.centerY
        visible: commandTouchPad.touchPad.visible
    }

    MenuPopUpVertical {
        id: popUpMenu
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }

    //time left board
    ScoreBoard {
        id: time_left
        width: 48*mm
        anchors.top: parent.top
        anchors.left: popUpMenu.right
        anchors.margins: defaultMargins
        label: qsTr("Time")
        score: timeCount/1000
    }

    PanelEndOfLevel2 {
        id: panelEOL
        pixelSize: 8*mm
        visible: false
        anchors.centerIn: parent
        showNextBtn: levels.levelUnlocked > 8
        onMenu: root.menu()
        onReplay: {
            levelEndCleanup()
            levelStartSetup()
        }
        onNext: root.nextLevel()
    }

    function levelStartSetup(){
        console.log("level 09 startup setup --------------------------------------------------------------------------------")
        lib.projectileShotDelay = false
        balloonCount = 0
        balloonDestroyedCount = 0
        panelEOL.visible = false
        //createItems()
        createConnections()
        hero.reset()
        createBow()
        dummyBalloon.reset()
        timerLevelFinished.start()
        //arr = lib.createOrderedArray(balloonColumnSize)
        levelFail = false
    }

    function levelEndCleanup(){
        lib.projectileShotDelay = true
        activeEnemiesList.clearEnemies()
        destroyItems()
        destroyConnections()
        destroyBow()

        dummyBalloon.alive = false
        dummyBalloon.active = false
        dummyBalloon.inMotion= false
    }

    //--connections------------------------------------------------------------
    function createConnections() {
        //moves the scene when the hero reachs the end of the screen
        lib.connectCommandWalk(commandTouchPad, hero)
        lib.connectCommandWalk(commandKeyb, hero)

        activeEnemiesList.allEnemiesCleared.connect( endOfLevel )
    }

    function destroyConnections() {
        //console.log("level09 remove connections")
        //flickLevel.contentX = 0
        //flickLevel.contentY = 0
        lib.disconnectCommandWalk(commandTouchPad, hero)
        lib.disconnectCommandWalk(commandKeyb, hero)

        activeEnemiesList.allEnemiesCleared.disconnect( endOfLevel )
    }
    //--connections-------------------------------------------------------------


    function createBalloons() {
        //console.log("balloonColumnSize: "+balloonColumnSize)
        //console.log("balloonPerColumn: "+balloonPerColumn)

        shuffleArr = lib.shuffleArray(arr)
        console.log("shuffleArr: "+shuffleArr)
        console.log("balloonPerColumn"+balloonPerColumn)
        for (var i=0;i < balloonPerColumn;i++) {
            //console.log("create balloon....................................................................")
            balloonCount++;
            console.log("balloonCount: "+balloonCount)

            var ballonObj = lib.createItem("Balloon.qml", parent)
            //ballonObj.x = mainRoot.width -defaultMargins - ballonObj.width*i
            ballonObj.x = mainRoot.width - defaultMargins - ballonObj.width * (shuffleArr[i]+1);
            ballonObj.y = mainRoot.height// - defaultMargins - ballonObj.height

            ballonObj.addToEnemiesList.connect( activeEnemiesList.addEnemy )
            ballonObj.removeFromEnemiesList.connect( activeEnemiesList.removeEnemy )
            //activeEnemiesList.addEnemy(ballonObj)

            //var num = Math.floor( Math.random() * 7 ) + 1
            var num = lib.getRandom(1,8)
            ballonObj.imageName = "balloon0" + num + ".png"
            ballonObj.type = "Balloon"

            destroyItems.connect ( ballonObj.enemyDestroyed )
            ballonObj.enemyTarget = hero
            ballonObj.destroyTarget.connect( hero.imDestroyed )

            ballonObj.imHit.connect( countDestroyedBallons )

            ballonObj.inMotion = Qt.binding( function(){ return  parent.visible === true } )
            //ballonObj.active = false//Qt.binding( function(){ return  parent.visible === true } )
            //ballonObj.active = true
            ballonObj.deltaHit = ballonObj.height*0.6

        }
    }//createBalloons

    onDestroyItems: {

    }

    //-- Bow & Arrows-----------------------------------------------
    function createBow(){
        lib.createBow(hero, parent)
        commandTouchPad.shotFiredXY.connect( createArrow )
    }

    function destroyBow(){
        commandTouchPad.shotFiredXY.disconnect( createArrow )
        lib.destroyBow()
    }

    function createArrow(touchX, touchY) {
        //var arrow = lib.createArrow(touchX, touchY, bowObj, screenNumber, screenRow, root)
        var arrow = lib.createProjectile(touchX, touchY, lib.bowObj.midX, lib.bowObj.midY, 0, 0, parent)
        if (arrow === null)
            return;

        arrow.imageName = "arrow.png"
        arrow.projectileType = "arrow"
        arrow.width = 20*mm
        arrow.height = 20*mm*0.273
        arrow.velocity = 4*mm
    }
    //-- Bow & Arrows-----------------------------------------------

}
