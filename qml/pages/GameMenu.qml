import QtQuick 2.3
import "components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    //color: "AntiqueWhite"
    color: "blue"

    Image {
        id: background
        source: "qrc:/images/superepicmegaheroBG.png"
        mipmap: true
    }

    signal level01Intro()
    signal level02Intro()
    signal level03()
    signal level04()
    signal level05()
    signal level06()
    signal level07()
    signal level08()
    signal level09()
    signal level10()
    signal level11()
    signal level12()
    signal level13()
    signal level14()
    signal level15()
    signal about()
    signal settings()
    signal sigExit()

    signal destroyStars()
    onVisibleChanged: {

        if (visible) {

            //create random stars-------------------------------
            for (var i = 0; i < 30;i++) {
                //console.log(Math.random()*root.width + ":"+Math.random()*root.height)
                var stars = lib.createItem("Star01.qml", root)
                //stars.x = Math.random()*root.width
                //stars.y = Math.random()*root.height
                stars.x = Qt.binding( function(){ return Math.random()*root.width } )
                stars.y = Qt.binding( function(){ return Math.random()*root.height } )
                destroyStars.connect( stars.destroyItem )
            }//create random stars-------------------------------

        }
        else {
            destroyStars()
        }
    }

    Grid {
        id: grid
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: defaultMargins
        //columns: 4
        columns: (grid.width)/(btnLevel01.width+spacing)
        spacing: defaultMargins*3
        z: 1

        MyButton {
            id: btnLevel01
            text: "01"
            onBtnClick: {
                level01Intro()
            }
        }

        MyButton {
            id: btnLevel02
            text: "02"
            onBtnClick: level02Intro()
            opacity: levels.levelUnlocked >= 2 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 2
        }

        MyButton {
            id: btnLevel03
            text: "03"
            onBtnClick: level03()
            opacity: levels.levelUnlocked >= 3 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 3
        }

        MyButton {
            id: btnLevel04
            text: "04"
            onBtnClick: level04()
            opacity: levels.levelUnlocked >= 4 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 4
        }

        MyButton {
            id: btnLevel05
            text: "05"
            onBtnClick: level05()
            opacity: levels.levelUnlocked >= 5 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 5
        }

        MyButton {
            id: btnLevel06
            text: "06"
            onBtnClick: level06()
            opacity: levels.levelUnlocked >= 6 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 6
        }

        MyButton {
            id: btnLevel07
            text: "07"
            onBtnClick: level07()
            opacity: levels.levelUnlocked >= 7 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 7
        }

        MyButton {
            id: btnLevel08
            text: "08"
            onBtnClick: level08()
            opacity: levels.levelUnlocked >= 8 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 8
        }

        MyButton {
            id: btnLevel09
            text: "09"
            onBtnClick: level09()
            opacity: levels.levelUnlocked >= 9 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 9
        }

        MyButton {
            id: btnLevel10
            text: "10"
            onBtnClick: level10()
            opacity: levels.levelUnlocked >= 10 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 10
        }

        MyButton {
            id: btnLevel11
            text: "11"
            onBtnClick: level11()
            opacity: levels.levelUnlocked >= 11 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 11
        }

        /*
        MyButton {
            id: btnLevel12
            text: "12"
            onBtnClick: level12()
            opacity: levels.levelUnlocked >= 12 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 12
        }

        MyButton {
            id: btnLevel13
            text: "13"
            onBtnClick: level13()
            opacity: levels.levelUnlocked >= 13 ? 1 : 0.5
            enabled: levels.levelUnlocked >= 13
        } */

    } //Grid

    Row {
        id: row
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        z: 1
        spacing: defaultMargins
        //effectiveLayoutDirection:
        //layoutDirection: Qt.RightToLeft
        //LayoutMirroring.enabled: true

        LayoutMirroring.enabled: true
        //LayoutMirroring.childrenInherit: true

        MyButton {
            id: btnExit
            width: btnSize*2
            text: qsTr("Exit")
            onBtnClick: msgExit.visible = true
        }

        MyButton {
            id: btnSettings
            width: btnSize*2
            text: qsTr("Settings")
            //image: "settings.png"
            onBtnClick: settings()
        }

        MyButton {
            id: btnAbout
            width: btnSize*2
            text: qsTr("About")
            //image: "settings.png"
            onBtnClick: about()
        }


    }

    /*MyButton {
        id: btnSettings
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: defaultMargins
        //text: qsTr("Settings")
        image: "settings.png"
        onBtnClick: settings()
    }

    MyButton {
        id: btnExit
        anchors.bottom: parent.bottom
        anchors.left: btnSettings.right
        anchors.margins: defaultMargins
        text: qsTr("Exit")
        onBtnClick: msgExit.visible = true
    }*/

    MessageBoard {
        id: msgExit
        anchors.centerIn: parent
        opacity: 1
        visible: false
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        pixelSize: 10*mm
        text: qsTr("Exit game ?")
        showCancelBtn: true
        bntCancelText: qsTr("No")
        bntOkText: qsTr("Yes")
        onOk: sigExit()
        onCancel: msgExit.visible = false
    }

}
