import QtQuick 2.3

Item {
    id: root
    width: 50*mm
    height: 60*mm

    property bool isOpen: false

    Image {
        id: cage
        width: parent.width
        height: parent.height
        //fillMode: Image.PreserveAspectFit
        source: isOpen ? "qrc:/images/cageOpen.png" : "qrc:/images/cage.png"
        mipmap: true
    }
}
