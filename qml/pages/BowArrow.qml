import QtQuick 2.3

Item {
    id: bowArrow
    width: 25*mm
    height: 25*mm
    //rotation: angle

    property real midX: x+width/2
    property real midY: y+height/2

    property real angle: 0
    property bool isRight: true
    //signal prepareFire()
    //signal fire(real fx, real fy)

    function setAngle(xx, yy) {
        //angle = Math.atan2(yy, xx)
        angle = isRight ? Math.atan2(yy, xx) : Math.atan2(yy, xx) + Math.PI
        //console.log("xx: "+xx+" yy: " +yy +" angleRad: "+angle + " angleDed: "+ angle * 57.2957795131)
    }

    function resetAngle() {
        angle = 0//Qt.binding( function(){ return isRight ? 0 : Math.PI })
    }

    transformOrigin: Item.Center

    /*onPrepareFire: {
        isRight ? fire(x+width, y) : fire(x, y)
    }*/



    Image {
        id: bowArrowImg
        width: parent.width
        height: parent.height
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/bowArrow.png"
        rotation: mirror ? angle * 57.2957795131  : angle * 57.2957795131
        mirror: !isRight
        mipmap: true
    }

}
