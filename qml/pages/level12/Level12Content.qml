import QtQuick 2.3
import "../"
import "../components"
import "../libraries"

LevelContentTemplate {
    id: root
    width: mainRoot.width * 6
    height: mainRoot.height

    grass.color: "SaddleBrown"

    walkPartnerActive: false

    girl.visible: false
    hero.imgName: "heroCarriesGirl0"

    Person3 {
        id: forestMan
        imgName: "forestMan"
        height: 40*mm
        width: height * imageSourceSize.width / imageSourceSize.height
        //girl.mirrorImage: -1
        x: mainRoot.width*1.35
        z: 1
        worldLimitGround: groundLimit

        function reset() {
            dir = 1
            visible = true
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
            //worldLimitGround = groundLimit
            x = mainRoot.width*1.2
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
        }
    }


    //onVisibleChanged: visible ? levelStartSetup() : levelEndCleanup()
    onLevelStartSetup: {
        //this is called in second place, after the LevelContentTemplate
        console.log("level 12 ContentTemplate start setup")
        createItems()
        girl.visible = false
        hero.x = mainRoot.width*0.3
        forestMan.reset()
        //talkBoard.visible = false
    }

    Image {
        id: imgCastle
        width: height * 689/450 //0.652 689x450
        height: 115*mm
        //fillMode: Image.PreserveAspectFit
        source: "qrc:/images/castle.png"
        mipmap: true
        x: mainRoot.width*0.5
        y: grass.y+grass.height*0.25-imgCastle.height
    }

    Image {
        id: shack
        width: height * sourceSize.width/sourceSize.height
        height: 115*mm
        //fillMode: Image.PreserveAspectFit
        source: "qrc:/images/shack.png"
        mipmap: true
        x: mainRoot.width*2.25
        y: grass.y+grass.height*0.25-shack.height
    }

    CheckPersonNear{
        id: checkHeroNearForestMan
        me: forestMan
        target: hero
        distanceNear: hero.height
        isEnabled: screenNumber === 1 // && parent.visible
        onTargetChangedNear: {
            createDialogueHeroForestMan()
            talkBoard.show()
        }
        onTargetChangedFar: {
            //talkBoard.visible = false
        }
        //debugInfo: "forestMan hero conversation"
    }

    /*ConversationBoard2 {
     //   id: talkBoard
        width: mainRoot.width * 0.6
        height: mainRoot.height * 0.6//0.5
        x: 0
        y: mainRoot.height+defaultMargins
        z: 2
        opacity: 0.75
        imageLeft: "heroHead.png"
        imageRight: "unicornHorseHead.png"

        imageBottomWidth: 20*mm
        imageBottomHeight: 20*mm
        visible: true
        onEndDialog: {
            opacity = 0.75
        }
    }*/

    ConversationBoard2{
        id: talkBoard
        width: mainRoot.width * 0.6
        height: mainRoot.height * 0.6//0.5
        x: mainRoot.width*0.5 - talkBoard.width/2
        y: defaultMargins
        z: 1
        opacity: 0.75
        //imageBottomWidth: 20*mm
        //imageBottomHeight: 20*mm
        visible: false
        //onEndDialog: showAboutDialogue()
    }

    function createItems() {

        var i=0;
        for (i=0;i < 5; i++) {
            //forest trees------------------------------------------
            var treeObj = lib.createItem("components/PersonCharacter.qml", root)
            treeObj.imgName = "treePine.png"
            treeObj.height = 88*mm
            treeObj.width = treeObj.height * treeObj.img.sourceSize.width / treeObj.img.sourceSize.height
            treeObj.x = mainRoot.width * 1.1 + treeObj.width*i
            treeObj.y = sky.height - treeObj.height + treeObj.height*0.2
            treeObj.z = 0.5
            destroyItems.connect( treeObj.destroy )
        }
    }

    function createDialogueHeroForestMan() {

        console.log("forest man talk ..................................................")

        talkBoard.x = mainRoot.width*0.5 - talkBoard.width/2
        talkBoard.y = defaultMargins

        //talkBoard.y = mainRoot.height + defaultMargins
        talkBoard.clear()

        talkBoard.imageBottomWidth = talkBoard.width*0.85
        talkBoard.imageBottomHeight = talkBoard.height*0.85

        talkBoard.addMessage4(qsTr("Hi there, I see you meet my husband unicorn Einstein. I'm Mary Mare."),"mareHead.png", 2, false)

        talkBoard.showDelay(1500)

    }

}
