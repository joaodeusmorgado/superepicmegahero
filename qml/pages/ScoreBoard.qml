import QtQuick 2.0

Rectangle {
    id: root
    width: textScore.contentWidth+6*mm < 50*mm ? 50*mm : textScore.contentWidth+6*mm //210*mm
    height: 12*mm
    radius: height/2
    color: "white"

    property string label: qsTr("Score")
    property int score: 0

    Text {
        id: textScore
        text: label+": "+score.toString()
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: defaultMargins
    }


}
