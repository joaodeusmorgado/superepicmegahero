import QtQuick 2.3

Rectangle {
    id: root
    width: 15*mm //tex.contentWidth > 15*mm ? tex.contentWidth : 15*mm
    height: width
    opacity: 0.5
    color: "lightblue"
    radius: width/2

    property alias label: tex
    property alias imageName: image.source

    property string powerUpMessage: "PowerUp Shield!!!\nProtects from the enemy bullets."
    property real scorePoints: 20


    // powerUpTarget should be set to the starship, or other object that should catch it
    property variant powerUpTarget
    signal powerUpCaught()
    signal powerUpCaughtScorePoints(real scorePoints_)
    signal sendPowerUpMessage(string message)
    signal destroyItem()

    onDestroyItem: {
        root.destroy()
        //console.log("destroyItem powerups ...................................")
    }


    onPowerUpCaught: {
        sendPowerUpMessage( powerUpMessage )
    }


    property bool timerRunning: y > spaceLimitYmin && y < spaceLimitYmax
    //shield should only move when it is in screen bondaries
    property real spaceLimitYmin
    property real spaceLimitYmax

    //shield moves horizontally widthin spaceWidt limits
    property real spaceWidth: mainRoot.width
    property real dist: 0.4*mm

    signal setSpaceLimitsY(var ymin, var ymax)
    onSetSpaceLimitsY: {
        spaceLimitYmin = ymin;
        spaceLimitYmax = ymax;
    }



    Image {
        id: image
        anchors.fill: parent
        //source: "qrc:/images/"+imageName
        mipmap: true
    }



    function move() {
        root.x += dist
        if (root.x > mainRoot.width)
            timerRunning = false
        //destroyShield()
    }

    function destroy() {
        timer.running = false
        root.destroy()
    }

    Text {
        id: tex
        anchors.centerIn: parent
        text: ""
    }

    property real yVelocity: 2*mm

    Timer {
        id: timer
        interval: fps_ms; running: timerRunning; repeat: true
        onTriggered: {
            move()

            if (powerUpTarget){
                if ( lib.distance(powerUpTarget.x+powerUpTarget.width, powerUpTarget.y+powerUpTarget.height, x, y,
                              powerUpTarget.width ) )
                {
                    powerUpCaught()
                    powerUpCaughtScorePoints(scorePoints)

                    //power up was caught, let's destroy it
                    root.visible = false
                    timerRunning = false
                    root.destroy()
                }
            }
        } //onTriggered
    }

    onTimerRunningChanged: {
        if (!timerRunning) {
            //console.log("destroying powerUps out of reach----------------------------------")
            root.destroy()

        }
    }


}


