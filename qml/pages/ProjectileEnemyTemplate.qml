import QtQuick 2.0

ProjectilePhysics2 {
    id: root

    property var bulletTarget: null

    /*property real hitDamage: 1
    property string projectileType: ""
    signal hit(var damage_, var projectileType_) // send when there's a collision*/

    onMove: {
        if (bulletTarget){


            if (lib.checkCollison(
                        Qt.point(bulletTarget.x,bulletTarget.y)//target topLeft
                        ,Qt.point(bulletTarget.x+bulletTarget.width,
                                  bulletTarget.y+bulletTarget.height)//target bottomRight
                        ,Qt.point(x, y) // projectile topLeft
                        ,Qt.point(x+width, y+height) // projectile bottomRight
                        ))
            //if ( lib.distance(bulletTarget.x+bulletTarget.width*0.5, bulletTarget.y+bulletTarget.height*0.5,
              //            midX, midY, bulletTarget.width ) )

            {
                hit(hitDamage, projectileType)
                destroyBullet() //destroy this bullet, it as hit the target
            }//if (lib.checkCollison
        }//if (bulletTarget)
    }//onMove
}
