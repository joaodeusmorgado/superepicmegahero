import QtQuick 2.0
import com.Time 1.0
import "../"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    property real scenarioMoveVelocity: 2

    signal spaceLimitsY(real ymin, real ymax)
    signal spaceLimitYmin(real ymin)
    signal menu()
    signal nextLevel()
    signal youWin()
    signal youLose()
    signal destroyItems()
    signal pressToFire()

    onYouWin: {
        destroyItems()
        panelEOL.text = qsTr("Victory!")
        //panelEOL.showNextBtn = true
        levels.levelUnlocked = levels.levelUnlocked === 1 ? 2 : levels.levelUnlocked
        panelEOL.visible = true
    }

    onYouLose: {
        destroyItems()
        panelEOL.text = qsTr("You lost.")
        //panelEOL.showNextBtn = false
        panelEOL.visible = true
    }

    onVisibleChanged: {
        if (visible) {
            levelStartSetup()
        }
        else {
            levelEndCleanup()
        }
    }


    Flickable {
        id: flickLevel01

        width: mainRoot.width
        height: mainRoot.height
        contentWidth: level01Content.width
        contentHeight: level01Content.height

        interactive: false

        contentY: contentHeight - height
        //onContentYChanged: console.log("contentY:"+contentY)
        Level01Content {
            id: level01Content
            width: mainRoot.width
            height: mainRoot.height * 10
            //widthSpace: mainRoot.width
            //heightSpace: mainRoot.height * 2
        }

    }//Flickable



    MouseArea {
        id: mouseAreaShotFired
        anchors.fill: parent
        onPressed: pressToFire()
    }

    MenuPopUpVertical {
        id: menuPopUp
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }

    ScoreBoard {
        id: scoreBoard
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: defaultMargins
        visible: false
    }

    LifeBar {
        id: starshipLifeBar
        width: 55*mm > parent.width/2  ? parent.width/2 : 55*mm
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
    }

    LifeBar {
        id: badBossLifeBar
        width: starshipLifeBar.width
        anchors.top: starshipLifeBar.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        barColor: "LawnGreen"
        visible: false
    }

    NotificationsBoard {
        id: notificationBoard
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: starshipLifeBar.bottom
        anchors.margins: defaultMargins
    }

    PanelEndOfLevel2 {
        id: panelEOL
        visible: false
        anchors.centerIn: parent
        showNextBtn: levels.levelUnlocked > 1
        onMenu: root.menu()
        onReplay: {
            levelEndCleanup()
            levelStartSetup()
        }
        onNext: root.nextLevel()
    }


    Timer {
        id: timer
        interval: fps_ms
        running: root.visible && !menuPopUp.expand//flickLevel01.contentY > 0;
        repeat: true
        onTriggered: {

            if (flickLevel01.contentY > 0) {
                flickLevel01.contentY -= scenarioMoveVelocity
                spaceLimitsY(flickLevel01.contentY, flickLevel01.contentY+flickLevel01.height)
                spaceLimitYmin(flickLevel01.contentY)

            }
            //var tt = _Time.elapsed();
            //console.log("time elapsed: " +tt)
            //var fps = 1000/tt
            //console.log("fps: "+ fps)
            //_Time.restart();

        }
    }

    //TODO: move levelStartSetup() to Level01Content and create functions to create and destroy connections
    function levelStartSetup() {
        panelEOL.visible = false
        flickLevel01.contentY = flickLevel01.contentHeight - flickLevel01.height
        scoreBoard.score = 0 // todo: create a level score property
        level01Content.createItems()
        //commandTilt.calibrate()
        //commandAccelerometer.calibrateNow = true
    }

    function levelEndCleanup() {
        destroyItems()
        activeEnemiesList.clearEnemies()
    }

}
