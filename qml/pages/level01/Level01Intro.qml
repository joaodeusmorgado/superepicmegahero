import QtQuick 2.0
import "../components"
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    signal close()

    ConversationBoard2 {
        id: talkBoard
        anchors.top: parent.top
        anchors.bottom: btnClose.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        width: parent.width - btnClose.width*2 - 4*defaultMargins

        imageLeft: "asteroidSnorlex.png"
        imageRight: "robonoid.png"
        imageBottom: "asteroidSnorlex.png"

        imageBottomWidth: 50*mm
        imageBottomHeight: 50*mm

        visible: false

        onEndDialog: close()
    }

    MyButton {
        id: btnClose
        //width: btnSize * 1.25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onBtnClick: close()
        text: qsTr("Skip")
    }

    onVisibleChanged: {
        if (visible) {
            addTalk()
            talkBoard.visible = true
        }
    }

    function addTalk() {
        talkBoard.clear()
        //talkBoard.addMessage4(qsTr("Congratulations on finishing the previous level. You're a real hero :)", "asteroidSnorlex.png", 2, false))
        talkBoard.addMessage5(qsTr("Our hero, captain Andrew, travels through space, in his spaceship, looking for adventures."),
                              "", -1, false, "nave.png")
        talkBoard.addMessage5(qsTr("There are enemies everywhere, they can attack at any time..."),
                              "", -1, false, "inimigo02.png")
        talkBoard.addMessage5(qsTr("Control the spaceship by rotating your mobile to up, down, left or right."
                                   + " Click anywhere on the screen fire."
                                   + "\nYour phone will be calibrated, put it in a confortable position and click >> to start."
                                   + "\nIf you have a keyboard, use the arrows key to move, and space key to fire."),
                              "", -1, false, "calibratePhone.png")

    }

}
