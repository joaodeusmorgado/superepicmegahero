import QtQuick 2.3
//import QtMultimedia 5.6
import "../"

Rectangle {
    id: root
    //width: mainRoot.width
    //height: mainRoot.height * 5
    color: "darkblue"

    property int starCount: 80
    property int enemy1Count: 1
    property int enemy2count: 25

    signal scoreUp(real score)

    onScoreUp: {
        scoreBoard.score += score
    }

    /*onNotificationBoard: {
        var notificationBoard = lib.createItem("NotificationsBoard.qml", parent)
        notificationBoard.anchors.centerInParent = level01.Center
    }*/

    /*Connections {
        target: command
        onMove: starship.move(cx, cy)
        onShotFired: starship.prepareFire()
    }*/

    property variant starship
    //property variant doubleFirePower
    //property real yMinLimits: 0 //we'll use this to destroy the spaceship bullets, once they are off screen limits


    /*Connections {
        target: level01
        ignoreUnknownSignals: true
        onSpaceLimitsY: {
            //if (powerUpCow)
              //  powerUpCow.setSpaceLimitsY(ymin, ymax)
        }
    }*/

    /*Connections {
        id: connectCow
        target: powerUpCow
        onPowerUpCaught: {
            starship.activateCow()
        }
        onSendPowerUpMessage: {
            notificationBoard.showMessage(powerUpCow.powerUpMessage)
        }
    }*/

    /*PowerUpsTemplate {
        id: powerUpCow
        x: 0
        y: parent.height/4
        powerUpTarget: starship
        powerUpMessage: "Apanhaste a vaquinha da via láctea!!!\nO leite é delicioso :)"
        imageName: "qrc:/images/vaquinha.png"
    }*/


    Image {
        id: universe
        anchors.fill: parent
        source: "qrc:/images/superepicmegaheroBG.png"
        fillMode: Image.Tile
        horizontalAlignment: Image.AlignLeft
        verticalAlignment: Image.AlignTop
    }

    //cow image
    Image {
        width: 23*mm
        height: 23*mm
        x: parent.width*0.05
        y: parent.height*4/5
        //anchors.fill: parent
        //sourceSize.width: 33
        //sourceSize.height: 33
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/vaquinha1.png"
        rotation: 90
        mipmap: true
    }
    //}


    Component.onCompleted: {
        //createItems()
    }

    CommandTilt {
        id: commandTilt
        active: root.visible && (Qt.platform.os === "android" || Qt.platform.os === "ios")
    }

    CommandRotation {
        id: commandRotation
        active: root.visible && Qt.platform.os === "linux" //for sailfish OS
    }

    function createItems() {

        activeEnemiesList.removeEnemy()

        //console.log("activeEnemiesList.count: "+activeEnemiesList.count+"............................................................")

        //create random stars-------------------------------
        for (var i = 0; i < starCount;i++) {
            //console.log(Math.random()*root.width + ":"+Math.random()*root.height)
            var stars = lib.createItem("Star01.qml", parent)
            stars.x = Math.random()*root.width
            stars.y = Math.random()*root.height
            destroyItems.connect( stars.destroyItem )
        }//create random stars-------------------------------


        // dynamic starship----------------------------------------
        var starshipObj = lib.createItem("Starship.qml", parent)
        starship = starshipObj
        starshipObj.x = parent.width/2
        starshipObj.y = parent.height-starshipObj.height
        starshipObj.z = 1
        starshipObj.spaceLimitXmin = 0
        starshipObj.spaceLimitXmax = parent.width// - starshipObj.width
        starshipObj.spaceLimitYmin = 0
        starshipObj.spaceLimitYmax = parent.height //- starshipObj.height
        //connections
        destroyItems.connect( starshipObj.destroyItem )

        commandKeyb.move.connect( starshipObj.move ) //keyboard move
        commandKeyb.shotFired.connect( starshipObj.prepareFire ) //keyboard fire

        //commandMultiTouchPad.move.connect( starshipObj.move ) //move
        //commandMultiTouchPad.shotFired.connect( starshipObj.prepareFire )//shot fire

        if (Qt.platform.os === "android" || Qt.platform.os === "ios")
            commandTilt.move.connect( starshipObj.move ) //tilt move
        else
            commandRotation.move.connect( starshipObj.move )

        //commandAccelerometer.move.connect( starshipObj.move )

        pressToFire.connect( starshipObj.prepareFire ) //press to fire

        starshipObj.fire.connect( starshipShot )
        starshipObj.doubleFire.connect( starshipDoubleShot )
        //command.shotFired.connect( starshipObj.prepareFire ) //to delete later

        level01.spaceLimitsY.connect( starshipObj.setSpaceLimitsY )//moves the starship with the screen movement
        starshipLifeBar.lifeBarInit(starshipObj.life)
        starshipLifeBar.hitDamage = starshipObj.hitDamage
        starshipObj.imHitAndDestroyed.connect( youLose )
        // dynamic starship----------------------------------------


        //create enemy01------------------------------------------------------------
        for (i = 0; i < enemy1Count;i++) {
            var enemy1 = lib.createItem("Enemy01.qml", parent)
            enemy1.x = Math.random()* (parent.width - enemy1.width)
            enemy1.y = Math.random()*parent.height
            //enemy1.spaceWidth = parent.width
            //enemy1.fire.connect( enemyShot )
            //enemiesModel.append({"obj": enemy1})
        }
        //create enemy01------------------------------------------------------------


        //create enemy02------------------------------------------------------------
        for (i = 0; i < enemy2count;i++) {
            var enemy2 = lib.createItem("Enemy02.qml", parent)
            enemy2.x = Math.random()* (parent.width - enemy2.width)
            enemy2.y = Math.random()* (parent.height - mainRoot.height - enemy2.height)
            enemy2.enemyTarget = starshipObj
            enemy2.fire.connect( enemyShot )

            //make the last enemy the boss
            if (i === enemy2count-1) {
                enemy2.isBoss = true
                //enemy2.x = parent.width/2
                enemy2.x = (parent.width/2 - enemy2.width/2)
                enemy2.y = 1
                enemy2.bossDestroyed.connect( youWin )

                enemy2.gamePaused = Qt.binding( function(){ return menuPopUp.expand} )
                badBossLifeBar.visible = Qt.binding( function(){ return enemy2.timerRunning;} )
                badBossLifeBar.lifeBarInit(enemy2.life)
                badBossLifeBar.hitDamage = enemy2.hitDamage
                enemy2.imHit.connect( badBossLifeBar.hit )
            }

            enemy2.scoreUp.connect( scoreUp ) //when enemy is destroyed you score
            //enemy2.collision.connect( starshipObj.collision ) // add later
            enemy2.collision.connect( youLose )
            enemy2.remove.connect( activeEnemiesList.removeEnemy )
            level01.spaceLimitsY.connect( enemy2.setSpaceLimitsY )
            destroyItems.connect( enemy2.destroyItem )
            //enemy2.collision.connect( collision )
            //enemiesModel.append({"obj": enemy2})
        }
        //create enemy02------------------------------------------------------------

        // dynamic PowerUp Shield
        var shieldObj = lib.createItem("PowerUpsTemplate.qml", parent)
        shieldObj.x = 0
        shieldObj.y = parent.height*3/4
        shieldObj.powerUpTarget = starshipObj
        shieldObj.powerUpMessage = qsTr("Energy shield.")
        //shieldObj.powerUpMessage = "Apanhaste um escudo de energia.\nProtege dos tiros inimigos, parabéns :)"

        shieldObj.powerUpCaught.connect( starshipObj.activateShield )
        shieldObj.sendPowerUpMessage.connect( notificationBoard.showMessage )
        destroyItems.connect( shieldObj.destroyItem )
        level01.spaceLimitsY.connect( shieldObj.setSpaceLimitsY )


        // dynamic PowerUp DoubleFire
        var doubleFireObj = lib.createItem("PowerUpsTemplate.qml", parent)
        //doubleFirePower = doubleFireObj
        doubleFireObj.x = 0
        doubleFireObj.y = parent.height*2/4
        doubleFireObj.powerUpTarget = starshipObj
        doubleFireObj.color = "orange"
        level01.spaceLimitsY.connect( doubleFireObj.setSpaceLimitsY )
        doubleFireObj.powerUpCaught.connect( starshipObj.activateDoubleFirePower )
        //doubleFireObj.powerUpMessage = "Ganhaste tiros duplos.\nViva!!!"
        doubleFireObj.powerUpMessage = qsTr("Double shots!!!")
        doubleFireObj.sendPowerUpMessage.connect( notificationBoard.showMessage )
        destroyItems.connect( doubleFireObj.destroyItem )


        //dymanic PowerUp coins, it increases scores

        var coinsObj = lib.createItem("PowerUpsTemplate.qml", parent)
        coinsObj.x = 0
        coinsObj.y = parent.height*0.90
        //coinsObj.width = 10*mm
        coinsObj.powerUpTarget = starshipObj
        if (heroMap.money > 5)
            coinsObj.scorePoints = 2
        else
            coinsObj.scorePoints = 20
        coinsObj.label.text = qsTr(coinsObj.scorePoints+"$")

        coinsObj.color = "gold"
        coinsObj.opacity = 1
        coinsObj.label.color = "coral"
        coinsObj.label.text.bold = true
        level01.spaceLimitsY.connect( coinsObj.setSpaceLimitsY )
        coinsObj.powerUpCaughtScorePoints.connect( heroMap.addMoney )
        coinsObj.powerUpMessage = qsTr("You win") + " " +coinsObj.scorePoints + " " +qsTr("coins.")
        coinsObj.sendPowerUpMessage.connect( notificationBoard.showMessage )
        destroyItems.connect( coinsObj.destroyItem )



        //dynamic powerup cow, it doesnt do nothing, but the milk is great :)
        var powerUPCowObj = lib.createItem("PowerUpsTemplate.qml", parent)
        powerUPCowObj.x = 0
        //powerUPCowObj.y = parent.height/4
        powerUPCowObj.y = parent.height*0.82
        powerUPCowObj.powerUpTarget = starshipObj

        powerUPCowObj.powerUpMessage = qsTr("Delicious milky way cow milk:)")

        powerUPCowObj.powerUpCaught.connect( heroMap.setMilk )
        powerUPCowObj.sendPowerUpMessage.connect( notificationBoard.showMessage )
        powerUPCowObj.imageName = "qrc:/images/vaquinha.png"
        level01.spaceLimitsY.connect( powerUPCowObj.setSpaceLimitsY )
        destroyItems.connect( powerUPCowObj.destroyItem )

    }

    /*Audio {
        id: sound
        source: "qrc:/sounds/laserfire01.ogg"
        onError: console.log("error play music: "+errorString)
        onPlaying: console.log("start play music")
        onPlaybackStateChanged: console.log("playbackState: "+playbackState)
        onStatusChanged: console.log("status: "+status)
    }*/

    function starshipShot(fx, fy) {
        starshipShotColor(fx, fy, "greenyellow")
    }

    function starshipShotColor(fx, fy, bulletColor) {
        var mybullet = lib.createItem("Bullet.qml", root);
        if (lib.errorCreateObject) {
            console.log("Error creating bullet")
            mybullet = null;
            return;
        }
        //else
          //  console.log("Bullet created sucessfull")

        //sound.stop()
        //sound.play()

        mybullet.width = 1.5*mm
        mybullet.height = 8*mm
        mybullet.color = bulletColor
        mybullet.init(fx, fy)
        //console.log("mybullet.angle: "+mybullet.angle)
        mybullet.angle = -Math.PI/2 //works for android, destop, fails on sailfish
        //mybullet.angle = -Math.PI
        //console.log("mybullet.angle: "+mybullet.angle)

        //mybullet.listOfEnemies = activeEnemiesList.activeEnemiesList

        var aux = (flickLevel01.contentY - fy)/( mybullet.velocity + scenarioMoveVelocity)
        var bulletOffScreenPosition = flickLevel01.contentY + aux * mybullet.velocity

        mybullet.xMinBulletDestroy = 0
        mybullet.xMaxBulletDestroy = mainRoot.width
        mybullet.yMinBulletDestroy = bulletOffScreenPosition
        mybullet.yMaxBulletDestroy = parent.height
    }

    function starshipDoubleShot(fx1, fy1, fx2, fy2) {
        starshipShotColor(fx1, fy1, "red")
        starshipShotColor(fx2, fy2, "red")
    }

    //create bullets fired by the enemy
    function enemyShot(xx, yy) {
        if (starship === null)
            return;
        var enemyBullet = lib.createItem("EnemyBullet.qml", parent)
        enemyBullet.width = 1.5*mm
        enemyBullet.height = 8*mm
        enemyBullet.init(xx, yy)
        enemyBullet.spaceLimitYmax = parent.height
        enemyBullet.bulletTarget = starship
        enemyBullet.yBulletDestroy = yy + mainRoot.height
        enemyBullet.hit.connect( starshipLifeBar.hit )
        enemyBullet.hit.connect( starship.imHit )
    }
}
