import QtQuick 2.3
//import com.Timer 1.0

/*
  enemies are usually added to a list of enemies (defined somewhere else), that way the game loop can check for a hero collision with all enemies
  enemies should only be added to the list when they are "active" ie when they are in the visible area of the screen, for performance reasons
  there's no point of looping throw a list of enemies that are still not visible.
  They should be removed from the list when they are out of the screen, or when they are destroyed.
  The point is, if something is a enemy, it can be destroyed by the hero arrows, thats why they need to be added or removed from a enemies list.
  If enemies are spaceships, dragons, they have motion (inMotion=true), if they are rocks or other objects they should not move (inMotion = false).
*/

Item {
    id: enemyTemplate
    width: 30*mm
    height: width

    property point topLeft: Qt.point(x,y)
    property point bottomRight: Qt.point(x+width,y+height)

    property string name: ""
    property string type: "enemyTemplate"
    property string imageName
    property alias mirrorImage:  enemyImg.mirror
    property bool inMotion: false
    property bool alive: true
    property bool active: false
    property int lifePower: 2 //when lifePower runs out, enemy is destroyed
    property real hitDamage: 1
    property real deltaHit: width*0.5
    property var enemyTarget: null //({})
    property alias enemyImage: enemyImg
   // property var listOfEnemies: ({})
    property real t: 0
    property real dt: 0.04
    property real timerMovementInterval: fps_ms
    property alias timerMovement: timerMovement
    property real enemyHittedScore: 10 //when enemyTemplate is hit we can send the target a enemyHittedScore
    property real enemyDestroyedScore: 50//when enemyTemplate is destroyed we can send the target a enemyDestroyedScore

    property bool showExplosionOnDestroy: false
    property real countexplosionDuration: 0
    property real explosionDuration: 1000

    property bool immuneToArrows: false
    //property bool immuneToAddOthers .....

    //projectile can be "arrow", "girlFirePower", "heroSpaceShipShot"

    signal imHit(var projectilePower_, var projectileType_) // enemy was hit by a generic damage
    signal imHitDamageMade(var projectileDamage_)
    //signal newLifePower(var lifePower_)

    signal scoreEnemyHit(real enemyHittedScore) // enemy was hit, send a enemyHit score
    signal enemyDestroyed() // enemy is about to be destroyed, send a signal that can be used by hero
    signal scoreEnemyDestroyed(real enemyDestroyedScore) // enemy is about to be destroyed, send a enemyDestroyed score

    signal addToEnemiesList(var enemy)
    signal removeFromEnemiesList2(var enemy)
    signal removeFromEnemiesList()

    signal destroyTarget()

    signal move()

    onActiveChanged: {
        if (active) {
            //listOfEnemies.append({"obj": enemyTemplate})
            //console.log("EnemyTemplate signal addToEnemiesList(enemyTemplate)")
            addToEnemiesList(enemyTemplate)
        }
        else
            removeFromEnemiesList()
    }

    onImHit: {
        //console.log("EnemyTemplate.qml signal onImHit:....................................")
        //console.log("immuneToArrows: "+immuneToArrows)
        //console.log("projectileType: "+projectileType_)


        if (immuneToArrows && projectileType_ === "arrow")
            return;
        //add more: if (immuneTo.... && projectileType_ === "otherProjectileTypes......")
        //we can also add shields to protect the enemy from certain types of projectiles


        imHitDamageMade(projectilePower_)//connect to enemy LifeBar

        lifePower -= projectilePower_;
        if (lifePower === 0) {
            prepareDestruction()
            return true; //destroyed
        }
        else {
            scoreEnemyHit(enemyHittedScore)
            return false; //hit but still alive
        }
    }

    onImHitDamageMade: {
        //console.log("EnemyTemplate onImHitDamageMade: "+projectileDamage_)
    }

    Image {
        id: enemyImg
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source: imageName === "" ? "" : "qrc:/images/"+imageName
        visible: true
        mipmap: true
    }

    Timer {
        id: timerMovement
        interval: timerMovementInterval
        running: inMotion
        repeat: true
        onTriggered: {
            //console.log("Enemy "+ name+ " movement.................")
            move()
            t += dt; //makes the enemy move

            //lib.checkCollison(enemyTarget, enemyTemplate)
            checkEnemyHeroCollision()
        }
    }

    //--explosion----------------------------------------------------
    Image {
        id: explosionImg
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/Explosion.png"
        visible: false
        mipmap: true
    }

    Timer {
        id: explosionTimer
        interval: fps_ms
        running: false//!alive && !gamePaused
        repeat: true
        onTriggered: {
            countexplosionDuration += fps_ms
            parent.opacity = 1 - countexplosionDuration/explosionDuration
            if (countexplosionDuration >= explosionDuration) {
                running = false
                doDestruction()
            }
        }
    }
    //--explosion----------------------------------------------------

    function checkEnemyHeroCollision() {
        //console.log("checkEnemyHeroCollision-----------------------")
        //console.log("enemyTarget.x: "+enemyTarget.x)
        //console.log("enemyTemplate.x: "+enemyTemplate.x)

        if (lib.checkCollison(enemyTarget.topLeft, enemyTarget.bottomRight,
                              enemyTemplate.topLeft, enemyTemplate.bottomRight)) {
            //console.log("enemy template - colission detected..........................................")
            destroyTarget() // destroy the hero
            prepareDestruction()
        }
    }

    function show() {
        console.log("I am "+enemyTemplate+", type: "+type)
    }

    function prepareDestruction() {
        if (showExplosionOnDestroy){
            explosionImg.visible = true
            explosionTimer.running = true
        }
        else {
            doDestruction()
        }
    }

    function doDestruction(){
        scoreEnemyDestroyed(enemyDestroyedScore)
        enemyDestroyed()
        inMotion = false
        alive = false
        active = false
        removeFromEnemiesList()
        enemyTemplate.destroy()
    }

}
