import QtQuick 2.3
//import com.Timer 1.0

Item {
    id: dragon
    width: 60*mm
    height: 30*mm

    x: xPos + xMovement * Math.cos(Math.PI*t/9)
    y: yPos + yMovement * Math.cos(Math.PI*t/2)

    signal remove()

    property bool dragonRunning: false
    onDragonRunningChanged: {
        if (dragonRunning)
            activeEnemiesList.append({"obj": dragon})

        console.log("dragonRunning: "+dragonRunning)
    }

    property real xPos
    property real yPos
    property real xPrevious: xPos + xMovement * Math.cos(Math.PI*(t-dt)/9)

    property real xMovement: mainRoot.width/3
    property real yMovement: mainRoot.height/16

    property real t: 0
    property real dt: 0.05

    property bool bForward: x >= xPrevious ? true : false

    signal fire(real fx, real fy)
    //signal destroyItem()
    //onDestroyItem: dragon.destroy()

    state: x >= xPrevious ? "forwardState" : "backwardState"
    /*onStateChanged: {
        console.log("state: " + state + "\nimage rotation: " + bForward)
        console.log("x: "+x + "\nxPrevious: "+xPrevious)
    }*/

    Image {
        id: dragonImg
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/dragao.png"
        visible: true
        mipmap: true
    }

    //dragon spits fire at every 1 second
    Timer {
        id: dragonFire
        interval: 1500;
        running: dragonRunning//dragonMovement.running
        repeat: true
        onTriggered: {
            fire(x+width*0.05, y+height*0.15)
        }
    }


    Timer {
        id: dragonMovement
        interval: fps_ms
        running: dragonRunning
        repeat: true
        onTriggered: {
            t += dt;
            //explosionImg.rotation = dragon.x >= dragon.xPrevious ? 5 : -5
            //dragon.xPrevious = dragon.x
        }
    }


    states: [
        State {
            name: "forwardState"
            PropertyChanges { target: dragonImg; rotation: 10}
        },
        State {
            name: "backwardState"
            PropertyChanges { target: dragonImg; rotation: -10}
        }
    ]

    transitions: [
        Transition {
            NumberAnimation { property: "rotation"; easing.type: Easing.InOutQuad; duration: 1500  }
        }
    ]


}
