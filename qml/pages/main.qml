import QtQuick.Window 2.2
//import QtQuick 2.3
//import QtQml 2.2

Window {
    width: Qt.platform.os === "android" || Qt.platform.os === "ios" ? Screen.width : 800
    height: Qt.platform.os === "android" || Qt.platform.os === "ios" ? Screen.height : 600

    /*
    "android" - Android
    "ios" - iOS
    "tvos" - tvOS
    "linux" - Linux
    "osx" - macOS
    "qnx" - QNX (since Qt 5.9.3)
    "unix" - Other Unix-based OS
    "windows" - Windows
    "wasm" - WebAssembly
*/

    visible: true
    visibility: Qt.platform.os === "android" || Qt.platform.os === "ios" ?
                    Window.FullScreen : Window.AutomaticVisibility

    //flags: Qt.MSWindowsFixedSizeDialogHint
    LevelStatesManagerAndroidDesktop {
        anchors.fill: parent
    }
}
