import QtQuick 2.0

Item {
    id: root

    //property real gravityForce: 0.5*mm
    property bool isFalling: false

    property real worldLimitLeft: 0
    property real worldLimitRight: mainRoot.width
    property real worldLimitSky: 0
    property real worldLimitGround: mainRoot.height

    onXChanged: {
        //console.log("new Item x:"+x)
        //console.log("new Item width x:"+width)
    }


    //if a person cannot move inside a wall (obstacle) save that frontier obstacle position
    //property real obstacleFrontierX
    //property real obstacleFrontierY

    signal startFalling()
    signal stopFalling()

    onIsFallingChanged: isFalling ? startFalling() : stopFalling()

    onStartFalling: console.log("start falling........................")
    onStopFalling: console.log("stop falling.........................")



    function checkPath(nextX, nextY, currentX, currentY) {

        //--check obstacles---------------------------
       // if (checkObstaclesCollision(Qt.point(nextX, nextY), Qt.point(nextX+parent.width, nextY+parent.height))) {
         //   return Qt.point(currentX, currentY);
        //}


        //--check world limits-------------------------

        var ret = Qt.point(nextX, nextY);

        if (nextX < worldLimitLeft)
            ret.x = currentX;

        if (nextX > worldLimitRight)
            ret.x = currentX;

        if (nextY === currentY)
            return ret;

        if (nextY < worldLimitGround) {
            isFalling = true
            //ret.y = ret.y+gravityForce
        }
        else {
            isFalling = false
            ret.y = worldLimitGround
        }        
        return ret;
    }




    ListModel {
        id: obstaclesList
        //obstacles are rectangle that cannot be transposed, like walls for example
        //l is the top left coordinate
        //r is the bottom right coordinate
    }

    function addObstacle(l, r) {
        //console.log("adding obstacles to list")
        obstaclesList.append({"l": l, "r": r})
    }

    function clearObstacles() {
        obstaclesList.clear()
    }

    function checkObstaclesCollision(nextL, nextR) {
        if (obstaclesList.count === 0)
            return false;

        for (var i=0; i < obstaclesList.count; i++) {
            /*
            console.log("object l.x: "+nextL.x)
            console.log("object l.y: "+nextL.y)
            console.log("object r.x: "+nextR.x)
            console.log("object r.y: "+nextR.y)

            console.log("obstacle l.x: "+obstaclesList.get(i).l.x)
            console.log("obstacle l.y: "+obstaclesList.get(i).l.y)
            console.log("obstacle r.x: "+obstaclesList.get(i).r.x)
            console.log("obstacle r.y: "+obstaclesList.get(i).r.y)*/

            if ( rectanglesOverlap(nextL, nextR, obstaclesList.get(i).l , obstaclesList.get(i).r) )
                return true;
            }
        return false;
    }

    function rectanglesOverlap(l1, r1, l2, r2) {
        // If one rectangle is on left side of other
        if (r1.x < l2.x || r2.x < l1.x)
            return false;

        // If one rectangle is above other
        if (r1.y < l2.y || r2.y < l1.y)
            return false;

        return true;
    }


    //check if rectangle 1 is inside rectangle 2
    //l is the top left coordinate
    //r is the bottom right coordinate
    function rectangleInside(l1, r1, l2, r2) {

    }




}
