import QtQuick 2.3

Item {
    id: enemy01
    width: 30*mm
    height: width

    property real spaceWidth: mainRoot.width
    property real spaceLimitYmin
    property real spaceLimitYmax

    Image {
        id: enemyImage
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        //property string num: Math.floor((Math.random() * 3) + 1);
        source: "qrc:/images/inimigoDispara.png"
        mipmap: true
    }

    RotationAnimation on rotation {
        running: true
        duration: 3000
        loops: Animation.Infinite
        from: 0
        to: 360
    }

}
