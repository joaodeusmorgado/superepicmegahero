import QtQuick 2.0
//import com.Timer 1.0

EnemyTemplate {
    id: dragon

    width: 60*mm
    height: width * 428/500
    imageName: "dragon2.png"

    signal fire(real fx, real fy)
    signal destroyItemXY(real x, real y)

    onEnemyDestroyed: {
        destroyItemXY(dragon.x, dragon.y)
    }

    property real xMovement: mainRoot.width/3
    property real yMovement: mainRoot.height/16
    property real xPos
    property real yPos

    lifePower: 3
    dt: 0.05
    x: xPos + xMovement * Math.cos(Math.PI*t/9)
    y: yPos + yMovement * Math.cos(Math.PI*t/2)

    property real xPrevious: xPos + xMovement * Math.cos(Math.PI*(t-dt)/9)
    property bool bForward: x >= xPrevious ? true : false

    state: x >= xPrevious ? "forwardState" : "backwardState"

    states: [
        State {
            name: "forwardState"
            PropertyChanges { target: enemyImage; rotation: 10}
        },
        State {
            name: "backwardState"
            PropertyChanges { target: enemyImage; rotation: -10}
        }
    ]

    transitions: [
        Transition {
            NumberAnimation { property: "rotation"; easing.type: Easing.InOutQuad; duration: 1500  }
        }
    ]

    //dragon spits fire at every 1500 milisecond
    Timer {
        id: dragonFire
        interval: 1500
        running: inMotion
        repeat: true
        onTriggered: {
            fire(x+width*0.05, y+height*0.4)
        }
    }

}
