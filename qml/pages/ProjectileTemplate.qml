import QtQuick 2.0

ProjectilePhysics2 {
    id: root

    property var listOfEnemies: null//({})
    //property real delta: 0 //if enemy distance from bullet is smaller than delta, we have a collision

    Component.onCompleted: {
        listOfEnemies = activeEnemiesList.activeEnemiesList
    }

    onMove: {
        _checkCollisionWithActiveEnemies()
    }

    function _checkCollisionWithActiveEnemies(){
        //todo: check delta value
        for (var i = 0; i<listOfEnemies.count; i++)
        {
            //delta = listOfEnemies.get(i).obj.width/2
            //console.log("listOfEnemies.count"+listOfEnemies.count)
            //console.log("red enemy y: "+ listOfEnemies.get(0).obj.y)
            //console.log("arrow y: "+ bullet.y)

            //check collision between bullet and enemies list,
            //if there's a collision, destroy bullet and send a imHit() signal to the enemy
            //console.log("enemy name: "+listOfEnemies.get(i).obj.name)


            /*if ( lib.distance(listOfEnemies.get(i).obj.x + listOfEnemies.get(i).obj.width/2,
                                         listOfEnemies.get(i).obj.y + listOfEnemies.get(i).obj.height/2,
                                         midX, midY, listOfEnemies.get(i).obj.deltaHit) )*/
            if (lib.checkCollison(
                        Qt.point(listOfEnemies.get(i).obj.x,
                                 listOfEnemies.get(i).obj.y)//enemy topLeft
                        ,Qt.point(listOfEnemies.get(i).obj.x+listOfEnemies.get(i).obj.width,
                                  listOfEnemies.get(i).obj.y+listOfEnemies.get(i).obj.height)//enemy bottomRight
                        ,Qt.point(x, y) // projectile topLeft
                        ,Qt.point(x+width, y+height) // projectile bottomRight
                        ))
            {
                //console.log("ProjectileTemplate.qml - Enemy hit")
                listOfEnemies.get(i).obj.imHit(hitDamage, projectileType) // todo: set a value for the hit power
                destroyBullet()
                return;

            }
        }
    }
}
