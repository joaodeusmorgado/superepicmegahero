import QtQuick 2.0
//import QtMultimedia 5.6

import "level01"
import "level02"
import "level03"
import "level04"
import "level05"
import "level06"
import "level07"
import "level08"
import "level09"
import "level10"
import "level11"
import "level12"
import "level13"
import "levelSettings"
import "libraries"

Rectangle {
    id: levels

    property real btnSize: 20 * mm
    property real defaultMargins: 2 * mm
    property int fps: 60
    property int fps_ms: 1000/fps

    property bool releaseMode: true //set false for development mode, true for releasing the app
    property int levelUnlocked: 1 //releaseMode ? 1 : 100
    //by default 1, only the first level is unlocked at the begining

    property bool girlIsReleased: false
    property bool denWallDestroyed: false


    //property alias heroMap: heroMap
    property alias money: heroMap.money
    property alias moneyUnlocked: heroMap.moneyUnlocked
    property alias fuel: heroMap.fuel
    property alias fuelCan: heroMap.fuelCan
    property alias milk: heroMap.milk
    property alias fruit: heroMap.fruit
    property alias bowAndArrowUnlocked: heroMap.bowAndArrowUnlocked
    property alias starKeyUnlocked: heroMap.starKeyUnlocked
    property alias swordUnlocked: heroMap.swordUnlocked
    property alias carrotsUnlocked: heroMap.carrotsUnlocked
    property alias waterPowderUnlocked: heroMap.waterPowderUnlocked
    property alias treasureBlueDiamondsUnlocked: heroMap.treasureBlueDiamondsUnlocked
    property alias bookOfHapinessUnlocked: heroMap.bookOfHapinessUnlocked

    //property alias levelUnlocked: levels.levelUnlocked
    //property alias girlIsReleased: mainRoot.girlIsReleased
    //property alias denWallDestroyed: mainRoot.denWallDestroyed


    /*Audio {
        id: sound
        source: "qrc:/sounds/bensound-hey.ogg"
        loops: Audio.Infinite
        volume: 0.3
    }*/


    state: "initialPage"

    states: [
        State {
            name: "initialPage"
            PropertyChanges { target: initialPage; opacity: 1 }
        },
        State {
            name: "menu"
            PropertyChanges { target: gameMenu; opacity: 1 }
        },
        State {
            name: "level01Intro"
            PropertyChanges { target: level01Intro; opacity: 1 }
        },
        State {
            name: "level01"
            //PropertyChanges { target: initialPage; opacity: 0 }
            PropertyChanges { target: level01; opacity: 1 }
        },
        State {
            name: "level02Intro"
            PropertyChanges { target: level02Intro; opacity: 1 }
        },
        State {
            name: "level02"
            PropertyChanges { target: level02; opacity: 1 }
        },
        State {
            name: "level03Intro"
            PropertyChanges { target: level03Intro; opacity: 1 }
        },
        State {
            name: "level03"
            PropertyChanges { target: level03; opacity: 1 }
        },
        State {
            name: "level04Intro"
            PropertyChanges { target: level04Intro; opacity: 1 }
        },
        State {
            name: "level04"
            PropertyChanges { target: level04; opacity: 1 }
        },
        State {
            name: "level05"
            PropertyChanges { target: level05; opacity: 1 }
        },
        /*State {
            name: "level05KoniaIntro"
            PropertyChanges { target: level05KoniaIntro; opacity: 1 }
        },*/
        State {
            name: "level05Konia"
            PropertyChanges { target: level05Konia; opacity: 1 }
        },
        State {
            name: "level06"
            PropertyChanges { target: level06; opacity: 1 }
        },
        State {
            name: "level07"
            PropertyChanges { target: level07; opacity: 1 }
        },
        State {
            name: "level07Intro"
            PropertyChanges { target: level07Intro; opacity: 1 }
        },
        State {
            name: "level08Intro"
            PropertyChanges { target: level08Intro; opacity: 1 }
        },
        State {
            name: "level08"
            PropertyChanges { target: level08; opacity: 1 }
        },
        State {
            name: "level09Intro"
            PropertyChanges { target: level09Intro; opacity: 1 }
        },
        State {
            name: "level09"
            PropertyChanges { target: level09; opacity: 1 }
        },
        /*State {
            name: "shop"
            PropertyChanges { target: shop; opacity: 1 }
        },*/
        State {
            name: "level10"
            PropertyChanges { target: level10; opacity: 1 }
        },
        State {
            name: "level11"
            PropertyChanges { target: level11; opacity: 1 }
        },
        State {
            name: "level12"
            PropertyChanges { target: level12; opacity: 1 }
        },
        State {
            name: "level13"
            PropertyChanges { target: level13; opacity: 1 }
        },
        State {
            name: "levelSettings"
            PropertyChanges { target: levelSettings; opacity: 1 }
        },
        State {
            name: "levelFake"
            PropertyChanges { target: levelFake; opacity: 1 }
        },
        State {
            name: "aboutPage"
            PropertyChanges { target: aboutPage; opacity: 1 }
        }
        /*,
        State {
            name: "licensePage"
            PropertyChanges { target: licensePage; opacity: 1 }
        }*/
    ]

    transitions: [
        Transition {
            //NumberAnimation { property: "opacity"; easing.type: Easing.InOutQuad; duration: 4500  }
            NumberAnimation { property: "opacity"; duration: 1000  }
        }
    ]

    //property string currentPage: "initialPage"

    InitialPage {
        id: initialPage
        opacity: 0
        visible: opacity
        onPlay: levels.state = "menu"
    }


    GameMenu {
        id: gameMenu
        opacity: 0
        visible: opacity//mainRoot.state === "menu"
        //opacity: mainRoot.state === "menu" ? 1 : 0
        //enabled: visible
        onLevel01Intro: levels.state = "level01Intro"
        onLevel02Intro: levels.state = "level02Intro"
        onLevel03: levels.state = "level03Intro"
        onLevel04: levels.state = "level04Intro"
        onLevel05: levels.state = "level05"
        onLevel06: levels.state = "level06"
        onLevel07: levels.state = "level07Intro"
        onLevel08: levels.state = "level08Intro"
        onLevel09: levels.state = "level09Intro"
        onLevel10: levels.state = "level10"
        onLevel11: levels.state = "level11"
        onLevel12: {
            levels.state = "level12"
            console.log("levels.state: "+levels.state)
        }
        onLevel13: levels.state = "level13"
        onAbout: levels.state = "aboutPage"
        onSettings: levels.state = "levelSettings"
        onSigExit: Qt.quit();
    }

    Level01Intro {
        id: level01Intro
        opacity: 0
        visible: opacity//mainRoot.state === "level01"
        //opacity: mainRoot.state === "level01" ? 1 : 0
        //enabled: visible
        onClose: levels.state = "level01"
    }

    Level01 {
        id: level01
        opacity: 0
        visible: opacity//mainRoot.state === "level01"
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level02Intro"
    }

    Level02Intro {
        id: level02Intro
        opacity: 0
        visible: opacity
        onClose: levels.state = "level02"
    }

    Level02 {
        id: level02
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level03Intro"
    }

    Level03Intro {
        id: level03Intro
        opacity: 0
        visible: opacity
        onClose: levels.state = "level03"
    }


    Level03 {
        id: level03
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level04Intro"
    }


    Level04Intro {
        id: level04Intro
        opacity: 0
        visible: opacity
        onClose: levels.state = "level04"
    }

    Level04 {
        id: level04
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level05"
    }

    Level05 {
        id: level05
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onGoSavePhones: levels.state = "level05Konia"
        //onNextLevel: levels.state = "level06"
    }

    /*Level05KoniaIntro {
        id: level05KoniaIntro
        opacity: 0
        visible: opacity
        onGoSavePhones: levels.state = "level05Konia"
        //onMenu: mainRoot.state = "menu"
    }*/

    Level05Konia {
        id: level05Konia
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level06"
    }

    Level06 {
        id: level06
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level07Intro"
    }

    Level07Intro {
        id: level07Intro
        opacity: 0
        visible: opacity
        onClose: levels.state = "level07"
    }

    Level07 {
        id: level07
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level08Intro"
    }

    Level08Intro {
        id: level08Intro
        opacity: 0
        visible: opacity
        onClose: levels.state = "level08"
    }

    Level08 {
        id: level08
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level09Intro"
    }

    Level09Intro {
        id: level09Intro
        opacity: 0
        visible: opacity
        onClose: levels.state = "level09"
    }

    Level09 {
        id: level09
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level10"
    }

    Level10 {
        id: level10
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onNextLevel: levels.state = "level11"
        //onEnterShop: levels.state = "shop"
        //onEnterTreasureMountain: levels.state = "levelMountainLair"
    }

    Level11 {
        id: level11
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
        onShowAboutDialogue: levels.state = "aboutPage"
    }

    /*Loader {
        id: level12
        anchors.fill: parent
        enabled: mainRoot.state === "level12"
        onEnabledChanged: {
            console.log("level12 enabled: "+enabled)
            source = visible ? "LevelWorld3D.qml" : ""
        }
    }*/

    Level12 {
        id: level12
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
    }

    Level13 {
        id: level13
        opacity: 0
        visible: opacity
        onMenu: levels.state = "menu"
    }

    Level0fake {
        id: levelFake
        opacity: 0
        visible: opacity//mainRoot.state === "levelFake"
        onClose: levels.state = "menu"
    }

    LevelSettings {
        id: levelSettings
        opacity: 0
        visible: opacity//mainRoot.state === "levelSettings"
        onExit: levels.state = "menu"
        onShowLicense: levels.state = "licensePage"
    }

    About {
        id: aboutPage
        //id: licensePage
        opacity: 0
        visible: opacity//mainRoot.state === "aboutPage"
        onClose: levels.state = "menu"
    }

    /*LicenseFile {
        id: licensePage
        //id: aboutPage
        opacity: 0
        visible: opacity
        onClose: levels.state = "menu"
    }*/

    //Comon stuff for all levels-------------------------------

    CommandKeyboard {
        id: commandKeyb
    }

    /*CommandTilt {
        id: commandTilt
        active: level01.visible && Qt.platform.os === "android"
        //onActiveChanged: console.log("commandTilt active: " + commandTilt.active)
        //Component.onCompleted: console.log("Component.onCompleted Tilt---------"+commandTilt.active)
    }

    CommandRotation {
        id: commandRotation
        active: level01.visible && Qt.platform.os === "linux" //for sailfish OS
        //onActiveChanged: console.log("command Rotantion active is: "+active)
    }*/

    /*CommandAccelerometer {//works with android but fails on sailfish
        id: commandAccelerometer
        active: false//level01.visible
    }*/

    MyBook {
        id: heroMap
        visible: false
    }

    MyLibrary {
        id: lib
    }

    ListOfEnemies {
        id: activeEnemiesList
    }

    ObstacleCollision {
        id: obstaclesList
    }

}
