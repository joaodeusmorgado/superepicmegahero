import QtQuick 2.3

Item {
    id: root
    width: 45*mm //22*mm
    height: width

    rotation: 45
    //radius: width/2
    //border.width: mm
    //border.color: "black"
    //opacity: 0.6

   /* Rectangle {
        id: ci2
        width: parent.width/2
        height: ci2.width
        radius: ci2.width/2
        anchors.centerIn: parent
        border.color: "black"
        opacity: parent.opacity / 2
    }*/

    property real opac: 0.8
    Grid {
        anchors.fill: parent
        columns: 2
        spacing: 0

        Image {
            id: img1
            width: parent.width*0.5; height: parent.height*0.5
            mipmap: true
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/commandCircular.png"
            opacity: opac
        }
        Image {
            id: img2
            width: parent.width*0.5; height: parent.height*0.5
            mipmap: true
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/commandCircular.png"
            opacity: opac
            rotation: 90
        }
        Image {
            id: img3
            width: parent.width*0.5; height: parent.height*0.5
            mipmap: true
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/commandCircular.png"
            opacity: opac
            rotation: -90
        }
        Image {
            id: img4
            width: parent.width*0.5; height: parent.height*0.5
            mipmap: true
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/commandCircular.png"
            opacity: opac
            rotation: 180
        }
    }//Grid

}

/*
Rectangle {
    id: root
    width: 45*mm //22*mm
    height: width
    radius: width/2
    //border.width: mm
    border.color: "black"
    opacity: 0.6

   Rectangle {
        id: ci2
        width: parent.width/2
        height: ci2.width
        radius: ci2.width/2
        anchors.centerIn: parent
        border.color: "black"
        opacity: parent.opacity / 2
    }
}
*/
