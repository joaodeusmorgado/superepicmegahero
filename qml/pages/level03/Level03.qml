import QtQuick 2.0
import "../"
import "../components"

Rectangle {
    id: level03id
    width: mainRoot.width
    height: mainRoot.height
    color: "grey"


    signal menu()
    signal nextLevel()
    signal levelWin()

    onLevelWin: {
        levels.levelUnlocked = levels.levelUnlocked === 3 ? 4 : levels.levelUnlocked
        level03Content.levelEndCleanup()
        panelEOL.text = qsTr("Victory!")
        //panelEOL.showNextBtn = true
        panelEOL.visible = true
    }

    CommandMultiTouchPad3 {
       id: commandTouchPad
       anchors.left: parent.left
       anchors.bottom: parent.bottom
       anchors.margins: defaultMargins
    }

    Flickable {
        id: flickLevel
        width: mainRoot.width
        height: mainRoot.height
        contentWidth: level03Content.width
        contentHeight: level03Content.height
        interactive: false
        Level03Content {
            id: level03Content
            width: mainRoot.width * 11
            height: mainRoot.height * 2
        }
    }//Flickable


    TouchPad2 {
        id: touchPad
        centerX: commandTouchPad.touchPad.centerX
        centerY: commandTouchPad.touchPad.centerY
        visible: commandTouchPad.touchPad.visible
    }

    MenuPopUpVertical {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }

    PanelEndOfLevel2 {
        id: panelEOL
        visible: false
        anchors.centerIn: parent
        showNextBtn: levels.levelUnlocked > 3
        onMenu: parent.menu()
        onReplay: {
            level03Content.levelStartSetup()
        }
        onNext: parent.nextLevel()
    }

    LifeBar {
        id: heroLifeBar
        anchors.bottom: parent.bottom
        anchors.right: dragonLifeBar.left
        anchors.margins: defaultMargins
        //visible: false //level03Content.leaderObj === level03Content.hero
        visible: ( level03Content.screenNumber === 3 || level03Content.screenNumber === 7
                    || level03Content.screenNumber === level03Content.badBossScreenNumber ) && (level03Content.hero === level03Content.leaderObj)
    }

    LifeBar{
        id: girlLifeBar
        anchors.bottom: parent.bottom
        anchors.right: dragonLifeBar.left
        anchors.margins: defaultMargins
        barColor: "pink"
        visible: ( level03Content.screenNumber === 3 || level03Content.screenNumber === 7
                    || level03Content.screenNumber === level03Content.badBossScreenNumber ) && (level03Content.girl === level03Content.leaderObj)
    }

    LifeBar {
        id: dragonLifeBar
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: defaultMargins
        visible: level03Content.screenNumber === 3 && level03Content.dragonObj !== null
        barColor: "orange"
    }

    LifeBar {
        id: badBossLifeBar
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: defaultMargins
        visible: level03Content.screenNumber === level03Content.badBossScreenNumber && level03Content.enemyBossObj !== null
        barColor: "blue"
    }

}
