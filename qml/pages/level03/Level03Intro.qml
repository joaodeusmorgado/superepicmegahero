import QtQuick 2.0
import "../"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    //color:  colors.mainColor

    signal close()

    ConversationBoard2 {
        id: talkBoard
        //color: "lightcyan"
        anchors.top: parent.top
        anchors.bottom: btnClose.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        width: parent.width - btnClose.width*2 - 4*defaultMargins

        imageLeft: "asteroidSnorlex.png"
        imageRight: "robonoid.png"
        imageBottom: "asteroidSnorlex.png"

        imageBottomWidth: talkBoard.width * 0.8
        imageBottomHeight: talkBoard.height * 0.6 //180*mm

        visible: false

        onEndDialog: close()//btnClose.text = qsTr("Ok")
    }

    MyButton {
        id: btnClose
        //width: btnSize * 1.25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onBtnClick: close()
        text: qsTr("Skip")
    }

    onVisibleChanged: {
        if (visible) {
            addTalk()
            talkBoard.visible = true
        }
    }

    function addTalk() {
        talkBoard.clear()
        talkBoard.addMessage5(qsTr("Captain Andrew arrives to a new planet, it's time to explore it. "
                                   +"Poke around, there's lots of hidden stuff to be found that can help you when you're stuck.")
                                   , "", -1, false, "hero1.png")
        talkBoard.addMessage5(qsTr("Press anywhere, a move command will appear.\nSlide left or right to move, slide up & left or right to jump."
                                   +"\nIf you're using a keyboard, you can use the arrows keys to move left, right and jump.")
                                   , "", -1, false, "commandHeroMovements.png")


        //talkBoard.addMessage5(qsTr("To fire a bow, press anywhere to bring the move command. Don't release, press with the other finger"
          //                         + " the position where to shot the arrow. Go now!!!")
            //                       , "", -1, false, "commandShots.png")

    }






    /*signal close()

    Flickable {
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: parent.height //- btnOkCancel.height
        contentWidth: parent.width
        contentHeight: helpColumn.height + defaultMargins * 2

        Column {
            id: helpColumn
            spacing: defaultMargins
            anchors.top: parent.top
            anchors.topMargin: defaultMargins
            anchors.left: parent.left
            anchors.leftMargin: defaultMargins
            anchors.right: parent.right
            anchors.rightMargin: defaultMargins

            ImageFrame {
                width: 20 * mm//root.width * 0.2
                source: "qrc:/images/hero01.png"
            }

            Rectangle {
                id: rect1
                width: parent.width
                height: aboutText.height + 2 * defaultMargins
                radius: 2 * mm
                color: root.color
                border.width: mm
                border.color: "orange"


                TextTemplate {
                    id: aboutText
                    width: parent.width - 2 * defaultMargins
                    anchors.top: rect1.top
                    anchors.left: parent.left
                    anchors.margins: defaultMargins
                    bold: false
                    font: "Purisa"
                    //pixelSize: 18//8*mm

                    //property string messagePT: "O capitão André chega a um planeta, com atmosfera igual à do seu planeta."
                    //+" É hora de explorar o novo planeta, à procura de aventuras."

                    label: qsTr("Captain Andrew arrives to a planet, with atmosphere equal to his planet."
                                + "\nIt's time to explore the new planet searching for adventures.")
                }
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: close()
    }
*/
}
