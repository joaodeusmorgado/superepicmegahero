import QtQuick 2.3
import "../"
import "../components"
import "../libraries"

Rectangle {
    id: root
    width: mainRoot.width * 11
    height: mainRoot.height * 2
    color: "grey"

    property alias hero: hero
    property alias girl: girl
    property var leaderObj: hero //horseHeroGirl
    property var followerObj: girl
    property var bushPlantObj: ({})
    property var enemyRobotSwordObj: null
    property var dragonObj: null
    property var enemyBossObj: null
    property var swordRobotObj: null
    property var starKeyObj: ({})
    property var swordObj: null
    property var checkHeroHoleDistanceObj: ({})
    property bool isStarKey: false  //same for starKey
    property real surfaceGroundLimit: sky.height+hero.height/2
    property real denGroundLimit: parent.height - den.rocksHeight - defaultMargins
    property int screenNumber: Math.floor(leaderObj.x / flickLevel.width)
    property int screenRow: Math.floor(leaderObj.y / flickLevel.height)

    property bool girIsFiring: false
    property bool girlPowerUnleashed: false

    property int badBossScreenNumber: 9

    onEnemyBossObjChanged: {
        //console.trace()
        console.log("enemyBoss changed: "+enemyBossObj)
    }

    onScreenNumberChanged: {
        //console.log("I'm on screen number: "+screenNumber)
        connectionEnabler()
    }

    onScreenRowChanged: {
        //console.log("I'm on screen row: "+screenRow)
        connectionEnabler()
    }

    function connectionEnabler() {
        if (screenRow === 1 && screenNumber === 4)
            wallConnection.target = activeEnemiesList
        else
            wallConnection.target = null
    }

    signal destroyItems()
    signal levelFailed()
    signal wallDestroyed()

    signal badBossDestroyed()

    onBadBossDestroyed: {
        console.log("Bad Boss destroyed.........................")
        createDialogueGirlWinsBadBossFight()
        talkBoard.showDelay(1000)
    }

    onVisibleChanged: {
        if (visible) {
            levelStartSetup()
        }
        else {
            levelEndCleanup()
        }
    }

    onLevelFailed: {
        levelEndCleanup()
        panelEOL.text = qsTr("You lost.")
        panelEOL.visible = true
    }

    Connections {
        id: wallConnection
        target: null//activeEnemiesList
        onClearedLastEnemy: wallDestroyed()
        //enabled: (screenRow === 1 && screenNumber === 4)
        // comment above line on sailfish os, for compatibility
        //logic is moved in function connectionEnabler()
      }

    onWallDestroyed: {
        console.log("wall destroyed......................................")
        denWallDestroyed = true
        hero.worldLimitRight = mainRoot.width * 4 + den.width - den.rocksWidth
        girl.worldLimitRight = mainRoot.width * 4 + den.width - den.rocksWidth
    }

    Rectangle {
        id: sky
        width: parent.width
        height: mainRoot.height * 0.7 // 70% of the screen height
        color: "lightcyan"
        anchors.top: parent.top

        gradient: Gradient {
                  GradientStop { position: 0.0; color: "#0099ff" }
                  GradientStop { position: 0.33; color: "lightblue" }
                  GradientStop { position: 1.0; color: "lightcyan" }
              }
    }

    property alias grass: grass
    Rectangle {
        id: grass
        width: parent.width
        height: mainRoot.height - sky.height
        color: "lime"
        anchors.top: sky.bottom

        gradient: Gradient {
                  GradientStop { position: 0.0; color: "lime" }
                  GradientStop { position: 0.66; color: "#52d633" }
                  GradientStop { position: 1.0; color: "#339933" }//#42b842
              }
    }

    Tree {
        id: tree01
        x: level03id.width * 0.5
        anchors.bottom: grass.bottom
        anchors.bottomMargin: grass.height * 0.8
        onFruitFalled: {
            heroMap.fruit = true
            msgBoardlevel3.x = mainRoot.width*0.5 - msgBoardlevel3.width/2
            msgBoardlevel3.y = defaultMargins
            msgBoardlevel3.text = qsTr("You found a delicious fruit!!!")
            msgBoardlevel3.image = "fruit.png"
            msgBoardlevel3.imageHeight = 20*mm
            msgBoardlevel3.imageWidth = msgBoardlevel3.imageHeight * 51/66
            msgBoardlevel3.visible = true
        }
    }

    PersonCharacter {
        id: milkGuy
        x: level03id.width * 1.5
        y: sky.height + grass.height * 0.2 - this.height
        imgName: "robonoid.png"
    }

    Person2 {
        id: girl
        imgName: "girl"
        imgFireShot: "girlFire"
        name: "Pandora"
        z: 1
        //worldLimitLeft: 0
        //worldLimitRight: parent.width
        //worldLimitSky: 0
        //worldLimitGround: cage.y+cage.height-defaultMargins //groundLimit
        onImHit: girlLifeBar.hit()
        maxLife: 10
        onImDestroyed: {
            levelFailed(qsTr("Level failled!!!"))
        }
        function reset() {
            dir = girlIsReleased ? 1 : -1
            x = girlIsReleased ? hero.x : cage.x+cage.width-girl.width

            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0

            lifeInit()
            girlLifeBar.lifeBarInit(girl.life)

            if (girlIsReleased) {
                interfaceWalkGirlHero.leader = hero
                interfaceWalkGirlHero.follower = girl
                interfaceWalkGirlHero.connect()
                worldLimitGround = surfaceGroundLimit
                y1 = worldLimitGround
                y1Previous = worldLimitGround
                dy = 0 //quick fix a strange bug where default value of dy is not 0
            }
            else {
                worldLimitGround = denGroundLimit
                y1 = cage.y+cage.height-defaultMargins
                y1Previous = cage.y+cage.height-defaultMargins
                dy = 0 //quick fix a strange bug where default value of dy is not 0
            }
        }
        mouseAreaEnabled: false
        onDoubleClick: {
            console.log("girl mouse area is:"+mouseAreaEnabled)
            setLeaderFollower(girl, hero)
            mouseAreaEnabled = false
            hero.mouseAreaEnabled = true
        }
    }

    Cage {
        id: cage
        x: den.x+mainRoot.width*1.5
        y: root.height-cage.height-den.rocksHeight
        z: 1.1
        isOpen: girlIsReleased
    }

    /*Connections {
        enabled: true
        target: hero
        onImDestroyed: levelFailed()
    }*/

    Person2 {
        id: hero
        width: 18*mm
        height: width * 320/156
        //jumpLenght: width*3.5
        name: "Andrew"
        z: 1.2
        //worldLimitGround: surfaceGroundLimit
        onImDestroyed: {
            //heroLifeBar.hitDestroy()
            levelFailed(qsTr("Level failled!!!"))
        }
        onImHit: heroLifeBar.hit()

        function reset(){
            hero.dir = 1
            visible = true
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
            worldLimitGround = surfaceGroundLimit
            x = 0//8.5*mainRoot.width
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
            lifeInit()
            heroLifeBar.lifeBarInit(hero.life)
            leaderObj = hero
        }
        mouseAreaEnabled: false
        onDoubleClick: {
            setLeaderFollower(hero, girl)
            mouseAreaEnabled = false
            girl.mouseAreaEnabled = true
        }

        onLifeChanged: {
            if (maxLife > 3 && life === 3 && screenNumber === 9) {
                console.log("Hero is fighting the bad boss and about to die.................")
                talkBoard.x = mainRoot.width*9.5 - talkBoard.width/2
                talkBoard.y = defaultMargins

                talkBoard.conversationType = "girlAskIfHeroNeedsHelpToFight"
                createDialogueGirlAskIfHeroNeedsHelpToFight()
                talkBoard.show()
            }
        }

    }

    Sword {
        id: sword
        x: den.x+mainRoot.width*2.5
        y: root.height-sword.width-den.rocksHeight
        rotation: 90
        visible: !heroMap.swordUnlocked
    }

    Image {
        id: btnUp
        width: btnSize
        height: btnSize * 87/73
        x: mainRoot.width * 7 - btnUp.width - den.rocksWidth
        y: mainRoot.height * 2 - btnUp.height - hero.height - den.rocksHeight
        source: btnUpMA.pressed ? "qrc:/images/buttonUp2.png" : "qrc:/images/buttonUp.png"
        mipmap: true
        MouseArea {
            id: btnUpMA
            anchors.fill: parent
            onClicked: upDown = !upDown
            enabled: !elevatorUp.running && !elevatorDown.running
        }
        visible: girlIsReleased ? (hero.x > elevator.x) && (girl.x > elevator.x) : (hero.x > elevator.x)
        //visible: leaderObj.x > (elevator.x + elevator.width*0.5)
    }

    property bool upDown: false
    onUpDownChanged: {
        if (upDown)
            elevatorUp.start()
        else
            elevatorDown.start()
    }

    Image {
        id: elevator
        width: hero.width*3
        height: den.rocksHeight
        x: mainRoot.width * 7 - elevator.width - den.rocksWidth
        y: mainRoot.height * 2 - elevator.height
        source: "qrc:/images/elevator.png"
        mipmap: true
    }

    NumberAnimation {
        id: elevatorUp
        target: elevator; properties: "y"; to: mainRoot.height
        duration: 2500
        onStarted: {
            heroUp.start()
            if (girlIsReleased)
                girlUp.start()
        }
        onStopped: {
            if (elevator.y === mainRoot.height)
            outOfDen()
        }
    }

    NumberAnimation {
        id: elevatorDown
        target: elevator; properties: "y"; to: denGroundLimit//mainRoot.height*2-den.rocksHeight
        duration: elevatorUp.duration
    }

    NumberAnimation {
        id: heroUp
        targets: hero; properties: "y1"; to: mainRoot.height
        duration: elevatorUp.duration
    }

    NumberAnimation {
        id: girlUp
        targets: girl; properties: "y1"; to: mainRoot.height
        duration: elevatorUp.duration
    }


    DenContent {
        id: den
        anchors.top: grass.bottom
        height: mainRoot.height
        width: mainRoot.width * 3
        x: mainRoot.width * 4
        //visible: (screenRow === 1 && screenNumber === 4)
        onVisibleChanged: {
            //console.log("den visible:"+den.visible)
            if (visible) {
                den.addRocks()
                if (!denWallDestroyed)
                    den.createDestroyableRocks()
            }
            else {
                den.removeRocks()
                //den.removeAllDestroyableRocks //causes a bug of hero not being stopped by the wall
            }
        }
    }

    /* //when hero falls in hole, hide it under this rectangle
    Rectangle {
        id: grassUnderHoleToHideFallingHero
        color: "lightblue"//grass.color
        //x: hole.x//mainRoot.width * 4.2
        //width: hole.width
        width: hole.width*2
        anchors.horizontalCenter: hole.horizontalCenter
        anchors.top: hole.bottom
        anchors.topMargin: -hole.height/2
        anchors.bottom: grass.bottom
        //height: mainRoot.height - hole.y - hole.height+defaultMargins
        //holeObj//.width =  40*mm //50*mm
    }*/

    PersonCharacter {
        id: hole
        width: 40*mm //50*mm
        height: width*0.221 //25*mm
        x: mainRoot.width * 4.3
        y: sky.height+hero.height/2-height*0.5//sky.height + hole.height
        imgName: "hole.png"
    }

    Image {
        id: flower1
        width: height * 196/342
        height: 38*mm
        x: mainRoot.width * 6.3
        y: sky.height+hero.height/2 - height
        source: "qrc:/images/flower1.png"
        mipmap: true
        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                msgBoardlevel3.x = mainRoot.width*6.5 - msgBoardlevel3.width*0.5
                msgBoardlevel3.y = defaultMargins
                if (hero.maxLife < 5) {
                    hero.maxLife = 5
                    heroLifeBar.lifeBarInit(5)
                    msgBoardlevel3.text = qsTr("I will increase your life power to 5!!!")
                    msgBoardlevel3.visible = true
                }
                else {
                    msgBoardlevel3.text = qsTr("I like to drink a lot of water. Delicious!!!")
                    msgBoardlevel3.visible = true
                }
            }
        }
    }

    Image {
        id: flower2
        width: height * 114/354
        height: 38*mm
        x: mainRoot.width * 8.3
        y: sky.height+hero.height/2 - height
        source: "qrc:/images/flower2.png"
        mipmap: true
        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                msgBoardlevel3.x = mainRoot.width*8.5 - msgBoardlevel3.width*0.5
                msgBoardlevel3.y = defaultMargins
                if (hero.maxLife < 10) {
                    hero.maxLife = 10
                    heroLifeBar.lifeBarInit(10)
                    msgBoardlevel3.text = qsTr("I will increase your life power to 10!!!")
                    msgBoardlevel3.visible = true
                }
                else {
                    msgBoardlevel3.text = qsTr("I really like sunny days, they're so good to my petals!")
                    msgBoardlevel3.visible = true
                }
            }
        }
    }

    /*Image {
        id: flower2
        width: height * 114/354
        height: 38*mm
        anchors.bottom: flower1.bottom
        anchors.left: flower1.right
        anchors.leftMargin: flower2.width*2
        source: "qrc:/images/flower2.png"
        mipmap: true
    }*/




    PersonCharacter {
        id: flagEndOfLevel
        width: height * 124/350
        height: 80*mm //25*mm
        x: mainRoot.width * 10.5
        //y: grass.y+grass.height/2 - flagEndOfLevel.height
        y: surfaceGroundLimit - height
        imgName: "flag.png" // to change later
    }

    Grid {
        anchors.right: parent.right
        anchors.top: grass.top
        height: grass.height
        width: height
        rows: 3

        Image {
            height: grass.height/3; width: height;source: "qrc:/images/gopherHole.png"; mipmap: true
        }
        Image {
            height: grass.height/3; width: height;source: "qrc:/images/gopherHole.png"; mipmap: true
        }
        Image {
            height: grass.height/3; width: height;source: "qrc:/images/gopherHole.png"; mipmap: true
        }
        Image {
            height: grass.height/3; width: height;source: "qrc:/images/gopherHole.png"; mipmap: true
        }
        Image {
            height: grass.height/3; width: height;source: "qrc:/images/gopherHole.png"; mipmap: true
        }
        Image {
            height: grass.height/3; width: height;source: "qrc:/images/gopherHole.png"; mipmap: true
        }
        Image {
            height: grass.height/3; width: height;source: "qrc:/images/gopherHole.png"; mipmap: true
        }
        Image {
            height: grass.height/3; width: height;source: "qrc:/images/gopherHole.png"; mipmap: true
        }
        Image {
            height: grass.height/3; width: height;source: "qrc:/images/gopherHole.png"; mipmap: true
        }
    }

    /*InterfaceWalkPartner {
        id: interfaceWalkGirlHero
        //leader: hero
        //follower: girl
        //call function connect() when the hero saves the girl from the cage
        // that will make the girl walk, following the hero
    }*/

    InterfaceWalkPartner {
        id: interfaceWalkGirlHero
        leader: leaderObj
        follower: followerObj
        //call function connect() to make the follower follow the leader
    }


    CheckPersonNear {
        id: checkFlagHeroDistance
        me: flagEndOfLevel
        target: leaderObj
        onTargetChangedNear: levelWin()
        isEnabled: screenNumber === 10
        debugInfo: "target flag end of level"
    }

    CheckPersonNear {
        id: checkMilkGuyHeroDistanceObj
        me: milkGuy
        target: hero
        onTargetChangedNear: {
            createDialogueHeroMilkGuy()
            talkBoard.x = milkGuy.x - talkBoard.width/2
            talkBoard.y = defaultMargins
            talkBoard.show()
        }
        onTargetChangedFar: talkBoard.visible = false
        isEnabled: screenNumber === 1
        debugInfo: "target milk guy"
    }

    CheckPersonNear {
        id: checkHeroHoleDistance
        me: hole
        target: leaderObj
        distanceNear: leaderObj.height*0.75//hero.width/2+hole.width/2
        onTargetChangedNear: {
            fallOnHole()
        }
        isEnabled: screenNumber === 4 && screenRow === 0
        debugInfo: "target hole"
    }


    CheckPersonNear {
        id: checkHeroCageDistance
        me: cage
        target: hero
        distanceNear: cage.width
        onTargetChangedNear: {
            talkBoard.x = mainRoot.width*5.5 - talkBoard.width/2
            talkBoard.y = mainRoot.height+defaultMargins
            createDialogueHeroGirl()
            talkBoard.show()
        }
        onTargetChangedFar: {
            talkBoard.visible = false
        }
        isEnabled: screenNumber === 5 && screenRow === 1 && !girlIsReleased
        debugInfo: "target girl show dialogue"
    }


    CheckPersonNear {
        id: checkStarKeyCageDistance
        me: cage
        target: starKeyObj
        onTargetChangedNear: {
            //cage.isOpen = true
            interfaceWalkGirlHero.leader = hero
            interfaceWalkGirlHero.follower = girl
            interfaceWalkGirlHero.connect()
            girlIsReleased = true
        }
        distanceNear: cage.width * 1.2 // todo: check this value
        isEnabled: isStarKey && screenNumber === 5 && screenRow === 1
        debugInfo: "target keystar cage"
    }


    CheckPersonNear {
        id: checkHeroSwordDistance
        me: sword
        target: hero
        distanceNear: hero.height
        debugInfo: "target sword"
        onTargetChangedNear: {
            //console.log("hero is near the sword")
            talkBoard.x = mainRoot.width*6.5 - talkBoard.width/2
            talkBoard.y = mainRoot.height+defaultMargins
            heroMap.swordUnlocked = true
            createDialogSwordUnlocked()
            talkBoard.show()
        }
        onTargetChangedFar: sword.visible = false
        isEnabled: (screenNumber === 6) && (screenRow === 1) && !heroMap.swordUnlocked
    }

    CheckPersonNear {
        id: checkHeroRobotSwordDistance
        me: enemyRobotSwordObj
        target: leaderObj
        distanceNear: hero.height
        debugInfo: "target swordRobot"
        onTargetChangedNear: {
            talkBoard.x = mainRoot.width*7.5 - talkBoard.width/2
            talkBoard.y = defaultMargins
            createDialogWithRobotSword()
            talkBoard.show()
        }
        onTargetChangedFar: talkBoard.visible = false
        isEnabled: (screenNumber === 7) && (screenRow === 0)
    }

    function levelStartSetup(){
        console.log("level startup setup init")
        lib.projectileShotDelay = true
        panelEOL.visible = false
        talkBoard.visible = false
        msgBoardlevel3.visible = false
        createItems()
        createConnections()
        hero.reset()
        girl.reset()
        //sound.play()
        console.log("level startup setup end")
    }

    function levelEndCleanup(){
        //console.log("levelEndCleanup")
        //cleanup current objects, if they are selected
        if (heroMap.bowAndArrowSelected)
            destroyBow()

        if (heroMap.swordSelected)
            destroySword()

        heroMap.currentItemID = ""
        heroMap.currentIndex = -1

        talkBoard.visible = false
        talkBoard.clear()

        activeEnemiesList.clearEnemies()
        destroyItems()
        destroyConnections()
        //sound.stop()
        //console.log("levelEndCleanup")
    }

    //connections-----------------------------------------------------------------------
    function createConnections() {
        heroMap.showSword.connect( createSword )
        heroMap.hideSword.connect( destroySword )
        heroMap.showBowAndArrow.connect( createBow )
        heroMap.hideBowAndArrow.connect( destroyBow )
        heroMap.showStarKey.connect( createStarKey )
        heroMap.hideStarKey.connect( destroyStarKey )

        flickLevel.contentX = Qt.binding( function(){ //moves the scene when the hero reachs the end of the screen
            return Math.floor(leaderObj.x / flickLevel.width) * flickLevel.width} )

        flickLevel.contentY = Qt.binding( function(){
            return Math.floor(leaderObj.y / flickLevel.height) * flickLevel.height} )

        lib.connectCommandWalk(commandTouchPad, leaderObj)
        lib.connectCommandWalk(commandKeyb, leaderObj)

        if (girlIsReleased) {
            interfaceWalkGirlHero.leader = leaderObj
            interfaceWalkGirlHero.follower = followerObj
            interfaceWalkGirlHero.connect()
        }

        if (girlPowerUnleashed && leaderObj === girl)
            girlShotCreateConnection()
    }

    function destroyConnections() {
        heroMap.showSword.disconnect( createSword )
        heroMap.hideSword.disconnect( destroySword )
        heroMap.showBowAndArrow.disconnect( createBow )
        heroMap.hideBowAndArrow.disconnect( destroyBow )

        lib.disconnectCommandWalk(commandTouchPad, leaderObj)
        lib.disconnectCommandWalk(commandKeyb, leaderObj)
        leaderObj.isWalking = false

        if (girlIsReleased) {
            interfaceWalkGirlHero.disconnect()
        }

        if (girlPowerUnleashed && leaderObj === girl) {
            girlShotDestroyConnection()
        }
    }

    function girlShotCreateConnection() {
        console.log("girl shot connectio created....................................")
        commandTouchPad.shotFiredXY.connect( createGirlShot )
        createGirlShot.connect( girl.fireShot )

    }

    function girlShotDestroyConnection() {
        commandTouchPad.shotFiredXY.disconnect( createGirlShot )
        createGirlShot.disconnect( girl.fireShot )
    }
    //connections-----------------------------------------------------------------------


    function createItems() {

        //bush plant------------------------------------------
        bushPlantObj = lib.createItem("components/PersonCharacter.qml", parent)
        bushPlantObj.imgName = "bushplant.png"
        bushPlantObj.width = 80 * mm
        bushPlantObj.height = bushPlantObj.width * 186/320
        bushPlantObj.x = mainRoot.width * 7 - bushPlantObj.width
        bushPlantObj.y = mainRoot.height - bushPlantObj.height
        destroyItems.connect( bushPlantObj.destroy )
        //bush plant------------------------------------------


        //red ball enemy---------------------------------------------
        for (var i = 0; i < 2;i++) {
            var redBallEnemy = lib.createItem("EnemyRedBall2.qml", parent)
            redBallEnemy.x = Qt.binding( function(){ return  level03id.width*3 - redBallEnemy.width*1.5 - 2*redBallEnemy.width*i} )
            redBallEnemy.yPos = Qt.binding( function(){ return  sky.height + grass.height * 0.5 - this.height} )
            redBallEnemy.teta = i
            redBallEnemy.inMotion = Qt.binding( function(){ return  screenNumber === 2 } )
            redBallEnemy.active = Qt.binding( function(){ return  screenNumber === 2 } )
            redBallEnemy.showExplosionOnDestroy = true

            redBallEnemy.enemyTarget = hero
            redBallEnemy.destroyTarget.connect( hero.imDestroyed )
            //redBallEnemy.destroyTarget.connect(levelFailed)

            redBallEnemy.removeFromEnemiesList.connect( activeEnemiesList.removeEnemy )
            redBallEnemy.addToEnemiesList.connect( activeEnemiesList.addEnemy )

            destroyItems.connect( redBallEnemy.destroy )

        }
        //red ball enemy---------------------------------------------


        //dragon---------------------------------------------
        dragonObj = lib.createItem("Dragon2.qml", parent)
        dragonObj.xPos = level03id.width * 3.5//Qt.binding( function(){ return  level03id.width * 3.5 } )
        dragonObj.yPos = sky.height/4 //Qt.binding( function(){ return  sky.height/4 } )
        dragonObj.fire.connect( dragonFire )
        dragonObj.inMotion = Qt.binding( function(){ return  screenNumber === 3 } )
        dragonObj.active = Qt.binding( function(){ return  screenNumber === 3 } )
        dragonObj.enemyDestroyed.connect( starKeyUnlocked )
        dragonObj.removeFromEnemiesList.connect( activeEnemiesList.removeEnemy )
        dragonObj.addToEnemiesList.connect( activeEnemiesList.addEnemy )
        dragonObj.enemyTarget = leaderObj
        dragonObj.destroyTarget.connect(levelFailed)
        destroyItems.connect( dragonObj.destroy )
        dragonLifeBar.lifeBarInit(dragonObj.lifePower)
        dragonObj.imHit.connect( dragonLifeBar.hit )
        dragonObj.showExplosionOnDestroy = true
        //dragon---------------------------------------------


        //enemy sword---------------------------------------------
        enemyRobotSwordObj = lib.createItem("EnemyRobotSword.qml", parent)
        enemyRobotSwordObj.height = 45*mm//enemyRobotSwordObj.width * 428/217
        enemyRobotSwordObj.width = enemyRobotSwordObj.height * 217/428
        enemyRobotSwordObj.imageName = "robotEnemy.png"
        //enemyRobotSwordObj.active = Qt.binding( function(){ return  screenNumber === 7 } )
        enemyRobotSwordObj.x = level03id.width * 7.6 //Qt.binding( function(){ return  level03id.width * 7.5 } )
        //enemyRobotSwordObj.y = sky.height - enemyRobotSwordObj.height/2 //Qt.binding( function(){ return  sky.height * 1.1 - 60*mm } )
        enemyRobotSwordObj.y = surfaceGroundLimit - enemyRobotSwordObj.height
        destroyItems.connect( enemyRobotSwordObj.destroy )

        swordRobotObj = lib.createItem("Sword.qml", parent)
        swordRobotObj.direction = -1
        swordRobotObj.x = enemyRobotSwordObj.x
        swordRobotObj.y1 = enemyRobotSwordObj.y+enemyRobotSwordObj.height*0.5
        //enemy sword---------------------------------------------


        //enemy boss in end of level----------------------------------------------
        enemyBossObj = lib.createItem("EnemyBadBoss.qml", parent)

        enemyBossObj.height = 55*mm//enemySwordObj.width * 428/217
        enemyBossObj.width = enemyBossObj.height * 715/725
        enemyBossObj.imageName = "enemyBoss.png"
        enemyBossObj.name = "BadBossEnemy"
        enemyBossObj.immuneToArrows = true
        enemyBossObj.x = level03id.width * (badBossScreenNumber + 0.8) //1.8 //9.8//Qt.binding( function(){ return  level03id.width * 7.5 } )
        //enemyBossObj.y = sky.height - enemyBossObj.height/2 //Qt.binding( function(){ return  sky.height * 1.1 - 60*mm } )
        enemyBossObj.y = surfaceGroundLimit - enemyBossObj.height
        enemyBossObj.groundPositionY = enemyBossObj.y
        enemyBossObj.enemyTarget = leaderObj//Qt.binding( function(){ return  leaderObj})
        enemyBossObj.inMotion = Qt.binding( function(){ return  (screenNumber === badBossScreenNumber)
                                                     && (msgBoardlevel3.visible === false) && (talkBoard.visible === false)  } )
        enemyBossObj.active = Qt.binding( function(){ return  screenNumber === badBossScreenNumber } )
        //checks for enemy collision
        //if enemyBossObj.active is false, the hero/girl shots will not damage him
        enemyBossObj.fire.connect( dragonFire )
        enemyBossObj.destroyTarget.connect( leaderObj.imDestroyed )
        //enemyBossObj.destroyTarget.connect(levelFailed)
        enemyBossObj.enemyDestroyed.connect( badBossDestroyed )
        destroyItems.connect( enemyBossObj.destroy )
        enemyBossObj.removeFromEnemiesList.connect( activeEnemiesList.removeEnemy )
        enemyBossObj.addToEnemiesList.connect( activeEnemiesList.addEnemy )
        badBossLifeBar.lifeBarInit(enemyBossObj.lifePower)
        //enemyBossObj.imHit.connect( badBossLifeBar.hit )
        enemyBossObj.imHitDamageMade.connect( badBossLifeBar.projectileDamage )
        enemyBossObj.showExplosionOnDestroy = true
        enemyBossObj.explosionDuration = 3000
        console.log("Create Items enemyBossObj: "+enemyBossObj)
        //enemy boss in end of level----------------------------------------------

    }//createItems


    function createSword() {
        swordObj = lib.createSword(swordObj, leaderObj, parent)

        //todo: connect add attack signals
        lib.connectSwordAttack(commandTouchPad, swordObj)
        lib.connectSwordAttack(commandKeyb, swordObj)

        /*commandTouchPad.swordAttack.connect( swordObj.startAttack )
        commandTouchPad.swordDefenseStart.connect( swordObj.startDefense )
        commandTouchPad.swordDefenseStop.connect( swordObj.stopDefense )

        commandKeyb.swordAttack.connect( swordObj.startAttack )
        commandKeyb.startDefense.connect( swordObj.startDefense )
        commandKeyb.stopDefense.connect( swordObj.stopDefense )*/

        swordObj.attackIsRunning.connect( updateRec)//visual sword attack debug
        destroyItems.connect( swordObj.destroy )
        //sword------------------------------
    }

    //for debbuging sword attack position
    function updateRec(xx, yy){
        redrec.x = xx
        redrec.y = yy
    }

    //for debbuging sword attack position
    Rectangle {
        id: redrec
        width: 2
        height: 2
        color: "red"
    }

    function destroySword() {
        //todo: add disconnections made in createSword()
        //commandTouchPad.swordAttack.disconnect( swordObj.startAttack )
        //commandKeyb.shotFired.disconnect( swordObj.startAttack )

        lib.disconnectSwordAttack(commandTouchPad, swordObj)
        lib.disconnectSwordAttack(commandKeyb, swordObj)

        swordObj.destroy()
        swordObj = null

    }


    //-- Bow & Arrows-----------------------------------------------
    function createBow(){
        lib.createBow(leaderObj, parent)
        commandTouchPad.shotFiredXY.connect( createArrow )
    }

    function destroyBow(){
        commandTouchPad.shotFiredXY.disconnect( createArrow )
        lib.destroyBow()
    }

    function createArrow(touchX, touchY) {
        var arrow = lib.createProjectile(touchX, touchY, lib.bowObj.midX, lib.bowObj.midY, screenNumber, screenRow, parent)
        if (arrow === null)
            return;

        arrow.imageName = "arrow.png"
        arrow.projectileType = "arrow"
        arrow.width = 20*mm
        arrow.height = 20*mm*0.273
    }
    //-- Bow & Arrows-----------------------------------------------


    //-- Girl Shot-----------------------------------------------
    signal createGirlShot(var touchX, var touchY)

    onCreateGirlShot: {
        if (girl !== leaderObj || swordObj !== null || lib.bowObj !== null)
            return;//todo: remove this return and fix the connection to only be made when girl is leader

        girIsFiring = true;
        var girlShotObj = lib.createProjectile(touchX, touchY, girl.midX, girl.midY-girl.height*0.2, screenNumber, screenRow, root)
        if (girlShotObj === null) {
            girIsFiring = false;
            return;
        }

        girlShotObj.width = 14*mm
        girlShotObj.height = girlShotObj.width
        girlShotObj.color = "white"
        girlShotObj.border.width = 1*mm
        girlShotObj.border.color = "dimgrey"
        girIsFiring = false;
    }
    //-- Girl Shot-----------------------------------------------


    onDestroyItems: {
        //avoid broken connection with deleted objects
        //if (!heroObj)//Note: this if just avoids warning: TypeError: Cannot read property 'walk' of undefined
          //  return;
        talkBoard.clear()
    }


    ConversationBoard2 {
        id: talkBoard
        width: mainRoot.width * 0.6
        height: mainRoot.height * 0.6//0.5

        //imageLeft: "heroHead.png"
        //imageRight: "robonoid.png"

        property string conversationType: ""

        imageBottomWidth: 20*mm
        imageBottomHeight: 20*mm

        visible: false
        onEndDialog: {
            if (heroMap.milk) {
                heroMap.bowAndArrowUnlocked = true
                //make the ronoid smile if he get's his milk
                //milkGuyObj.imgName = "robonoidSmile.png"
                milkGuy.imgName = "robonoidSmile.png"
            }

            if (conversationType === "girlAskIfHeroNeedsHelpToFight") {
                conversationType = ""
                msgBoardlevel3.x = mainRoot.width*9.5 - msgBoardlevel3.width*0.5
                msgBoardlevel3.y = defaultMargins
                msgBoardlevel3.showCancelBtn = true
                msgBoardlevel3.bntCancelText = qsTr("A")
                msgBoardlevel3.bntOkText = qsTr("B")
                msgBoardlevel3.text = qsTr("A - No, let me keep fighting.\n"
                                           +"B - Yes, please help me fight.")
                msgBoardlevel3.msgString = "girlHelpHero"
                msgBoardlevel3.visible = true
            }

            if (conversationType === "badbossRestartFight") {
                conversationType = ""
            }

        }
    }

    MessageBoard2 {
        id: msgBoardlevel3
        width: mainRoot.width * 0.5
        height: mainRoot.height * 0.5
        text: ""
        pixelSize: 10*mm
        horizontalAlignment: Text.AlignHCenter //Text.AlignJustify
        verticalAlignment: Text.AlignVCenter
        visible: false
        property string msgString: ""

        //onVisibleChanged: console.log("msgBoardlevel3 visible: "+msgBoardlevel3.visible)

        onOk: {
            msgBoardlevel3.visible = false
            msgBoardlevel3.image = ""

            if ( talkBoard.conversationType === "girlAskIfHeroNeedsHelpToFight") {
                createDialogGirlHelpHero()
                talkBoard.conversationType = "girlHelpHero"
                talkBoard.show()
            }

            if (msgString === "girlHelpHero") {
                msgString === ""
                showCancelBtn = false
                bntCancelText = qsTr("Cancel")
                bntOkText = qsTr("Ok")
                talkBoard.conversationType = "badbossRestartFight"
                createDialogGirlHelpHero()
                //talkBoard.conversationType = "girlHelpHero"
                talkBoard.show()
            }
        }
        onCancel: {
            msgBoardlevel3.visible = false
            msgBoardlevel3.image = ""
        }
    }

    function dragonFire(xx, yy) {
        //console.log("create dragon fire....................................")
        //var dragonFireObj = lib.createItem("DragonFire.qml", parent)
        var dragonFireObj = lib.createItem("ProjectileEnemy.qml", parent)

        dragonFireObj.imageName = "dragonfire.png"
        dragonFireObj.width = 10*mm
        dragonFireObj.height = 10*mm
        dragonFireObj.xMinBulletDestroy = mainRoot.width * screenNumber
        dragonFireObj.xMaxBulletDestroy = mainRoot.width * screenNumber + mainRoot.width
        dragonFireObj.yMinBulletDestroy = 0
        dragonFireObj.yMaxBulletDestroy = mainRoot.height

        //dragonFireObj.bulletTarget = Qt.binding( function(){ return  leaderObj})//leaderObj
        dragonFireObj.bulletTarget = leaderObj
        dragonFireObj.setMidPosXY(xx, yy)
        //console.log("hero.x: "+heroObj.x+" hero.y: "+heroObj.y+" xx: " +xx+" yy: "+yy)
        dragonFireObj.setAngle(leaderObj.midX-xx, leaderObj.midY-yy)
        dragonFireObj.running = true
        dragonFireObj.hit.connect( leaderObj.imHit )
        destroyItems.connect( dragonFireObj.destroy )
    }

    function fallOnHole() {

        hero.worldLimitGround = denGroundLimit
        hero.worldLimitLeft = mainRoot.width * 4 + den.rocksWidth /*+ bowArrowObj.width/2*/
        hero.worldLimitRight = denWallDestroyed ? mainRoot.width * 4 + den.width - den.rocksWidth
                                                : mainRoot.width * 4 + den.rockWallPositionX
        hero.fall()

        if (girlIsReleased) {
            girl.worldLimitGround = denGroundLimit
            girl.worldLimitLeft = mainRoot.width * 4 + den.rocksWidth /*+ bowArrowObj.width/2*/
            girl.worldLimitRight = denWallDestroyed ? mainRoot.width * 4 + den.width - den.rocksWidth
                                                    : mainRoot.width * 4 + den.rockWallPositionX
            girl.fall()
        }
    }

    function outOfDen() {

        hero.worldLimitLeft = 0
        hero.worldLimitRight = parent.width
        hero.y1  = sky.height+hero.height/2

        //very important, groundLimit must be set after hero.y1 and girl.y1 are set
        //else it wont work because setting y1 will fail, because they are outside of worldLimitGround
        hero.worldLimitGround = surfaceGroundLimit

        if (girlIsReleased) {
            girl.worldLimitLeft = 0
            girl.worldLimitRight = parent.width
            girl.y1 = sky.height+hero.height/2

            girl.worldLimitGround = surfaceGroundLimit
        }

    }

    // start key----------------------------------------------------------------------------
    function starKeyUnlocked() {
        heroMap.starKeyUnlocked = true
        talkBoard.x = mainRoot.width*3.5 - talkBoard.width/2
        talkBoard.y = defaultMargins
        createDialogStarKeyUnlocked()
        talkBoard.show()
    }

    function createStarKey() {
        if (isStarKey)
            return;
        isStarKey = true;
        //create starKey-------------------
        starKeyObj = lib.createItem("StarKey.qml", parent)
        starKeyObj.x = Qt.binding( function(){ return hero.dir === 1 ?
                                                    hero.x+hero.width*0.6 : hero.x-hero.width } )
        starKeyObj.y = Qt.binding( function(){ return hero.y+hero.height*0.15 } )
        starKeyObj.mirror = Qt.binding( function(){ return hero.dir === 1 ? false : true } )
    }

    function destroyStarKey() {
        isStarKey = false;
        starKeyObj.destroy()
    }
    // start key----------------------------------------------------------------------------

    function setLeaderFollower(leader, follower) {
        if (leader === leaderObj)//if there is no change return
            return;

        destroyConnections()//destroy connections with current leader
        if (heroMap.bowAndArrowSelected) //destroy bow if exists
            destroyBow()

        if (heroMap.swordSelected)//destroy connections with current leader, and destroy sword if exists
            destroySword()

        leaderObj = leader//set new leader
        followerObj = follower//and new follower

        if (heroMap.bowAndArrowSelected)//create bow if existed
            createBow()

        if (heroMap.swordSelected)//create sword if existed before chaging leader
            createSword()

        createConnections()//restore connections with the new leader
    }

    // --Conversations--------------------------------------------------------------------------

    function createDialogueHeroMilkGuy() {

        if (heroMap.bowAndArrowUnlocked)
            dialogueMilkGuySaysHello()
        else
            dialogueMilkGuyOffersBow()
    }

    function dialogueMilkGuyOffersBow() {
        talkBoard.clear()

        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = "robonoid.png"

        //create messages
        //var message1PT = "Olá. Estou com muita sede. Tens alguma coisa para eu beber ?"
        //var message1EN = "Hello. I'm really thirsty. Do you have any thing to drink ?"
        var message1 = qsTr("Hello. I'm really thirsty. Do you have anything to drink ?")

        //var message2PT = "Olá. Tenho leite da vaquinha da via láctea. Por favor bebe."
        //var message2EN = "Hi. I have milk from the milky way cow. Please have a drink."
        var message2 = qsTr("Hi. I have milk from the milky way cow. Please have a drink.")

        //var message3PT = "Hum, é delicioso. Obrigado, como recompensa ofereço-te este arco e flechas.\nProcura-o no teu livro, depois para disparar clica em qualquer sitio no ecrã."
        //var message3EN = "Hum, delicious. Thank you, please let me offer you this bow and arrow.\nTo shoot an arrow tap anywhere in the screen."
        var message3 = qsTr("Hum, delicious. Thank you, please let me offer you this bow and arrows.\nLook for it in your book, then to shoot an arrow tap anywhere in the screen.")

        //var message4PT = "Desculpa, não tenho nada para te oferecer."
        //var message4EN = "Sorry, I have nothing to give you."
        var message4 = qsTr("Sorry, I have nothing to give you.")

        //var message5PT = "Que pena, ouvi dizer que havia um leite excelente num nivel anterior.\nBem, adeus"
        //var message5EN = "Too bad, I heard there was a good milk in a previous level.\nOh well, bye!"
        var message5 = qsTr("Too bad, I'm really thirsty. Oh well, bye!")


        //add messages to talk board
        //"heroHead.png"
        //"robonoid.png"
        talkBoard.addMessage2(message1, "robonoid.png")

        if (heroMap.milk) {
            talkBoard.addMessage2(message2, "heroHead.png")
            //talkBoard.addMessage2(message3, "robonoidSmile.png")
            talkBoard.addMessage5(message3, "robonoidSmile.png", 1, false, "bowArrow.png")

            /*conversationBoard.addMessage5("Para veres os objectos clica no botão Pausa no top à esquerda, e depois no botão Livro.",
                                      "asteroidSnorlexHead.png", 1, false, "btnsPauseBook.png")*/
        }
        else {
            talkBoard.addMessage2(message4, "heroHead.png")
            talkBoard.addMessage2(message5, "robonoid.png")
        }
    }

    function dialogueMilkGuySaysHello() {//because he already offered the bow
        talkBoard.clear()
        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = "robonoidSmile.png"
        talkBoard.addMessage2(qsTr("Hi, have a safe journey and be happy!!!"), "robonoidSmile.png")
    }


    function createDialogStarKeyUnlocked() {
        talkBoard.clear()
        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = ""
        talkBoard.addMessage5(qsTr("You found the starKey!!!"), "", 1, false, "keyStar.png")
    }

    function createDialogSwordUnlocked() {
        talkBoard.clear()
        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = ""
        talkBoard.addMessage5(qsTr("You found a sword!!!"), "", 1, false, "sword.png")
    }


    function createDialogueHeroGirl() {
        talkBoard.clear()

        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = "girlHead.png"

        //talkBoard.addMessage4("Olá, sou a Luísa, fui raptada. Consegues ajudar-me?", "girlHead.png", 2, true)
        talkBoard.addMessage4(qsTr("Hi, I'm Louise Pandora. I was kidnapped, can you help me?"), "girlHead.png", 2, true)

        //talkBoard.addMessage4("Olá, sou o capitão André! Não sei como abrir a jaula, parece muito forte.", "heroHead.png", 1, false)
        talkBoard.addMessage4(qsTr("Hi, I'm captain Andrew! I don't know how to open the cage."), "heroHead.png", 1, false)

        //talkBoard.addMessage4("Esta jaula só abre com uma chave mágica de estrela.", "girlHead.png", 2, true)
        talkBoard.addMessage4(qsTr("This cage only opens with a magic star key.", "girlHead.png"), 2, true)

        if (levels.starKeyUnlocked) { //hero has the starkey
            //talkBoard.addMessage4("Tenho uma chave de estrela que ganhei numa luta contra um dragão. Posso tentar usá-la.", "heroHead.png", 1, false)
            talkBoard.addMessage4(qsTr("I have a star key, I won it fighting a dragon. Let me try."), "heroHead.png", 1, false)
        }
        else {//hero doenst have the starkey
            //talkBoard.addMessage4("Não tenho nenhuma chave de estrela.", "heroHead.png", 1, false)
            talkBoard.addMessage4(qsTr("I don't have a star key."), "heroHead.png", 1, false)

            //talkBoard.addMessage4("Por favor encontra uma chave e liberta-me.", "girlHead.png", 2, true)
            talkBoard.addMessage4(qsTr("Please find one and set me free."), "girlHead.png", 2, true)

            //talkBoard.addMessage4("Muito bem, vou à procura da chave.", "heroHead.png", 1, false)
            talkBoard.addMessage4(qsTr("Ok, I will look for it."), "heroHead.png", 1, false)
        }
    }

    function createDialogWithRobotSword() {
        talkBoard.clear()

        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = "robotEnemyBody.png"

        var message = qsTr("We're supposed to have a sword fight now, but lucky for you the"
                           + " the great coder still hasn't finished the sword fight algorithm for this game."
                           +"\nGo in peace now, we'll meet in future updates.")

        talkBoard.addMessage4(message, "robotEnemyBody.png", 2, true)
        talkBoard.addMessage4(qsTr("Great coder ? Game ? I don't understand what you say.\nWhat a crazy robot, I'm out of here."), "heroHead.png", 1, false)
    }

    function createDialogueGirlAskIfHeroNeedsHelpToFight() {
        talkBoard.clear()

        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = "girlHead.png"

        talkBoard.addMessage4(qsTr("The arrows don't do nothing to him, and you're about to die.\nLet me help you fight!!!"), "girlHead.png", 2, true)
    }

    function createDialogGirlHelpHero() {

        talkBoard.x = mainRoot.width*9.5 - talkBoard.width/2
        talkBoard.y = defaultMargins

        talkBoard.clear()

        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = "girlHead.png"

        talkBoard.addMessage4(qsTr("Quick, we don't have much time!"
                               +"Double click on my head and I will take charge of the fight. I will fight without the bow.")
                              , "girlHead.png", 2, true)
        girl.mouseAreaEnabled = true
        girlPowerUnleashed = true;
        heroMap.bowAndArrowSelected = false
        destroyBow();
    }

    function createDialogueGirlWinsBadBossFight() {

        talkBoard.x = mainRoot.width*9.5 - talkBoard.width/2
        talkBoard.y = defaultMargins

        talkBoard.clear()

        talkBoard.imageLeft = "heroHead.png"
        talkBoard.imageRight = "girlHead.png"

        talkBoard.addMessage4(qsTr("Oh my God, you fought like a mighty warrior!!!\n"
                                   + "Who are you ???"), "heroHead.png", 1, false)
        talkBoard.addMessage4(qsTr("I am Louise Pandora ... and I have unleashed my power."), "girlHead.png", 2, true)
        talkBoard.addMessage4(qsTr("From now on, I will take the lead if my head is double clicked."
                                   +"\nYou, Captain Andrew, will take the lead if your head is double clicked."), "girlHead.png", 2, true)

    }

}
