import QtQuick 2.0
//import com.Timer 1.0

Rectangle {
    id: root
    width:  text.contentWidth+6*mm < 160*mm ? 160*mm : text.contentWidth+6*mm //210*mm
    height: 18*mm
    radius: height/2
    color: "orange"
    //opacity: 0.6
    visible: false

    //property alias message: text.text
    signal showMessage(string message_)

    onShowMessage: {
        root.visible = false
        text.text = message_
        root.visible = true
    }

    Text {
        id: text
        //text: qsTr("")
        anchors.centerIn: parent
    }

    Timer {
        id: timerLifeDuration
        interval: 3000;
        running: visible
        repeat: false
        onTriggered: {
            root.visible = false
        }
    }

}
