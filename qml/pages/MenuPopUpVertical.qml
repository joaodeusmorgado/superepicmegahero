import QtQuick 2.0
import "components"

Item {

    width: col.width + btnBook.width + defaultMargins
    height: col.height //+ btnBook.height + defaultMargins
    property bool expand: false
    signal gotoMenu()
    signal book()

    Column {
        id: col
        spacing: defaultMargins

        MyButton {
            anchors.margins: defaultMargins
            backGrdColor: "lightgrey"
            pressedColor: "darkgrey"
            border.color: "white"
            image: expand ? "play.png" : "pause.png"
            onBtnClick: expand = !expand
        }

        /*MyButton {
            anchors.margins: defaultMargins
            backGrdColor: "lightgrey"
            pressedColor: "darkgrey"
            border.color: "white"
            image: "play.png"
            visible: expand
        }*/

        MyButton {
            anchors.margins: defaultMargins
            backGrdColor: "lightgrey"
            pressedColor: "darkgrey"
            border.color: "white"
            image: "btnMenu.png"
            onBtnClick: {
                gotoMenu()
                expand = false
            }
            visible: expand
        }

    }//Column

    MyButton {
        id: btnBook
        anchors.top: col.top
        anchors.left: col.right
        anchors.leftMargin: defaultMargins
        backGrdColor: "lightgrey"
        pressedColor: "darkgrey"
        border.color: "white"
        image: "book.png"
        onBtnClick: {
            book()
        }
    }

}
