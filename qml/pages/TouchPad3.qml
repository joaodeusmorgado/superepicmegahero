import QtQuick 2.3

Item {
    id: root
    width: 45*mm //22*mm
    height: width

    property real commandX
    property real commandY

    //dont set explicit x,y
    //set centerX, centerY
    property real centerX
    property real centerY
    property bool arrowVisible: false

    x: centerX-width*0.5
    y: centerY-height*0.5

    clip: true

    //bigger rectangle
    Image  {
        anchors.fill: parent
        //source: arrowVisible ? "qrc:/images/wheelArrowBlack.png" : "qrc:/images/wheelCommand.png"
        source: arrowVisible ? "qrc:/images/wheelArrowWhite.png" : "qrc:/images/wheelCommand.png"
        mipmap: true
    }

    /*Rectangle {
        id: ret1
        anchors.centerIn: parent
        width: parent.width
        height: width
        radius: width*0.5
        border.width: mm
        border.color: "black"
        opacity: 0.6
        clip: true

        Image {
            id: arrow
            source: "qrc:/images/wheelArrowWhite.png"
            //source: "qrc:/images/wheelArrowBlack.png"
            height: parent.height * 0.5
            width: parent.width * 0.4//height * imageSourceSize.width / imageSourceSize.height
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            mipmap: true
            visible: false
        }

        /*Rectangle {
            id: ret2
            //x: commandX-ret2.width*0.5
            //y: commandY-ret2.height*0.5
            anchors.centerIn: parent
            //width: parent.width*0.65
            width: parent.width*0.35
            height: width
            radius: width*0.5
            border.width: mm
            border.color: "black"
            opacity: 0.4
        }
    }*/
}
