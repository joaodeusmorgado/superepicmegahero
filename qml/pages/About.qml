import QtQuick 2.0
import "components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    //color: "blue"

    property real textSize: 8*mm
    property string fontFamily: "Purisa"
    property color fontColor: "black"//"orange"
    property color fontHiperLinkColor: "blue"//"orange"

    //onVisibleChanged: visible ? sound.play() : sound.stop()

    Flickable {
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: parent.height - okBtn.height
        contentWidth: parent.width
        contentHeight: helpColumn.height + defaultMargins * 2

        Column {
            id: helpColumn
            spacing: defaultMargins
            anchors.top: parent.top
            anchors.topMargin: defaultMargins
            anchors.left: parent.left
            anchors.leftMargin: defaultMargins
            anchors.right: parent.right
            anchors.rightMargin: defaultMargins

            ImageFrame {
                width: 60 * mm//root.width * 0.2
                source: "qrc:/images/nave.png"
            }


            Text {
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: textSize
                font.family: fontFamily
                color: fontColor
                textFormat: Text.RichText
                text: "<style>a:link { color: " + fontHiperLinkColor + "; }</style>"
                    + qsTr("Super Epic Mega Hero - a hero in search of adventures.")
                    + "<br> <a href=\"https://superepicmegahero.carrd.co/\"> https://superepicmegahero.carrd.co/</a><br>"
                onLinkActivated: Qt.openUrlExternally("https://superepicmegahero.carrd.co/")
            }

            Text {
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: textSize
                font.family: fontFamily
                color: fontColor
                textFormat: Text.RichText
                text: "<style>a:link { color: " + fontHiperLinkColor + "; }</style>"
                    + qsTr("If you like this game please buy me a coffe 😊" )
                    + "<br> <a href=\"https://ko-fi.com/joaodeus\"> https://ko-fi.com/joaodeus</a><br>"
                onLinkActivated: Qt.openUrlExternally("https://ko-fi.com/joaodeus")
            }

            Text {
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: textSize
                font.family: fontFamily
                color: fontColor
                //textFormat: Text.RichText
                text: qsTr("Developer: João Morgado"
                           +"\nGraphics: André Morgado & João Morgado"
                           +"\nVersion: 1.0.2"
                           +"\nEmail: joaodeusmorgado@yahoo.com")
            }


            Text {
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: textSize
                font.family: fontFamily
                color: fontColor
                textFormat: Text.RichText
                text: "<br><style>a:link { color: " + fontHiperLinkColor + "; }</style>"
                    + qsTr("This program was developed using Qt5 opensource version")
                    + "<br> <a href=\"https://www.qt.io/\"> https://www.qt.io/</a><br>"
                onLinkActivated: Qt.openUrlExternally("https://www.qt.io/")
            }

            Text {
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: textSize
                font.family: fontFamily
                color: fontColor
                textFormat: Text.RichText
                text: "<style>a:link { color: " + fontHiperLinkColor + "; }</style>"
                    + qsTr("Source code: ")
                    + "<br> <a href=\"https://bitbucket.org/joaodeusmorgado/superepicmegahero\"> https://bitbucket.org/joaodeusmorgado/superepicmegahero</a>"
                onLinkActivated: Qt.openUrlExternally("https://bitbucket.org/joaodeusmorgado/superepicmegahero")
            }

        }
    }


    signal close()

    MyButton {
        id: okBtn
        width: btnSize*2
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: defaultMargins
        text: qsTr("Ok")
        onBtnClick: close()
    }

    MyButton {
        id: licenseBtn
        width: btnSize*2
        anchors.bottom: parent.bottom
        anchors.right: okBtn.left
        anchors.margins: defaultMargins
        text: qsTr("License")
        onBtnClick: licensePage.visible = true
    }

    LicenseFile {
        id: licensePage
        visible: false
        onClose: visible = false
    }

}

