import QtQuick 2.0
import "components"

Rectangle {
    id: root
    width: mainRoot.width / 2
    height: mainRoot.height / 2
    color: "purple"
    opacity: 0.75
    property alias text: label.text
    property alias showNextBtn: btnNextLevel.visible
    property alias pixelSize: label.font.pixelSize

    signal replay()
    signal menu()
    signal next()

    Text {
        id: label
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        font.bold: false
        font.family: "Purisa"
        font.pixelSize: 18*mm
        color: "orange"
        elide: Text.ElideMiddle
        wrapMode: Text.WordWrap
        text: qsTr("Victory")

    }

    MyButton {
        id: btnReplay
        backGrdColor: "red"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        anchors.left: parent.left
        anchors.leftMargin: parent.width/4 - width
        image: "btnReplay.png"
        onBtnClick: replay()
    }


    MyButton {
        id: btnMenu
        backGrdColor: "red"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        anchors.horizontalCenter: parent.horizontalCenter
        image: "btnMenu.png"
        onBtnClick: menu()
    }

    MyButton {
        id: btnNextLevel
        backGrdColor: "red"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        anchors.right: parent.right
        anchors.rightMargin: parent.width/4 - width
        image: "arrowRight.png"
        onBtnClick: next()
    }


}

