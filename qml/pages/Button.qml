import QtQuick 2.0

Rectangle {
    id: root
    width: 9*mm
    height: width
    radius: width/2
    border.color: "black"

    signal pressed()
    MouseArea {
        anchors.fill: parent
        //propagateComposedEvents: true
        onPressed: {
            root.pressed()
          //  mouse.accepted = false
        }
    }

}
