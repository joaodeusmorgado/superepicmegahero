import QtQuick 2.0

Item {
    id: root

    property var leader: 0//({})
    property var follower: 0//({})
    property real followingDistance: leader.width*1.5
    property real leaderPreviousXPosition
    property real leaderPreviousYPosition


    signal leaderMoved()


    onLeaderChanged: {
        if (leader !== 0) {
            leaderPreviousXPosition = leader.x
            leaderPreviousYPosition = leader.y
        }
    }

    function connect() {
        if (leader === 0) {
            console.log("Connection failed, leader not set.")
            return;
        }

        if (follower === 0) {
            console.log("Connection failed, follower not set.")
            return;
        }

        leader.onXChanged.connect( leaderMoved )
        console.log("InterfaceWalkObject connectin made")
    }

    function disconnect() {
        leader.onXChanged.disconnect( leaderMoved )
    }

    onLeaderMoved: {
        var leaderDistanceMovedX = leader.x - leaderPreviousXPosition
        var leaderDistanceMovedY = leader.y - leaderPreviousYPosition

        console.log("InterfaceWalkObject leader moved")
        console.log("InterfaceWalkObject leaderDistanceMovedX: "+leaderDistanceMovedX)


        //todo : fix the followinf algorithm
        if (Math.abs(leader.x - follower.xPos) > followingDistance)
            follower.xPos += leaderDistanceMovedX

        /*if (leaderDistanceMovedX > 0) {
            if ( (leader.x-followingDistance) > follower.xPos)
                follower.xPos += leaderDistanceMovedX
        }
        else {
            if ( (leader.x+followingDistance) < follower.xPos)
                follower.xPos -= leaderDistanceMovedX
        }*/

        leaderPreviousXPosition = leader.x
        leaderPreviousYPosition = leader.y
    }


    //--------------------------------------------------------
    /*signal destroyItem()

    onDestroyItem: {
        disconnect()
        root.destroy()
    }

    function connect() {
        if (leader && follower) {
            leader.startWalking.connect( follower.startWalking )
            leader.stopWalking.connect( follower.stopWalking )
            leader.direction.connect( follower.direction )
            leader.jump.connect( follower.jump )
            leader.walk.connect( root.walkAtDistance )
        }
    }

    function disconnect() {
        if (leader && follower) {
            leader.startWalking.disconnect( follower.startWalking )
            leader.stopWalking.disconnect( follower.stopWalking )
            leader.direction.disconnect( follower.direction )
            leader.jump.disconnect( follower.jump )
            leader.walk.disconnect( follower.walk )
        }
    }

    signal walkAtDistance()
    onWalkAtDistance: {
       // console.log("follow the leader")
        //var dist = leader.width*1.5//16*mm
        //if ( Math.abs(leader.x -follower.x) > followingDistance )
          // follower.walk()
        if (leader.dir === 1) {
            if ( (leader.x-followingDistance) > follower.x)
                follower.walk()
        }

        if (leader.dir === -1) {
            if ( (leader.x+followingDistance) < follower.x)
                follower.walk()
        }

    }*/
}
