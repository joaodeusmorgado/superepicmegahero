import QtQuick 2.3

Item {
    id: root
    width: 20*mm
    height: width

    property real life: 20
    property real hitDamage:1
    //property alias drag: mouseArea.drag

    property string imageName: "nave.png"
    property bool shieldActivated: false
    property bool doubleFirePowerActivated: false
    property bool poweUpCowActivated: false

    property real spaceLimitXmin
    property real spaceLimitXmax
    property real spaceLimitYmin
    property real spaceLimitYmax

    signal prepareFire()
    signal fire(real fx, real fy)
    signal activateDoubleFirePower()
    signal activateShield()
    signal activateCow()
    signal doubleFire(real fx1, real fy1, real fx2, real fy2)
    signal imHit()
    signal imHitAndDestroyed()

    signal destroyItem()

    onDestroyItem: {
        console.log("starship destroy")
        root.destroy()
    }


    onPrepareFire: {
        doubleFirePowerActivated ? doubleFire(x+width*0.2, y, x+width*0.75, y) : fire(x+width/2, y)
    }

    onActivateDoubleFirePower: doubleFirePowerActivated = true
    onActivateShield: shieldActivated = true
    onActivateCow: poweUpCowActivated = true


    onImHit: {
        life -= hitDamage;
        if (life <= 0)
            imHitAndDestroyed()
        //console.log("Im hit:"+life)
    }

    PowerUpsTemplate {
        id: powerShield
        anchors.fill: parent
        visible: shieldActivated
        Timer {
            //id: timer
            interval: 10000; running: shieldActivated; repeat: false
            onTriggered: shieldActivated = false
        }
    }


    PowerUpsTemplate {
        id: doubleFirePower
        anchors.fill: parent
        color: "orange"
        visible: doubleFirePowerActivated
        Timer {
            interval: 10000; running: doubleFirePowerActivated; repeat: false
            onTriggered: doubleFirePowerActivated = false
        }
    }

    PowerUpsTemplate {
        id: cow
        anchors.fill: parent
        color: "yellow"
        visible: poweUpCowActivated
        Timer {
            interval: 10000; running: poweUpCowActivated; repeat: false
            onTriggered: poweUpCowActivated = false
        }
    }

    Image {
        id: image
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/"+imageName
        mipmap: true
    }

    function move(xx, yy) {
        if (!root) return;
        //console.log("move:"+x)
        root.x += xx
        root.y -= yy

        if (root.x < spaceLimitXmin)
            root.x = spaceLimitXmin;
        if (root.x > spaceLimitXmax-width)
            root.x = spaceLimitXmax-width;

        if (root.y < spaceLimitYmin)
            root.y = spaceLimitYmin;
        if (root.y > spaceLimitYmax-height)
            root.y = spaceLimitYmax-height;
    }


    function moveX(deltaX) {
        if (!root) return;
        console.log("Starship moveX:"+deltaX)
        root.x += (deltaX)

        if (root.x < spaceLimitXmin)
            root.x = spaceLimitXmin;
        if (root.x > spaceLimitXmax-width)
            root.x = spaceLimitXmax-width;
    }

    function moveY(deltaY) {
        if (!root) return;
        //console.log("move:"+x)
        root.y += (deltaY)

        if (root.y < spaceLimitYmin)
            root.y = spaceLimitYmin;
        if (root.y > spaceLimitYmax-height)
            root.y = spaceLimitYmax-height;
    }

    function setSpaceLimitsY(ymin, ymax) {
        if (!root) return;
        spaceLimitYmin = ymin;
        spaceLimitYmax = ymax;
        if (root.y < ymin) root.y = ymin
        if (root.y +root.height > ymax) root.y = ymax-root.height
    }

}
