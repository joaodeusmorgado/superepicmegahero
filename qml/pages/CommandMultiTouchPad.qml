import QtQuick 2.0
//import com.Timer 1.0

Item {
    id: root
    width: mainRoot.width
    height: mainRoot.height


    //property real value: slider

    property real velocity: 16
    property real sliderValued: 1
    property bool isFlying: false

    property real touchPadCenterX: touchPad.x + touchPad.width/2
    property real touchPadCenterY: touchPad.y + touchPad.height/2

    signal press()
    signal release()
    signal move(real cx, real cy)// not used anymore, it was used for moving the spaceship with the touchpad
    signal shotFired()
    signal shotFiredXY(real cx, real cy)
    signal direction(int direct)// -1->left ; 1->right
    signal jump()
    signal startFlying()
    signal stopFlying()
    signal fly()
    signal startWalking()
    signal stopWalking()
    signal walk()

    onStartFlying: isFlying = true
    onStopFlying: isFlying = false


    TouchPad {
        id: touchPad
        //anchors.centerIn: parent
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins * 4
    }

    MultiPointTouchArea {
        anchors.fill: parent
        //mouseEnabled: false
        touchPoints: [
            TouchPoint {
                id: point1
                onPressedChanged: {
                    if (pressed)
                        touch1Pressed(x, y)
                    else
                        touch1Released(x, y)
                }
            },
            TouchPoint { id: point2
                onPressedChanged: {
                    if (pressed)
                        touch1Pressed(x, y)
                    else
                        touch1Released(x, y)
                }
            }
        ]
    }


    Button {
        id: btnFire
        width: 22*mm
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins * 4
        opacity: touchPad.opacity
        visible: false
    }

    Timer {
        id: timerMultiTouchPad
        interval: fps_ms
        running: point1.pressed
        repeat: true
        onTriggered: {
            _move()

            if (isFlying)
                fly()
        }
    }

    function isTouchInsideTouchPad(xx, yy) {
        if ( xx >= touchPad.x && xx <= touchPad.x+touchPad.width &&
                yy >= touchPad.y && yy <= touchPad.y+touchPad.height )
            return true;
        else
            return false;
    }


    function touch1Pressed(xx, yy) {
        if (isTouchInsideTouchPad(xx, yy)) {
            //console.log("start walking..................................")
            startWalking()
        }
        else {
            shotFiredXY(xx, yy)
            startFlying()
        }
    }

    function touch1Released(xx, yy) {
        if (isTouchInsideTouchPad(xx, yy)) {
            //console.log("stop walking..................................")
            stopWalking()
        }
        else {
            stopFlying()
        }
    }


    function touch2Pressed(xx, yy) {
        if (isTouchInsideTouchPad(xx, yy)) {
            startWalking()
        }
        else {
            shotFiredXY(xx, yy)
            startFlying()
        }
    }

    function touch2Released(xx, yy) {
        if (isTouchInsideTouchPad(xx, yy)) {
            stopWalking()
        }
        else {
            stopFlying()
        }
    }


    function _move() {

        var xx = (point1.x-touchPadCenterX)
        var yy = (-(point1.y-touchPadCenterY))

        // exit if touch is out of the touchpad area
        if(Math.abs(xx) > touchPad.width/2)
            return;
        if(Math.abs(yy) > touchPad.height/2)
            return;

        //touch is inside touchpad area, lets continue

        // move, right now it's not being used, it was for spaceship, but we replace it with keyboard and sensors command
        move(xx * velocity / touchPad.width, yy * velocity / touchPad.height)
        walk() // walk the hero

        if (xx > 0)
            direction(1)
        else
            direction(-1)

        if (yy > touchPad.height/4)
            jump()
    }

}
