import QtQuick 2.3

ProjectilePhysics {
    id: root
    width: 20*mm
    height: width*0.273
    //transformOrigin: Item.Left
    rotation: angle

    property string imageName: "arrow.png"


    //tip of the arrow position
    //property real tipX: x + width*Math.cos(angle)
    //property real tipY: y + width*Math.sin(angle)



    //property real velocity: 2*mm
    //property real angle: 0

    /*signal destroyArrow()
    onDestroyArrow: {
        console.log("Arrow destroyed..........")
        root.destroy()
    }
    onDestroyBullet: destroyArrow()*/

    //onDestroyed: console.log("signal destroyed on Arrow.qml")

    Image {
        id: arrowImg
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        rotation: angle * 57.2957795131
        source: "qrc:/images/"+imageName
        mipmap: true
    }
}
