import QtQuick 2.3

Item {
    id: sword
    width: 25*mm
    height: width*0.275//12*mm

    property real y1 //y1 = y + height
    y: y1-height //we avoid having to explicit define y with this biding

    property int direction: 1
    property bool bAttack: false

    property var enemySword: null // the sword that is held by the enemy in a attack
    property var enemy: null //the enemy that hokds anathoer sword in a fight


    property real swordSteady: direction === 1 ? -60 : -120
    property real swordAttack: direction === 1 ? 0 : -180
    property real swordDefense: direction === 1 ? -80 : -100
    property real strikeDuration: 200

    signal startAttack()
    signal attackIsRunning(real x, real y)
    signal startDefense()
    signal stopDefense()

    onStartDefense: startDefenseAnimation.start()
    onStopDefense: stopDefenseAnimation.start()
    onStartAttack: animationAttack.start()

    transformOrigin: Item.Left
    rotation: swordSteady

    onRotationChanged: {
        //console.log("sword attack x: "+ Math.cos(sword.rotation* Math.PI / 180.0))
        //console.log("sword attack y: "+ Math.sin(sword.rotation* Math.PI / 180.0))
        //console.log("sword attack.............................")
        attackIsRunning(sword.x + sword.width*Math.cos(sword.rotation* Math.PI / 180.0),
                    sword.y + sword.height/2 + sword.width*Math.sin(sword.rotation* Math.PI / 180.0))
    }

    Image {
        id: imgSword
        width: parent.width
        height: parent.height
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/sword.png"
        mipmap: true

    }

    NumberAnimation {
        id: startDefenseAnimation
        target: sword
        property: "rotation"
        duration: strikeDuration
        easing.type: Easing.InOutQuad
        from: swordSteady
        to: swordDefense
    }

    NumberAnimation {
        id: stopDefenseAnimation
        target: sword
        property: "rotation"
        duration: strikeDuration
        easing.type: Easing.InOutQuad
        //from: swordDefense
        to: swordSteady
        //alwaysRunToEnd: false
    }

    SequentialAnimation {
        id: animationAttack

        NumberAnimation {
            target: sword
            property: "rotation"
            duration: strikeDuration
            easing.type: Easing.InOutQuad
            from: swordSteady
            to: swordAttack
        }

        NumberAnimation {
            target: sword
            property: "rotation"
            duration: strikeDuration
            easing.type: Easing.InOutQuad
            from: swordAttack
            to: swordSteady
        }
    } //SequentialAnimation



}
