import QtQuick 2.3


Item {
    id: mountainID
    width: mainRoot.width
    height: mainRoot.height*2

    property color mountainColor: "SaddleBrown"

    //mountain Top
    Rectangle {
        id: treasureMountainTop
        width: parent.width
        height: parent.height*0.5-mountainTreasureEntraceLeft.height

        //x: mainRoot.width * 7.7
        //x: mainRoot.width * 8 - mountainTreasureEntraceLeft.width
        //z: 0.5
        color: mountainColor
    }

    //mountain middle
    Rectangle {
        id: treasureMountainMiddle
        width: treasureMountainTop.width
        x: treasureMountainTop.x
        color: treasureMountainTop.color
        anchors.top: treasureMountainTop.bottom
        anchors.bottom: treasureMountainBottom.top
    }

    //mountain middle auxiliar to hide the horse when entering the mountain lair
    Rectangle {
        id: treasureMountainMiddleAux
        width: treasureMountainTop.width - mountainTreasureEntraceLeft.width*2
        color: treasureMountainTop.color
        anchors.top: treasureMountainTop.bottom
        anchors.bottom: treasureMountainBottom.top
        anchors.horizontalCenter: treasureMountainMiddle.horizontalCenter
        z: 1.5
    }


    //mountain left entrance
    Item {
        id: mountainTreasureEntraceLeft
        //color: "black"
        width: 10*mm
        //radius: width
        height: mareGirlHero.height*1.5
        x: treasureMountainMiddleAux.x-width
        anchors.top: treasureMountainTop.bottom
        //z: 1.5
        Image {
            anchors.fill: parent
            fillMode: Image.Stretch
            source: "qrc:/images/mountainEntrance.png"
            mipmap: true
        }
    }

    //mountain right entrance
    Item {
        id: mountainTreasureEntraceRight
        //color: mountainTreasureEntraceLeft.color
        width: mountainTreasureEntraceLeft.width
        //radius: mountainTreasureEntraceLeft.radius
        height: mountainTreasureEntraceLeft.height
        x: treasureMountainMiddleAux.x+treasureMountainMiddleAux.width
        y: mountainTreasureEntraceLeft.y
        //z: 1.5
        Image {
            anchors.fill: parent
            fillMode: Image.Stretch
            source: "qrc:/images/mountainEntrance.png"
            mipmap: true
        }
    }

    // mountain bottom
    Rectangle {
        id: treasureMountainBottom
        width: treasureMountainTop.width
        x: treasureMountainTop.x
        anchors.top: mountainTreasureEntraceLeft.bottom
        anchors.bottom: parent.bottom
        color: treasureMountainTop.color
    }

    /* width: mainRoot.width
    height: mainRoot.height*0.7

    property color mountainColor: "SaddleBrown"
    property real baseHeight: mountainID.height * 0.4
    property real middleHeight: mountainID.height * 0.3
    property real topHeight: mountainID.height - baseHeight - middleHeight

    Image {
        id: mountainLeft
        width: 25*mm
        height: baseHeight
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        source: "qrc:/images/mountainSide.png"
        mipmap: true
    }

    Image {
        id: mountainRight
        width: 25*mm
        height: baseHeight
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        source: "qrc:/images/mountainSide.png"
        mipmap: true
        mirror: true
    }

    Rectangle {
        id: mountainMiddleBase
        height: baseHeight
        anchors.bottom: parent.bottom
        anchors.left: mountainLeft.right
        anchors.leftMargin: -mm
        anchors.right: mountainRight.left
        anchors.rightMargin: -mm
        color: mountainColor
    }

    Rectangle {
        id: mountainMiddleCenter
        height: middleHeight
        anchors.bottom: mountainLeft.top
        anchors.left: mountainEntrance.horizontalCenter
        anchors.right: mountainExit.horizontalCenter
        color: mountainColor
    }

    Image {
        id: mountainEntrance
        width: height * 147/862
        height: middleHeight
        source: "qrc:/images/mountainEntrance.png"
        anchors.bottom: mountainLeft.top
        anchors.horizontalCenter: mountainLeft.right
        mipmap: true
    }

    Image {
        id: mountainExit
        width: height * 147/862
        height: middleHeight
        source: "qrc:/images/mountainEntrance.png"
        anchors.bottom: mountainLeft.top
        anchors.horizontalCenter: mountainRight.left
        mipmap: true
        mirror: true
    }

    Image {
        id: mountainTop
        height: topHeight
        anchors.bottom: mountainEntrance.top
        anchors.bottomMargin: -mm
        anchors.left: mountainEntrance.horizontalCenter
        anchors.right: mountainExit.horizontalCenter
        source: "qrc:/images/mountainTop.png"
        mipmap: true
    }
*/

}
