import QtQuick 2.0
import "../"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    //color:  colors.mainColor

    signal close()

    ConversationBoard2 {
        id: talkBoard
        anchors.top: parent.top
        anchors.bottom: btnClose.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        width: parent.width - btnClose.width*2 - 4*defaultMargins

        imageLeft: "asteroidSnorlex.png"
        imageRight: "robonoid.png"
        imageBottom: "asteroidSnorlex.png"

        color: "darkblue"

        //imageBottomWidth: 50*mm
        //imageBottomHeight: 50*mm
        imageBottomWidth: talkBoard.width*0.7
        imageBottomHeight:talkBoard.height*0.7

        visible: false

        onEndDialog: close()//btnClose.text = qsTr("Ok")
    }

    MyButton {
        id: btnClose
        //width: btnSize * 1.25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onBtnClick: close()
        text: qsTr("Skip")
    }

    onVisibleChanged: {
        if (visible) {
            addTalk()
            talkBoard.visible = true
        }
    }

    function addTalk() {
        talkBoard.clear()
        talkBoard.addMessage5(qsTr("There are forests with strange magic creatures in this planet."
                                   + "\nKeep exploring, brave hero."),
                              "", -1, false, "unicornhorse1.png")
    }
}
