import QtQuick 2.0
import "../"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "grey"

    signal menu()
    signal nextLevel()
    signal levelWin()
    //signal enterShop()
    //signal enterTreasureMountain()

    onLevelWin: {
        levels.levelUnlocked = levels.levelUnlocked === 9 ? 10 : levels.levelUnlocked
        levelContent.levelEndCleanup()
        panelEOL.text = qsTr("Victory!")
        //panelEOL.showNextBtn = true
        panelEOL.visible = true
    }

    CommandMultiTouchPad3 {
       id: commandTouchPad
       anchors.left: parent.left
       anchors.bottom: parent.bottom
       anchors.margins: defaultMargins
    }

    Flickable {
        id: flickLevel
        width: mainRoot.width
        height: mainRoot.height
        contentWidth: levelContent.width
        contentHeight: levelContent.height
        interactive: false
        Level09Content {
            id: levelContent
            width: mainRoot.width * 12
            height: mainRoot.height * 2
            onEnterShop: {
                shop.visible = true
            }
        }
    }//Flickable

    Shop {
        id: shop
        anchors.fill: parent
        visible: false
        onExitShop: {
            levelContent.exitShop()
            visible = false
        }
    }

    TouchPad2 {
        id: touchPad
        centerX: commandTouchPad.touchPad.centerX
        centerY: commandTouchPad.touchPad.centerY
        visible: commandTouchPad.touchPad.visible
    }

    MenuPopUpVertical {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }

    PanelEndOfLevel2 {
        id: panelEOL
        visible: false
        anchors.centerIn: parent
        showNextBtn: levels.levelUnlocked > 9
        onMenu: parent.menu()
        onReplay: {
            levelContent.levelEndCleanup()
            levelContent.levelStartSetup()
        }
        onNext: parent.nextLevel()
    }

}
