import QtQuick 2.3
import QtQuick.Particles 2.0
import "../"
import "../components"
import "../libraries"

Rectangle {
    id: root
    width: mainRoot.width * 12
    height: mainRoot.height * 2
    color: "grey"

    property var leaderObj: hero //horseHeroGirl
    property real leaderX: leaderObj.x
    property real leaderY: leaderObj.y
    property var followerObj: girl
    property var swordObj: null
    property bool showInitialHorseDialogue: true
    property bool carrotEaten: false
    property real groundLimit

    property int screenNumber: Math.floor(leaderObj.x / flickLevel.width)
    property int screenRow: Math.floor(leaderObj.y / flickLevel.height) //screen 0: upper; screenRow 1: lower

    property bool bookKeyUnlocked: false //after purchasing the book the alien near the flag will unlock the book

    signal destroyItems()
    signal levelFailed(string failMessage)
    signal enterShop()
    signal exitShop()
    //signal enableMareJumpIn()
    signal enterTreasureMontain()
    signal exitTreasureMontain()

    onVisibleChanged: {
        if (visible) {
            //console.log("leaderObj: " + leaderObj)
            //console.log("hero: " + hero)
            //console.log("girl: " + girl)
            levelStartSetup()
        }
        else {
            levelEndCleanup()
        }
    }

    onLevelFailed: {
        //destroyItems()
        //lib.removeEnemy()
        levelEndCleanup()
        panelEOL.text = failMessage //qsTr("Perdeste")
        panelEOL.visible = true
    }

    onLeaderXChanged: {

        if (screenNumber === 6 || screenNumber === 7) {
            //console.log("leaderObj.x: "+leaderObj.x)
            //console.log("river.x: "+river.x)

            //if (leaderObj.x > river.x && (leaderObj.x - leaderObj.width) < (river.x+river.width) ) {
            if ( leaderObj.x > river.x && (leaderObj.x+leaderObj.width) < (river.x+river.width) ) {
                groundLimit = mainRoot.height*2
                console.log("groundLimit: "+groundLimit)
            }
            else {
                groundLimit = sky.height+hero.height/2
                console.log("groundLimit: "+groundLimit)
            }
        }
    }

    onLeaderYChanged: {

        if (screenNumber === 6 || screenNumber === 7) {

            if ( leaderObj.x > river.x && (leaderObj.x+leaderObj.width) < (river.x+river.width) ) {
                //if ( (leaderY + leaderObj.height) > (river.y+ river.height*0.8) ) {
                if ( (leaderY + leaderObj.height) > (river.y+river.height*0.8) ) {
                    //panelEOL.x = mainRoot.width*screenNumber + mainRoot.width*0.5 - panelEOL.width*0.5
                    //panelEOL.y = mainRoot.height * screenRow + defaultMargins
                    console.log("river drowning----------------------------------------")
                    console.log("river drowning screenNumber:"+screenNumber)
                    console.log("river drowning screenRow:"+screenRow)
                    console.log("river drowning panel x:"+panelEOL.x)
                    console.log("river drowning panel y:"+panelEOL.y)
                    groundLimit = sky.height+hero.height/2 // restore ground level
                    levelFailed(qsTr("You drowned."))
                }
            }
        }
    }

    onScreenNumberChanged: {
        //console.log("I'm on screen number: "+screenNumber)
        if( screenNumber === 2 && screenRow === 1 && carrotEaten === false)
            showInitialHorseDialogue = true
    }

    /*onScreenRowChanged: {
        console.log("I'm on screen row: "+screenRow)
    }*/

    onEnterShop: {
        destroyConnections()
    }

    onExitShop: {
        createConnections()
    }

    onEnterTreasureMontain: {
        //treasureMountainMiddle.color = "grey"
    }

    onExitTreasureMontain: {
        //treasureMountainMiddle.color = "blue"
    }

    //onEnableMareJumpIn: {
        //mare.mouseAreaEnabled = true
    //}

    Rectangle {
        id: sky
        width: parent.width
        height: mainRoot.height + mainRoot.height * 0.7 // 70% of mainRoot screen height
        color: "lightcyan"
        anchors.top: parent.top
    }

    //property alias grass: grass
    Rectangle {
        id: grass
        width: mainRoot.width * 2.5
        height: mainRoot.height * 2 - sky.height
        color: "lime"
        anchors.top: sky.bottom
    }

    Rectangle {
        id: florestGround
        width: parent.width *grass.width
        height: mainRoot.height * 2 - sky.height
        color: "SaddleBrown" // "Sienna"
        anchors.top: sky.bottom
        anchors.left: grass.right
    }

    Image {
        id: grassForestTransition
        width: height * 109/300
        height: grass.height
        x: florestGround.x - width/2
        y: grass.y
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/grassForestTransition.png"
        mipmap: true
    }

    Rectangle {
        id: river
        color: "aqua"
        width: mainRoot.width
        height: grass.height
        x: mainRoot.width*6.5
        anchors.bottom: parent.bottom
    }

    Image {
        id: riverBankRight
        width: height * 218/300
        height: grass.height+3*defaultMargins
        x: river.x - width
        anchors.bottom: parent.bottom
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/riverBank.png"
        mipmap: true
    }

    Image {
        id: riverBankLeft
        width: height * 218/300
        height: grass.height+3*defaultMargins
        x: river.x +river.width //- width/2
        anchors.bottom: parent.bottom
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/riverBank.png"
        mipmap: true
        mirror: true
    }


    /*Mountain2 {
        id: mountain
        width: mainRoot.width * 1.5
        height: mainRoot.height * 0.6
        baseHeight: hero.height * 1.2
        centerHeight: horseHeroGirl.height * 1.2
        topHeight: horseHeroGirl.height * 1.2
        x: mainRoot.width * 3.25
        y: sky.height - mountain.height + hero.height*0.5
    }*/

    Rectangle {
        id: smallMountain
        width: mainRoot.width*0.5
        height: hero.height*1.2
        x: mainRoot.width * 4.75
        y: groundLimit-height //sky.height - mountain.height + hero.height*0.5
        z: 0.6
        radius: defaultMargins
        color: "SaddleBrown"
        border.color: "black"
    }

    Image {
        id: treePine
        width: height * 402/732
        height: 120*mm
        x: mainRoot.width*5.3 - width/2
        y: groundLimit-height
        source: "qrc:/images/tree2.png"
        mipmap: true
    }

    /*Image {
        id: treePine1
        width: height * 410/600
        height: 120*mm
        x: mainRoot.width*4.7 - width/2
        y: groundLimit-height
        source: "qrc:/images/tree3.png"
    }*/

    Image {
        id: unicornHome
        width: 120*mm
        height: width*218/300
        x: mainRoot.width*5.5 - width/2
        y: groundLimit-height
        source: "qrc:/images/unicornHome.png"
        mipmap: true

        Rectangle {
            id: houseBoard
            width: parent.width*0.35
            height: parent.height*0.3
            radius: defaultMargins

            //property color boardBackground: "cyan"

            property color boardBorder: "blue"
            property color boardBackground: "yellow"
            property color boardText: "red"
            //property color boardText: "darkgreen"

            color: boardBorder
            anchors.horizontalCenter: parent.horizontalCenter

            anchors.top: parent.top
            anchors.topMargin: 4*defaultMargins

            Rectangle {
                anchors.fill: parent
                anchors.margins: defaultMargins
                color: houseBoard.boardBackground
                TextTemplate {
                    anchors.fill: parent
                    label: qsTr("Unicorn\nHome")
                    color: houseBoard.boardText
                    horizontalAlignment: Text.AlignHCenter
                }
            }

        }
    }


    Image {
        id: shop
        //width: shop.height * 739/644
        width: shop.height * sourceSize.width/sourceSize.height
        height: 100*mm
        x: mainRoot.width*0.5-shop.width/2
        y: sky.height-shop.height + hero.height/4
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/shop.png"
        mipmap: true

        MouseArea {
            anchors.fill: parent
            onDoubleClicked: {
                console.log("signal enter the shop.................")
                enterShop()
            }
        }
    }

    Image {
        id: roadSign
        width: height * 167/139
        height: 70*mm
        x: mainRoot.width*2.25-roadSign.width/2
        y: sky.height-roadSign.height + hero.height/4
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/roadSign.png"
        mipmap: true
        TextTemplate {
            width: parent.width
            height: parent.height*0.5
            anchors.top: parent.top
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            label: qsTr("Unicorn\nforest")
            bold: true
        }
    }



    //mountain ----------------------------------------
    //mountain Top
    Rectangle {
        id: treasureMountainTop
        width: mainRoot.width + mountainTreasureEntraceLeft.width
        height: mainRoot.height*0.5-mountainTreasureEntraceLeft.height
        x: mainRoot.width * 9 - mountainTreasureEntraceLeft.width*0.5
        color: "SaddleBrown"
    }

    // mountain bottom
    Rectangle {
        id: treasureMountainBottomLeft
        width: mountainTreasureEntraceLeft.width
        x: treasureMountainTop.x
        anchors.top: mountainTreasureEntraceLeft.bottom
        anchors.bottom: parent.bottom
        color: treasureMountainTop.color
    }

    Rectangle {
        id: treasureMountainBottomRight
        width: treasureMountainBottomLeft.width
        x: treasureMountainTop.x+treasureMountainTop.width-width
        anchors.top: mountainTreasureEntraceLeft.bottom
        anchors.bottom: parent.bottom
        color: treasureMountainTop.color
    }

    Rectangle {
        id: mountainBackGroundMiddle
        color: "darkgrey"
        //width: treasureMountainTop.width - mountainTreasureEntraceLeft.width
        //x: treasureMountainTop.x+mountainTreasureEntraceLeft.width*0.5
        anchors.top: treasureMountainTop.bottom
        anchors.bottom: sky.bottom
        anchors.left: treasureMountainBottomLeft.right
        anchors.right: treasureMountainBottomRight.left
    }

    Rectangle {
        id: mountainBackGroundBottom
        color: "grey"
        width: mountainBackGroundMiddle.width
        anchors.top: mountainBackGroundMiddle.bottom
        anchors.bottom: parent.bottom
        x: mountainBackGroundMiddle.x
    }


    //mountain left entrance
    Rectangle {
        id: mountainTreasureEntraceLeft
        color: mountainBackGroundMiddle.color
        width: 20*mm
        //radius: width
        height: mareGirlHero.height*1.5
        x: treasureMountainTop.x //treasureMountainMiddleAux.x-width
        anchors.top: treasureMountainTop.bottom
        //z: 1.5
        Image {
            anchors.fill: parent
            fillMode: Image.Stretch
            source: "qrc:/images/mountainEntrance.png"
            mipmap: true
        }
    }

    //mountain right entrance
    Rectangle {
        id: mountainTreasureEntraceRight
        color: mountainTreasureEntraceLeft.color
        width: mountainTreasureEntraceLeft.width
        //radius: mountainTreasureEntraceLeft.radius
        height: mountainTreasureEntraceLeft.height
        x: treasureMountainTop.x + treasureMountainTop.width-width//treasureMountainMiddleAux.x+treasureMountainMiddleAux.width
        y: mountainTreasureEntraceLeft.y
        //z: 1.5
        Image {
            anchors.fill: parent
            fillMode: Image.Stretch
            source: "qrc:/images/mountainEntrance.png"
            mirror: true
        }
    }

    Image {
        id: treasure
        width: 35*mm
        height: width*120/123
        y: groundLimit-height//mountainBackGroundMiddle.bottom
        fillMode: Image.Stretch
        source: "qrc:/images/diamondArk.png"
        anchors.horizontalCenter: mountainBackGroundMiddle.horizontalCenter
        mipmap: true
        //anchors.bottomMargin: defaultMargins
    }

    //end of mountain---------------------------

    /*ArrowSign {
        id: mountainTreasureSign
        width: height * 142/161
        height: 90*mm
        x: mainRoot.width*7.3-roadSign.width/2
        y: groundLimit - height //sky.height-roadSign.height + hero.height/4
        z: 0.5
    }*/

    Image {
        id: mountainTreasureSign
        width: height * 167/139
        height: 70*mm
        x: mainRoot.width*8.3-roadSign.width/2
        y: groundLimit - height //sky.height-roadSign.height + hero.height/4
        z: 0.5
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/roadSign.png"
        mipmap: true
        TextTemplate {
            width: parent.width
            height: parent.height/2
            anchors.top: parent.top
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            label: qsTr("Treasure\nMountain")
            bold: true
        }
    }


    //-clouds-------------------------------------------------------

    Image {
        id: cloud1
        width: 80*mm; height: width*119/280
        x: mainRoot.width*0.4; y: mainRoot.height*0.3
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/cloud01.png"
        mipmap: true
        rotation: 12
    }

    Image {
        id: cloud2
        width: 80*mm; height: width*119/280
        x: mainRoot.width*1.4; y: mainRoot.height*0.3
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/cloud02.png"
        mipmap: true
        rotation: -4
    }

    Image {
        id: cloud3
        width: 80*mm; height: width*119/280
        x: mainRoot.width*2.3; y: mainRoot.height*0.3
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/cloud01.png"
        mipmap: true
        rotation: -5
    }

    Image {
        id: cloud01
        width: 80*mm; height: width*119/280
        x: mainRoot.width*3.3; y: mainRoot.height*0.3
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/cloud02.png"
        mipmap: true
    }

    Image {
        id: cloud02
        width: 80*mm; height: width*102/180
        x: mainRoot.width*4.35; y: mainRoot.height*0.3
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/cloud02.png"
        mipmap: true
    }

    Image {
        id: cloudTurtle1
        width: 80*mm; height: width*140/320
        x: mainRoot.width*5.25; y: mainRoot.height*0.3
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/cloudTurtle.png"
        mipmap: true; mirror: true
    }

    Image {
        id: cloudTurtle2
        width: 80*mm
        height: width*140/320
        y: mainRoot.height*0.3
        anchors.left: cloudTurtle1.right
        anchors.leftMargin: 10*defaultMargins
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/cloudTurtle.png"
        mipmap: true
    }

    Image {
        id: cloudHorse
        width: 80*mm; height: width*180/320
        x: mainRoot.width*6.3; y: mainRoot.height*0.3
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/cloudsHorse.png"
        mipmap: true; mirror: true
    }

    Image {
        id: cloudHorse1
        width: 80*mm; height: width*180/320
        anchors.left: cloudHorse.right
        anchors.leftMargin: 10*defaultMargins
        y: mainRoot.height*0.3
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/cloudsHorse.png"
        mipmap: true; mirror: true
    }

    //-clouds-------------------------------------------------------



    Person2 {
        id: mare
        width: height * 320/236
        height: 48*mm
        x: mainRoot.width*5.75
        worldLimitGround: groundLimit
        //y: groundLimit-height
        z: 1
        imgName: "mareflying"
        dir: -1
        mouseAreaEnabled: leaderObj !== horseHeroGirl
        onMouseAreaEnabledChanged: console.log("mare mouseArea is now: "+mouseAreaEnabled)
        onDoubleClick: {
            visible = false
            jumpInHorseMare(mareGirlHero, mare)
            //mareGirlHero.visible = true
        }
        function reset(){
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
            //worldLimitGround = groundLimit

            x = mainRoot.width*5.75
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
            visible = true
        }
    }

    Person2 {
        id: mareGirlHero
        width: height * 320/236
        height: 48*mm
        z: 1
        imgName: "mareflyingHG"
        jumpLenght: height*1.5
        dir: -1
        onDoubleClick: jumpOffHorseMare(mareGirlHero, mare)
        worldLimitGround: groundLimit
        function reset(){
            dir = 1
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
             //sky.height+hero.height/2
            x = mainRoot.width*5.75
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
            visible = false
        }
    }

    /*Timer {
        id: timerMareFlying
        interval: fps_ms
        running: isFlying && (leaderObj === mareGirlHero)
        repeat: true
        onTriggered: {
            mareGirlHero.y1 -= flyVelocityY
        }
        onRunningChanged: {
            if (!running) {
                console.log("mare fall")
                mareGirlHero.fall()
            }
        }
    }*/    

    Image {
        id:photoMachine
        anchors.bottom: scientistMan.bottom
        anchors.right: scientistMan.left
        height: scientistMan.height*0.7
        width: height * 178/280
        source: "qrc:/images/futurePhotoMachine.png"
        mipmap: true
        mirror: true
    }

    PersonCharacter {
        id: scientistMan
        x: mainRoot.width * 1.75
        y: groundLimit-height
        width: height * 125/230
        height: 39*mm //width * 320/125
        imgName: "scientist.png"
    }


    CheckPersonNear {
        id: checkScientistHeroDistance
        me: scientistMan
        target: leaderObj

        onTargetChangedNear: {
            dialogueScientistMan()
            talkBoard.show()
            //isEnabled = false
        }
        onTargetChangedFar: talkBoard.visible = false
        isEnabled: screenNumber === 1
        debugInfo: "target scientistMan"
    }


    //--unicorn farting :) ---------------------------
    ParticleSystem {
        id: unicornFartParticleSystem
        running: false
    }

    Emitter {
        id: fireEmitter
        width: 2*mm; height: width
        x: horseHeroGirl.dir === 1 ? horseHeroGirl.x : horseHeroGirl.x+horseHeroGirl.width
        y: horseHeroGirl.y+horseHeroGirl.height/2

        system: unicornFartParticleSystem
        emitRate: 30; lifeSpan: 800; lifeSpanVariation: 300
        size: 4*mm
        endSize: size*0.75
        velocity: AngleDirection {
                    angle: horseHeroGirl.dir === 1 ?180 : 0
                    angleVariation: 20
                    magnitude: horseHeroGirl.width //8*mm
                    magnitudeVariation: mm
                }
    }

    ImageParticle {
        //source: "qrc:/images/fireParticle2.png"
        source: "qrc:/images/Explosion.png"
        system: unicornFartParticleSystem
        color: "darkgrey" //"#FFD700"
        colorVariation: 0.2
        entryEffect: ImageParticle.Scale
        z:1
    }

    Timer {
        id: timerUnicornFarting
        running: false
        repeat: false
        interval: 1000
        onRunningChanged: {
            if(running)
                unicornFartParticleSystem.start()
        }

        onTriggered: {
            unicornFartParticleSystem.restart()
            unicornFartParticleSystem.stop()
            unicornFart()
        }
    }

    //--unicorn farting :) ---------------------------


    Person2 {
        id: horse
        width: height * 463/391
        height: 48*mm
        z: 1
        imgName: "unicornhorse"
        dir: -1
        worldLimitGround: groundLimit
        mouseAreaEnabled: carrotEaten && leaderObj !== mareGirlHero
        onMouseAreaEnabledChanged: console.log("horse mouseArea is now: "+mouseAreaEnabled)

        function reset(){
            worldLimitLeft = 0
            worldLimitRight = parent.width-horse.width
            worldLimitSky = 0
            //worldLimitGround = groundLimit
            x = mainRoot.width * 3.5
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
            carrotEaten = false
            visible = true
        }

        Timer {
            id: horseRidding
            running: screenNumber === 3 && !talkBoard.visible && horse.visible && !heroMap.visible && !carrotEaten
            interval: fps_ms
            repeat: true
            onRunningChanged: {
                //console.log("horse running is "+horseRidding.running+" .........................................")
                running ? horse.startWalking() : horse.stopWalking()
            }

            onTriggered: {
                horse.walk()
                if (horse.x < mainRoot.width*3+horse.width)
                    horse.dir *= -1
                if (horse.x > mainRoot.width*4 - horse.width)
                    horse.dir *= -1
            }
        }
        onDoubleClick: {
            if (carrotEaten)
                //jumpInHorse()
                jumpInHorseMare(horseHeroGirl, horse)
        }
    }


    Person2 {
        id: horseHeroGirl
        width: height * 473/397
        height: 48*mm//48*mm
        z: 1
        worldLimitGround: groundLimit
        imgName: "horseHeroGirl"
        jumpLenght: height*1.5
        function reset(){
            dir = 1
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
            //worldLimitGround = groundLimit
            x = 0
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
            visible = false
        }
        onDoubleClick: jumpOffHorseMare(horseHeroGirl, horse)
        lowerMouseAreaEnabled: true
        onLowerMouseAreaDoubleClick: {
            timerUnicornFarting.start()
            /*if (unicornFartParticleSystem.running) {
                unicornFartParticleSystem.restart()
                unicornFartParticleSystem.stop()
            }
            else {
                unicornFartParticleSystem.start()
            }*/
        }
    }

    Person2 {
        id: girl
        imgName: "girl"
        imgFireShot: "girlFire"
        worldLimitGround: groundLimit
        z: 1
        function reset() {
            dir = 1
            visible = true
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
            //worldLimitGround = groundLimit
            x = 0
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
        }
        mouseAreaEnabled: true
        onDoubleClick: {
            setLeaderFollower(girl, hero)
            mouseAreaEnabled = false
            hero.mouseAreaEnabled = true
        }
    }

    Person2 {
        id: hero
        width: 18*mm
        height: width * 320/156
        //jumpLenght: width*3.5
        z: 1
        worldLimitGround: groundLimit
        onImDestroyed: {
            //heroLifeBar.hitDestroy()
            levelFailed(qsTr("You lost!!!"))
        }
        onImHit: heroLifeBar.hit()

        function reset(){
            hero.dir = 1
            visible = true
            worldLimitLeft = 0
            worldLimitRight = parent.width
            worldLimitSky = 0
            //worldLimitGround = groundLimit
            x = 0
            y1 = worldLimitGround
            y1Previous = worldLimitGround
            dy = 0 //quick fix a strange bug where default value of dy is not 0
            //heroLifeBar.initialLife = 3
            //heroLifeBar.reset()
            leaderObj = hero
        }
        mouseAreaEnabled: false
        onDoubleClick: {
            setLeaderFollower(hero, girl)
            mouseAreaEnabled = false
            girl.mouseAreaEnabled = true
        }
    }

    Person {
        id: carrot
        width: 15*mm
        height: width
        x: 0
        y: sky.height
        z: 1
        imgName: "carrot"
        imgWalk: "0"
        rotation: 33
        visible: false
    }

    PersonCharacter {
        id: alienEndOfLevel
        x: mainRoot.width * 11.5
        y: groundLimit-height
        z: 0.8
        width: height * 186/179
        height: 29*mm //width * 320/125
        imgName: "robonoid2.png"
    }

    PersonCharacter {
        id: flagEndOfLevel
        width: height * 124/350
        height: 80*mm //25*mm
        x: mainRoot.width * 11.8
        y: grass.y+grass.height*0.25 - flagEndOfLevel.height
        z: 0.8
        imgName: "flag.png" // to change later
    }

    CheckPersonNear {
        id: checkAlienEndLevelHeroDistance
        me: alienEndOfLevel
        target: leaderObj
        onTargetChangedNear: {
            dialogueAlienEndLevel()
            talkBoard.show()
        }
        onTargetChangedFar: {
            talkBoard.visible = false
        }
        isEnabled: screenNumber === 11
        debugInfo: "target alien end of level"
    }


    InterfaceWalkPartner {
        id: interfaceWalkGirlHero
        leader: leaderObj
        follower: followerObj
        //call function connect() to make the girl follow the hero
    }

    CheckPersonNear {
        id: checkFlagHeroDistance
        me: flagEndOfLevel
        target: leaderObj
        onTargetChangedNear: {
            if (bookKeyUnlocked)//heroMap.bookOfHapinessUnlocked
                levelWin()
        }
        isEnabled: screenNumber === 11
        debugInfo: "target flag end of level"
    }


    CheckPersonNear {
        id: checkHeroHorseDistance
        me: leaderObj
        target: horse
        distanceNear: horse.width
        debugInfo: "horse hero distance"
        onTargetChangedNear: {
            console.log("hero is near the horse..........................................................")
            talkBoard.x = mainRoot.width*3.5 - talkBoard.width/2
            talkBoard.y = mainRoot.height + defaultMargins
            createDialogueHeroHorse()
            talkBoard.show()
            showInitialHorseDialogue = false
            //console.log("near horse")
        }
        onTargetChangedFar: {
            talkBoard.visible = false
            //console.log("far away from horse")
        }
        isEnabled: (screenNumber === 3) && (screenRow === 1) && showInitialHorseDialogue && (carrotEaten === false)
        onIsEnabledChanged: console.log("CheckPersonNear on hero horse is ....................................... "+isEnabled)
    }

    CheckPersonNear {
        id: checkCarrotHorseDistance
        me: carrot
        target: horse
        distanceNear: carrot.width
        onTargetChangedNear: {
            createDialogueHorseEatsCarrot()
            talkBoard.show()
            carrotEaten = true
            console.log("carrot near horse................................................")
        }
        isEnabled: (screenNumber === 3) && (screenRow === 1) && carrot.visible && (!carrotEaten)
        debugInfo: "carrot - horse ......................................"
    }

    property bool horseArrivedAtHome: false
    CheckPersonNear {
        id: checkHorseHome
        me: unicornHome
        target: horseHeroGirl
        distanceNear: horseHeroGirl.width//unicornHome.width*0.5
        isEnabled: screenNumber === 5 && (!horseArrivedAtHome)
        debugInfo: "horse near is home"
        onTargetChangedNear: {
            console.log("target changed - horse near to it's home")
            createDialogueHorseArrivesHome()
            talkBoard.show()
            //isEnabled = false
        }
        onTargetChangedFar: {
            console.log("target changed - horse far from it's home")
            horseArrivedAtHome = true
            talkBoard.visible = false
        }
    }

    property bool heroMeetsMare: false
    CheckPersonNear {
        id: checkMareHeroGirl
        me: mare
        target: hero//or girl - dont use leaderObj because we want only to disply the dialogue if the hero/girl have jumped off the horse
        distanceNear: mare.width
        isEnabled: screenNumber === 5 && (!heroMeetsMare)
        debugInfo: "hero or girl near mare"
        onTargetChangedNear: {
            dialogueHeroGirlHorsine()
            talkBoard.opacity = 1
            talkBoard.show()
            //isEnabled = false
        }
        onTargetChangedFar: {
            heroMeetsMare = true
            talkBoard.visible = false
        }
    }

    /*CheckPersonNear {
        id: checkFlyingMareMountainEntrance
        me: treasureMountainMiddle
        target: mareGirlHero
        distanceNear: treasureMountainMiddle.width*0.5
        isEnabled: screenNumber === 7 || screenNumber === 8
        debugInfo: "flying mare near mountain entrance left"
        onTargetChangedNear: {
            enterTreasureMontain()
        }
        onTargetChangedFar: {
            exitTreasureMontain()
        }
    }*/

    CheckPersonNear {
        id: checkMareHGTreasure
        me: treasure
        target: mareGirlHero
        distanceNear: treasure.width
        isEnabled: screenNumber === 9
        onTargetChangedNear:  {
            console.log("treasure found.......................................")
            dialogueTreausureFound()
            talkBoard.show()
            heroMap.treasureBlueDiamondsUnlocked = true
            //isEnabled = false
            //treasureCaught()
        }
        onTargetChangedFar: {
            talkBoard.visible = false
        }
    }

    ConversationBoard2 {
        id: talkBoard
        width: mainRoot.width * 0.6
        height: mainRoot.height * 0.6//0.5
        //x: 0
        //y: mainRoot.height+defaultMargins
        z: 1
        opacity: 0.75
        imageLeft: "heroHead.png"
        imageRight: "unicornHorseHead.png"

        imageBottomWidth: 20*mm
        imageBottomHeight: 20*mm
        visible: false
        onEndDialog: {
            if (carrotEaten) {
                carrot.visible = false
                //horse.mouseAreaEnabled = true
            }
            opacity = 0.75
        }
        //Component.onCompleted: createDialogueHeroHorse()
    }


    function levelStartSetup(){
        bookKeyUnlocked = false
        lib.projectileShotDelay = true
        console.log("level 10 startup setup")
        groundLimit = sky.height+hero.height/2
        console.log("groundLimit: "+ groundLimit)
        panelEOL.visible = false
        leaderObj = hero
        createItems()
        createConnections()
        hero.reset()
        girl.reset()
        horse.reset()
        mare.reset()
        horseHeroGirl.reset()
        mareGirlHero.reset()
        showInitialHorseDialogue = true
        addObstacles()
        horseArrivedAtHome = false
        heroMeetsMare = false
    }

    /*Rectangle {
        id: rect
        color: "red"
        width: mainRoot.width
        height: mainRoot.height*0.1
        x: mainRoot.width*0.7
        y: groundLimit-height
    }*/

    function addObstacles() {
        obstaclesList.clearObstacles()

        obstaclesList.addObstacle(smallMountain.x, smallMountain.y,
                                  smallMountain.x+smallMountain.width, smallMountain.y+smallMountain.height)
        obstaclesList.addObstacle(treasureMountainTop.x, treasureMountainTop.y,
                                  treasureMountainTop.x+treasureMountainTop.width,treasureMountainTop.y+treasureMountainTop.height)

        obstaclesList.addObstacle(treasureMountainTop.x, treasureMountainTop.y,
                                  treasureMountainTop.x+treasureMountainTop.width,treasureMountainTop.y+treasureMountainTop.height)
        obstaclesList.addObstacle(treasureMountainBottomLeft.x, treasureMountainBottomLeft.y,
                                  treasureMountainBottomLeft.x+treasureMountainBottomLeft.width,treasureMountainBottomLeft.y+treasureMountainBottomLeft.height)
        obstaclesList.addObstacle(treasureMountainBottomRight.x, treasureMountainBottomRight.y,
                                  treasureMountainBottomRight.x+treasureMountainBottomRight.width,treasureMountainBottomRight.y+treasureMountainBottomRight.height)

    }


    function levelEndCleanup(){
        activeEnemiesList.clearEnemies()
        destroyItems()
        destroyConnections()
        obstaclesList.clearObstacles()
        hero.stopWalking()
        girl.stopWalking()
        horse.stopWalking()
        mare.stopWalking()
        horseHeroGirl.stopWalking()
        mareGirlHero.stopWalking()
    }


    function createItems() {

        var i=0;
        for (i=0;i < 15; i++) {
            //unicorn forest trees------------------------------------------
            var treeObj = lib.createItem("components/PersonCharacter.qml", root)
            treeObj.imgName = "tree2.png"
            //treeObj.width = height * 0.8
            //treeObj.height = 80*mm
            treeObj.width = 88*mm * 455/774
            treeObj.height = 88*mm
            treeObj.x = mainRoot.width * 2.5 + treeObj.width*i
            treeObj.y = sky.height - treeObj.height + treeObj.height*0.1
            treeObj.z = 0.5
            destroyItems.connect( treeObj.destroy )
        }


        for (i=0;i < 6; i++) {
            //forest trees after treasure mountain----------------------------------
            var treeObj2 = lib.createItem("components/PersonCharacter.qml", root)
            treeObj2.imgName = "tree2.png"
            //treeObj.width = height * 0.8
            //treeObj.height = 80*mm
            treeObj2.width = 88*mm * 455/774
            treeObj2.height = 88*mm
            treeObj2.x = mainRoot.width * 10.5 + treeObj2.width*i
            treeObj2.y = sky.height - treeObj2.height + treeObj2.height*0.1
            treeObj2.z = 0.5
            destroyItems.connect( treeObj2.destroy )
        }

    }//createItems



    onDestroyItems: {

    }


    function createConnections() {

        //console.log("createConnections commandTouchPad"+commandTouchPad)
        //console.log("connectCommandWalk commandTouchPad: "+commandTouchPad)
        //console.log("connectCommandWalk commandKeyb: "+commandKeyb)
        //console.log("connectCommandWalk leaderObj: "+leaderObj)

        lib.connectCommandWalk(commandTouchPad, leaderObj)
        lib.connectCommandWalk(commandKeyb, leaderObj)


        if (leaderObj === mareGirlHero) {
            lib.connectCommandFly(commandTouchPad, leaderObj)
            lib.connectCommandFly(commandKeyb, leaderObj)
        }


        interfaceWalkGirlHero.leader = leaderObj
        interfaceWalkGirlHero.follower = followerObj
        interfaceWalkGirlHero.connect()

        heroMap.showBowAndArrow.connect( createBow )
        heroMap.hideBowAndArrow.connect( destroyBow )

        heroMap.showSword.connect( createSword )
        heroMap.hideSword.connect( destroySword )

        flickLevel.contentX = Qt.binding( function(){ //moves the scene when the hero reachs the end of the screen
            return Math.floor(leaderObj.x / flickLevel.width) * flickLevel.width} )

        flickLevel.contentY = Qt.binding( function(){
            return Math.floor(leaderObj.y / flickLevel.height) * flickLevel.height} )

        heroMap.showCarrots.connect( showCarrot )


        if (leaderObj === girl)
            girlShotCreateConnection()
    }

    function destroyConnections() {
        lib.disconnectCommandWalk(commandTouchPad, leaderObj)
        lib.disconnectCommandWalk(commandKeyb, leaderObj)

        if (leaderObj === mareGirlHero) {
            lib.disconnectCommandFly(commandTouchPad, leaderObj)
            lib.disconnectCommandFly(commandKeyb, leaderObj)
        }

        interfaceWalkGirlHero.disconnect()

        heroMap.showBowAndArrow.disconnect( createBow )
        heroMap.hideBowAndArrow.disconnect( destroyBow )

        //flickLevel.contentX = 0
        //flickLevel.contentY = 0

        heroMap.hideCarrots.connect( hideCarrot )

        if (leaderObj === girl)
            girlShotDestroyConnection()
    }


    //-- sword--------------------------------------------
    function createSword() {
        swordObj = lib.createSword(swordObj, leaderObj, parent)

        //todo: connect add attack signals
        lib.connectSwordAttack(commandTouchPad, swordObj)
        lib.connectSwordAttack(commandKeyb, swordObj)

        swordObj.attackIsRunning.connect( updateRec)//visual sword attack debug
        destroyItems.connect( swordObj.destroy )
    }

    //for debbuging sword attack position
    function updateRec(xx, yy){
        redrec.x = xx
        redrec.y = yy
    }

    //for debbuging sword attack position
    Rectangle {
        id: redrec
        width: 2
        height: 2
        color: "red"
    }

    function destroySword() {
        //todo: add disconnections made in createSword()
        lib.disconnectSwordAttack(commandTouchPad, swordObj)
        lib.disconnectSwordAttack(commandKeyb, swordObj)
        swordObj.destroy()
        swordObj = null

    }
    //-- sword--------------------------------------------


    //-- Bow & Arrows-----------------------------------------------
    function createBow(){
        lib.createBow(leaderObj, parent)
        commandTouchPad.shotFiredXY.connect( createArrow )
    }

    function destroyBow(){
        commandTouchPad.shotFiredXY.disconnect( createArrow )
        lib.destroyBow()
    }

    function createArrow(touchX, touchY) {
        //var arrow = lib.createArrow(touchX, touchY, bowObj, screenNumber, screenRow, root)
        var arrow = lib.createProjectile(touchX, touchY, lib.bowObj.midX, lib.bowObj.midY, screenNumber, screenRow, parent)
        if (arrow === null)
            return;

        arrow.imageName = "arrow.png"
        arrow.projectileType = "arrow"
        arrow.width = 20*mm
        arrow.height = 20*mm*0.273
    }
    //-- Bow & Arrows-----------------------------------------------

    //-- Girl Shot-----------------------------------------------
    property bool girIsFiring: false
    signal createGirlShot(var touchX, var touchY)

    onCreateGirlShot: {
        if (girl !== leaderObj || swordObj !== null || lib.bowObj !== null)
            return;//todo: remove this return and fix the connection to only be made when girl is leader

        girIsFiring = true;
        var girlShotObj = lib.createProjectile(touchX, touchY, girl.midX, girl.midY-girl.height*0.2, screenNumber, screenRow, root)
        if (girlShotObj === null) {
            girIsFiring = false;
            return;
        }

        girlShotObj.width = 14*mm
        girlShotObj.height = girlShotObj.width
        girlShotObj.color = "white"
        girlShotObj.border.width = 1*mm
        girlShotObj.border.color = "dimgrey"
        girIsFiring = false;
    }
    //-- Girl Shot-----------------------------------------------


    function girlShotCreateConnection() {
        console.log("girl shot connectio created....................................")
        commandTouchPad.shotFiredXY.connect( createGirlShot )
        createGirlShot.connect( girl.fireShot )

    }

    function girlShotDestroyConnection() {
        commandTouchPad.shotFiredXY.disconnect( createGirlShot )
        createGirlShot.disconnect( girl.fireShot )
    }



    function jumpInHorseMare(hmHeroGirl, horseMare) {
        //hmHeroGirl - is horse or mare with hero and girl jumped in
        //horseMare - is horse or mare without the hero and girl

        // disable both to avoid hero and girl jumping in mare when they already in the horse, and vice versa
        //mareGirlHero.mouseAreaEnabled = false
        //horseHeroGirl.mouseAreaEnabled = false

        hmHeroGirl.x = horseMare.x
        hmHeroGirl.y1 = horseMare.y1
        hmHeroGirl.dir = horseMare.dir
        horseMare.visible = false
        hero.visible = false
        hero.mouseAreaEnabled = false
        girl.visible = false
        girl.mouseAreaEnabled = false
        hmHeroGirl.visible = true
        //hmHeroGirl.mouseAreaEnabled = true
        hideCarrot()

        //hmHeroGirl.setLeader()
        destroyConnections()//destroy connections with current leader, and destroy bow if exists
        if (heroMap.bowAndArrowSelected)
            destroyBow()
        leaderObj = hmHeroGirl
        createConnections()//restore connections with the new leader
    }

    function jumpOffHorseMare(hmHeroGirl, horseMare) {
        horseMare.dir = hero.dir = girl.dir = hmHeroGirl.dir
        horseMare.x = hmHeroGirl.x
        hero.y1 = girl.y1 = horseMare.y1 = hmHeroGirl.y1

        hero.x = girl.x = horseMare.x

        hmHeroGirl.visible = false
        //hmHeroGirl.mouseAreaEnabled = false

        //horseMare.mouseAreaEnabled = true
        horseMare.visible = true

        hero.visible = girl.visible = true

        destroyConnections()//destroy connections with current leader
        leaderObj = hero //todo: check if previous leader was the girl (before jumping in)
        createConnections()//restore connections with the new leader
    }

    function setLeaderFollower(leader, follower) {
        if (leader === leaderObj)//if there is no change return
            return;

        destroyConnections()//destroy connections with current leader, and destroy bow if exists
        if (heroMap.bowAndArrowSelected)
            destroyBow()

        if (heroMap.swordSelected)//destroy connections with current leader, and destroy sword if exists
            destroySword()

        leaderObj = leader//set new leader
        followerObj = follower//and new follower

        if (heroMap.bowAndArrowSelected)//create bow if existed
            createBow()

        if (heroMap.swordSelected)//create sword if existed before chaging leader
            createSword()

        createConnections()//restore connections with the new leader
    }

    function showCarrot() {
        carrot.visible = true
        carrot.x = Qt.binding( function() {return leaderObj.dir === 1 ?
                                               leaderObj.x+leaderObj.width*0.5 : leaderObj.x-carrot.width*0.5} )
        carrot.y = Qt.binding( function() {return leaderObj.y+leaderObj.height*0.4} )
    }

    function hideCarrot() {
        carrot.visible = false
        carrot.x = 0
        carrot.y = 0
    }

    //--dialogues------------------------------------------------------

    function createDialogueHeroHorse() {
        talkBoard.clear()

        talkBoard.addMessage4(qsTr("brrrh..., brrrh..., clip clop, clip clop, brrrh..."),"unicornHorseHead.png", 2, true)
        talkBoard.addMessage4(qsTr("Hi horsy. I don't understand what you say!"), "heroHead.png", 1, false)
        talkBoard.addMessage4(qsTr("What a cute horse. I think he's hungry!!!"), "girlHead.png", 1, false)
    }

    function createDialogueHorseEatsCarrot() {
        talkBoard.clear()

        //talkBoard.imageBottomWidth = 35*mm
        //talkBoard.imageBottomHeight = 35*mm

        talkBoard.imageBottomWidth = talkBoard.width*0.7
        talkBoard.imageBottomHeight = talkBoard.height*0.7

        talkBoard.addMessage5(qsTr("How!!! I can talk! Thoose carrots must be magic! Thank you."),"unicornHorseHead.png", 2, true, "horseEatingCarrot.png")
        talkBoard.addMessage4(qsTr("Can you help us? We're going to explore the florest."), "heroHead.png", 1, false)
        talkBoard.addMessage4(qsTr("Sure, I will give you a ride."),"unicornHorseHead.png", 2, true)
        talkBoard.addMessage4(qsTr("Just double click my head to jump in, and double click again to jump off."
                                   +"\nAlso did you know I can jump very high?"),"unicornHorseHead.png", 2, true)
        talkBoard.addMessage4(qsTr("But what ever you do, please don't ever double click my feet."),"unicornHorseHead.png", 2, true)

    }

    function createDialogueHorseArrivesHome() {
        talkBoard.x = mainRoot.width*5.5 - talkBoard.width/2
        talkBoard.y = mainRoot.height + defaultMargins
        talkBoard.clear()

        talkBoard.addMessage4(qsTr("I can't go further, there's a river ahead. My wife will take you."),"unicornHorseHead.png", 1, false)

        talkBoard.addMessage4(qsTr("She can swim ?"), "heroHead.png", 1, false)
        talkBoard.addMessage4(qsTr("No, she can fly. Double click my head to jump off and go talk to her."
                                   +"\nHave a safe journey."), "unicornHorseHead.png", 1, false)
    }

    function dialogueHeroGirlHorsine() {
        talkBoard.x = mainRoot.width*5.5 - talkBoard.width/2
        talkBoard.y = mainRoot.height + defaultMargins
        talkBoard.clear()

        //talkBoard.imageBottomWidth = 80*mm
        //talkBoard.imageBottomHeight = 70*mm
        talkBoard.imageBottomWidth = talkBoard.width*0.85
        talkBoard.imageBottomHeight = talkBoard.height*0.85

        talkBoard.addMessage4(qsTr("Hi there, I see you meet my husband unicorn Einstein. I'm Mary Mare."),"mareHead.png", 2, false)
        talkBoard.addMessage4(qsTr("Hello I'm captain Andrew."), "heroHead.png", 1, false)
        talkBoard.addMessage4(qsTr("Hi, I'm Louise..."), "girlHead.png", 1, false)
        talkBoard.addMessage4(qsTr("I know who you are princess Pandora. Welcome!"),"mareHead.png", 2, false)
        talkBoard.addMessage4(qsTr("Princess Pandora ???\nYou didn't tell me you are a princess!"), "heroHead.png", 1, false)
        talkBoard.addMessage4(qsTr("You didn't ask 😊"), "girlHead.png", 1, false)

        talkBoard.addMessage4(qsTr("Double click my head to jump aboard, I will take to see the forest. "
                                   +"Press and slide anywhere on the screen to walk, then while walking, use "
                                   +"another finger to press anywhere to fly."),"mareHead.png", 2, false)

        talkBoard.addMessage5("","mareHead.png", 2, false, "commandFlyMare.png")

        talkBoard.addMessage4(qsTr("If you have a keyboard, press the f key to fly and arrows keys to move."),"mareHead.png", 2, false)

        //enableMareJumpIn()
    }

    function dialogueTreausureFound() {
        talkBoard.clear()

        talkBoard.x = mainRoot.width * 9.5 - talkBoard.width/2
        talkBoard.y = mainRoot.height + defaultMargins
        talkBoard.imageBottomWidth = talkBoard.width*0.6
        talkBoard.imageBottomHeight = talkBoard.height*0.6

        talkBoard.addMessage5(qsTr("You found the blue diamonds treasure."),"", 0, false, "diamondArk.png")
    }

    function dialogueScientistMan() {
        talkBoard.x = mainRoot.width*1.5 - talkBoard.width/2
        talkBoard.y = mainRoot.height + defaultMargins
        talkBoard.clear()
        talkBoard.opacity = 1.0

        //talkBoard.imageBottomWidth = 80*mm
        //talkBoard.imageBottomHeight = 50*mm
        talkBoard.imageBottomWidth = talkBoard.width*0.85
        talkBoard.imageBottomHeight = talkBoard.height*0.85

        talkBoard.addMessage4(qsTr("Hi there, I'm a scientist.\n"
                                   +"I have invented a future photo machine."),"scientistHead.png", 2, false)

        talkBoard.addMessage4(qsTr("You can peak in the len and see a random photo"
                                   +" of you in the future. I saw myself winning the nobel prize 😊\n"
                                   +"Go ahead and have a look."),"scientistHead.png", 2, false)


        talkBoard.addMessage4(qsTr("Let me try."),"heroHead.png", 1, false)

        talkBoard.addMessage5("", "heroHead.png", 1, false, "world3D.png")


        talkBoard.addMessage4(qsTr("Ohhh. I saw some weird picture of a very strange place 😯!!!"
                                   +"\nA thought came to my mind: \"3D world is coming!\"\nPandora, now you take a look."), "heroHead.png", 1, false)


        talkBoard.addMessage5("", "girlHead.png", 1, false, "family.png")

        talkBoard.addMessage4(qsTr("What 😠 ??? I'm seeing a picture of... never mind, it's all blurry."
                                   +"\nHow does this machine work, is this real ?"), "girlHead.png", 1, false)


        talkBoard.addMessage4(qsTr("Well, I discovered a new type of atomic particles called TimeQuarks wich represent time particles and can be codified "
                                   + "with complex math equations based on four dimensional matrices and quaternions. As you know "
                                   + "quaternions are represent like a+b*i+c*j+d*k where a, b, c, and d are real numbers, and i, j, and k are "
                                   + "the fundamental quaternion units. They are very usefull to represent the 3-sphere or glomes. What my photo time machine "
                                   + "really does is to use a sensor that captures photons to compose a normal regular photo, and use another special type "
                                   + "of sensor that can capture TimeQuarks that coexist all around us along with the photons. The machine then uses is "
                                   + "very powerfull GPUs to process the image using vertex and fragment shaders, while the compute shaders process "
                                   + "the TimeQuarks particles using complex algorithms based on the quaternions and matrices, wich are then applyied "
                                   + "on the original photo, given us the photo of what will happen in the future. Is not quite accurate because algorithms "
                                   + "use numeric variables that can accumulate numeric errors, but it works pretty good. I took a picture of a banana "
                                   + "and got a picture of a banana pie, and my mother really used that banana to bake a pie, and it turned out to be "
                                   + "a very delicious pie!"
                                   ),"scientistHead.png", 2, false)

    }

    function unicornFart() {
        talkBoard.x = mainRoot.width * screenNumber + mainRoot.width*0.5 - talkBoard.width/2
        talkBoard.y = mainRoot.height + defaultMargins
        talkBoard.clear()

        talkBoard.opacity = 1.0

        talkBoard.addMessage4(qsTr("Opps, I just farted 😯"),"unicornHorseHead.png", 1, false)
        talkBoard.addMessage4(qsTr("What 😊 ??? Ahahahah!"), "girlHead.png", 1, false)

        //talkBoard.addMessage4(qsTr("Opps, dei uma bufa 😯"),"unicornHorseHead.png", 1, false)
        //talkBoard.addMessage4(qsTr("O quê ??? Ahahahah! 😊"), "girlHead.png", 1, false)

        talkBoard.showDelay(500)
    }

    function dialogueAlienEndLevel() {
        talkBoard.x = mainRoot.width * screenNumber + mainRoot.width*0.5 - talkBoard.width/2
        talkBoard.y = mainRoot.height + defaultMargins
        talkBoard.clear()

        talkBoard.opacity = 1.0

        if (heroMap.bookOfHapinessUnlocked) {
            talkBoard.addMessage4(qsTr("Hi there. I see that you have the book of hapyness. You have finish all your tasks and you can read in the next level."
                                       +" Congratulations 😊!"),"robonoid2.png", 2, false)
            bookKeyUnlocked = true
        }
        else {
            talkBoard.addMessage4(qsTr("Hi there. You haven't finish all your tasks and can not pass the level yet."
                                       +" Keep exploring, brave hero 😟!"),"robonoid2.png", 2, false)
        }

    }

}
