import QtQuick 2.3

Item {
    id: root
    width: 45*mm //22*mm
    height: width

    property real commandX
    property real commandY

    //dont set explicit x,y
    //set centerX, centerY
    property real centerX
    property real centerY

    x: centerX-width*0.5
    y: centerY-height*0.5

    clip: true

    //bigger rectangle
    Rectangle {
        id: ret1
        anchors.centerIn: parent
        width: parent.width
        height: width
        radius: width*0.5
        border.width: mm
        border.color: "black"
        opacity: 0.6
        clip: true

        /*Rectangle {
            width: parent.width*0.5
            height: parent.height*0.5
            border.width: mm
            border.color: "black"
            opacity: 0.6
            clip: true
        }*/

        // small rectangle that moves with the touch movement
        Rectangle {
            id: ret2
            //x: commandX-ret2.width*0.5
            //y: commandY-ret2.height*0.5
            anchors.centerIn: parent
            width: parent.width*0.65
            height: width
            radius: width*0.5
            border.width: mm
            border.color: "black"
            opacity: 0.4
        }

    }



}
