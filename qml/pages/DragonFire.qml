import QtQuick 2.3
//import com.Timer 1.0

Item {
    id: root
    width: 7*mm
    height: 7*mm
    rotation: angle

    //bullet should be destroyed when is off the screen limits
    property real xMinBulletDestroy: 0
    property real xMaxBulletDestroy: mainRoot.width
    property real yMinBulletDestroy: 0
    property real yMaxBulletDestroy: mainRoot.height

    property alias running: timer.running
    property variant bulletTarget
    property real spaceLimitYmax: mainRoot.height

    property real hitDamage: 1


    signal hit(real damage)


    function init(xPos,yPos) {
        root.x = xPos
        root.y = yPos
    }

    function setAngle(xx, yy) {
        angle = Math.atan2(yy, xx)
        //console.log("xx: "+xx+" yy: " +yy +" angleRad: "+angle + " angleDed: "+ angle * 57.2957795131)
    }

    //vx,vy default direction vector
    //property real vx: 0
    property real velocity: 3.5*mm
    property real angle: 0

    function move() {
        //console.log("dragon move")
        x += velocity*Math.cos(angle)
        y += velocity*Math.sin(angle)
    }

    Timer {
        id: timer
        interval: fps_ms; running: false; repeat: true
        onTriggered: {
            move()


            if (bulletTarget){
                if ( lib.distance(bulletTarget.x+bulletTarget.width, bulletTarget.y+bulletTarget.height,
                              x, y, bulletTarget.width ) )
                {
                    hit(hitDamage)
                    timer.running = false
                    root.destroy() //destroy this bullet, it has hit the target
                }
            }


            /*if (root.x < xMinBulletDestroy || root.x > xMaxBulletDestroy
                    || root.y < yMinBulletDestroy || root.y > yMaxBulletDestroy) {
                timer.running = false
                root.destroy()
            }*/

        } //onTriggered
    }

    Image {
        id: dragonFireImg
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        rotation: angle * 57.2957795131
        source: "qrc:/images/dragonfire.png"
        mipmap: true
    }

}
