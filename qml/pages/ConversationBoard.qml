import QtQuick 2.3
import "components"

Rectangle {
    id: root
    width: mainRoot.width / 2
    height: mainRoot.height / 2
    color: "purple"
    radius: 5*mm

    signal endDialog()

   // property alias text: tex.label
    property var conversationArray: new Array() // message to be displayed
    property var imageNameArray: new Array() //file image name
    property var imagePostionArray: new Array() // left=1, right=2
    property var mirrorImageArray: new Array() // normal image= false, mirror image=true

    property int index: -1
    property real imgSize: 20*mm
    property string imageLeft: "heroHead.png"
    property string imageRight: "robonoid.png"
    property alias mirrorImageLeft: imgLeft.mirror
    property alias mirrorImageRight: imgRight.mirror
    property alias verticalAlignment: tex.verticalAlignment

    //function addMessage4(message, position, mirror) - 4 argument function
    //message: message to display
    //position: left = 1, right = 2
    //mirror: show image has is = false, show image horizontally inverted (mirrored) = true
    function addMessage4(conversation, imageName, imagePosition, mirror) {
        conversationArray.push(conversation)
        imageNameArray.push(imageName)
        imagePostionArray.push(imagePosition)
        mirrorImageArray.push(mirror)
    }


    function addMessage2(conversation, imageName) {
        conversationArray.push(conversation)
        imageNameArray.push(imageName)
        //add default images position left and right in default sequence left(1), right(2), left(1), right(2), ....
        imagePostionArray.push( (imagePostionArray.length % 2) ? 1 : 2)
        mirrorImageArray.push(false) // default
    }



    function addMessage(conversation) {
        conversationArray.push(conversation)
        //add default images names in imageLeft and imageRight in default sequence imageRight, imageRight, imageRight, imageRight, ...
        imageNameArray.push( (imageNameArray.length % 2) ? imageLeft : imageRight)
        //add default images position left and right in default sequence left(1), right(2), left(1), right(2), ....
        imagePostionArray.push( (imagePostionArray.length % 2) ? 1 : 2)
        mirrorImageArray.push(false) // default
    }


    function clear() {
        //root.visible = false
        conversationArray = []
        imageNameArray = []
        imagePostionArray = []
        mirrorImageArray = []
        index = -1
    }

    onEndDialog: {
        root.visible = false
    }

    onVisibleChanged: {
        if (visible) {
            index = -1
            _nextMessage()
        }
    }


    Image {
        id: imgLeft
        width: visible ? imgSize : 0//imageName === "" ? 0 : 20*mm
        height: width
        mipmap: true
        //anchors.centerIn: parent
        anchors.top: parent.top
        anchors.left: parent.left
        fillMode: Image.PreserveAspectFit
        source: visible ? "qrc:/images/"+imageNameArray[index] : "" //imageLeft === "" ? "" : "qrc:/images/"+imageLeft
        visible: imagePostionArray[index] === 1 ? true : false
        mirror: (index < 0 || index >= mirrorImageArray.length) ? false : mirrorImageArray[index]
    }

    Image {
        id: imgRight
        width: visible ? imgSize : 0
        height: width
        mipmap: true
        //anchors.centerIn: parent
        anchors.top: parent.top
        anchors.right: parent.right
        fillMode: Image.PreserveAspectFit
        source: visible ? "qrc:/images/"+imageNameArray[index] : "" //imageRight === "" ? "" : "qrc:/images/"+imageRight
        visible: imagePostionArray[index] === 2 ? true : false
        mirror: (index < 0 || index >= mirrorImageArray.length) ? false : mirrorImageArray[index]
    }

    TextTemplate {
        id: tex
        anchors.top: parent.top
        anchors.topMargin: defaultMargins//imgLeft.visible || imgRight.visible ? imgSize : defaultMargins

        anchors.bottom: btnNext.top
        anchors.bottomMargin: defaultMargins

        anchors.left: imgLeft.right //parent.left
        anchors.leftMargin: defaultMargins
        anchors.right: imgRight.left //parent.right
        anchors.rightMargin: defaultMargins
        verticalAlignment: Text.AlignTop
        //width: parent.width
        //label: "Olá..........."//dialogueArray[0]
        //pointSize: 16
        color: "orange"
        //label: dialogueArray.length === 0 ? "" : dialogueArray[index]
        label: (index < 0 || index >= conversationArray.length) ? "" : conversationArray[index]
    }

    MyButton {
        id: btnNext
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        anchors.horizontalCenter: parent.horizontalCenter
        backGrdColor: "yellow"
        text: ">>"
        onBtnClick: _nextMessage()
    }

    function _nextMessage() {
        index++

        if (index >= conversationArray.length) {
            endDialog()
        }
    }

}
