import QtQuick 2.0
//import com.Timer 1.0

Rectangle {
    id: root
    width: 2*mm
    height: width
    radius: width/2
    color: "yellow"
    visible: false
    property real yBulletDestroy: 0//bullet should be destroyed when is off the screen


    signal hit(real hx, real hy)

    property variant bulletTarget
    property real spaceLimitYmax: mainRoot.height



    function init(xPos,yPos) {
        root.x = xPos
        root.y = yPos
        visible = true
        timer.running = true
    }

    //vx,vy default direction vector
    //property real vx: 0
    property real yVelocity: 2*mm

    function move() {
        y += yVelocity
    }

    Timer {
        id: timer
        interval: fps_ms; running: false; repeat: true
        onTriggered: {
            move()


            if (bulletTarget){
                if ( lib.distance(bulletTarget.x+bulletTarget.width, bulletTarget.y+bulletTarget.height,
                              x, y, bulletTarget.width ) )
                {
                    if (!bulletTarget.shieldActivated)//if spaceship shield is active, the bullet will not hit the spaceship
                        hit(x, y)

                    //console.log("I was shot")
                    timer.running = false
                    root.destroy()
                }
            }


            if (root.y > yBulletDestroy) {
                //console.log("Bullet destroyed in Bullet.qml ...........")
                timer.running = false
                root.destroy()
            }

        } //onTriggered
    }

}
