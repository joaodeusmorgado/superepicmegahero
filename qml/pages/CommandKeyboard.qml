import QtQuick 2.0
//import com.Timer 1.0

Item {
    id: root
    //width: 1
    //height: 1
    focus: true

    property real velocityOnKeyPress: 2*mm

    property bool bAutoRepeat: false

    property bool canFire: false
    property bool key_Space: false

    property bool key_Left: false
    property bool key_Right: false
    property bool key_Up: false
    property bool key_Down: false

    property bool isPressed: false
    property bool isFlying: false

    property bool key_A: false
    property bool key_D: false
    property bool key_W: false
    property bool key_S: false
    property bool key_R: false
    property bool key_F: false

    property bool key_Y: false
    property bool key_H: false
    property bool key_U: false
    property bool key_J: false
    property bool key_I: false
    property bool key_K: false

    property bool key_4: false
    property bool key_6: false


    signal fly() //used for flying the hero
    signal walk() // used for walking the hero
    signal move(real cx, real cy) // used for moving the spaceship
    signal shotFired()
    signal startWalking()
    signal stopWalking()
    signal jump()
    //signal jumpLeft()
    //signal jumpRight()
    signal direction(int direct)// -1->left ; 1->right
    signal swordDefenseStart()
    signal swordDefenseStop()
    signal swordAttack()



    //property int key
    signal keyPressed(int key)
    signal keyIsPressed(int key)
    signal keyReleased(int key)


    signal keyPressed_A()
    signal keyIsPressed_A()
    signal keyReleased_A()
    signal keyPressed_D()
    signal keyIsPressed_D()
    signal keyReleased_D()
    signal keyPressed_W()
    signal keyIsPressed_W()
    signal keyReleased_W()
    signal keyPressed_S()
    signal keyIsPressed_S()
    signal keyReleased_S()
    signal keyPressed_R()
    signal keyIsPressed_R()
    signal keyReleased_R()
    signal keyPressed_F()
    signal keyIsPressed_F()
    signal keyReleased_F()


    signal keyPressed_Y()
    signal keyIsPressed_Y()
    signal keyReleased_Y()
    signal keyPressed_H()
    signal keyIsPressed_H()
    signal keyReleased_H()
    signal keyPressed_U()
    signal keyIsPressed_U()
    signal keyReleased_U()
    signal keyPressed_J()
    signal keyIsPressed_J()
    signal keyReleased_J()
    signal keyPressed_I()
    signal keyIsPressed_I()
    signal keyReleased_I()
    signal keyPressed_K()
    signal keyIsPressed_K()
    signal keyReleased_K()

    signal keyPressed_Right()
    signal keyIsPressed_Right()
    signal keyReleased_Right()
    signal keyPressed_Left()
    signal keyIsPressed_Left()
    signal keyReleased_Left()
    signal keyPressed_Up()
    signal keyIsPressed_Up()
    signal keyReleased_Up()
    signal keyPressed_Down()
    signal keyIsPressed_Down()
    signal keyReleased_Down()

    signal keyPressed_4()
    signal keyIsPressed_4()
    signal keyReleased_4()
    signal keyPressed_6()
    signal keyIsPressed_6()
    signal keyReleased_6()

    signal keyPressed_Space()
    signal keyIsPressed_Space()
    signal keyReleased_Space()


    signal startFlying()
    signal stopFlying()

    onKeyPressed_F: startFlying()
    onKeyReleased_F: stopFlying()

    onStartFlying: isFlying = true
    onStopFlying: isFlying = false


    Keys.onPressed: {
        isPressed = true
        //console.log("key pressed")
        keyPressed(event.key)

        if (event.isAutoRepeat && !bAutoRepeat)
            return;

        //console.log("key pressed")

        //---shot fire-----------------------------
        //if (event.key === Qt.Key_Space) { canFire = true; key_Space = true;event.accepted = true; }
        if (event.key === Qt.Key_Space) {keyPressed_Space(); canFire = true; key_Space = true }

        if (event.key === Qt.Key_6) { keyPressed_6(); key_6 = true; event.accepted = true }
        if (event.key === Qt.Key_4) { keyPressed_4(); key_4 = true; event.accepted = true }

        if (event.key === Qt.Key_Right) { direction(1); startWalking(); keyPressed_Right(); key_Right = true }
        if (event.key === Qt.Key_Left) { direction(-1); startWalking(); keyPressed_Left(); key_Left = true }
        if (event.key === Qt.Key_Up) { keyPressed_Up(); key_Up = true }
        if (event.key === Qt.Key_Down) { keyPressed_Down(); key_Down = true }


        if (event.key === Qt.Key_Shift){ swordDefenseStart(); }

        if (event.key === Qt.Key_F){ keyPressed_F(); key_F = true }
        if (event.key === Qt.Key_A){ keyPressed_A(); key_A = true }
        if (event.key === Qt.Key_D){ keyPressed_D(); key_D = true }
        if (event.key === Qt.Key_W){ keyPressed_W(); key_W = true }
        if (event.key === Qt.Key_S){ keyPressed_S(); key_S = true }
        if (event.key === Qt.Key_R){ keyPressed_R(); key_R = true }

        if (event.key === Qt.Key_Y){ keyPressed_Y(); key_Y = true }
        if (event.key === Qt.Key_H){ keyPressed_H(); key_H = true }
        if (event.key === Qt.Key_U){ keyPressed_U(); key_U = true }
        if (event.key === Qt.Key_J){ keyPressed_J(); key_J = true }
        if (event.key === Qt.Key_I){ keyPressed_I(); key_I = true }
        if (event.key === Qt.Key_K){ keyPressed_K(); key_K = true }


        if (event.key === Qt.Key_Control){ swordAttack() }

    } //Keys.onPressed

    Keys.onReleased: {
        isPressed = false

        keyReleased(event.key)

        if (event.isAutoRepeat && !bAutoRepeat)
            return;

        if (event.key === Qt.Key_Space) { keyReleased_Space(); canFire = false; key_Space = false }

        if (event.key === Qt.Key_6) { keyReleased_6(); key_6 = false}
        if (event.key === Qt.Key_4) { keyReleased_4(); key_4 = false }

        if (event.key === Qt.Key_Right) { keyReleased_Right(); key_Right = false; stopWalking() }
        if (event.key === Qt.Key_Left) { keyReleased_Left(); key_Left = false; stopWalking() }
        if (event.key === Qt.Key_Up) {keyReleased_Up(); key_Up = false }
        if (event.key === Qt.Key_Down) {keyReleased_Down(); key_Down = false }

        if (event.key === Qt.Key_Shift){
            swordDefenseStop()
            return;
        }

        if (event.key === Qt.Key_F){ keyReleased_F(); key_F = false }
        if (event.key === Qt.Key_A){ keyReleased_A(); key_A = false }
        if (event.key === Qt.Key_D){ keyReleased_D(); key_D = false }
        if (event.key === Qt.Key_W){ keyReleased_W(); key_W = false }
        if (event.key === Qt.Key_S){ keyReleased_S(); key_S = false }
        if (event.key === Qt.Key_R){ keyReleased_R(); key_R = false }

        if (event.key === Qt.Key_Y){ keyReleased_Y(); key_Y = false }
        if (event.key === Qt.Key_H){ keyReleased_H(); key_H = false }
        if (event.key === Qt.Key_U){ keyReleased_U(); key_U = false }
        if (event.key === Qt.Key_J){ keyReleased_J(); key_J = false }
        if (event.key === Qt.Key_I){ keyReleased_I(); key_I = false }
        if (event.key === Qt.Key_K){ keyReleased_K(); key_K = false }

        //console.log("release:"+event.key)
       // event.accepted = true;
    } //Keys.onReleased


    Timer {
        id: timer
        interval: fps_ms
        running: isPressed = true
        repeat: true
        onTriggered: {

            if (key_Space) {
                keyIsPressed_Space()
                if (canFire) {
                  shotFired()
                  canFire = false
                }
                jump()
            }

            if (key_Left) { keyIsPressed_Left(); walk(); move(-velocityOnKeyPress,0) }
            if (key_Right) { keyIsPressed_Right(); walk(); move(velocityOnKeyPress,0) }
            if (key_Up) { keyIsPressed_Up(); move(0,velocityOnKeyPress); jump()}
            if (key_Down) { keyIsPressed_Down(); move(0, -velocityOnKeyPress) }

            if (isFlying) { fly() }

            if ( key_4) { keyIsPressed_4() }
            if ( key_6) { keyIsPressed_6() }

            if ( key_A) { keyIsPressed_A() }
            if ( key_D) { keyIsPressed_D() }
            if ( key_W) { keyIsPressed_W() }
            if ( key_S) { keyIsPressed_S() }
            if ( key_R) { keyIsPressed_R() }
            if ( key_F) { keyIsPressed_F() }

            if ( key_Y) { keyIsPressed_Y() }
            if ( key_H) { keyIsPressed_H() }
            if ( key_U) { keyIsPressed_U() }
            if ( key_J) { keyIsPressed_J() }
            if ( key_I) { keyIsPressed_I() }
            if ( key_K) { keyIsPressed_K() }
         }
    }//Timer
}
