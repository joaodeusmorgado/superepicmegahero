import QtQuick 2.0


Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "grey"

    Flickable {
        id: flick

        width: mainRoot.width
        height: mainRoot.height
        contentWidth: denContent.width
        contentHeight: denContent.height
        //interactive: false

        DenContent {
            id: denContent
            width: mainRoot.width * 3
            height: mainRoot.height
        }

    }//Flickable



}
