import QtQuick 2.0
import QtQuick.Window 2.2

//import Qt.labs.settings 1.0 //for android OS
import Nemo.Configuration 1.0 //for sailfish OS
import "level01"
import "level02"
import "level03"
import "level04"
import "level05"
import "level06"
import "level07"
import "level08"
import "level09"
import "level10"
import "levelSettings"

Item {
    id: mainRoot

    //property string rootLocale: "EN" // "PT"

    //property real calibrationFactor: Qt.platform.os === "android" ? 0.4 : 0.8
    property real calibrationFactor: 0.4 //sailfish OS

    property real mm: Screen.pixelDensity * calibrationFactor
    //intex aqua fish: 294 dpi = 11.58 dots per milimeter
    //inch = 25.4mm
    //property real mm: 11.58 * calibrationFactor // for sailfish OS

    /*property real btnSize: 20 * mm
    property real defaultMargins: 2 * mm
    property int fps: 60
    property int fps_ms: 1000/fps*/


    ConfigurationGroup {
        id: settings
        path: "/apps/harbour-superepicmegahero"
    }

    Component.onCompleted: {
        levels.money = settings.value("money", 0)
        levels.moneyUnlocked = settings.value("moneyUnlocked", false)
        levels.fuelCan = settings.value("fuelCan", false)
        levels.fuel = settings.value("fuel", 0)
        levels.milk = settings.value("milk", false)
        levels.fruit = settings.value("fruit", false)
        levels.bowAndArrowUnlocked = settings.value("bowAndArrowUnlocked", false)
        levels.starKeyUnlocked = settings.value("starKeyUnlocked", false)
        levels.swordUnlocked = settings.value("swordUnlocked", false)
        levels.carrotsUnlocked = settings.value("carrotsUnlocked", false)
        levels.waterPowderUnlocked = settings.value("waterPowderUnlocked", false)
        levels.treasureBlueDiamondsUnlocked = settings.value("treasureBlueDiamondsUnlocked", false)
        levels.bookOfHapinessUnlocked = settings.value("bookOfHapinessUnlocked", false)
        levels.releaseMode ? levels.levelUnlocked = settings.value("levelUnlocked", false)
                           : levels.levelUnlocked = 100
        levels.girlIsReleased = settings.value("girlIsReleased", false)
        levels.denWallDestroyed = settings.value("denWallDestroyed", false)
    }

    Component.onDestruction: {
        settings.setValue("money", levels.money)
        settings.setValue("moneyUnlocked", levels.moneyUnlocked)
        settings.setValue("fuelCan", levels.fuelCan)
        settings.setValue("fuel", levels.fuel)
        settings.setValue("milk", levels.milk)
        settings.setValue("fruit", levels.fruit)
        settings.setValue("bowAndArrowUnlocked", levels.bowAndArrowUnlocked)
        settings.setValue("starKeyUnlocked", levels.starKeyUnlocked)
        settings.setValue("swordUnlocked", levels.swordUnlocked)
        settings.setValue("carrotsUnlocked", levels.carrotsUnlocked)
        settings.setValue("waterPowderUnlocked", levels.waterPowderUnlocked)
        settings.setValue("treasureBlueDiamondsUnlocked", levels.treasureBlueDiamondsUnlocked)
        settings.setValue("bookOfHapinessUnlocked", levels.bookOfHapinessUnlocked)
        levels.releaseMode ? settings.setValue("levelUnlocked", levels.levelUnlocked)
                           : levels.levelUnlocked = 100
        settings.setValue("girlIsReleased", levels.girlIsReleased)
        settings.setValue("denWallDestroyed", levels.denWallDestroyed)
    }

    LevelStates {
        id: levels
        anchors.fill: parent
    }
}
