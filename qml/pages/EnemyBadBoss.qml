import QtQuick 2.0
//import com.Timer 1.0

EnemyTemplate {
    id: root

    width: 60*mm
    height: width * 428/500
    imageName: "enemyBoss.png"
    lifePower: 10

    property real groundPositionY
    property real jumpHeight: 38*mm
    property real jumpWidth: 25*mm
    property real jumpTimeDuration: 800

    property real xx: enemyTarget === null ? 0 : enemyTarget.x
    property real yy: enemyTarget === null ? 0 : enemyTarget.y

    onXxChanged: checkEnemyHeroCollision()
    onYyChanged: checkEnemyHeroCollision()


    timerMovementInterval: 3000
    //timerMovement.running: false

    signal fire(real fx, real fy)

    //onInMotionChanged: console.log("BadBoss inMotion: "+inMotion)

    onMove: {
        jumpWidth *= -1
        jumpAnimation.start()
    }

    ParallelAnimation {
        id: jumpAnimation
        alwaysRunToEnd: false

        // rise and fall
        SequentialAnimation {
            //start raising
            NumberAnimation {
                target: root; property: "y";
                //from: jumpStartY; to: jumpEndY;
                from: groundPositionY; to: groundPositionY-jumpHeight;
                duration: jumpTimeDuration/2
                easing {  type: Easing.OutQuad }
            }

            ScriptAction { script: fire(x+width*0.5, y+height*0.5); }

            //start falling
            NumberAnimation {
                target: root; property: "y";
                from: groundPositionY-jumpHeight; to: groundPositionY;
                duration: jumpTimeDuration/2
                easing { type: Easing.InQuad }
            }
        }//SequentialAnimation

        NumberAnimation {
            target: root; property: "x";
            from: x; to: x+jumpWidth;
            duration: jumpTimeDuration
            //easing { type: Easing.InQuad }
        }

    }//ParallelAnimation - jump
    //--jump----------------------------------------

}
