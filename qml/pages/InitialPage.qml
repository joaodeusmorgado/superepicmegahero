import QtQuick 2.3
import "components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    //color: "AntiqueWhite"
    color: "blue"


    signal destroyStars()
    onVisibleChanged: {

        if (visible) {

            //create random stars-------------------------------
            for (var i = 0; i < 30;i++) {
                //console.log(Math.random()*root.width + ":"+Math.random()*root.height)
                var stars = lib.createItem("Star01.qml", parent)
                //stars.x = Math.random()*root.width
                //stars.y = Math.random()*root.height
                stars.x = Qt.binding( function(){ return Math.random()*root.width } )
                stars.y = Qt.binding( function(){ return Math.random()*root.height } )
                stars.z = 0
                destroyStars.connect( stars.destroyItem )
            }//create random stars-------------------------------

        }
        else {
            destroyStars()
        }
    }

    Text {
        id: title
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: defaultMargins
        horizontalAlignment: Text.AlignHCenter
        font.bold: false
        font.family: "Purisa"
        font.pixelSize: 12*mm
        wrapMode: Text.WordWrap
        fontSizeMode: Text.Fit
        minimumPointSize: 2
        color: "orange"
        text: qsTr("Super Epic Mega Hero")
        z: 1
    }

    Image {
        id: imageStarship
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: title.bottom
        anchors.margins: defaultMargins * 6
        width: 40*mm
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/nave.png"
        mipmap: true
        z: 1
    }

    signal play()
    MyButton {
        id: btnPlay
        width: mainRoot.width/4 < 40*mm ? mainRoot.width/4 : 40*mm
        anchors.centerIn: parent
        //anchors.horizontalCenter: parent.horizontalCenter
        //anchors.verticalCenter: parent.verticalCenter
        //y: (parent.height - (imageStarship.y+imageStarship.height))/2 + imageStarship.y+imageStarship.height

        //anchors.bottom: parent.bottom
        //anchors.bottomMargin: defaultMargins*4

        //anchors.left: parent.left
        //anchors.leftMargin: width/2

        text: qsTr("Start")// : "Jogar"  //qsTr("Jogar")
        onBtnClick: play()
        z: 1
    }

}
