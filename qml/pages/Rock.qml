import QtQuick 2.0

EnemyTemplate {
    id: rock
    width: 10*mm
    height: width

    property string rockNumber: "1"
    imageName: "rocks0"+rockNumber+".png"

    Component.onCompleted: {
        rockNumber = lib.getRandom(1, 5).toString()
    }
}
