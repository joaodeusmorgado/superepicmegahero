import QtQuick 2.3
import QtQuick.Particles 2.0
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "darkblue"

    property bool starShipIsBinding: false
    property real meteorSize: parent.height*0.3
    property real velocityX: 2*mm

    property real levelDuration: 30000 //30 seconds
    property real seconds2End: levelDuration/1000
    property real time2VanishAsteroids: 3 //last second where the asteroid will no be move to the beginning
    property real delta: meteorSize*0.6 //todo: do more testings on the size

    signal menu()
    signal nextLevel()
    signal levelFailed()
    signal levelWin()

    onLevelFailed: {
        levelEndCleanup()
        panelEOL.text = qsTr("You lost.")
        panelEOL.visible = true
    }

    onLevelWin: {
        levels.levelUnlocked = levels.levelUnlocked === 7 ? 8 : levels.levelUnlocked
        levelEndCleanup()
        panelEOL.text = qsTr("Victory!")
        //panelEOL.showNextBtn = true
        panelEOL.visible = true
    }

    onVisibleChanged: {
        if (visible) {
            levelStartSetup()
        }
        else {
            levelEndCleanup()
        }
    }

    CommandTilt {
        id: commandTilt
        active: parent.visible && Qt.platform.os === "android"
    }

    CommandRotation {
        id: commandRotation
        active: parent.visible && Qt.platform.os === "linux" //for sailfish OS
    }

    Starship {
        id: starShip
        rotation: 90
        y: parent.height/2 //initial position vertically centered
        //Behavior on x { SmoothedAnimation { velocity: 50 } }
        /*Behavior on y {
            SmoothedAnimation { //reversingMode: SmoothedAnimation.Sync
                velocity: 200
                //duration: 3000
            }
        }*/

        spaceLimitXmin: 0
        spaceLimitXmax: parent.width// - starshipObj.width
        spaceLimitYmin: 0
        spaceLimitYmax: parent.height //- starshipObj.height
    }

    //starship fire particles---------------------------------------
    ParticleSystem {
        id: fireParticleSystem
    }

    Emitter {
        id: fireEmitter
        width: 2*mm; height: width
        x: starShip.x
        y: starShip.y+starShip.height/2

        system: fireParticleSystem
        emitRate: 30; lifeSpan: 800; lifeSpanVariation: 300
        size: 4*mm
        endSize: size*0.75
        velocity: AngleDirection {
                    angle: 180
                    angleVariation: 20
                    magnitude: starShip.width //8*mm
                    magnitudeVariation: mm
                }
    }

    ImageParticle {
        //source: "qrc:/images/fireParticle2.png"
        source: "qrc:/images/Explosion.png"
        system: fireParticleSystem
        color: "darkgrey" //"#FFD700"
        colorVariation: 0.2
        entryEffect: ImageParticle.Scale
    }
    //starship fire particles---------------------------------------


    Image {
        id: meteor1
        width: meteorSize
        height: width
        x: parent.width*1.3
        y: 0
        source: "qrc:/images/astroid01.png"
        mipmap: true
    }

    Image {
        id: meteor1_1
        width: meteorSize
        height: width
        x: meteor1.x + parent.width/2
        y: 0
        source: "qrc:/images/astroid03.png"
        mipmap: true
    }


    Image {
        id: meteor2
        width: meteorSize
        height: width
        x: parent.width + parent.width*2/3 //parent.width/2 //parent.width-meteor2.width
        y: parent.height/2-meteor2.height/2
        source: "qrc:/images/astroid02.png"
        mipmap: true
    }

    Image {
        id: meteor2_1
        width: meteorSize
        height: width
        x: meteor2.x + parent.width/2 //parent.width + parent.width*2/3 //parent.width/2 //parent.width-meteor2.width
        y: parent.height/2-meteor2.height/2
        source: "qrc:/images/astroid01.png"
        mipmap: true
    }


    Image {
        id: meteor3
        width: meteorSize
        height: width
        x: parent.width*2
        y: parent.height-meteor3.height
        source: "qrc:/images/astroid03.png"
        mipmap: true
    }


    Image {
        id: meteor3_1
        width: meteorSize
        height: width
        x: meteor3.x + parent.width/2 //parent.width*2 - 3*starShip.width
        y: parent.height-meteor3.height
        source: "qrc:/images/astroid02.png"
        mipmap: true
    }

    //time left board
    ScoreBoard {
        id: time_left
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: defaultMargins
        label: qsTr("Time left")
        score: seconds2End
    }



    Timer {
        id: timerGameDuration
        interval: 1000 // 1 second
        running: seconds2End > 0 && parent.visible
        repeat: false
        onTriggered: {
            seconds2End -= 1

            if (seconds2End === 1)
                levelWin()
        }
    }



    Timer {
        id: timerMeteor
        interval: fps_ms
        running: timerGameDuration.running && parent.visible
        repeat: true
        onTriggered: {

            meteor1.x -= velocityX
            meteor1_1.x -= velocityX
            meteor2.x -= velocityX
            meteor2_1.x -= velocityX
            meteor3.x -= velocityX
            meteor3_1.x -= velocityX


            //move asteroids--------------
            if (meteor1.x < -meteor1.width && seconds2End > time2VanishAsteroids)
                meteor1.x = parent.width

            if (meteor1_1.x < -meteor1_1.width && seconds2End > time2VanishAsteroids)
                meteor1_1.x = parent.width

            if (meteor2.x < -meteor2.width && seconds2End > time2VanishAsteroids)
                meteor2.x = parent.width

            if (meteor2_1.x < -meteor2_1.width && seconds2End > time2VanishAsteroids)
                meteor2_1.x = parent.width

            if (meteor3.x < -meteor3.width && seconds2End > time2VanishAsteroids)
                meteor3.x = parent.width

            if (meteor3_1.x < -meteor3_1.width && seconds2End > time2VanishAsteroids)
                meteor3_1.x = parent.width
            //move asteroids--------------




            //check asteroids collision-----------------------------
            if (lib.distance(starShip.x+starShip.width*0.5, starShip.y+starShip.height*0.5,
                     meteor1.x+meteor1.width*0.5, meteor1.y+meteor1.height*0.5, delta)) {
                //console.log("meteor1 crash..............................")
                levelFailed()
            }


            if (lib.distance(starShip.x+starShip.width*0.5, starShip.y+starShip.height*0.5,
                     meteor1_1.x+meteor1_1.width*0.5, meteor1_1.y+meteor1_1.height*0.5, delta)) {
                //console.log("meteor1_1 crash..............................")
                levelFailed()
            }

            if (lib.distance(starShip.x+starShip.width*0.5, starShip.y+starShip.height*0.5,
                     meteor2.x+meteor2.width*0.5, meteor2.y+meteor2.height*0.5, delta)) {
                //console.log("meteor2 crash..............................")
                levelFailed()
            }

            if (lib.distance(starShip.x+starShip.width*0.5, starShip.y+starShip.height*0.5,
                     meteor2_1.x+meteor2_1.width*0.5, meteor2_1.y+meteor2_1.height*0.5, delta)) {
                //console.log("meteor2_1 crash..............................")
                levelFailed()
            }

            if (lib.distance(starShip.x+starShip.width*0.5, starShip.y+starShip.height*0.5,
                     meteor3.x+meteor3.width*0.5, meteor3.y+meteor3.height*0.5, delta)) {
                //console.log("meteor3 crash..............................")
                levelFailed()
            }

            if (lib.distance(starShip.x+starShip.width*0.5, starShip.y+starShip.height*0.5,
                    meteor3_1.x+meteor3_1.width*0.5, meteor3_1.y+meteor3_1.height*0.5, delta)) {
                //console.log("meteor3_1 crash..............................")
                levelFailed()
            }
            //check asteroids collision-----------------------------
        }
    }

    /*MouseArea {
        id: ma
        anchors.fill: parent
        onPressed: {

            if (starShipIsBinding === false) {
                //Note: this if() is a small hack to delay binding until the first screen touch
                //because we want the starship to start vertically centered, not in y=0 position
                starShip.y = Qt.binding( function(){ return ma.mouseY - starShip.height/2 } )
                starShipIsBinding = true
            }
        }
        onMouseYChanged: {
            console.log(mouseY)
        }

        drag.target: starShip
        drag.axis: Drag.XAxis
        drag.minimumX: 0
        drag.maximumX: parent.width*0.3

    }*/

    MenuPopUpVertical {
        id: menuPopUp
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }


    PanelEndOfLevel2 {
        id: panelEOL
        visible: false
        anchors.centerIn: parent
        showNextBtn: levels.levelUnlocked > 7
        onMenu: parent.menu()
        onReplay: {
            levelStartSetup()
        }
        onNext: nextLevel()
    }


    function levelStartSetup() {
        panelEOL.visible = false

        starShip.x = 0
        starShip.y = parent.height*0.5
        commandKeyb.move.connect( starShip.move ) //keyboard move

        if (Qt.platform.os === "android")
            commandTilt.move.connect( starShip.move ) //tilt move
        else
            commandRotation.move.connect( starShip.move )

        seconds2End = levelDuration/1000

        meteor1.x = parent.width*1.3 + parent.width
        meteor1_1.x = meteor1.x + parent.width/2
        meteor2.x = parent.width*2 + parent.width*2/3
        meteor2_1.x = meteor2.x + parent.width/2
        meteor3.x = parent.width*3
        meteor3_1.x = meteor3.x + parent.width/2
    }

    function levelEndCleanup() {
        commandKeyb.move.disconnect( starShip.move ) //keyboard move

        if (Qt.platform.os === "android")
            commandTilt.move.disconnect( starShip.move ) //tilt move
        else
            commandRotation.move.disconnect( starShip.move )

        seconds2End = 0
    }


}
