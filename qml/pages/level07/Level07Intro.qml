import QtQuick 2.0
import "../"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    signal close()

    ConversationBoard2 {
        id: talkBoard
        anchors.top: parent.top
        anchors.bottom: btnClose.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: defaultMargins
        width: parent.width - btnClose.width*2 - 4*defaultMargins

        //imageBottomWidth: 80*mm
        //imageBottomHeight: 80*mm

        imageBottomWidth: width*0.6
        imageBottomHeight: height*0.6

        visible: false

        onEndDialog: close()
    }

    MyButton {
        id: btnClose
        //width: btnSize * 1.25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onBtnClick: close()
        text: qsTr("Skip")
    }

    onVisibleChanged: {
        if (visible) {
            addTalk()
            talkBoard.visible = true
        }
    }

    function addTalk() {
        talkBoard.clear()
        talkBoard.addMessage5(qsTr("Captain and Pandora successfully arrived at the end of the desert. They rest for a while before continuing and "
                                   +"captain tells Pandora a story when he was travelling in his spaceship and had to cross a tremendous asteroid field."),
                              "", -1, false, "heroHelmet1.png")

    }

}
