import QtQuick 2.0

EnemyTemplate {
    id: balloon
    width: 24*mm
    height: 26*mm
    imageName: "balloon03.png"

    lifePower: 1

    active: y > -height && y < mainRoot.height
    inMotion: y > -height
    //alive: y > 0// -height

    property real dy: mm
    onMove: {
        y -= dy

        if (y <= -height)
            prepareDestruction()
    }

    //onAddToEnemiesList: console.log("balloon add to enemies list...."+enemy)
    //onRemoveFromEnemiesList2: console.log("balloon remove from enemies list...."+enemy)
}
