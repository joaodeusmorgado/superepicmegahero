import QtQuick 2.0

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    Text {
        id: name
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        font.bold: false
        font.family: "Purisa"
        font.pixelSize: 8*mm
        color: "black"
        text: qsTr(
                  "Este nivel não existe :)"
                  +"\n\nJogar muitas horas é cansativo, é melhor fazer uma pausa."
                  +"\n\nSugestão: podes ir brincar ao ar livre com os amigos, andar de bicicleta,"
                  +"\n\nou brincar com papagaio, comer um gelado, ou ler um bom livro."
                  +"\n\nDiverte-te e sê feliz!"
                  )
    }

    signal close()
    MouseArea {
        anchors.fill: parent
        onClicked: close()
    }

}
