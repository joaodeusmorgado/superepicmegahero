import QtQuick 2.3
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    signal goSavePhones()

    //Sky
    Rectangle {
        id: sky
        width: parent.width
        height: parent.height / 2
        anchors.top: parent.top
        color: "aliceblue"
    }

    //ocean
    Rectangle {
        id: sea
        width: parent.width
        height: parent.height / 2
        anchors.bottom: parent.bottom
        //color: "#aadbc7"
        color: "aqua" //"lightblue"
    }

    //Sun
    Image {
        id: sun
        width: 30*mm
        height: width
        source: "qrc:/images/sun.png"
        anchors.top: parent.top
        anchors.right: parent.right
        mipmap: true
    }

    //boat
    Image {
        id:boat
        width: 30*mm
        height: width * 92/88
        x: parent.width/2 - boat.width/2
        y: parent.height - sea.height/2 - boat.height/2

        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/boat_Jola.png"
        mipmap: true
    }

    MessageBoard {
        id: msg
        width: mainRoot.width * 0.6
        height: mainRoot.height * 0.6//0.5
        x: parent.width * 0.5 - msg.width/2
        anchors.top: parent.top
        anchors.topMargin: defaultMargins
        horizontalAlignment: Text.AlignJustify
        text: qsTr("Our hero sails throught the ocean.\nIs mission: arrive at the burning Konia factory. "
                   +"Move the boat swiping your finger, try to catch the falling Meeg phones."
                   +"\nGet ready, go!!!")
        onOk: goSavePhones()
    }
}
