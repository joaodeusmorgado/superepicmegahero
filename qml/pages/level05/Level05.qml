import QtQuick 2.0
import "../"
//import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "grey"

    property var heroObj: null
    property int screenNumber: Math.floor(heroObj.x / flickLevel.width)


    signal menu()
    signal nextLevel()
    signal goSavePhones()
    onGoSavePhones: console.log("go save phones----------------------")
    //signal levelKoniaIntro()

    CommandMultiTouchPad2 {
       id: commandTouchPad
       anchors.left: parent.left
       anchors.bottom: parent.bottom
       anchors.margins: defaultMargins
    }

    Flickable {
        id: flickLevel

        width: mainRoot.width
        height: mainRoot.height
        contentWidth: levelContent.width
        contentHeight: levelContent.height
        interactive: false
        contentX: Math.floor(heroObj.x / flickLevel.width) * flickLevel.width

        Level05Content {
            id: levelContent
            width: mainRoot.width * 2
            height: mainRoot.height// * 1
            onSailOnBoat: goSavePhones()
        }
    }//Flickable


    TouchPad2 {
        id: touchPad
        centerX: commandTouchPad.touchPad.centerX
        centerY: commandTouchPad.touchPad.centerY
        visible: commandTouchPad.touchPad.visible
    }

    MenuPopUpVertical {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }
}
