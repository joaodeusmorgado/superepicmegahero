import QtQuick 2.3
import "../"
import "../components"
import "../libraries"

Rectangle {
    id: root
    //width: mainRoot.width
    //height: mainRoot.height
    color: "lightcyan"

    signal sailOnBoat()

    //onSailOnBoat: console.log("sail on boat.............................")

    Component.onCompleted: {
        heroObj = hero
    }

    onVisibleChanged: {
        if (visible) {
            levelStartSetup()
        }
        else {
            levelEndCleanup()
        }
    }

    Rectangle {
        id: grass
        width: mainRoot.width * 0.7
        height: mainRoot.height * 0.3
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        color: "lime"
    }

    Image {
        id: sand
        height: parent.height * 0.3
        //width: height
        anchors.left: grass.right
        anchors.right: ocean.left
        anchors.bottom: parent.bottom
        //x: parent.width - width
        //y: parent.height - height
        fillMode: Image.Tile
        source: "qrc:/images/sand.png"
        mipmap: true
        mirror: true
    }

    Rectangle {
        id: ocean
        width: mainRoot.width * 0.15
        height: parent.height * 0.3
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        color: "aqua"
    }


    PersonCharacter {
        id: appleGuy
        x: mainRoot.width * 1.5
        y: grass.y - height/2
        imgName: "robonoid2.png"
    }

    Person {
        id: hero
        width: 18*mm
        height: width * 320/156
        x: 0
        y: grass.y-hero.height/2

        imgName: "hero"

        worldLimitLeft: 0
        worldLimitRight: parent.width-hero.width
        worldLimitSky: 0
        worldLimitGround: grass.y-hero.height/2

        function reset(){
            x = 0//mainRoot.width * 4
            y = grass.y-hero.height/2
            hero.dir = 1

            hero.worldLimitLeft = 0
            hero.worldLimitRight = parent.width-hero.width
            hero.worldLimitSky = 0
            hero.worldLimitGround = grass.y-hero.height/2
        }
    }

    Person {
        id: girl
        imgName: "girl"

        //width: 18*mm
        //height: width * 320/156
        x: 0
        y: grass.y-girl.height/2

        worldLimitLeft: 0
        worldLimitRight: parent.width-girl.width
        worldLimitSky: 0
        worldLimitGround: grass.y-girl.height/2

        function reset(){
            x = 0//mainRoot.width * 4
            y = grass.y-girl.height/2
            girl.dir = 1

            girl.worldLimitLeft = 0
            girl.worldLimitRight = parent.width-girl.width
            girl.worldLimitSky = 0
            girl.worldLimitGround = grass.y-girl.height/2
        }


    }


    InterfaceWalkPartner {
        id: interfaceWalkGirlHero
        leader: hero
        follower: girl
        //call function connect() when the hero saves the girl from the cage
        // that will make the girl walk, following the hero
    }

    CheckPersonNear {
        id: checkHeroRobonoidDistance
        me: appleGuy
        target: hero
        distanceNear: hero.height/2//hero.width/2+hole.width/2
        onTargetChangedNear: conversation.visible = true
        onTargetChangedFar: conversation.visible = false
        isEnabled: screenNumber === 1
        debugInfo: "hero robonoid level06"
    }

    CheckPersonNear {
        id: checkHeroBoatDistance
        me: boat
        target: hero
        distanceNear: hero.height
        onTargetChangedNear: sailOnBoat()//change level in the sea
        isEnabled: screenNumber === 1 && heroMap.fruit
        debugInfo: "hero boat level05"
    }

    //boat
    Image {
        id: boat
        width: height * 88/92 //88*96
        height: 70*mm
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: mainRoot.height * 0.15
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/boat_Jola.png"
        mipmap: true
    }

    ConversationBoard2 {
        id: conversation
        width: mainRoot.width * 0.6
        height: mainRoot.height * 0.6//0.5
        //anchors.horizontalCenter: parent.horizontalCenter
        x: mainRoot.width * 1.5 - conversation.width/2
        anchors.top: parent.top
        anchors.topMargin: defaultMargins
        visible: false
    }

    function createConversation() {
        conversation.clear()

        conversation.imageLeft = "heroHead.png"
        conversation.imageRight = "robonoid2.png"

        conversation.addMessage4(qsTr("Hi, I'm hungry, do you have any fruit for me?"), "robonoid2.png", 2, false)

        if (levels.fruit) { //hero has fruit
            conversation.addMessage4(qsTr("I have a fruit I caught early, it fall from a tree! Take it!"), "heroHead.png", 1, false)

            // burning plataform story... :)
            conversation.addMessage4(qsTr("Delicious, thank you.\nDid you know people live sad because they can't comunicate properly?")
                                     , "robonoid2.png", 2, false)

            conversation.addMessage4(qsTr("We had great phones called Meeg, made in our Konia factory in the midle of the ocean. "
                                  +"Untill a mad man set a factory on fire, like a burning plataform "
                                  +"and now phones are falling into the ocean because of the explosions and fire.")
                                  , "robonoid2.png", 2, false)

            conversation.addMessage4(qsTr("Take my boat and please help us. Try to save the Meeg phones moving the boat to catch them. "
                                   +" Hurry and sail away!!!")
                                  , "robonoid2.png", 2, false)

            conversation.addMessage4(qsTr("Ok, I will sail on!"), "heroHead.png", 1, false)
        }
        else {
            conversation.addMessage4(qsTr("Sorry I have nothing!\nCan I sail on your boat?"), "heroHead.png", 1, false)
            conversation.addMessage4(qsTr("No you can not."), "robonoid2.png", 2, false)
        }

    }

    function levelStartSetup() {
        hero.reset()
        girl.reset()
        interfaceWalkGirlHero.connect()
        lib.connectCommandWalk(commandTouchPad, hero)
        lib.connectCommandWalk(commandKeyb, hero)
        createConversation()
    }

    function levelEndCleanup() {
        interfaceWalkGirlHero.disconnect()
        lib.disconnectCommandWalk(commandTouchPad, hero)
        lib.disconnectCommandWalk(commandKeyb, hero)
    }

}
