import QtQuick 2.0

Rectangle {
    id: phone
    width: 10*mm//12.5*mm
    height: width*1.5
    color: "#00ff00" // green

    property bool modelMego: true
    property color oceanMask: "aqua" //"lightblue"

    property int animationDuration: animationFallingDuration + animationDronwningDuration
    property int animationFallingDuration: 1000
    property int animationDronwningDuration: 800//500

    property bool animateFalling: false
    property bool animateDrowning: false

    property int finalPosition_X
    property int finalPosition_Y
    property int initialPosition_X
    property int initialPosition_Y

    Rectangle {
        width: parent.width * 0.75
        height: width*1.5
        anchors.centerIn: parent
        border.color: "black"//parent.color
        border.width: 0.5*mm

        Text {
            id: tex
            anchors.fill: parent
            anchors.centerIn: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: parent.width * 0.7
            text: modelMego ? qsTr("M") : qsTr("W")
        }
    }

    Rectangle {
        id: phoneDrowningSimulation
        width: parent.width
        height: 0
        anchors.bottom: parent.bottom
        color: oceanMask
    }

    //Animations----------------------------------------------------------------

    //phone start falling
    ParallelAnimation {
        id: phoneFallingAnimation
        running: animateFalling

        onStarted: {
            phone.visible = true
            console.log("phone start falling")
        }

        onStopped: {
            animateFalling = false
            animateDrowning = true
        }
            //isPhoneFalling = false

        NumberAnimation {
            target: phone; property: "x";
            from: initialPosition_X;  to: finalPosition_X ;
            duration: animationFallingDuration
        }

        NumberAnimation {
            target: phone; property: "y";
            from: initialPosition_Y; to: finalPosition_Y;
            duration: animationFallingDuration
            easing {
                type: Easing.InCubic
            }
        }
    }//phone stop falling


    //phone start drowning animation
    SequentialAnimation {
        id: phoneDrowningAnimation

        running: animateDrowning
        onStarted: console.log("phone start drowning")
        onStopped: {
            animateDrowning = false
            phoneDrowningSimulation.height = 0
            phone.visible = false
            console.log("end drowning")
        }

        //phone start drowning
        ParallelAnimation {
            NumberAnimation {
                target: phoneDrowningSimulation
                property: "height"
                from: 0;  to: phone.height;
                duration: animationDronwningDuration
            }
            NumberAnimation {
                target: phone
                property: "y"
                from: finalPosition_Y
                to: finalPosition_Y+phone.height
                duration: animationDronwningDuration
            }
        }//phone stop drowning

    }//phone stop drowning animation



    //We define the percentage probablility of being a Meego
    //percentageOfBeingMeego value should be between 0-1
    function setPhoneRandomModel2(percentageOfBeingMego)
    {
        var x = Math.random()
        if (x < percentageOfBeingMego)
            modelMego = true
        else
            modelMego = false
    }

    function setPhoneRandomModel()
    {
        //By default, we will make it 70% chances of being a Mego Phone, 30% being a W Phone
        if (Math.random() > 0.3)
            modelMego = true
        else
            modelMego = false
    }

    function setPhoneMeegoModel() {
        modelMego = true
    }


    function setPhoneRandomColor()
    {
        var phonecolor=Math.floor( Math.random() * 6 )

        switch(phonecolor) {
            case 0:
                color = "black"
                break
            case 1:
                color = "#05c1e9" //light blue
                break
            case 2:
                color = "#00ff00" //green
                break
            case 3:
                color = "#ff8040" // orange
                break
            case 4:
                color = "white"
                break
            case 5:
                color = "#fff600" // yellow
                break
            default:
                color = "#fff600" //default yellow
        }
    }


}
