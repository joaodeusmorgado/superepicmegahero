import QtQuick 2.3
import QtQuick.Particles 2.0
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    property bool animation: false
    property int scorePoints: 0

    property int gameDuration: 60000 //miliseconds
    property int gameDurationCount: gameDuration

    //property int countMeegoPhones: 0
    property int countMeegoPhonesCatched: 0
    property int countMeegoPhonesMissed: 0

    signal menu()
    signal nextLevel()

    onVisibleChanged: {
        if (visible)
            levelStartSetup()
        else
            levelEndCleanup()
    }

    /*CommandTilt {
        id: commandTilt
        active: root.visible && Qt.platform.os === "android"
    }

    CommandRotation {
        id: commandRotation
        active: root.visible && Qt.platform.os === "linux" //for sailfish OS
    }*/

    function levelStartSetup() {
        panelEOL.visible = false
        gameDurationCount = gameDuration
        scorePoints = 0
        //countMeegoPhones = 0
        countMeegoPhonesCatched = 0
        countMeegoPhonesMissed = 0
        animation = true

        /*if (Qt.platform.os === "android")
            commandTilt.move.connect( boat.move ) //tilt move
        else
            commandRotation.move.connect( boat.move )*/
    }

    function levelEndCleanup() {
        animation = false

        /*if (Qt.platform.os === "android")
            commandTilt.move.disconnect( boat.move ) //tilt move, for android OS
        else
            commandRotation.move.disconnect( boat.move )*/
    }

    //Sky
    Rectangle {
        id: sky
        width: parent.width
        height: parent.height / 2
        anchors.top: parent.top
        color: "aliceblue"
    }

    //ocean
    Rectangle {
        id: sea
        width: parent.width
        height: parent.height / 2
        anchors.bottom: parent.bottom
        //color: "#aadbc7"
        color: "aqua" //"lightblue"
    }

    //Waves above the sea
    Image {
        id: waves
        width: parent.width
        anchors.bottom: sea.top
        source: "qrc:/images/waves.png"
        mipmap: true
    }

    //Sun
    Image {
        id: sun
        width: 30*mm
        height: width
        anchors.top: parent.top
        anchors.right: parent.right
        source: "qrc:/images/sun.png"
        mipmap: true
    }


    // beach image
    Image {
        id: beach
        width: parent.width * 0.15
        height: width*256/188
        anchors.left: parent.left
        anchors.bottom: sea.top
        source: "qrc:/images/beach_palmtree.png"
        mipmap: true
    }

    //another beach image
    Image {
        id: beach2
        width: beach.width
        height: width
        anchors.right: parent.right
        anchors.bottom: sea.top
        source: "qrc:/images/beach_sand.png"
        mipmap: true
    }


    //Plataform
    Image {
        id: plataform
        width: height*153/196
        height: (55*mm < sky.height*0.9) ? 55*mm : sky.height*0.9
        anchors.bottom: sky.bottom// sea.top
        anchors.horizontalCenter: parent.horizontalCenter
        //fillMode: Image.PreserveAspectFit
        source: "qrc:/images/plataform.png"
        mipmap: true
    }

    Phone {
        id: m_phone
        x: parent.width/2
        y: parent.height - sea.height -plataform.height
        initialPosition_X: parent.width/2 -m_phone.width/2
        initialPosition_Y: parent.height - sea.height -plataform.height
        visible: false

        oceanMask: sea.color

        //when phone stops drowning, check for boat collision
        onAnimateDrowningChanged: {
            if (!animateDrowning) { //if phone finished drowning, check if the boat is there to catch it
                var collision = lib.distance(m_phone.x+m_phone.width/2, m_phone.y+m_phone.height/2,
                         boat.x+boat.width/2, boat.y+boat.height/2,
                         boat.height*0.75)
                if (collision) {
                    //console.log("phone catched!!!!!!!!!!!!!!!!!!!!!")
                    scorePoints += 2
                    countMeegoPhonesCatched++


                    /*if (m_phone.modelMego) {
                        scorePoints += 2
                        countMeegoPhonesCatched++
                    }
                    else
                        scorePoints -= 1*/


                }
                else {
                    countMeegoPhonesMissed++
                    //console.log("didnt catch phone................")
                }
            }
        } //onAnimateDrowningChanged

    }

    //boat controled by MouseArea touch
    Image {
        id:boat
        width: 30*mm
        height: width * 92/88
        x: parent.width/2 - boat.width/2
        y: parent.height - sea.height/2 - boat.height/2
        fillMode: Image.PreserveAspectFit
        source: "qrc:/images/boat_Jola.png"
        mipmap: true        
    }

    //boat controlled by sensors
    /*Starship {
        id: boat
        width: 30*mm
        height: width * 92/88
        x: parent.width/2 - boat.width/2
        y: parent.height - sea.height/2 - boat.height/2
        imageName: "boat_Jola.png"
        spaceLimitXmin: 0
        spaceLimitXmax: parent.width
        spaceLimitYmin: parent.height - sea.height - boat.height*0.75
        spaceLimitYmax: parent.height
    }*/




    MouseArea {
        anchors.fill: parent
        drag.target: boat
        drag.minimumY: parent.height - sea.height - boat.height*0.75
        drag.maximumY: parent.height - boat.height
        drag.minimumX: 0
        drag.maximumX: parent.width - boat.width
    }


    MenuPopUpVertical {
        id: menuVertical
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        onGotoMenu: menu()
        onBook: heroMap.visible = true
    }

    ScoreBoard {
        id: scoreboard
        anchors.top: parent.top
        anchors.left: menuVertical.right //parent.left
        anchors.margins: defaultMargins
        label: qsTr("Catched")
        score: countMeegoPhonesCatched//scorePoints
    }

    ScoreBoard {
        id: missedPhones
        anchors.top: scoreboard.bottom
        anchors.topMargin: defaultMargins
        anchors.left: scoreboard.left
        label: qsTr("Missed")
        score: countMeegoPhonesMissed
    }

    //time left board
    ScoreBoard {
        id: time_left
        anchors.top: missedPhones.bottom
        anchors.left: scoreboard.left
        label: qsTr("Time left")
        score: gameDurationCount/1000
    }

    //water particles---------------------------------------
    //water splash
    ParticleSystem {
        id: particleSystem
    }

    //water splash
    Emitter {
        id: emitter
        width: 2*mm; height: width
        x: m_phone.finalPosition_X + m_phone.width/2
        y: m_phone.finalPosition_Y + m_phone.height
        system: particleSystem
        emitRate: 20; lifeSpan: 1000; lifeSpanVariation: 500
        size: 4*mm
        endSize: size*0.75
        velocity: AngleDirection {
                    angle: -90
                    angleVariation: 60
                    magnitude: m_phone.height//8*mm
                    magnitudeVariation: mm
                }
    }

    //water splash
    ImageParticle {
        source: "qrc:/images/water_splash.png"
        system: particleSystem
        //color: '#FFD700'
        //colorVariation: 0.2
       // entryEffect: ImageParticle.Scale
        visible: m_phone.animateDrowning
    }
    //water particles---------------------------------------

    //fire particles---------------------------------------
    ParticleSystem {
        id: fireParticleSystem
    }

    Emitter {
        id: fireEmitter
        width: 2*mm; height: width
        x: parent.width/2
        anchors.top: plataform.top
        anchors.margins: defaultMargins*4
        system: fireParticleSystem
        emitRate: 200; lifeSpan: 3000; lifeSpanVariation: 500
        size: 4*mm
        endSize: size*0.75
        velocity: AngleDirection {
                    angle: -90
                    angleVariation: 80
                    magnitude: m_phone.height//8*mm
                    magnitudeVariation: mm
                }
    }

    ImageParticle {
        //source: "qrc:/images/fireParticle2.png"
        source: "qrc:/images/Explosion.png"
        system: fireParticleSystem
        color: "orange" //"#FFD700"
        colorVariation: 0.5
        entryEffect: ImageParticle.Scale
    }
    //fire particles---------------------------------------

    PanelEndOfLevel2 {
        id: panelEOL
        visible: false
        anchors.centerIn: parent
        showNextBtn: levels.levelUnlocked > 5
        onMenu: parent.menu()
        onReplay: levelStartSetup()
        onNext: parent.nextLevel()
        //text: "Vitória!\nApanhaste "+countMeegoPhonesCatched+"em "+countMeegoPhones+"!!!"
        pixelSize: 10*mm

        onVisibleChanged: {
            if (visible) {
                checkForVitory()
            }
        }
    }

    //vitory happens if you grab more than 70% of Meego phone
    function checkForVitory() {

        var totalPercent = countMeegoPhonesCatched/(countMeegoPhonesCatched+countMeegoPhonesMissed)
        if (totalPercent >= 0.7) {
            //panelEOL.showNextBtn = true
            panelEOL.text = qsTr("Vitory!!!\nYou caught more than 70% of the Meeg phones!!!")

            levels.levelUnlocked = levels.levelUnlocked === 5 ? 6 : levels.levelUnlocked
        }
        else {
            //panelEOL.showNextBtn = false
            panelEOL.text = qsTr("You loose!\nYou need to catch more than 70% of phones!")
        }
    }

    Timer {
        id: timerAnimation
        interval: m_phone.animationDuration * 1.1
        running: animation
        repeat: true
        onTriggered: {
            gameDurationCount -= m_phone.animationDuration
            if (gameDurationCount <= 0)
            {
                animation = false
                gameDurationCount = 0
                panelEOL.visible = true
            }

            m_phone.finalPosition_X = Math.random() * (parent.width-m_phone.width)
            m_phone.finalPosition_Y = parent.height - sea.height + Math.random() * (sea.height - m_phone.height)

            //m_phone.setPhoneRandomModel()
            //if (m_phone.modelMego === true)
              //  countMeegoPhones++
            m_phone.setPhoneMeegoModel()
            //countMeegoPhones++
            m_phone.setPhoneRandomColor()
            m_phone.animateFalling = true
        }
    }

}
