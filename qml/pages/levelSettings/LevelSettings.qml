import QtQuick 2.0
import QtSensors 5.0
import QtQuick.Window 2.2
import com.QMLTranslator 1.0
import "../components"
import "../"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: "blue"

    //onVisibleChanged: visible ? sound.play() : sound.stop()

    signal exit()
    signal showLicense()

    Flickable {
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: parent.height //- btnOkCancel.height
        contentWidth: parent.width
        contentHeight: col.height + defaultMargins * 2

        Column {
            id: col
            spacing: defaultMargins
            anchors.top: parent.top
            anchors.topMargin: defaultMargins
            anchors.left: parent.left
            anchors.leftMargin: defaultMargins
            anchors.right: parent.right
            anchors.rightMargin: defaultMargins

            TextTemplate {
                id: aboutText
                width: parent.width - 2 * defaultMargins
                bold: false
                color: "orange"
                label: qsTr("Reset Game")
            }

            MyButton {
                id: btnResetGame
                width: 4*btnSize
                text: qsTr("Reset Game")
                onBtnClick: confirmDialog.visible = true
            }

            /*
              // language selector - hide
              // for now hide the language selector, language is automatically selected with locale
            TextTemplate {
                id: lang
                width: parent.width - 2 * defaultMargins
                bold: false
                color: "orange"
                label: qsTr("\n\nSelect language")
            }

            MyButton {
                id: langEN
                //anchors.left: langPT.right
                //anchors.verticalCenter: langPT.verticalCenter
                //anchors.margins: defaultMargins
                text: qsTr("en")
                onBtnClick: {
                    qmlTranslator.setLanguage("en")
                    //rootLocale = "EN"
                }
            }

            MyButton {
                id: langPT
                text: "pt"
                onBtnClick: {
                    qmlTranslator.setLanguage("pt")
                    //rootLocale = "PT"
                }
            }*/
            // language selector - hide


        }//Column

    }//Flickable


    MyButton {
        id: btnOk
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: defaultMargins
        width: 2*btnSize
        text: "Ok"
        onBtnClick: exit()
    }


    MessageBoard {
        id: confirmDialog
        anchors.centerIn: parent
        visible: false
        horizontalAlignment: Text.AlignHCenter
        text: qsTr("Reset game?\nAll progress will be lost and the game will exit!!!\nAre you sure?")
        bntCancelText: qsTr("No")
        bntOkText: qsTr("Yes")
        showCancelBtn: true
        onCancel: confirmDialog.visible = false
        onOk: {
            confirmDialog.visible = false
            resetGame()
            Qt.quit();
        }
    }


    function resetGame() {

        heroMap.money = 0
        heroMap.moneyUnlocked = false
        heroMap.fuel = 0
        heroMap.fuelCan = false
        heroMap.milk = false
        heroMap.fruit = false
        heroMap.bowAndArrowUnlocked = false
        heroMap.starKeyUnlocked = false
        heroMap.swordUnlocked = false
        heroMap.carrotsUnlocked = false
        heroMap.waterPowderUnlocked = false
        heroMap.treasureBlueDiamondsUnlocked = false
        heroMap.bookOfHapinessUnlocked = false
        levels.levelUnlocked = 1 // default should be 1, keep it to 11 for development/debbugging porpuse
        levels.girlIsReleased = false
        levels.denWallDestroyed = false
    }
}
