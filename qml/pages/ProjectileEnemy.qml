import QtQuick 2.3

ProjectileEnemyTemplate {
    id: root

    width: 7*mm
    height: width

    property string imageName: ""

    property alias color: bullet.color
    property alias border: bullet.border

    Rectangle {
        id: bullet
        anchors.fill: parent
        radius: width/2
        color: "grey"
        border.width: 0.5*mm
        visible: imageName === ""
    }

    Image {
        id: projectileImg
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        rotation: angle * 57.2957795131
        source: imageName !== "" ? "qrc:/images/"+imageName : ""
        visible: imageName !== ""
        mipmap: true
    }
}
