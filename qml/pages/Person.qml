//import QtQuick 2.0
import QtQuick 2.3
//import com.Timer 1.0

MyPath {
    id: root
    width: 18*mm
    height: 35*mm

    property bool isWalking:false
    property string imgName: "hero"
    property string imgWalk: "1" //to refactor later, right now "01" means
    //that an animation is expecting 3 files with names "xxx"+"01/02/03.png"
    // and "0" means no animation
    //property alias path: mPath

    property real midX: x+width/2
    property real midY: y+height/2

    property real velocityX: 1.5*mm
    property real velocityY: 5*mm
    property int dir: 1//direction left = -1 ; right = 1

    property real life: 3
    //property real hitDamage: 1
    signal imHit(real hitDamage)
    signal imDestroyed()

    onImHit: {
        life -= hitDamage
        console.log("I'm hit")
        if (life === 0)
            imDestroyed()
    }

    //onImDestroyed: root.destroy()

    //coeficients a, b, c for the quadratic jump (use c=0), ax^2+bx+c=0
    /*property real jumpXdist: 2*mm
    property real jumpYdist: 2*mm
    property real a: -4*jumpYdist/(jumpXdist*jumpXdist) //-4*jumpYdist/(jumpXdist*jumpXdist)
    property real b: 4*jumpYdist/jumpXdist //4*jumpYdist/jumpXdist

    Component.onCompleted: {
        console.log("truck jump a:"+a)
        console.log("truck jump b:"+b)
    }*/

    property real a: 2//2
    property real b: 4//6

    property bool jumpAnimationRotation: false
    property real jumpRotationDelta: 0.8

    property bool isJump: false
    //property bool isJumpLeft: false
    //property bool isJumpRight: false
    property real auxX
    //property real auxY
    property real t
    property real dt: 0.1//jumpXdist *0.05

    signal startWalking()
    signal stopWalking()
   // signal walkTriggered()
    signal direction(int d)
    signal jump()
    signal walk()
    signal fall()

    onStartWalking: isWalking = true
    onStopWalking: isWalking = false
    onDirection: dir = d

    Image {
        id: heroImage
        width: parent.width
        height: parent.height
        fillMode: Image.PreserveAspectFit
        source: imgWalk === "0" ? "qrc:/images/"+imgName+".png" : "qrc:/images/"+imgName+imgWalk+".png"
        mirror: dir === 1 ? false : true
        mipmap: true
    }

    Timer {
        id: jumpTimer
        interval: fps_ms
        running: isJump//isJumpLeft || isJumpRight
        repeat: true
        onTriggered: {

            /*t += dt
            root.x += velocityX * dir
            root.y -= (-2*t*t+6*t)
            if (y > auxY) {
                t=0
                isJump = false
                y = auxY
            }*/

            //todo: improve y value on jump
            t += dt
            //var dX = velocityX * dir
            //var dY = (-2*t*t + 6*t)
            var dX = velocityX * dir;
            var dY =  -a*t*t + b*t;

            if (jumpAnimationRotation) {
                if (dY < 0){
                    root.rotation += jumpRotationDelta * dir
                }
                else {
                    root.rotation -= jumpRotationDelta * dir
                }
            }



            var p = checkPath(root.x+dX, root.y+dY, root.x, root.y)
            root.x = p.x
            root.y -= dY

            if (y > worldLimitGround) {
                t=0
                isJump = false
                y = worldLimitGround
                root.rotation = 0
            }
        }
    }

    Timer {
        id: animateHeroWalkingTimer
        interval: 90
        running: isWalking
        repeat: true
        onTriggered: {

            //if (isJump)
              //  return;

            if (imgWalk === "1")
                imgWalk = "2"
            else if (imgWalk === "2")
                imgWalk = "3"
            else if (imgWalk === "3")
                imgWalk = "1"
        }

        onRunningChanged: {
            if (!running)
                imgWalk = "1"
        }
    }

    onJump: {
        if (isJump)
            return;
        //imgWalk = "03"
        console.log("onJump")
        auxX = x
        //auxY = y
        t = 0
        b = 6
        isJump = true
    }

    onFall: {
        if (isJump)
            return;
        auxX = x
        //auxY = y
        t = 0
        b = 0
        isJump = true
    }

    onWalk: {
        if (isJump)
            return;
        //root.x += velocityX * dir
        var dX = velocityX * dir
        var dY = 0
        var p = checkPath(root.x+dX, root.y+dY, root.x, root.y)
        root.x = p.x
        root.y = p.y
    }

    signal doubleClick()
    MouseArea {
        anchors.fill: parent
        onDoubleClicked: parent.doubleClick()
        enabled: false
    }
}
