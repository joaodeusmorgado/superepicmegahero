# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-superepicmegahero

QT += qml

CONFIG += sailfishapp

SOURCES += src/harbour-SuperEpicMegaHero.cpp \
    mytimer.cpp \
    mytime.cpp \
    mytimer2.cpp \
    qmltranslator.cpp \

DISTFILES += qml/harbour-superepicmegahero.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    qml/pages/SecondPage.qml \
    rpm/harbour-superepicmegahero.changes.in \
    rpm/harbour-superepicmegahero.spec \
    rpm/harbour-superepicmegahero.yaml \
    translations/*.ts \
    harbour-superepicmegahero.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
#TRANSLATIONS += translations/SuperEpicMegaHero_pt.ts
TRANSLATIONS += translations/harbour-superepicmegahero-pt.ts
# add more languages here

RESOURCES += \
    qml/pages/qml.qrc \
    qml/pages/images.qrc \
    qml/pages/qmlsailfish.qrc \
    qml/pages/sounds.qrc

HEADERS += \
    mytimer.h \
    mytime.h \
    mytimer2.h \
    qmltranslator.h
