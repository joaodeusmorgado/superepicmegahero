#ifndef MYTIMER_H
#define MYTIMER_H

#include <QObject>
#include <QTimer>

class MyTimer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int interval READ interval WRITE setInterval NOTIFY intervalChanged)
    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)
    Q_PROPERTY(bool repeat READ repeat WRITE setRepeat NOTIFY repeatChanged)
public:
    explicit MyTimer(QObject *parent = 0);

    int interval(){return m_interval;}
    bool running(){return m_running;}
    bool repeat(){return m_repeat;}

signals:
    void triggered();
    void intervalChanged(int newInterval);
    void runningChanged(bool newRunning);
    void repeatChanged(bool newRepeat);

public slots:
    void timerUpdate();
    void setInterval(const int &interval_);
    void setRunning(const bool &running_);
    void setRepeat(const bool &repeat_);

    void start();
    void stop();

private:
    QTimer m_timer;
    int m_interval;
    bool m_running;
    bool m_repeat;
};

#endif // MYTIMER_H
