#ifndef PLACEMENT_H
#define PLACEMENT_H

#include <QObject>
#include <QVector3D>
#include <QQuaternion>
#include <QMatrix4x4>

class Placement : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQuaternion rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
    Q_PROPERTY(QVector3D position READ position WRITE setPosition NOTIFY positionChanged)
public:
    explicit Placement(QObject *parent = nullptr);

    void resetPosition();
    void resetRotation();

public slots:
    void pitch(float angle);
    void yaw(float angle);
    void roll(float angle);

    void moveFront(float d);
    void moveBack(float d);
    void moveLeft(float d);
    void moveRight(float d);
    void moveUp(float d);
    void moveDown(float d);

public:
    QVector3D up();
    QVector3D front();
    void getAxes(QVector3D *xAxis, QVector3D *yAxis, QVector3D *zAxis) const;

public slots:
    void setRotation(const QQuaternion &q);
    void setPosition(const QVector3D &pos);
    void setPosition(const float &x, const float &y, const float &z);

signals:
    void rotationChanged(QQuaternion newRotation);
    void positionChanged(QVector3D newPosition);
    void moveFrontDelta(float d);//used in girl animation

public:
    QVector3D position();
    QQuaternion rotation();
    QQuaternion delayRotation();

    QMatrix4x4 matrixModel();

protected:
    QQuaternion m_rotation;
    QVector3D m_position;

    //used to delay the camera rotation and position, this gives a nice effect when the camera is following a moving object
    QVector<QQuaternion> m_rotationDelay;
    int delaySize;
    int delayIndex;
    bool bDelayFollowingCamera;
};

#endif // PLACEMENT_H
